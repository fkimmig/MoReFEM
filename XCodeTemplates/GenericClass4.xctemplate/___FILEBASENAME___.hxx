//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
#define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace ___VARIABLE_namespace2___
    {
        
        
        namespace ___VARIABLE_namespace3___
        {
            
            
            namespace ___VARIABLE_namespace4___
            {
                
                
                
            } // namespace ___VARIABLE_namespace4___
            
            
        } // namespace ___VARIABLE_namespace3___
        
        
    } // namespace ___VARIABLE_namespace2___


} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
