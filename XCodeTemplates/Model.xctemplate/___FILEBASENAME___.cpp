//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "ModelInstances/___VARIABLE_groupName:identifier___/___FILEBASENAME___.hpp"


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }
        : parent(mpi_ptr, input_parameter_data)
        { }


        void Model::SupplInitialize(const morefem_data_type& morefem_data)
        {
            // TODO: Fill the content with whatever initialization is required by the problem.
            // What is already done in base method Initialize() (and therefore must not be repeated here) is:           
            // - Creation of the geometric mesh regions.
            // - Definitions of the GodOfDof
            // What typically might be added is initialization of variational formulation(s), print on screen,
            // run of the static case, ...
        }


        void Model::Forward()
        {
            // TODO: Define the forward operations. In case of a variational problem; it probably means delegating 
            // work to an underlying variational formulation object.
        }


        void Model::SupplFinalizeStep()
        {
            // TODO: Put there the steps to perform at the end of each time step.
            // Base class FinalizeStep() just update the time; it does so AFTER the call to current method
            // (so in current method you can still rely upon time informations related to the time step
            // being finalized).
        }
        


        void Model::SupplFinalize()
        { 
            // TODO: Put there what to do when all the time steps are done
        }


    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM
