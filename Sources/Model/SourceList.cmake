target_sources(${MOREFEM_MODEL}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Model.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
