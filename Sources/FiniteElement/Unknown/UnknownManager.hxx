///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 08:44:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_


namespace MoReFEM
{


    template<class UnknownSectionT>
    void UnknownManager::Create(const UnknownSectionT& section)
    {
        namespace ipl = Internal::InputParameterListNS;

        decltype(auto) name = ipl::ExtractParameter<typename UnknownSectionT::Name>(section);
        decltype(auto) str_nature = ipl::ExtractParameter<typename UnknownSectionT::Nature>(section);

        Create(section.GetUniqueId(), name, str_nature);
    }


    inline unsigned int UnknownManager::Nunknown() const noexcept
    {
        return static_cast<unsigned int>(unknown_list_.size());
    }


    inline const Unknown& UnknownManager::GetUnknown(unsigned int i) const noexcept
    {
        return *GetUnknownPtr(i);
    }


    inline const Unknown& UnknownManager::GetUnknown(const std::string& unknown_name) const
    {
        return *GetUnknownPtr(unknown_name);
    }


    inline const Unknown::vector_const_shared_ptr& UnknownManager::GetList() const noexcept
    {
        return unknown_list_;
    }




} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
