target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumUnknown.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/EnumUnknown.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownManager.hxx" / 
)

