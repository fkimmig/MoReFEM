target_sources(${MOREFEM_FELT}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/BoundaryConditions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Nodes_and_dofs/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FiniteElementSpace/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FiniteElement/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/RefFiniteElement/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Unknown/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/QuadratureRules/SourceList.cmake)
