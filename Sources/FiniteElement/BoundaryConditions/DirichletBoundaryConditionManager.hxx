///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_


namespace MoReFEM
{


    template<class DirichletBoundaryConditionSectionT>
    void DirichletBoundaryConditionManager::Create(const DirichletBoundaryConditionSectionT& section)
    {
        namespace ipl = Internal::InputParameterListNS;

        decltype(auto) name = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::Name>(section);
        decltype(auto) domain_id = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::DomainIndex>(section);
        decltype(auto) unknown_name = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::UnknownName>(section);
        decltype(auto) component = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::Component>(section);
        decltype(auto) value_per_component =
            ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::Values>(section);
        decltype(auto) is_mutable = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::IsMutable>(section);
        decltype(auto) may_overlap = ipl::ExtractParameter<typename DirichletBoundaryConditionSectionT::MayOverlap>(section);

        const auto& domain = DomainManager::GetInstance().GetDomain(domain_id, __FILE__, __LINE__);

        const auto& unknown = UnknownManager::GetInstance().GetUnknown(unknown_name);

        Create(section.GetUniqueId(),
               name,
               domain,
               unknown,
               value_per_component,
               component,
               is_mutable,
               may_overlap);
    }


    inline unsigned int DirichletBoundaryConditionManager::NboundaryCondition() const noexcept
    {
        return static_cast<unsigned int>(boundary_condition_list_.size());
    }


    inline const DirichletBoundaryCondition& DirichletBoundaryConditionManager
    ::GetDirichletBoundaryCondition(unsigned int i) const noexcept
    {
        return *GetDirichletBoundaryConditionPtr(i);
    }


    inline const DirichletBoundaryCondition& DirichletBoundaryConditionManager
    ::GetDirichletBoundaryCondition(const std::string& name) const noexcept
    {
        return *GetDirichletBoundaryConditionPtr(name);
    }


    inline DirichletBoundaryCondition& DirichletBoundaryConditionManager
    ::GetNonCstDirichletBoundaryCondition(unsigned int i) noexcept
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(i));
    }


    inline DirichletBoundaryCondition& DirichletBoundaryConditionManager
    ::GetNonCstDirichletBoundaryCondition(const std::string& name) noexcept
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(name));
    }


    inline const DirichletBoundaryCondition::vector_shared_ptr& DirichletBoundaryConditionManager
    ::GetList() const noexcept
    {
        return boundary_condition_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
