target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
