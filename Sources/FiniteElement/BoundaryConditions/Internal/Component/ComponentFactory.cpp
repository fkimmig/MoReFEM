///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <sstream>
#include <algorithm>
#include <cassert>

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"
#include "Utilities/Containers/Print.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace BoundaryConditionNS
        {
            
            
            const std::string& ComponentFactory::ClassName()
            {
                static std::string ret("ComponentFactory");
                return ret;
            }
            
            
            ComponentFactory::ComponentFactory() = default;
            
            
            
            ComponentManager::const_shared_ptr ComponentFactory::CreateFromName(const std::string& component_name) const
            {
                auto it = callbacks_.find(component_name);
                
                if (it == callbacks_.cend())
                    throw ExceptionNS::Factory::UnregisteredName(component_name, "component", __FILE__, __LINE__);
                
                ComponentManager::const_shared_ptr ret = it->second();
                
                return ret;
            }
            
            
        } // namespace BoundaryConditionNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
