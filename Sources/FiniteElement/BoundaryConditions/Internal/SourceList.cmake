target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofStorage.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Component/SourceList.cmake)
