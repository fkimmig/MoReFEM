target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Point.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Segment.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Triangle.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Point.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Quadrangle.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Segment.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Triangle.hpp" / 
)

