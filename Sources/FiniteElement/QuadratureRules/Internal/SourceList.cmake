target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GaussQuadratureFormula.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GaussQuadratureFormula.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GaussQuadratureFormula.hxx" / 
)

