target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Dof.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalNode.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Node.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NodeBearer.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Dof.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Dof.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalNode.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalNode.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Node.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Node.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NodeBearer.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NodeBearer.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
