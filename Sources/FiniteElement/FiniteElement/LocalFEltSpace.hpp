///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HPP_

# include <memory>
# include <vector>

# include "Utilities/Mutex/Mutex.hpp"
# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

# include "Geometry/GeometricElt/GeometricElt.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

# include "FiniteElement/FiniteElement/FElt.hpp"
# include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Domain;



    namespace Internal
    {


        namespace RefFEltNS
        {


            class RefLocalFEltSpace;


        } // namespace RefFEltNS



        namespace FEltSpaceNS
        {


            class CreateNodeListHelper;


        } // namespace FEltSpaceNS


    } // namespace Internal




    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /// \addtogroup FiniteElementGroup
    ///@{

    /*!
     * \brief This class holds all Finite elements related to a given GeometricElt.
     *
     * There might be several: there is in fact one FElt per Unknown present in the LocalFEltSpace.
     *
     * This is also this class that is in charge of giving away the local2global arrays.
     *
     */
    class LocalFEltSpace final : private Crtp::Mutex<LocalFEltSpace>
    {

    public:

        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<LocalFEltSpace>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to FEltSpace, the only class entitled to build a new LocalFEltSpace.
        friend class FEltSpace;

        //! Friendship to the only class entitled to add finite elements to the local felt space.
        friend class Internal::FEltSpaceNS::CreateNodeListHelper;

        //! Convenient storage: key is the index of the geometric element, value the actual pointer to the LocalFEltSpace.
        using per_geom_elt_index = std::unordered_map<unsigned int, LocalFEltSpace::shared_ptr>;

    private:

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit LocalFEltSpace(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                const GeometricElt::shared_ptr& geometric_elt);

    public:

        //! Destructor.
        ~LocalFEltSpace();

        //! Copy constructor - deactivated.
        LocalFEltSpace(const LocalFEltSpace&) = delete;

        //! Move constructor.
        LocalFEltSpace(LocalFEltSpace&&) = delete;

        //! Copy affectation - deactivated.
        LocalFEltSpace& operator=(const LocalFEltSpace&) = delete;

        //! Move affectation - deactivated.
        LocalFEltSpace& operator=(LocalFEltSpace&&) = delete;


        ///@}



        //! Access to the geometric element as a const reference.
        const GeometricElt& GetGeometricElt() const noexcept;

        //! Access to the geometric element as a smart pointer.
        GeometricElt::shared_ptr GetGeometricEltPtr() const noexcept;

        //! Access to the FElt matching the given \a extended_unknown.
        const FElt& GetFElt(const ExtendedUnknown& extended_unknown) const;

        //! Non-constant access to the FElt matching the given \a extended_unknown.
        FElt& GetNonCstFElt(const ExtendedUnknown& extended_unknown);

        //! Access to the FElt matching the given \a unknown.
        const FElt& GetFElt(const Unknown& unknown) const;

        //! Access to all FElts.
        const FElt::vector_shared_ptr& GetFEltList() const noexcept;

        //! Access to the list of nodes bearer.
        const NodeBearer::vector_shared_ptr& GetNodeBearerList() const noexcept;

        //! Set the list of nodes.
        void SetNodeBearerList(NodeBearer::vector_shared_ptr&& node_bearer_list);

        /*!
         * \brief Init for each numbering subset a global local2global array.
         *
         * In this array all the unknowns within the numbering subset are considered; their ordering is the same
         * as the ordering in \a felt_list_.
         *
         * This array is not accessible publicly; it is used only to generate the local2global required by the
         * operators (that are computed through a call to \a ComputeLocal2Global, issued in each
         * GlobalVariationalOperator constructor).
         *
         * \param[in] do_consider_processor_wise_local_2_global If yes, compute local -> global processor-wise index.
         * This is not necessary for all global operators, so do not clutter space when it doesn't matter.
         * \param[in] numbering_subset_list List of all numbering subsets for which local2global array should be
         * computed.
         *
         * \internal <b><tt>[internal]</tt></b> Processor- and program-wise local2global arrays are actually stored in
         * two different containers.
         */
        void InitLocal2Global(const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                              DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global);


        /*!
         * \brief Compute Local2GLobal for a subset of the unknowns.
         *
         * This method is supposed to be called by each operator, that asks upon its creation that the
         * local2global it will need are correctly built.
         *
         * It assumes that \a InitLocal2Global has been correctly called beforehand.
         *
         * \param[in] unknown_list List of unknowns for which the local2global array is required. They must all
         * belong to the same numbering subset.
         * \copydetails doxygen_hide_do_compute_processor_wise_local_2_global_arg
         */
        void ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                 DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global);

        /*!
         * \brief Get the local2global array related to a list of unknowns.
         *
         * \tparam MpiScaleT Whether processor- or program-wise local2global array is required.
         *
         * \warning Beware: processor-wise one must have been explicitly built with a dedicated argument
         * in \a ComputeLocal2Global.
         *
         * \param[in] unknown_list List of unknowns considered, along with their associated \a NumberingSubset
         * in the \a FEltSpace.
         *
         * \return Local2global array related to a list of unknowns.
         */
        template<MpiScale MpiScaleT>
        const std::vector<PetscInt>& GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const;


        /*!
         * \brief Get the local2global array related to a single unknown (in other terms, it is the local2global
         * related to a single finite element).
         *
         * \tparam MpiScaleT Whether processor- or program-wise local2global array is required.
         *
         * \warning Beware: processor-wise one must have been explicitly built with a dedicated argument in
         * \a ComputeLocal2Global.
         *
         * \param[in] extended_unknown Couple unknown/numbering subset for which the local -> global arrray is sought.
         *
         * \return Local2global array related to \a extended_unknown.
         */
        template<MpiScale MpiScaleT>
        const std::vector<PetscInt>& GetLocal2Global(const ExtendedUnknown& extended_unknown) const;


    private:

        /*!
         * \brief Add a new finite element and returns a non-constant reference to it.
         *
         * \internal <b><tt>[internal]</tt></b> This method should only be called within friend CreateNodeListHelper.
         *
         * \param[in] ref_felt Description of a reference finite element associated to an \a Unknown, a \a NumberingSubset
         * and a \a RefGeomElt.
         *
         * \return Reference to the newly created \a FElt.
         */
        FElt& AddFElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt);


        //! Non constant access to all FElts.
        FElt::vector_shared_ptr& GetNonCstFEltList() noexcept;

        //! Access to the reference felt space.
        const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace() const noexcept;

        //! Constant access to local_2_global_per_numbering_subset_.
        template<MpiScale MpiScaleT>
        const Internal::FEltNS::Local2GlobalStorage&
        GetLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Non constant access to local_2_global_per_numbering_subset_.
        template<MpiScale MpiScaleT>
        Internal::FEltNS::Local2GlobalStorage&
        GetNonCstLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset);

        //! Constant access to the whole local_2_global storage per numbering subset.
        template<MpiScale MpiScaleT>
        const std::vector<std::pair<unsigned int, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>&
        GetLocal2GlobalStorage() const noexcept;

        //! Clear the temporary data used to build properly the Internal::FEltNS::Local2GlobalStorage objects.
        void ClearTemporaryData() const noexcept;


    private:

        //! Reference to the related RefLocalFEltSpace.
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space_;

        /*!
         * \brief Finite element list
         *
         * There is one finite element per relevant unknown: a same unknown can be associated to only one
         * numbering subset in a given (global) finite element space.
         */
        FElt::vector_shared_ptr felt_list_;

        //! Pointer to the related GeometricElt.
        GeometricElt::shared_ptr geometric_elt_;

        //! List of nodes that belong to the LocalFEltSpace.
        NodeBearer::vector_shared_ptr node_bearer_list_;

        /*!
         * \brief Store for each numbering subset all the required program-wise local2global arrays.
         *
         * Key is the unique id of the numbering subset.
         * Value is the object in charge of holding the information.
         */
        std::vector<std::pair<unsigned int, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>
            local_2_global_program_wise_per_numbering_subset_;

        /*!
         * \brief Store for each numbering subset all the required processor-wise local2global arrays.
         *
         * Key is the unique id of the numbering subset.
         * Value is the object in charge of holding the information.
         */
        std::vector<std::pair<unsigned int, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>
            local_2_global_processor_wise_per_numbering_subset_;


        # ifndef NDEBUG

        //! Whether the local -> global (processor-wise) must also be built. If not local_2_global_processor_wise_ remains empty.
        DoComputeProcessorWiseLocal2Global do_consider_processor_wise_local_2_global_;

        # endif // NDEBUG


    };


    /*!
     * \brief Returns whether the \a local_felt_space is in \a domain or not.
     *
     * \internal <b><tt>[internal]</tt></b> felt object here just returns its underlying geometric_element;
     * it's upon this one the test is really performed (domain is a purely geometruc concept).
     *
     * \param[in] local_felt_space \a LocalFEltSpace being considered (here the only relevant information
     * about it is the \a GeometricElement to which it is related).
     * \param[in] domain Domain considered.
     *
     * \return True if the underlying \a GeometricElement of \a local_felt_space is in \a domain.
     */
    bool IsLocalFEltSpaceInDomain(const LocalFEltSpace& local_felt_space, const Domain& domain) noexcept;


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElement/LocalFEltSpace.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_LOCAL_F_ELT_SPACE_HPP_
