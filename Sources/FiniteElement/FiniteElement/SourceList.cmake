target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FElt.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalFEltSpace.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FElt.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FElt.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalFEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalFEltSpace.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
