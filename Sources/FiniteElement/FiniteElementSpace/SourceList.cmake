target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AllocateGlobalLinearAlgebra.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDof.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDofManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDof.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDof.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDofManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GodOfDofManager.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
