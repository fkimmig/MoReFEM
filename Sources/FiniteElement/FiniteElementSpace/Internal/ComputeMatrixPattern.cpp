///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Jan 2014 14:55:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "ThirdParty/Wrappers/Parmetis/Parmetis.hpp"

#include "Utilities/Containers/Vector.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "Geometry/Mesh/GeometricMeshRegion.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/ComputeMatrixPattern.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
       
            MatrixPattern::vector_const_unique_ptr
            ComputeMatrixPattern
            ::Perform(const unsigned int mpi_rank,
                      const FEltSpace::vector_unique_ptr& felt_space_list,
                      const NodeBearer::vector_shared_ptr& node_bearer_list,
                      const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                      const NdofHolder& Ndof_holder)
            {
                NodeBearer::vector_shared_ptr empty;
                
                auto connectivity = ComputeNodeBearerConnectivity(felt_space_list,
                                                                  mpi_rank,
                                                                  node_bearer_list.size(),
                                                                  KeepSelfConnexion::yes);
                
                MatrixPattern::vector_const_unique_ptr ret;
                
                assert(std::none_of(numbering_subset_list.cbegin(), numbering_subset_list.cend(),
                                      Utilities::IsNullptr<NumberingSubset::const_shared_ptr>));
                
                #ifndef NDEBUG
                std::vector<std::pair<unsigned int, unsigned int>> already_seen;
                #endif // NDEBUG
                
                for (const auto& row_numbering_subset_ptr : numbering_subset_list)
                {
                    for (const auto& col_numbering_subset_ptr : numbering_subset_list)
                    {
                        #ifndef NDEBUG
                        auto new_pair = std::make_pair(row_numbering_subset_ptr->GetUniqueId(),
                                                       col_numbering_subset_ptr->GetUniqueId());
                        
                        auto it = std::find(already_seen.cbegin(), already_seen.cend(), new_pair);
                        assert(it == already_seen.cend() && "One given numbering subset should be only once in the list!");
                        already_seen.push_back(new_pair);
                        #endif // NDEBUG
                        
                        auto&& matrix_pattern_ptr = std::make_unique<MatrixPattern>(row_numbering_subset_ptr,
                                                                                    col_numbering_subset_ptr,
                                                                                    connectivity,
                                                                                    node_bearer_list,
                                                                                    Ndof_holder);
                        
                        ret.push_back(std::move(matrix_pattern_ptr));
                    }
                }
                
          
                return ret;
            }
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
