///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
            
            
            namespace // anonymous
            {
                
                
                
                void ComputeNodeBearerConnectivityInFEltSpace(const FEltSpace& felt_space,
                                                              connectivity_type& connectivity,
                                                              const unsigned int mpi_rank);
                
                
                void CleanUpConnectivity(connectivity_type& connectivity,
                                         KeepSelfConnexion do_keep_self_connexion);
                
                
                
                
            } // namespace anonymous
            
            
            
            connectivity_type
            ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                          const unsigned int mpi_rank,
                                          const std::size_t Nnode_bearer,
                                          KeepSelfConnexion do_keep_self_connexion)
            {
                connectivity_type connectivity;
                connectivity.max_load_factor(Utilities::DefaultMaxLoadFactor());
                connectivity.reserve(Nnode_bearer);
                
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    assert(!(!felt_space_ptr));
                    ComputeNodeBearerConnectivityInFEltSpace(*felt_space_ptr,
                                                             connectivity,
                                                             mpi_rank);
                }
                
                // Reorder properly elements and get rid of duplicates.
                CleanUpConnectivity(connectivity, do_keep_self_connexion);
                
                assert("In both occasion this function is called, we are in measure to provide the exact size "
                       "of the connectivity, thus avoiding possibly expensive rehashing calls."
                       && connectivity.size() == Nnode_bearer);
                
                return connectivity;
            }
            

            namespace // anonymous
            {
                
                
                void ComputeNodeBearerConnectivityInFEltSpace(const FEltSpace& felt_space,
                                                              connectivity_type& connectivity,
                                                              const unsigned int mpi_rank)
                {
                    auto is_on_local_processor = [mpi_rank](const NodeBearer::shared_ptr& node_bearer_ptr)
                    {
                        assert(!(!node_bearer_ptr));
                        return node_bearer_ptr->GetProcessor() == mpi_rank;
                    };
                    
                    const auto& felt_list_per_type = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();
                    
                    // Iterate through all node_bearers and determine the list of connected node_bearers.
                    // In this first approach, a same node_bearer might be listed twice in the list.
                    
                    for (const auto& pair : felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;
                        
                        for (const auto& local_felt_space_pair : local_felt_space_list)
                        {
                            const auto& local_felt_space_ptr = local_felt_space_pair.second;
                            assert(!(!local_felt_space_ptr));
                            const auto& node_bearers_in_current_finite_element =
                                local_felt_space_ptr->GetNodeBearerList();
                            
                            for (const auto& node_bearer_ptr : node_bearers_in_current_finite_element)
                            {
                                if (!is_on_local_processor(node_bearer_ptr))
                                    continue;
                                
                                // The node_bearer might already exist; check that here!
                                auto it = connectivity.find(node_bearer_ptr);
                                
                                if (it == connectivity.cend())
                                    connectivity.insert({node_bearer_ptr, node_bearers_in_current_finite_element});
                                else
                                {
                                    // If already existing, add the new node_bearers to the existing vector.
                                    auto& second_member = it->second;
                                    
                                    for (const auto& connected_node_bearer_ptr : node_bearers_in_current_finite_element)
                                        second_member.push_back(connected_node_bearer_ptr);
                                }
                            }
                        }
                    }
                }
                
                
                void CleanUpConnectivity(connectivity_type& connectivity,
                                         KeepSelfConnexion do_keep_self_connexion)
                {
                    for (auto& item : connectivity)
                    {
                        auto& second_member = item.second;
                        
                        // There is in all likelihood duplicates in second member; remove them!
                        Utilities::EliminateDuplicate(second_member,
                                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                                      Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());
                        
                        if (do_keep_self_connexion == KeepSelfConnexion::no)
                        {
                            // Remove also self-connexion (harmful for instance if fed to Parmetis library, and trivial to
                            // add back anyway if you need it somewhere).
                            auto first_member = item.first;
                            
                            auto it = std::find(second_member.begin(), second_member.end(), first_member);
                            
                            if (it != second_member.end())
                                second_member.erase(it);
                        }
                    }
                    
                }
                
                
            } // namespace anonymous
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
