///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Apr 2015 12:13:39 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline const NumberingSubset& MatrixPattern::GetRowNumberingSubset() const
            {
                assert(!(!row_numbering_subset_));
                return *row_numbering_subset_;
            }


            inline const NumberingSubset& MatrixPattern::GetColumnNumberingSubset() const
            {
                assert(!(!column_numbering_subset_));
                return *column_numbering_subset_;
            }


            inline const ::MoReFEM::Wrappers::Petsc::MatrixPattern& MatrixPattern::GetPattern() const
            {
                assert(!(!matrix_pattern_));
                return *matrix_pattern_;
            }



        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HXX_
