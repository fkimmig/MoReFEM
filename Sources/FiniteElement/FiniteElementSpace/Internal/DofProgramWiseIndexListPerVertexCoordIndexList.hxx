///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline DofProgramWiseIndexListPerVertexCoordIndexList::storage_type& DofProgramWiseIndexListPerVertexCoordIndexList
            ::GetNonCstDofProgramWiseIndexPerCoordIndexList() noexcept
            {
                return const_cast<DofProgramWiseIndexListPerVertexCoordIndexList::storage_type&>(GetDofProgramWiseIndexPerCoordIndexList());
            }


            inline const FEltSpace& DofProgramWiseIndexListPerVertexCoordIndexList::GetFEltSpace() const noexcept
            {
                return felt_space_;
            }


            inline const NumberingSubset& DofProgramWiseIndexListPerVertexCoordIndexList::GetNumberingSubset() const noexcept
            {
                return numbering_subset_;
            }


            inline unsigned int DofProgramWiseIndexListPerVertexCoordIndexList::NprogramWisedof() const noexcept
            {
                return Nprogram_wise_dof_;
            }


            inline const DofProgramWiseIndexListPerVertexCoordIndexList::storage_type& DofProgramWiseIndexListPerVertexCoordIndexList
            ::GetDofProgramWiseIndexPerCoordIndexList() const noexcept
            {
                return dof_program_wise_index_per_coord_index_list_;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_HXX_
