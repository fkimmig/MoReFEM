///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 23 Dec 2013 11:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <unordered_map>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Mesh/GeometricMeshRegion.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CreateNodeListHelper.hpp"
#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
            
            
            CreateNodeListHelper::CreateNodeListHelper(NodeBearer::vector_shared_ptr& node_bearer_list)
            : node_bearer_list_(node_bearer_list)
            {
                vertex_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                edge_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                face_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
                volume_node_bearer_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }
            
            
            void CreateNodeListHelper::AddNodes(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                                LocalFEltSpace& local_felt_space)
            {
                NodeBearer::vector_shared_ptr node_bearer_for_current_local_felt_space;
                
                const auto& ref_felt_list = ref_felt_space.GetRefFEltList();
                
                const auto& geom_elt = local_felt_space.GetGeometricElt();
                
                for (const auto& ref_felt_ptr : ref_felt_list)
                {
                    assert(!(!ref_felt_ptr));
                    const auto& ref_felt = *ref_felt_ptr;
                    
                    auto& felt = local_felt_space.AddFElt(ref_felt);
                    
                    const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
                    
                    // Key is local node index.
                    // \todo I don't remember why a std::vector wouldn't work here?
                    std::map<unsigned int, Node::shared_ptr> node_for_current_finite_element;
                    
                    if (basic_ref_felt.AreNodesOnVertices())
                    {
                        AddInterfaceNodeList<Vertex>(geom_elt,
                                                     ref_felt,
                                                     node_bearer_for_current_local_felt_space,
                                                     node_for_current_finite_element,
                                                     vertex_node_bearer_list_);
                    }
                    
                    // Same for edges.
                    if (basic_ref_felt.AreNodesOnEdges())
                    {
                        AddInterfaceNodeList<OrientedEdge>(geom_elt,
                                                           ref_felt,
                                                           node_bearer_for_current_local_felt_space,
                                                           node_for_current_finite_element,
                                                           edge_node_bearer_list_);
                    }
                    
                    // Same for faces.
                    if (basic_ref_felt.AreNodesOnFaces())
                    {
                        AddInterfaceNodeList<OrientedFace>(geom_elt,
                                                           ref_felt,
                                                           node_bearer_for_current_local_felt_space,
                                                           node_for_current_finite_element,
                                                           face_node_bearer_list_);
                    }
                    
                    // Same for volume.
                    if (basic_ref_felt.AreNodesOnVolume())
                    {
                        AddInterfaceNodeList<Volume>(geom_elt,
                                                     ref_felt,
                                                     node_bearer_for_current_local_felt_space,
                                                     node_for_current_finite_element,
                                                     volume_node_bearer_list_);
                    }
                    
                    assert(node_for_current_finite_element.size() == basic_ref_felt.NlocalNode());
                    
                    for (const auto& pair : node_for_current_finite_element)
                        felt.AddNode(pair.second);
                    
                } // for (auto ref_felt_ptr : ref_felt_list)
                
                assert(!node_bearer_for_current_local_felt_space.empty());
                local_felt_space.SetNodeBearerList(std::move(node_bearer_for_current_local_felt_space));
            }
            
            
            namespace // anonymous
            {
                
                
                template<class OrientedInterfaceContainerT>
                void AddNodeBearerOnBoundaryCondition(const OrientedInterfaceContainerT& interface_list,
                                                      const std::unordered_map<unsigned int, NodeBearer::shared_ptr>& node_bearer_on_interface_type_list,
                                                      NodeBearer::vector_shared_ptr& node_bearer_list)
                {
                    for (const auto& interface_ptr : interface_list)
                    {
                        assert(!(!interface_ptr));
                        
                        auto it_node_bearer = node_bearer_on_interface_type_list.find(interface_ptr->GetIndex());
                        
                        // This might happen in problems that use different shape functions on different parts of the mesh.
                        if (it_node_bearer == node_bearer_on_interface_type_list.cend())
                            continue;
                        
                        const auto& node_bearer_ptr = it_node_bearer->second;
                        assert(!(!node_bearer_ptr));
                        assert(!node_bearer_ptr->IsEmpty());
                        
                        node_bearer_list.push_back(node_bearer_ptr);
                    }
                }
                
                
            } // namespace anonymous
            
            
            NodeBearer::vector_shared_ptr CreateNodeListHelper
            ::ComputeNodeBearerListOnBoundary(const GeometricMeshRegion& mesh,
                                              const DirichletBoundaryCondition& boundary_condition) const
            {
                const auto& boundary_condition_domain = boundary_condition.GetDomain();
                
                // \todo Needs to be done much more properly! It's still done à la Felisce (but BC have not been
                // refactored heavily yet). See #203.
                const auto& bag_of_boundary_domain = mesh.BagOfEltType(mesh.GetDimension() - 1u);
                
                NodeBearer::vector_shared_ptr node_bearer_on_bc;
                
                using iterator_geometric_element = GeometricMeshRegion::iterator_geometric_element;
                iterator_geometric_element begin, end;
                
                for (const auto& geometric_elt_type_ptr : bag_of_boundary_domain)
                {
                    const auto& geom_elt_type = *geometric_elt_type_ptr;
                    
                    if (!boundary_condition_domain.DoRefGeomEltMatchCriteria(geom_elt_type))
                        continue;
                    
                    std::tie(begin, end) = mesh.GetSubsetGeometricEltList(geom_elt_type);
                    
                    for (auto it = begin; it != end; ++it)
                    {
                        const auto& geometric_elt_ptr = *it;
                        assert(!(!geometric_elt_ptr));
                        const auto& geom_elt = *geometric_elt_ptr;
                        assert(geom_elt.GetIdentifier() == geom_elt_type.GetIdentifier()
                               && "If not, GetSubsetGeometricEltList() above is buggy!");
                        
                        if (!boundary_condition_domain.IsGeometricEltInside(geom_elt))
                            continue;
                        
                        
                        AddNodeBearerOnBoundaryCondition(geom_elt.GetVertexList(),
                                                         vertex_node_bearer_list_,
                                                         node_bearer_on_bc);
                        
                        if (!edge_node_bearer_list_.empty())
                        {
                            AddNodeBearerOnBoundaryCondition(geom_elt.GetOrientedEdgeList(),
                                                             edge_node_bearer_list_,
                                                             node_bearer_on_bc);
                        }
                        
                        if (!face_node_bearer_list_.empty())
                        {
                            AddNodeBearerOnBoundaryCondition(geom_elt.GetOrientedFaceList(),
                                                             face_node_bearer_list_,
                                                             node_bearer_on_bc);
                        }
                        
                    }
                }
                
                Utilities::EliminateDuplicate(node_bearer_on_bc);
                return node_bearer_on_bc;
            }
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
