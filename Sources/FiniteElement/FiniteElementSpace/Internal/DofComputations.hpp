///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 15:19:05 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HPP_


# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

# include "Core/Enum.hpp"

# include "Geometry/Mesh/GeometricMeshRegion.hpp"

# include "FiniteElement/Nodes_and_dofs/Dof.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/Impl/ComputeDofIndexes.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Forward declarations.
            // ============================


            class CreateNodeListHelper;


            // ============================
            // End of forward declarations.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



            /*!
             *
             * \class doxygen_hide_compute_dof_indexes_common_args
             *
             * \param[in] node_bearer_list List of node bearerss for which dof indexes are computed.
             * It is expected the structure of the nodes (its nodes, the exact number of dofs) is already
             * initialized.
             * \param[in] dof_numbering_scheme When considering a vectorial unknown, this argument reflects the choice
             * of numbering. Two possibilities at the moment: components of a same Node are given contiguous indexes
             * or on the contrary all dofs related to a same component (for instance displacement/x) are contiguous,
             * then all of the following component.
             */



            /*!
             * \brief Compute the dof indexes depending on the numbering subset.
             *
             * \tparam MpiScaleT Whether we are computing program_wise or processor (or ghost)-wise indexes.
             *
             * \copydetails doxygen_hide_compute_dof_indexes_common_args
             *
             * \param[in] numbering_subset Numbering subset for which an index is computed.
             * \param[in,out] current_dof_index In input, the starting point of the numbering. In output, the highest
             * dof index attributed. The reason for this is simply for processor-wise and ghosts: there are two
             * consecutive calls, the first on node on processor, the second on ghosted nodes, and the numbering
             * of ghosts is expected to begin where the one of node ended.
             */
            template<MpiScale MpiScaleT>
            void ComputeDofIndexesForNumberingSubset(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                     DofNumberingScheme dof_numbering_scheme,
                                                     const NumberingSubset::const_shared_ptr& numbering_subset,
                                                     unsigned int& current_dof_index);


            /*!
             * \brief Compute the dof indexes that are independant of numbering subset.
             *
             * \copydetails doxygen_hide_compute_dof_indexes_common_args
             *
             * \param[in,out] current_dof_index Current dof index. Is typically 0 before the first call
             * to this function (for processor-wise dofs) and Nprocessor_wise_dof for second and last call
             * (for ghost dofs).
             * \param[out] complete_dof_list List of dofs that have been attributed an index.
             */
            void ComputeProcessorWiseDofIndexes(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                DofNumberingScheme dof_numbering_scheme,
                                                unsigned int& current_dof_index,
                                                Dof::vector_shared_ptr& complete_dof_list);


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/DofComputations.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HPP_
