target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AttributeProcessorHelper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeDofIndexes.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeDofIndexes.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceInternalStorage.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceSpecialization.hxx" / 
)

