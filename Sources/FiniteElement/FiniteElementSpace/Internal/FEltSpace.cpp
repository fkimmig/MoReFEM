///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
