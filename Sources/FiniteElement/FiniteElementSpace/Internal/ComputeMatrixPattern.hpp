///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Nov 2014 15:25:03 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COMPUTE_MATRIX_PATTERN_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COMPUTE_MATRIX_PATTERN_HPP_

# include <unordered_map>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

# include "Utilities/Mpi/Mpi.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

# include "FiniteElement/FiniteElementSpace/Internal/FEltSpaceStorage.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/MatrixPattern.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricMeshRegion;


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            class NdofHolder;


        } // namespace FEltSpaceNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Computes the matrix pattern.
             *
             * \internal <b><tt>[internal]</tt></b> This is actually a struct to ease the syntax of
             * friendship declaration in NodeBearer class.
             *
             */
            struct ComputeMatrixPattern
            {
            public:

                /*!
                 * \brief Compute the pattern(s) that have to be covered within a GodOfDof.
                 *
                 * \param[in] mpi_rank Rank of current processor.
                 * \param[in] felt_space_list The list of finite elements spaces considered.
                 * \param[in] node_bearer_list The list of node bearers.  In output, some informations
                 * have been provided, such as the processor and the index of each node bearer.
                 * \param[in] Ndof_holder Object that knows the number of dofs per numbering subset.
                 * \param[in] numbering_subset_list List of \a NumberingSubset to consider.
                 *
                 * \return Pattern of the global matrix.
                 */
                static MatrixPattern::vector_const_unique_ptr
                Perform(const unsigned int mpi_rank,
                        const FEltSpace::vector_unique_ptr& felt_space_list,
                        const NodeBearer::vector_shared_ptr& node_bearer_list,
                        const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                        const NdofHolder& Ndof_holder);


            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COMPUTE_MATRIX_PATTERN_HPP_
