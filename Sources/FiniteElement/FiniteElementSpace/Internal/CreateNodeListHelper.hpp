///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 23 Dec 2013 11:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HPP_

# include <unordered_map>

# include "Utilities/Containers/PointerComparison.hpp"

# include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

# include "FiniteElement/Unknown/UnknownManager.hpp"

# include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/Impl/InterfaceSpecialization.hpp"



namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class DirichletBoundaryCondition;

    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Helper object used in \a Node creation.
             */
            class CreateNodeListHelper
            {
            public:


                /*!
                 * \brief Constructor.
                 *
                 * \param[out] node_list List of nodes the current class creates.
                 */
                explicit CreateNodeListHelper(NodeBearer::vector_shared_ptr& node_list);


                /*!
                 * \brief Add the nodes of a given finite element.
                 *
                 * \param[in] ref_felt_space Reference finite element space.
                 * \param[in,out] local_felt_space Local finite element space to which
                 * \a FElt created are added.
                 */
                void AddNodes(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                              LocalFEltSpace& local_felt_space);

                /*!
                 * \brief Establish the list of nodes affected by boundary condition.
                 *
                 * The nodes should already have been created before the call to this method.
                 *
                 * \param[in] mesh Mesh upon which the node are created.
                 * \param[in] boundary_condition Boundary condition for which additional node bearers might have to be
                 * computed.
                 *
                 * \return The list of node bearers.
                 */
                NodeBearer::vector_shared_ptr
                ComputeNodeBearerListOnBoundary(const GeometricMeshRegion& mesh,
                                                const DirichletBoundaryCondition& boundary_condition) const;


            private:

                /*!
                 * \class doxygen_hide_create_node_list_helper_param_node_for_current_finite_element
                 *
                 * \param[in,out] node_for_current_finite_element Key is the index of the local node considered, value
                 * a pointer to the associated \a Node.
                 */


                /*!
                 * \brief Add to \a node_for_current_finite_element all the \a ref_felt nodes of the \a GeometricElt that
                 * are on a \a OrientedInterfaceT.
                 *
                 * \copydoc doxygen_hide_oriented_interface_tparam
                 * \param[in] geom_elt Geometric element for which node are added.
                 * \param[in] ref_felt Reference finite element for a given \a Unknown and \a NumberingSubset.
                 * \param[in,out] node_list_for_current_local_felt_space List of nodes for the current \a LocalFEltSpace.
                 * \copydoc doxygen_hide_create_node_list_helper_param_node_for_current_finite_element
                 * \param[in,out] interface_node_bearer_list Key is the index of the \a OrientedInterfaceT, value is
                 * a pointer to the associated \a NodeBearer.
                 *
                 * All output parameters are also input ones because \a Node and \a NodeBearer might already exist from:
                 * - Another \a GeometricElt that shares the same \a OrientedInterfaceT.
                 * - Another \a Unknown that acted upon the same \a LocalFEltSpace (only for \a NodeBearer).
                 *
                 */
                template<class OrientedInterfaceT>
                void AddInterfaceNodeList(const GeometricElt& geom_elt,
                                          const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                          NodeBearer::vector_shared_ptr& node_list_for_current_local_felt_space,
                                          std::map<unsigned int, Node::shared_ptr>& node_for_current_finite_element,
                                          std::unordered_map<unsigned int, NodeBearer::shared_ptr>& interface_node_bearer_list);


                /*!
                 * \brief Helper method \a AddInterfaceNodeList(), which creates a new \a Node on a \a NodeBearer.
                 *
                 * \param[in] local_node_on_interface_list List of \a LocalNode to consider on the interface currently
                 * considered in \a AddInterfaceNodeList().
                 * \param[in] extended_unknown Couple (\a Unknown, \a NumberingSubset) for which new node is to be created.
                 * \param[in] Ncomponent Number of dofs to create in association with the new node.
                 * \param[in,out] node_bearer \a NodeBearer upon which new node is created. It is modified as well as new
                 * node is actually stored there.
                 * \copydoc doxygen_hide_create_node_list_helper_param_node_for_current_finite_element
                 */
                template<class LocalDofOnInterfaceT>
                void CreateNode(const LocalDofOnInterfaceT& local_node_on_interface_list,
                                const ExtendedUnknown& extended_unknown,
                                unsigned int Ncomponent,
                                NodeBearer& node_bearer,
                                std::map<unsigned int, Node::shared_ptr>& node_for_current_finite_element);



            private:


                //! List of nodes on the vertex.
                std::unordered_map<unsigned int, NodeBearer::shared_ptr> vertex_node_bearer_list_;

                //! List of nodes on the edges.
                std::unordered_map<unsigned int, NodeBearer::shared_ptr> edge_node_bearer_list_;

                //! List of nodes on the faces.
                std::unordered_map<unsigned int, NodeBearer::shared_ptr> face_node_bearer_list_;

                //! List of nodes on the volume. Not truly necessary; just there for symmetry purposes.
                std::unordered_map<unsigned int, NodeBearer::shared_ptr> volume_node_bearer_list_;

                //! Reference to the node_list_ of FEltSpace object.
                NodeBearer::vector_shared_ptr& node_bearer_list_;


            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/CreateNodeListHelper.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CREATE_NODE_LIST_HELPER_HPP_
