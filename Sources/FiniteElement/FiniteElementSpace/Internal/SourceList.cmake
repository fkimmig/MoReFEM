target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BreakCircularDependancy.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeMatrixPattern.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/CreateNodeListHelper.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofComputations.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexList.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexListManager.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceStorage.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Partition.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReduceToProcessorWise.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/BreakCircularDependancy.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeMatrixPattern.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Connectivity.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/CreateNodeListHelper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/CreateNodeListHelper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofComputations.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofComputations.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexListManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofProgramWiseIndexListPerVertexCoordIndexListManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpaceStorage.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MovemeshHelper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NdofHolder.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Partition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Partition.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReduceToProcessorWise.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
