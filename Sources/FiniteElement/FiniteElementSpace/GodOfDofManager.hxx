///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_


namespace MoReFEM
{


    template<class MeshSectionT>
    void GodOfDofManager::Create(const MeshSectionT& section,
                                 const Wrappers::Mpi& mpi)
    {
        const auto unique_id = section.GetUniqueId();

        auto& mesh = Internal::MeshNS::GeometricMeshRegionManager::GetInstance().GetNonCstMesh(unique_id);

        auto raw_ptr = new GodOfDof(mpi, mesh);

        auto ptr = GodOfDof::shared_ptr(raw_ptr);

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto insert_return_value = list_.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two god of dof objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id) + ").", __FILE__, __LINE__);
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(unsigned int unique_id) const
    {
        return *(GetGodOfDofPtr(unique_id));
    }


    inline GodOfDof& GodOfDofManager::GetNonCstGodOfDof(unsigned int unique_id)
    {
        return const_cast<GodOfDof&>(GetGodOfDof(unique_id));
    }


    inline const auto& GodOfDofManager::GetStorage() const noexcept
    {
        return list_;
    }




} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
