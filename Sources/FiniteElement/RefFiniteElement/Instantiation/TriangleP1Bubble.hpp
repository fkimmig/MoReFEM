///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_BUBBLE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_BUBBLE_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"
# include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle3.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hpp"



namespace MoReFEM
{


    namespace RefFEltNS
    {



        /*!
         * \brief Reference finite element for TriangleP1 with a bubble.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Triangle class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in RefGeomEltNS::TopologyNS::Triangle traits
         * class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */

        class TriangleP1Bubble
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Triangle,
            Internal::ShapeFunctionNS::TriangleP1Bubble,
            InterfaceNS::Nature::vertex,
            1u
        >
        {

        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = TriangleP1Bubble;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TriangleP1Bubble() = default;

            //! Destructor.
            virtual ~TriangleP1Bubble() override;

            //! Copy constructor.
            TriangleP1Bubble(const TriangleP1Bubble&) = default;

            //! Move constructor.
            TriangleP1Bubble(TriangleP1Bubble&&) = default;

            //! Copy affectation.
            TriangleP1Bubble& operator=(const TriangleP1Bubble&) = default;

            //! Move affectation.
            TriangleP1Bubble& operator=(TriangleP1Bubble&&) = default;

            ///@}

        private:



        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P1_BUBBLE_HPP_
