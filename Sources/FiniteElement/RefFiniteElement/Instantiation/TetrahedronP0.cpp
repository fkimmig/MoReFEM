///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/TetrahedronP0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            
            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<TetrahedronP0>();
            
            
        } // namespace // anonymous
        
        
        const std::string& TetrahedronP0::ShapeFunctionLabel()
        {
            static std::string ret("P0");
            return ret;
        }

        
        
        TetrahedronP0::~TetrahedronP0() = default;
        
        
    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
