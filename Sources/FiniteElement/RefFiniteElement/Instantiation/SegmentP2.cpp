///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/SegmentP2.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{
    
    
    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            
            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<SegmentP2>();
            
            
        } // namespace // anonymous
        

        const std::string& SegmentP2::ShapeFunctionLabel()
        {
            static std::string ret("P2");
            return ret;
        }
        
        
        SegmentP2::~SegmentP2() = default;        
        
        
    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
