///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/QuadrangleQ2c.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{
    
    
    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            
            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<QuadrangleQ2c>();
            
            
        } // namespace // anonymous
        
        
        const std::string& QuadrangleQ2c::ShapeFunctionLabel()
        {
            static std::string ret("Q2c");
            return ret;
        }

        
        
        QuadrangleQ2c::~QuadrangleQ2c() = default;
        
        
    } // namespace RefFEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
