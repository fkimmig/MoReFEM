target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1Bubble.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1Bubble.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Order0.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Order0.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1Bubble.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TetrahedronP1Bubble.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1Bubble.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TriangleP1Bubble.hxx" / 
)

