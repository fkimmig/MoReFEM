///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Dec 2015 14:30:24 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TETRAHEDRON_P1_BUBBLE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TETRAHEDRON_P1_BUBBLE_HPP_

# include <memory>
# include <array>

# include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp"
# include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ShapeFunctionNS
        {


            /*!
             * \brief Define shape functions of triangle with P1 bubble (additional dof in the interior).
             */
            struct TetrahedronP1Bubble
            : public RefGeomEltNS::ShapeFunctionNS::Crtp::AccessShapeFunction<TetrahedronP1Bubble>
            {

                enum
                {
                    Nderivate_component_ = 3,
                    Nphi_ = 5,
                    Order = 1
                };

                //! Alias to a function that takes a Coords and returns a double.
                using shape_function_type = MoReFEM::RefGeomEltNS::ShapeFunctionType;

                //! Shape functions.
                static const std::array<shape_function_type, Nphi_>& ShapeFunctionList();

                /*!
                 * \brief First derivative of the shape functions.
                 *
                 * Ordering:
                 *   \li d(phi[0], r), d(phi[0], s), d(phi[0], t)
                 *   \li d(phi[1], r), d(phi[1], s), d(phi[0], t)
                 *   etc...
                 *
                 * \return The derivatives as an array of functions (ordering defined just above)
                 */
                static const std::array<shape_function_type, Nphi_ * Nderivate_component_>& FirstDerivateShapeFunctionList();
            };


        } // namespace ShapeFunctionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TetrahedronP1Bubble.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_TETRAHEDRON_P1_BUBBLE_HPP_
