///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Sep 2016 13:48:46 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TetrahedronP1Bubble.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace ShapeFunctionNS
        {
            
            
            namespace // anonymous
            {
                
                
                std::array<TetrahedronP1Bubble::shape_function_type, 5> ComputeShapeFunctionList();
                
                
                std::array<TetrahedronP1Bubble::shape_function_type, 15> ComputeGradShapeFunctionList();
                
                
            } // namespace anonymous
            
            
            
            const std::array<TetrahedronP1Bubble::shape_function_type, 5>& TetrahedronP1Bubble::ShapeFunctionList()
            {
                static auto ret = ComputeShapeFunctionList();
                
                return ret;
            };
            
            
            
            const std::array<TetrahedronP1Bubble::shape_function_type, 15>& TetrahedronP1Bubble
            ::FirstDerivateShapeFunctionList()
            {
                static auto ret = ComputeGradShapeFunctionList();
                
                return ret;
            }
            
            
            namespace // anonymous
            {
                
                
                std::array<TetrahedronP1Bubble::shape_function_type, 5> ComputeShapeFunctionList()
                {
                    return
                    {
                        {
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                const auto buf = (1. - r - s - t);
                                
                                return buf - 64. * r * s * t * buf;
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return r - 64. * r * s * t * (1. - r - s - t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return s - 64. * r * s * t * (1. - r - s - t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return t - 64. * r * s * t * (1. - r - s - t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return 256. * r * s * t * (1. - r - s -t);
                            }
                        }
                    };
                };
                
                
                std::array<TetrahedronP1Bubble::shape_function_type, 15> ComputeGradShapeFunctionList()
                {
                    return
                    {
                        {
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return -1. - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return -1. - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return -1. - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return 1. - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return  - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return 1. - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                
                                return 1. - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return 256. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return 256. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                const auto t = local_coords.t();
                                return 256. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                            }
                        }
                    };
                    
                }
                
                
            } // namespace anonymous
            
            
        } // namespace ShapeFunctionNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
