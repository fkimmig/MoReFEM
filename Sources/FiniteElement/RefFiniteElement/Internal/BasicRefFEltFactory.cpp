///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Nov 2014 16:03:44 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

# include <sstream>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"
#include "FiniteElement/RefFiniteElement/Internal/Exceptions/BasicRefFEltFactory.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace RefFEltNS
        {
            
            
            const std::string& BasicRefFEltFactory::ClassName()
            {
                static std::string ret("BasicRefFEltFactory");
                return ret;
            }
            
            
            BasicRefFEltFactory::BasicRefFEltFactory()
            {
                basic_ref_felt_storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }
            
            
            const BasicRefFElt& BasicRefFEltFactory::FetchBasicRefFElt(const std::string& topology_name,
                                                                       const std::string& shape_function_label) const
            {
                auto&& key = GenerateKey(topology_name, shape_function_label);
                
                auto it = basic_ref_felt_storage_.find(key);
                
                if (it == basic_ref_felt_storage_.cend())
                    throw BasicRefFEltFactoryNS::ExceptionNS::UnregisteredObject(topology_name,
                                                                                 shape_function_label,
                                                                                 known_pair_,
                                                                                 __FILE__, __LINE__);
                const auto& holder_ptr = it->second;
                assert(!(!holder_ptr));
                return holder_ptr->GetBasicRefFElt();
            }
            
            
            std::string BasicRefFEltFactory
            ::GenerateKey(const std::string& topology_name, const std::string& shape_function_label)
            {
                std::ostringstream oconv;
                oconv << "topology=" << topology_name << "_shape_function=" << shape_function_label;
                
                std::string key(oconv.str());
                
                return key;
            }
            
            
        } // namespace RefFEltNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
