target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
