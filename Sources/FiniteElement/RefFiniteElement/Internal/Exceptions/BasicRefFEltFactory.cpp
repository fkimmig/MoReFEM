///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Nov 2014 16:03:44 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <sstream>

#include "FiniteElement/RefFiniteElement/Internal/Exceptions/BasicRefFEltFactory.hpp"


namespace // anonymous
{
    
    
    // Forward declarations here; definitions are at the end of the file
    std::string UnregisteredObjectMsg(const std::string& topology_name,
                                      const std::string& shape_function_label,
                                      const std::vector<std::pair<std::string, std::string> >& known_pair_list);
    
    
} // namespace anonymous





namespace MoReFEM
{


    namespace Internal
    {
        
        
        namespace RefFEltNS
        {
            
            
            namespace BasicRefFEltFactoryNS
            {
                
                
                namespace ExceptionNS
                {
                    
                    
                    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                    : ::MoReFEM::ExceptionNS::Factory::Exception(msg, invoking_file, invoking_line)
                    { }
                    
                    
                    Exception::~Exception() = default;

                    
                    UnregisteredObject::~UnregisteredObject() = default;
                    
                    
                    UnregisteredObject::UnregisteredObject(const std::string& topology_name,
                                                           const std::string& shape_function_label,
                                                           const std::vector<std::pair<std::string, std::string> >& known_pair_list,
                                                           const char* invoking_file, int invoking_line)
                    : Exception(UnregisteredObjectMsg(topology_name,
                                                      shape_function_label,
                                                      known_pair_list),
                                                      invoking_file, invoking_line)
                    { }

                    
                    
                    
                    
                } // namespace ExceptionNS
                
                
            } // namespace BasicRefFEltFactoryNS
            
            
        } // namespace RefFEltNS
        
        
    } // namespace Internal


} // namespace MoReFEM


namespace // anonymous
{
    
    
    
    std::string UnregisteredObjectMsg(const std::string& topology_name,
                                      const std::string& shape_function_label,
                                      const std::vector<std::pair<std::string, std::string> >& known_pair_list)

    {
        std::ostringstream oconv;
        
        oconv << "The combination '" << topology_name << "' / '" << shape_function_label << "' was not found in the "
        "factory. All the known combinations are: \n";
        
        for (const auto& pair : known_pair_list)
            oconv << "\t- '" << pair.first << "' / '" << pair.second << '\'' << std::endl;
        
        
        
        
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup FiniteElementGroup



#include "Utilities/Exceptions/Factory.hpp"


