///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 17:15:29 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HPP_

# include <memory>
# include <vector>
# include <algorithm>
# include <unordered_map>

# include "Utilities/Containers/UnorderedMap.hpp"
# include "Utilities/Miscellaneous.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            #ifndef NDEBUG

            /*!
             * \brief Debug function to check whether the list are well-formed.
             *
             * If not, an assert will occur.
             *
             * \param[in] local_node_list List of local nodes being scrutinized.
             * \param[in] nature All nodes in the list are supposed to be located on \a Interface of
             * this type.
             */
            void CheckLocalNodeListConsistency(const LocalNode::vector_const_shared_ptr& local_node_list,
                                               ::MoReFEM::InterfaceNS::Nature nature);
            #endif // NDEBUG


            /*!
             * \brief Abstract base class used to define a reference finite element.
             *
             * If a new type of finite element is to be added, it should inherit from present class. So far there
             * are two 'families' of finite elements:
             * - Those that uses up the geometric shape functions.
             * - Spectral ones.
             *
             * All the types of finite elements made accessible to an advanced user must be registered properly
             * in the BasicRefFEltFactory, in which the ShapeFunctionLabel() value acts as a key. See for instance
             * FiniteElement/RefFiniteElement/Instantiation/Spectral.cpp for an example of such a registration.
             *
             * The inherited class are in charge of five operations:
             * - Implementing the shape function.
             * - Implementing the first derivate of the shape function.
             * - Compute the local node list (pure virtual method to override).
             * - Call Init() method at the end of its constructor.
             * - Defining a static method ShapeFunctionLabel which returns a string that describes the shape function.
             *
             * \attention This class is really low-level and an advanced user or a developer shouldn't have to deal
             * directly with BasicRefFElt (except to define a new reference finite element).
             *
             * What a developer should use when defining its own operator is a \a RefFEltInLocalOperator, which is
             * an object built on top of a RefFEltInFEltSpace which is itself built upon a BasicRefFElt.
             *
             */
            class BasicRefFElt
            {

            public:

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const BasicRefFElt>;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 */
                BasicRefFElt();

                //! Destructor.
                virtual ~BasicRefFElt();

                //! Copy constructor.
                BasicRefFElt(const BasicRefFElt&) = default;

                //! Move constructor.
                BasicRefFElt(BasicRefFElt&&) = default;

                //! Affectation.
                BasicRefFElt& operator=(const BasicRefFElt&) = default;

                //! Affectation.
                BasicRefFElt& operator=(BasicRefFElt&&) = default;

                ///@}

            protected:

                /*!
                 * \brief The method that does the actual initialisation of the object.
                 *
                 * \attention This method must be called in the constructor of the derived object!
                 */
                template<class TopologyT>
                void Init();


            public:

                /// \name Accessors to the nodes regarless of the underlying interface.
                ///@{

                //! Return the number of local nodes.
                unsigned int NlocalNode() const noexcept;

                //! Access to the \a i -th local node, whatever its nature.
                const LocalNode& GetLocalNode(unsigned int i) const noexcept;

                //! Access to the complete list of local nodes, whatever their nature.
                const LocalNode::vector_const_shared_ptr& GetLocalNodeList() const noexcept;

                ///@}



                /// \name Accessors to the nodes on vertices.
                ///@{


                //! Whether there are nodes on vertices.
                bool AreNodesOnVertices() const noexcept;


                /*!
                 * \brief Return the LocalNode that matches the given \a topologic_vertex_index of the vertex
                 *
                 * \param[in] topologic_vertex_index Index defined in RefGeomEltNS::TopologyNS. For instance for Spectral
                 * finite element associated to a quadrangle:

                 *
                 *     3-----------2
                 *     |           |
                 *     | 	       |
                 *     |           |
                 *     | 	       |
                 *     | 	       |
                 *     0-----------1
                 *
                 * \return The LocalNode object associated to this vertex (its internal index is the one computed
                 * within current class). Might be nullptr if none.
                 */
                const LocalNode::const_shared_ptr& GetLocalNodeOnVertexPtr(unsigned int topologic_vertex_index) const noexcept;

                //! Access to the \a topologic_vertex_index -th local node on vertex.
                const LocalNode& GetLocalNodeOnVertex(unsigned int topologic_vertex_index) const noexcept;

                //! Access to the list of nodes on vertex.
                const LocalNode::vector_const_shared_ptr& GetLocalNodeOnVertexList() const noexcept;

                ///@}



                /// \name Accessors to the nodes on edges.
                ///@{

                //! Whether there are nodes on edges.
                bool AreNodesOnEdges() const noexcept;


                /*!
                 * \brief Return all the local nodes on the given geometric edge with the proper orientation.
                 *
                 * \param[in] topologic_edge_index Index defined in RefGeomEltNS::TopologyNS. For instance for Spectral
                 * finite element associated to a quadrangle:
                 * \param[in] orientation Index which gives away the orientation chosen. See \a OrientedEdge for more
                 * details.
                 
                 \verbatim
                 
                      3-----------2
                      |           |
                      |           |            Edges:   { { 0, 1 } },
                      |           |                     { { 1, 2 } },
                      |           |                     { { 3, 2 } },
                      | 	      |                     { { 0, 3 } }
                      0-----------1
                 \endverbatim
                 *
                 * \return The LocalNode objects associated to this edge (its internal index is the one computed
                 * within current class).
                 */
                const LocalNode::vector_const_shared_ptr& GetLocalNodeOnEdgeList(unsigned int topologic_edge_index,
                                                                                 unsigned int orientation) const noexcept;

                //! Return all the local nodes on the given geometric edge regardless of the orientation.
                const std::vector<LocalNode::vector_const_shared_ptr>&
                GetLocalNodeOnEdgeList(unsigned int topologic_edge_index) const noexcept;

                ///@}



                /// \name Accessors to the nodes on faces.
                ///@{

                //! Whether there are nodes on faces.
                bool AreNodesOnFaces() const noexcept;

                //! Same for faces.
                const LocalNode::vector_const_shared_ptr& GetLocalNodeOnFaceList(unsigned int topologic_face_index,
                                                                                 unsigned int orientation) const noexcept;

                //! Return all the local nodes on the given geometric face regardless of the orientation.
                const std::vector<LocalNode::vector_const_shared_ptr>&
                GetLocalNodeOnFaceList(unsigned int topologic_face_index) const noexcept;

                ///@}


                /// \name Accessors to the nodes on volume.
                ///@{

                //! Whether there are nodes on volume.
                bool AreNodesOnVolume() const noexcept;

                //! Same for volume.
                const LocalNode::vector_const_shared_ptr& GetLocalNodeOnVolumeList() const noexcept;

                ///@}



                /*!
                 * \brief Print the content of the ref finite element.
                 */
                void Print(std::ostream& out) const;

                //! Dimension of the underlying topology.
                unsigned int GetTopologyDimension() const noexcept;

                /*!
                 * \copydoc doxygen_hide_shape_function
                 */
                virtual double ShapeFunction(unsigned int local_node_index,
                                             const LocalCoords& local_coords) const = 0;


                /*!
                 * \copydoc doxygen_hide_first_derivate_shape_function
                 */
                virtual double FirstDerivateShapeFunction(unsigned int local_node_index,
                                                          unsigned int component,
                                                          const LocalCoords& local_coords) const = 0;


                //! \copydoc doxygen_hide_shape_function_order_method
                virtual unsigned int GetOrder() const noexcept = 0;

            private:

                /*!
                 * \brief Compute the local nodes.
                 *
                 * This method should not be called outside of Init() method.
                 *
                 * \return List of local nodes.
                 */
                virtual LocalNode::vector_const_shared_ptr ComputeLocalNodeList() = 0;

                //! Sort local_node_on_vertex_list_ so that its ordering match the one in TopologyT.
                template<class TopologyT>
                void SortLocalNodeOnVertexList();

                //! Set the list of local nodes on edges within the dedicated container.
                template <class TopologyT>
                std::enable_if_t<!std::is_same<typename TopologyT::EdgeTopology, std::false_type>::value, void>
                SetEdgeLocalNodeList(const LocalNode::vector_const_shared_ptr& local_node_on_edge_list);

                //! Placeholder for the case nothing needs to be done.
                template <class TopologyT>
                std::enable_if_t<std::is_same<typename TopologyT::EdgeTopology, std::false_type>::value, void>
                SetEdgeLocalNodeList(const LocalNode::vector_const_shared_ptr& local_node_on_edge_list);

                //! Set the list of local nodes on faces within the dedicated container.
                template <class TopologyT>
                std::enable_if_t<!std::is_same<typename TopologyT::FaceTopology, std::false_type>::value, void>
                SetFaceLocalNodeList(const LocalNode::vector_const_shared_ptr& local_node_on_face_list);

                //! Placeholder for the case nothing needs to be done.
                template <class TopologyT>
                std::enable_if_t<std::is_same<typename TopologyT::FaceTopology, std::false_type>::value, void>
                SetFaceLocalNodeList(const LocalNode::vector_const_shared_ptr& local_node_on_face_list);

                //! Return the number of nodes on vertices.
                unsigned int NnodeOnVertex() const noexcept;

                //! Return the number of nodes on edges.
                unsigned int NnodeOnEdge() const noexcept;

                //! Return the number of nodes on faces.
                unsigned int NnodeOnFace() const noexcept;


            private:


                //! List of local nodes, whatever their nature.
                LocalNode::vector_const_shared_ptr local_node_list_;

                //! Subset of local_node_list_ that includes only nodes on vertices.
                LocalNode::vector_const_shared_ptr local_node_on_vertex_list_;

                /*!
                 * \brief Local nodes on edges stored with their topologic index as a key.
                 *
                 * For each geometric key there is an array of two elements: one for each orientation.
                 * For each geometric key/orientation, there is the list of all nodes present.
                 */
                std::vector<std::vector<LocalNode::vector_const_shared_ptr>> local_node_on_edge_storage_;

                /*!
                 * \brief Local nodes on faces stored with their topologic index as a key.
                 *
                 * For each geometric key there is an array of several elements: one for each orientation.
                 * For each geometric key/orientation, there is the list of all nodes present.
                 */
                std::vector<std::vector<LocalNode::vector_const_shared_ptr>> local_node_on_face_storage_;

                //! Subset of local_node_list_ that includes only nodes on volume.
                LocalNode::vector_const_shared_ptr local_node_on_volume_list_;

                //! Dimension of the underlying topology.
                unsigned int topology_dimension_ = NumericNS::UninitializedIndex<unsigned int>();

            };



        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_BASIC_REF_F_ELT_HPP_
