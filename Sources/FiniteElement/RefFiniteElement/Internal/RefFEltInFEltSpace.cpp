///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:18:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace RefFEltNS
        {
            
            
            RefFEltInFEltSpace::RefFEltInFEltSpace(const BasicRefFElt& basic_ref_felt,
                             const ExtendedUnknown& extended_unknown,
                             const unsigned int mesh_dimension,
                             const unsigned int felt_space_dimension)
            : basic_ref_felt_(basic_ref_felt),
            extended_unknown_(extended_unknown),
            mesh_dimension_(mesh_dimension),
            felt_space_dimension_(felt_space_dimension)
            {
            }
            
            RefFEltInFEltSpace::~RefFEltInFEltSpace() = default;
            
            
            unsigned int RefFEltInFEltSpace::Ndof() const noexcept
            {
                const auto& unknown = GetExtendedUnknown().GetUnknown();
                const auto Nlocal_node = GetBasicRefFElt().NlocalNode();
                
                switch(unknown.GetNature())
                {
                    case UnknownNS::Nature::scalar:
                        return Nlocal_node;
                    case UnknownNS::Nature::vectorial:
                        return Nlocal_node * GetMeshDimension();
                }
                
                assert(false);
                return NumericNS::UninitializedIndex<unsigned int>();
            }
            
            
            unsigned int RefFEltInFEltSpace::Ncomponent() const
            {
                const auto unknown_nature = GetExtendedUnknown().GetUnknown().GetNature();
                
                switch(unknown_nature)
                {
                    case UnknownNS::Nature::scalar:
                        return 1u;
                    case UnknownNS::Nature::vectorial:
                        return GetMeshDimension();
                }
                
                assert(false);
                return NumericNS::UninitializedIndex<unsigned int>();
            }
            
            
        } // namespace RefFEltNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
