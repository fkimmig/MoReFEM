target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Comm.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Op.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Comm.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Op.hpp" / 
)

