target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Mpi.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Mpi.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mpi.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MpiScale.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/MacroEncapsulation/SourceList.cmake)
