///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HPP_



# include <vector>
# include <bitset>
# include <memory>
# include <numeric>

# include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

# include "Utilities/Numeric/Numeric.hpp"

# include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Op.hpp"
# include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Comm.hpp"

# include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hpp"
# include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hpp"



namespace MoReFEM
{


    namespace Wrappers
    {


        /// \addtogroup UtilitiesGroup
        ///@{



        /*!
         *
         * \brief A wrapper over MPI functions call, with some common parameters stored in an object.
         *
         * The purpose is to provide a slightly more user-friendly interface, and to make possible
         * to make once and for all some of the choices such as which is the root processor or whether
         * this root processor takes its share of the calculation or not.
         *
         * Only the functionalities required by the code have been put here; it is therefore just a subset of
         * what you can do with MPI. Don't hesitate to request additional features there if you need them.
         *
         * Error handling: currently there is the choice to use MPI_ERRORS_ARE_FATAL rather than MPI_ERRORS_RETURN;
         * that's the reason the error code are not checked.
         *
         */
        class Mpi
        {

        public:

            //! Alias to unique_ptr.
            using const_unique_ptr = std::unique_ptr<const Mpi>;

            /*!
             * \brief The value MPI_ANY_TAG doesn't seem to be accepted, so I defined one myself.
             *
             * Currentlu tags aren't used at all in the operations.
             *
             * \return Value 0
             */
            static constexpr int AnyTag();

        public:

            /*!
             * \brief Must be called before any Mpi class is created.
             *
             * \param[in] argc The first argument from main() function.
             * \param[in] argv The second argument from main() function.
             *
             */
            static void InitEnvironment(int argc, char** argv);

            /// \name Special members.

            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] root_processor Which processor is used as root processor.
             * \param[in] comm Communication channel used by MPI.
             */
            explicit Mpi(int root_processor, MpiNS::Comm comm);


            /*!
             * \brief Destructor.
             *
             * BEWARE: when all Mpi objects are destroyed the environment is destroyed; you can't revive
             * other MPI object unless InitEnvironment() is called again.
             */
            ~Mpi();


            //! Copy constructor.
            Mpi(const Mpi&) = delete;

            //! Move constructor.
            Mpi(Mpi&&) = delete;

            //! Copy affectation.
            Mpi& operator=(const Mpi&) = delete;

            //! Move affectation.
            Mpi& operator=(Mpi&&) = delete;



            ///@}


            /*!
             * \brief Get the rank of the processor.
             *
             * \tparam IntT Type in which the result is cast.
             * \return Rank of the calling processor.
             *
             */
            template<typename IntT>
            IntT GetRank() const;

            //! Get the communicator.
            MPI_Comm GetCommunicator() const;

            /*!
             * \brief Get the total number of processors.
             *
             * \tparam IntT Type in which the result is cast.
             * \return Total number of processors.
             */
            template<typename IntT>
            IntT Nprocessor() const;

            /*!
             * \brief Returns whether current proc is root processor.
             *
             */
            bool IsRootProcessor() const;

            /*!
             * \brief An interface over MPI_Gather().
             *
             * I had a hard time understanding how to use properly the original Mpi function, as I didn't understand
             * correctly one of the argument (the number of element sent, which is in fact the number of element sent
             * PER PROCESSOR).
             *
             * Present function aims to provide a much simpler interface, that is likely less powerful than the original
             * one but much more secure.
             *
             *
             *
             * \tparam T Type of the variable sent. Usually a POD C++ type, such as 'int'.
             *
             * \param[in] sent_data Data sent by the current processor. All sent_data must have the same size.
             * Otherwise use MPI_Gatherv (not implemented in the Wrapper yet).
             * \param[out] gathered_data Relevant only for the root processor. This vector includes all
             * the data gathered from the other processors. The ordering follows the ordering of processors.
             *
             *
             */
            template<typename T>
            void Gather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const;

            //! Overload for std::vector<bool>, to circumvent STL implementation with bits...
            void Gather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const;


            /*!
             * \brief An interface over MPI_Gatherv().
             *
             * I had a hard time understanding how to use properly the original Mpi function, as I didn't understand
             * correctly one of the argument (the number of element sent, which is in fact the number of element sent
             * PER PROCESSOR).
             *
             * Present function aims to provide a much simpler interface, that is likely less powerful than the original
             * one but much more secure.
             *
             *
             *
             * \tparam T Type of the variable sent. Usually a POD C++ type, such as 'int'.
             *
             * \param[in] sent_data Data sent by the current processor. The vectors can have different sizes on each processor.
             * \param[out] gathered_data Relevant only for the root processor. This vector includes all
             * the data gathered from the other processors. The ordering follows the ordering of processors.
             *
             *
             */
            template<typename T>
            void Gatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const;


            //! Overload for std::vector<bool>, to circumvent STL implementation with bits...
            void Gatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const;


            /*!
             * \brief An interface over MPI_Allgather().
             *
             * I had a hard time understanding how to use properly the original Mpi function, as I didn't understand
             * correctly one of the argument (the number of element sent, which is in fact the number of element sent
             * PER PROCESSOR).
             *
             * Present function aims to provide a much simpler interface, that is likely less powerful than the original
             * one but much more secure.
             *
             *
             *
             * \tparam T Type of the variable sent. Usually a POD C++ type, such as 'int'.
             *
             * \param[in] sent_data Data sent by the current processor. All sent_data must have the same size.
             * \param[out] gathered_data This vector includes all the data gathered from the other processors.
             * The ordering follows the ordering of processors.
             *
             *
             */
            template<typename T>
            void AllGather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const;

            //! Overload for std::vector<bool>, to circumvent STL implementation with bits...
            void AllGather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const;


            /*!
             * \brief An interface over MPI_Allgatherv().
             *
             * I had a hard time understanding how to use properly the original Mpi function, as I didn't understand
             * correctly one of the argument (the number of element sent, which is in fact the number of element sent
             * PER PROCESSOR).
             *
             * Present function aims to provide a much simpler interface, that is likely less powerful than the original
             * one but much more secure.
             *
             *
             *
             * \tparam T Type of the variable sent. Usually a POD C++ type, such as 'int'.
             *
             * \param[in] sent_data Data sent by the current processor. The vectors can have different sizes on each processor.
             * \param[out] gathered_data This vector includes all the data gathered from the other processors.
             * The ordering follows the ordering of processors. Contrary to Gatherv the data is present on each processor.
             *
             *
             */
            template<typename T>
            void AllGatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const;


            //! Overload for std::vector<bool>, to circumvent STL implementation with bits...
            void AllGatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const;


            /*!
             * \brief An interface over MPI_Bcast().
             *
             * \param[in,out] data If on the root processor, this input parameter is the data sent to all other processors.
             * If not, it is the variable into which the result is written.
             * \warning Size of data must be properly allocated before the call on each processor!
             *
             * \tparam T Type of the data being sent. This is a standard C++ type (double, unsigned int, ...) for which
             * a specialization of Internal::Wrappers::MpiNS::Datatype must exist.
             */
            template<typename T>
            void Broadcast(std::vector<T>& data) const;


            //! Overload for std::vector<bool>, to circumvent STL implementation with bits...
            void Broadcast(std::vector<bool>& data) const;


            /*!
             * \brief An interface over MPI_Allreduce().
             *
             * Each processor detains a vector of the exact same size; the reduction consists in agglomerating
             * all of them on each of the processor.
             *
             * For instance, in MoReFEM each processor fills a part of the sparse matrix, and put 0 for degree
             * of freedoms it doesn't manage. The call to MPI_Allreduce aggregate all those vectors and hence
             * form the global sparse matrix content.
             *
             * \tparam T Type of the variable sent. Usually a POD C++ type, such as 'int'.
             *
             * \param[in] sent_data Data sent by the current processor.
             * \param[in] mpi_operation The MPI operation used during the reduction.
             * \return Data gathered by the current processor.
             *
             */
            template<typename T>
            std::vector<T> AllReduce(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const;

            //! Overload for a single value.
            template<typename T>
            T AllReduce(T sent_data, MpiNS::Op mpi_operation) const;

            //! Overload for std::vector<bool>, to circumvent its STL implementation with bits...
            std::vector<bool> AllReduce(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const;


            /*!
             * \brief The actual implementation of the Allreduce() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor.
             * \param[in] mpi_operation The MPI operation used during the reduction.
             * \param[out] gathered_data gathered by the current processor.
             */
            template<class ContainerT>
            void AllReduce(const ContainerT& sent_data,
                           ContainerT& gathered_data,
                           MpiNS::Op mpi_operation) const;


            //! Reduce operation, which target is the root processor.
            template<typename T>
            std::vector<T> ReduceOnRootProcessor(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const;

            //! Overload for a single value.
            template<typename T>
            T ReduceOnRootProcessor(T sent_data, MpiNS::Op mpi_operation) const;

            //! Overload for std::vector<bool>, to circumvent its STL implementation with bits...
            std::vector<bool> ReduceOnRootProcessor(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const;


            //! Overload for std::bitset<N>.
            template<std::size_t N>
            std::bitset<N> ReduceOnRootProcessor(const std::bitset<N>& sent_data, MpiNS::Op mpi_operation) const;


            /*!
             * \brief Collect the values of a given data from each processor.
             *
             * \param[in] sent_data Processor-wise data which is collected.
             * \return A vector with one entry per processor: the value of the collected data.
             *
             * \internal <b><tt>[internal]</tt></b> This method calls AllReduce() under the hood, so no need to foresee all overloads
             * (the correct AllReduce will be chosen automatically).
             */
            template<typename T>
            std::vector<T> CollectFromEachProcessor(T sent_data) const;

            /*!
             * \brief Returns a string that gives the rank between [].
             *
             */
            const std::string& GetRankPreffix() const;


            /*!
             * \brief Call to MPI_Barrier that tells the processor to block process for other processors.
             */
            void Barrier() const;


            /*!
             * \class doxygen_hide_mpi_send_destination_arg
             *
             * \param[in] destination Rank of the processor to which the message is sent.
             */


            /*!
             * \brief Wrapper over MPI_Send for a single value.
             *
             * \tparam T Type of data sent (there must be a dedicated specialization of
             * Internal::Wrappers::MpiNS::Datatype<> template class for it).
             * \copydoc doxygen_hide_mpi_send_destination_arg
             * \param[in] data Single value to be sent.
             */
            template<typename T>
            void Send(unsigned int destination,
                      T data) const;

            /*!
             * \brief Wrapper over MPI_Send for a container.
             *
             * \tparam ContainerT Type of the container involved; it must define data(), size() and value_type. The
             * latter must be a POD type recognized by Openmpi (there must be a dedicated specialization of
             * Internal::Wrappers::MpiNS::Datatype<> template class for it).
             * \copydoc doxygen_hide_mpi_send_destination_arg
             * \param[in] data Container to be sent.
             */
            template<class ContainerT>
            void SendContainer(unsigned int destination,
                               const ContainerT& data) const;

            /*!
             * \class doxygen_hide_mpi_receive
             *
             * \tparam T Type of the data sent; this must
             * be a POD type recognized by Openmpi (there must be a dedicated specialization of
             * Internal::Wrappers::MpiNS::Datatype<> template class for it).
             * \param[in] rank Rank of the processor from which the message was sent.
             * \return A vector with all the data sent. It is redimensioned to match the content (so its size is lower
             * or equal to \a max_length).
             */

            /*!
             * \brief Wrapper over MPI_Recv for an array.
             *
             * \copydoc doxygen_hide_mpi_receive
             * \param[in] max_length Maximal number of expected items to be received.
             */
            template<typename T>
            std::vector<T> Receive(unsigned int rank,
                                   unsigned int max_length) const;

            /*!
             * \brief Wrapper over MPI_Recv for a single value.
             *
             * \copydoc doxygen_hide_mpi_receive
             */
            template<typename T>
            T Receive(unsigned int rank) const;


        private:

            /*!
             * \brief The actual implementation of the Gather() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor.
             * \param[out] gathered_data Relevant only for the root processor. This vector includes all
             * the data gathered from the other processors. The ordering follows the ordering of processors.
             *
             * \internal <b><tt>[internal]</tt></b> Assumes all processors take part in the calculation; if not Mpi_gatherv should be called instead
             * in the implementation!
             */
            template<class ContainerT>
            void GatherImpl(const ContainerT& sent_data,
                            ContainerT& gathered_data) const;


            /*!
             * \brief The actual implementation of the Gatherv() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor. The vectors can have different sizes on each processor.
             * \param[out] gathered_data Relevant only for the root processor. This vector includes all
             * the data gathered from the other processors. The ordering follows the ordering of processors.
             *
             */
            template<class ContainerT>
            void GathervImpl(const ContainerT& sent_data,
                            ContainerT& gathered_data) const;


            /*!
             * \brief The actual implementation of the AllGather() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor.
             * \param[out] gathered_data This vector includes all the data gathered from the other processors.
             * The ordering follows the ordering of processors.
             *
             */
            template<class ContainerT>
            void AllGatherImpl(const ContainerT& sent_data,
                               ContainerT& gathered_data) const;


            /*!
             * \brief The actual implementation of the AllGatherv() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor. The vectors can have different sizes on each processor.
             * \param[out] gathered_data This vector includes all the data gathered from the other processors.
             * The ordering follows the ordering of processors. Contrary to Gatherv the data is present on each processor.
             *
             */
            template<class ContainerT>
            void AllGathervImpl(const ContainerT& sent_data,
                                ContainerT& gathered_data) const;


            /*!
             * \brief The actual implementation of the Reduce() method.
             *
             * This implementation will be used for the generic case as well as the very specific case
             * of booleans, which are handled by a hand-made container.
             *
             *
             * \tparam ContainerT Either std::vector<T> or BoolArray expected (underlying type obtained through ContainerT::value_type).
             *
             * \param[in] sent_data Data sent by the current processor.
             * \param[in] mpi_operation The MPI operation used during the reduction.
             * \param[in] target_processor Processor on which the reduction occurs.
             * \param[out] gathered_data gathered by the current processor.
             */
            template<class ContainerT>
            void ReduceImpl(const ContainerT& sent_data,
                            ContainerT& gathered_data,
                            int target_processor,
                            MpiNS::Op mpi_operation) const;


        private:

            //! Number of MPI objects currently alive.
            static int& Nalive();

            //! Whether the environment has been set or not.
            static bool& IsEnvironment();

            //! Increment the counter. If first object, initialize MPI context.
            void IncrementNalive();

            //! Decrement the counter. If last object, call MPI::Finalize().
            void DecrementNalive();

            //! Access to the rank of the root processor.
            int GetRootProcessor() const;

            /*!
             * \brief If an error code is not MPI_SUCCESS, print the message on screen and abort the whole program.
             *
             * \param[in] rank Rank of the current processor.
             * \param[in] error_code Error code returned by a function of the mpi API.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            void AbortIfErrorCode(int rank, int error_code, const char* invoking_file, int invoking_line) const;



        private:


            //! Root processor. Should not be changed once object is built.
            int root_processor_;

            //! Total number of processors.
            int Nprocessor_ = NumericNS::UninitializedIndex<int>();

            //! Comm channel used with MPI.
            MPI_Comm comm_;

            //! Rank of the processor.
            int rank_ = NumericNS::UninitializedIndex<int>();
        };


        ///@} // \addtogroup


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Mpi/Mpi.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_HPP_
