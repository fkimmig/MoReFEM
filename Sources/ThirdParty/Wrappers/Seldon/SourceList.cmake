target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SeldonFunctions.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubVector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubVector.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubVector_Base.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubVector_Base.hxx" / 
)

