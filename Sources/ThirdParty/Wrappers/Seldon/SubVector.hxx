///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Aug 2014 15:52:43 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HXX_

#include "ThirdParty/Wrappers/Seldon/SubVector.hxx"



namespace Seldon
{


    ////////////////////////
    // VECTOR<SUBSTORAGE> //
    ////////////////////////


    /***************
     * CONSTRUCTOR *
     ***************/


    //! Main constructor.
    template <class T, class V, class Allocator>
    inline Vector<T, SubStorage<V>, Allocator>
    ::Vector(V& A, Vector<int> row_list):
    SubVector_Base<T, V, Allocator>(A, row_list)
    {
    }


    /**************
     * DESTRUCTOR *
     **************/


    //! Destructor.
    template <class T, class V, class Allocator>
    inline Vector<T, SubStorage<V>, Allocator>::~Vector()
    {
    }


    ///////////////
    // SUBVECTOR //
    ///////////////


    /***************
     * CONSTRUCTOR *
     ***************/


    //! Main constructor.
    template <class V>
    inline SubVector<V>
    ::SubVector(V& A, Vector<int> row_list):
    Vector<typename V::value_type,
    SubStorage<V>, typename V::allocator>(A, row_list)
    {
    }


} // namespace Seldon


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_SUB_VECTOR_HXX_
