///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 3 Mar 2015 10:12:24 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include "ThirdParty/Wrappers/Petsc/Print.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {

            
            void SynchronizedFlush(const Mpi& mpi,
                                   FILE* C_file,
                                   const char* invoking_file, int invoking_line)
            {
                int error_code = PetscSynchronizedFlush(mpi.GetCommunicator(), C_file);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscSynchronizedFlush", invoking_file, invoking_line);
            }
          
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
  

} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
