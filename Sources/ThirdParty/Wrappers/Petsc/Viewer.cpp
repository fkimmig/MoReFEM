///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Feb 2014 11:56:07 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            
            Viewer::Viewer(const Mpi& mpi,
                           const std::string& ascii_file,
                           PetscViewerFormat format,
                           const char* invoking_file, int invoking_line)
            : viewer_(nullptr)
            {
                int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerCreate", invoking_file, invoking_line);
                
                if (format == PETSC_VIEWER_ASCII_MATLAB && !Utilities::String::EndsWith(ascii_file, ".m"))
                    throw ExceptionNS::WrongMatlabExtension(ascii_file, invoking_file, invoking_line);
                
                error_code = PetscViewerASCIIOpen(mpi.GetCommunicator(),
                                                  ascii_file.c_str(),
                                                  &viewer_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerASCIIOpen", invoking_file, invoking_line);

                error_code = PetscViewerPushFormat(viewer_, format);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerSetFormat", invoking_file, invoking_line);
            }
            
            
            
            Viewer::Viewer(const Mpi& mpi,
                           const std::string& binary_file,
                           PetscFileMode file_mode,
                           const char* invoking_file, int invoking_line)
            : viewer_(nullptr)
            {
                int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerCreate", invoking_file, invoking_line);
                
                error_code = PetscViewerBinaryOpen(mpi.GetCommunicator(),
                                                   binary_file.c_str(),
                                                   file_mode,
                                                   &viewer_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscViewerBinaryOpen", invoking_file, invoking_line);
            }

            
            

            Viewer::~Viewer()
            {
                assert(!(!viewer_));
                int error_code = PetscViewerDestroy(&viewer_);
                static_cast<void>(error_code); // to avoid warning in release compilation.
                assert(!error_code);
            }

            
            PetscViewer& Viewer::GetUnderlyingPetscObject()
            {
                assert(!(!viewer_));
                return viewer_;
            }
            
            
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
