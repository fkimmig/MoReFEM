///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Sep 2016 22:55:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

# include <iostream>
# include <algorithm>

# include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace Wrappers
        {
            
            
            namespace Petsc
            {
                
                
                const std::string& CheckUpdateGhostManager::ClassName()
                {
                    static std::string ret("CheckUpdateGhostManager");
                    return ret;
                }
                
                
                void CheckUpdateGhostManager::UnneededCall(const std::string& file, int line)
                {
                    decltype(auto) log = GetNonCstLog();
                    
                    auto&& pair = std::make_pair(file, line);
                    
                    const auto it = log.find(pair);
                    
                    // Insert the entry with false only if it doesn't exist.
                    // If it exists, keep current value.
                    if (it == log.cend())
                        log.insert({pair, false});
                }
                
                
                void CheckUpdateGhostManager::NeededCall(const std::string& file, int line)
                {
                    decltype(auto) log = GetNonCstLog();
                    
                    auto&& pair = std::make_pair(file, line);
                    
                    const auto it = log.find(pair);
                    
                    if (it != log.cend())
                        it->second = true;
                    else
                        log.insert({pair, true});
                }
                
                
                void CheckUpdateGhostManager::Print() const
                {
                    decltype(auto) log = GetLog();
                    
                    if (std::all_of(log.cbegin(),
                                    log.cend(),
                                    [](const auto& pair)
                                    {
                                        return pair.second == true;
                                    }))
                        std::cout << "[NOTE] No unneeded call to UpdateGhosts()." << std::endl;
                    else
                    {
                        std::cout << "[WARNING] The following calls to UpdateGhosts() were probably unneeded. "
                        "However check your model was running with sufficient runs and a big enough mesh, and "
                        "proceed with caution: this is a mere indicator, that might be wrong. Check whether you "
                        "obtain the same result after removing the incriminated lines!" << std::endl;
                        
                        for (const auto& pair : log)
                        {
                            if (!pair.second)
                            {
                                const auto& file_line_pair = pair.first;
                                std::cout << "\t" << file_line_pair.first << ", line " << file_line_pair.second << std::endl;
                            }
                        }
                    }
                }
                
                
            } // namespace Petsc
            
            
        } // namespace Wrappers
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
