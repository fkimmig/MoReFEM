target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CheckUpdateGhostManager.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VectorHelper.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/CheckUpdateGhostManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/CheckUpdateGhostManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/VectorHelper.hpp" / 
)

