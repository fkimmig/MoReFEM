///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include <cassert>
#include <cmath>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Petsc/Print.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            #ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
            
            
            namespace // anonymous
            {
                
                // Extract ghost values only from vector.
                std::vector<double> ExtractGhostValues(const Vector& vector,
                                                       const char* invoking_file, int invoking_line);
                
                
            } // namespace anonymous
            
            
            #endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

            
            
            Vector::Vector()
            : petsc_vector_(PETSC_NULL),
            do_petsc_destroy_(false) // Nothing is assigned yet!
            { }
            
            
            Vector::Vector(const Vec& petsc_vector, bool do_destroy_petsc)
            : petsc_vector_(petsc_vector),
            do_petsc_destroy_(do_destroy_petsc)
            {
                assert(petsc_vector != PETSC_NULL);
            }
            
            
            Vector::Vector(const Vector& rhs)
            : petsc_vector_(PETSC_NULL),
            do_petsc_destroy_(rhs.do_petsc_destroy_)
            {
                CompleteCopy(rhs, __FILE__, __LINE__);
            }
            
            
            Vector::Vector(const Mpi& mpi,
                           const std::string& binary_file,
                           const char* invoking_file, int invoking_line)
            : petsc_vector_(PETSC_NULL),
            do_petsc_destroy_(true)
            {
                const auto communicator = mpi.GetCommunicator();
                
                Viewer viewer(mpi,
                              binary_file,
                              FILE_MODE_READ,
                              invoking_file, invoking_line);
                
                int error_code = VecCreate(communicator, &petsc_vector_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecCreate", invoking_file, invoking_line);
                
                error_code = VecLoad(petsc_vector_, viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecLoad", invoking_file, invoking_line);
            }

            
            
            
            Vector::~Vector()
            {
                if (do_petsc_destroy_)
                {
                    assert(petsc_vector_ != PETSC_NULL);
                    int error_code = VecDestroy(&petsc_vector_);
                    assert(!error_code && "Error in Vec destruction."); // no exception in destructors!
                    static_cast<void>(error_code); // to avoid arning in release compilation.
                }
            }
            
            
            void Swap(Wrappers::Petsc::Vector& A, Wrappers::Petsc::Vector& B)
            {
                using std::swap;
                swap(A.do_petsc_destroy_, B.do_petsc_destroy_);
                swap(A.petsc_vector_, B.petsc_vector_);
            }
            
            
            void Vector::InitSequentialVector(unsigned int size,
                                              const Mpi& mpi,
                                              const char* invoking_file, int invoking_line)
            {
                assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
                int error_code = VecCreateSeq(mpi.GetCommunicator(), static_cast<PetscInt>(size), &petsc_vector_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecCreateSeq", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }
            
            
            void Vector::InitMpiVector(unsigned int local_size, unsigned int global_size,
                                       const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
                assert(local_size <= global_size && "If not, either sequential mode or the local and global were "
                       "provided in the wrong order.");
                
                int error_code = VecCreateMPI(mpi.GetCommunicator(),
                                              static_cast<PetscInt>(local_size),
                                              static_cast<PetscInt>(global_size),
                                              &petsc_vector_);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecCreateMPI", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }
            
            
            void Vector::InitMpiVectorWithGhost(unsigned int local_size, unsigned int global_size,
                                                const std::vector<PetscInt>& ghost_padding,
                                                const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
                assert(local_size <= global_size && "If not, either sequential mode or the local and global were "
                       "provided in the wrong order.");
                
                const PetscInt Nghost = static_cast<PetscInt>(ghost_padding.size());
                
                int error_code = VecCreateGhost(mpi.GetCommunicator(),
                                                static_cast<PetscInt>(local_size),
                                                static_cast<PetscInt>(global_size),
                                                Nghost,
                                                ghost_padding.data(),
                                                &petsc_vector_);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecCreateGhost", invoking_file, invoking_line);
               
                do_petsc_destroy_ = true;
            }
            
            
            void Vector::InitSequentialFromFile(const std::string& file,
                                                const Mpi& mpi,
                                                const char* invoking_file, int invoking_line)
            {
                assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
                assert(mpi.Nprocessor<int>() == 1 && "This method assumes sequential case!");
                 
                std::ifstream stream;
                FilesystemNS::File::Read(stream, file, invoking_file, invoking_line);
                
                std::string line;
                std::vector<double> value_list;
                
                while (getline(stream, line))
                {
                    // All lines are expected to be one value.
                   value_list.push_back(stod(line));
                }
                
                // Now Init the vector with the appropriate size.
                const unsigned int Nvalue = static_cast<unsigned int>(value_list.size());
                
                InitSequentialVector(Nvalue, mpi, invoking_file, invoking_line);
                
                // And fill it with the values.
                AccessVectorContent<Utilities::Access::read_and_write> content(*this, invoking_file, invoking_line);
                
                for (auto i = 0u; i < Nvalue; ++i)
                    content[i] = value_list[static_cast<std::size_t>(i)];
            }
            
            
            void Vector::DuplicateLayout(const Vector& original, const char* invoking_file, int invoking_line)
            {
                assert(petsc_vector_ == PETSC_NULL && "Should not be initialized when this method is called!");
                int error_code = VecDuplicate(original.Internal(), &petsc_vector_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecDuplicate", invoking_file, invoking_line);

                do_petsc_destroy_ = original.do_petsc_destroy_;
            }
            
            
            PetscInt Vector::GetProcessorWiseSize(const char* invoking_file, int invoking_line) const
            {
                PetscInt ret;
                int error_code = VecGetLocalSize(Internal(), &ret);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetLocalSize", invoking_file, invoking_line);
                
                return ret;
            }
            
            
            PetscInt Vector::GetProgramWiseSize(const char* invoking_file, int invoking_line) const
            {
                PetscInt ret;
                int error_code = VecGetSize(Internal(), &ret);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetSize", invoking_file, invoking_line);
                
                return ret;
            }
            
            
            void Vector::ZeroEntries(const char* invoking_file, int invoking_line)
            {
                int error_code = VecZeroEntries(Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecZeroEntries", invoking_file, invoking_line);
            }
            
            
            void Vector::SetValues(const std::vector<PetscInt>& indexing,
                                   const PetscScalar* values,
                                   InsertMode insertOrAppend, const char* invoking_file, int invoking_line)
            {
                int error_code = VecSetValues(Internal(),
                                              static_cast<PetscInt>(indexing.size()),
                                              indexing.data(),
                                              values,
                                              insertOrAppend);
                               
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecSetValues", invoking_file, invoking_line);
            }
            
            
            void Vector::SetValue(PetscInt index, PetscScalar value,
                                  InsertMode insertOrAppend, const char* invoking_file, int invoking_line)
            {
                int error_code = VecSetValue(Internal(), index, value, insertOrAppend);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecSetValue", invoking_file, invoking_line);
            }
            
            
            void Vector::SetUniformValue(PetscScalar value, const char* invoking_file, int invoking_line)
            {
                int error_code = VecSet(Internal(), value);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecSetValue", invoking_file, invoking_line);
            }

            
            std::vector<PetscScalar> Vector::GetValues(const std::vector<PetscInt>& indexing,
                                                       const char* invoking_file, int invoking_line) const
            {
                std::vector<PetscScalar> ret(indexing.size());
                
                int error_code = VecGetValues(Internal(), static_cast<PetscInt>(indexing.size()), indexing.data(), ret.data());
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetValues", invoking_file, invoking_line);
                
                return ret;
            }
       
            void Vector::GetValues(const std::vector<PetscInt>& indexing,
                                   std::vector<PetscScalar>& values,
                                   const char* invoking_file, int invoking_line) const
            {
                assert(values.size() == indexing.size());
                
                int error_code = VecGetValues(Internal(), static_cast<PetscInt>(indexing.size()), indexing.data(), values.data());
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetValues", invoking_file, invoking_line);
            }
            
            PetscScalar Vector::GetValue(PetscInt index,
                                         const char* invoking_file, int invoking_line) const
            {
                std::vector<PetscInt> index_as_vector { index };
                
                const auto ret = GetValues(index_as_vector, invoking_file, invoking_line);
                assert(ret.size() == 1);
                return ret[0];
            }
            
            
            void Vector::Assembly(const char* invoking_file, int invoking_line,
                                  update_ghost do_update_ghost)
            {
                int error_code = VecAssemblyBegin(Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecAssemblyBegin", invoking_file, invoking_line);
                
                
                error_code = VecAssemblyEnd(Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecAssemblyEnd", invoking_file, invoking_line);
                
                UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }
            
            
            void Vector::Copy(const Vector& source, const char* invoking_file, int invoking_line,
                              update_ghost do_update_ghost)
            {
                int error_code = VecCopy(source.Internal(),
                                         Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecCopy", invoking_file, invoking_line);
                
                UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }
            
            
            void Vector::CompleteCopy(const Vector& source, const char* invoking_file, int invoking_line,
                                      update_ghost do_update_ghost)
            {
                DuplicateLayout(source, invoking_file, invoking_line);
                Copy(source, invoking_file, invoking_line, do_update_ghost);
            }
            
            
            void Vector::Scale(PetscScalar a, const char* invoking_file, int invoking_line,
                               update_ghost do_update_ghost)
            {
                int error_code = VecScale(Internal(), a);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecScale", invoking_file, invoking_line);
                
                UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }
            


            void Vector::Shift(PetscScalar a, const char* invoking_file, int invoking_line,
                               update_ghost do_update_ghost)
            {
                int error_code = VecShift(Internal(), a);
                if (error_code)
                throw ExceptionNS::Exception(error_code, "VecShift", invoking_file, invoking_line);
                
                UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }
            
            
            void Vector::UpdateGhosts(const char* invoking_file, int invoking_line)
            {
                #ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
                const auto initial_ghost_values = ExtractGhostValues(*this, invoking_file, invoking_line);
                #endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
                
                
                int error_code = VecGhostUpdateBegin(Internal(), INSERT_VALUES, SCATTER_FORWARD);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGhostUpdateBegin", invoking_file, invoking_line);
                
                error_code = VecGhostUpdateEnd(Internal(), INSERT_VALUES, SCATTER_FORWARD);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGhostUpdateEnd", invoking_file, invoking_line);
                
                
                #ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
                const auto final_ghost_values = ExtractGhostValues(*this, invoking_file, invoking_line);
                
                // To ignore this in sequential runs!
                if (!final_ghost_values.empty())
                {
                    decltype(auto) check_update_ghost_manager =
                        Internal::Wrappers::Petsc::CheckUpdateGhostManager::GetInstance();
                    
                    if (initial_ghost_values != final_ghost_values)
                        check_update_ghost_manager.NeededCall(invoking_file, invoking_line);
                    else
                        check_update_ghost_manager.UnneededCall(invoking_file, invoking_line);

                }
                #endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
            }
            
            
            void DisplaySomeValues(std::ostream& stream, const Vector& vector, PetscInt firstIndex, PetscInt lastIndex,
                                   int rankProc, const char* invoking_file, int invoking_line)
            {
                AccessVectorContent<Utilities::Access::read_only> local_array(vector, invoking_file, invoking_line);
                
                #ifndef NDEBUG
                const PetscInt size = vector.GetProcessorWiseSize(invoking_file, invoking_line);
                assert(local_array.GetSize(__FILE__, __LINE__) == static_cast<unsigned int>(size));

                assert(firstIndex < lastIndex);
                assert(lastIndex < size);
                #endif // NDEBUG
                stream << "On processor " << rankProc << " [";
                auto values = local_array.GetArray();
                
                for (PetscInt i = firstIndex; i <= lastIndex; ++i)
                    stream << values[i] << ", ";
                
                stream << ']' << std::endl;
            }
            
            
            bool AreEqual(const Vector& vec1, const Vector& vec2, const double epsilon, 
                          std::string& inequality_description,
                          const char* invoking_file, int invoking_line)
            {
                inequality_description.clear();
                
                PetscInt size = vec1.GetProcessorWiseSize(invoking_file, invoking_line);
                {
                    #ifndef NDEBUG
                    PetscInt size2 = vec2.GetProcessorWiseSize(invoking_file, invoking_line);
                    assert(size == size2);
                    #endif // NDEBUG
                }
                
                AccessVectorContent<Utilities::Access::read_only> local_array_1(vec1, invoking_file, invoking_line);
                AccessVectorContent<Utilities::Access::read_only> local_array_2(vec2, invoking_file, invoking_line);

                assert(static_cast<PetscInt>(local_array_1.GetSize(__FILE__, __LINE__)) == size);
                assert(static_cast<PetscInt>(local_array_2.GetSize(__FILE__, __LINE__)) == size);                
                
                const PetscScalar* values1 = local_array_1.GetArray();
                const PetscScalar* values2 = local_array_2.GetArray();
                
                bool ret = true;
                
                for (PetscInt index = 0; index < size && ret;)
                {
                    {
                        if (std::fabs(values1[index] - values2[index]) > epsilon)
                        {
                            std::ostringstream oconv;
                            oconv << "Inequality found for index " << index << ": vector 1 displays " << values1[index] << " while "
                            "vector2 displays " << values2[index] << std::endl;
                            inequality_description = oconv.str();
                            ret = false;
                        }
                        else
                            ++index;
                    }
                }
                
                return ret;
            }
            
            
            void AXPY(PetscScalar alpha,
                      const Vector& x,
                      Vector& y,
                      const char* invoking_file, int invoking_line,
                      update_ghost do_update_ghost)
            {
                int error_code = VecAXPY(y.Internal(), alpha, x.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecAXPY", invoking_file, invoking_line);
                
                y.UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }
            
            
            PetscScalar DotProduct(const Vector& x, const Vector& y, const char* invoking_file, int invoking_line)
            {
                PetscScalar ret;
                
                int error_code = VecDot(x.Internal(), y.Internal(), &ret);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecDot", invoking_file, invoking_line);
                
                return ret;
            }
            
                        
            std::pair<PetscInt, PetscReal> Vector::Min(const char* invoking_file, int invoking_line) const
            {
                PetscInt position;
                PetscReal value;
                
                int error_code = VecMin(Internal(), &position, &value);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecMin", invoking_file, invoking_line);
                
                return std::make_pair(position, value);
            }
            
            
            std::pair<PetscInt, PetscReal> Vector::Max(const char* invoking_file, int invoking_line) const
            {
                PetscInt position;
                PetscReal value;
                
                int error_code = VecMax(Internal(), &position, &value);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecMax", invoking_file, invoking_line);
                
                return std::make_pair(position, value);
            }
            
            
            double Vector::Norm(NormType type, const char* invoking_file, int invoking_line) const
            {
                PetscReal norm;
                
                assert(type == NORM_1 || type == NORM_2 || type == NORM_INFINITY);
                
                int error_code = VecNorm(Internal(), type, &norm);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecNorm", invoking_file, invoking_line);
                
                return static_cast<double>(norm);
            }
            
            
            
            void Vector::View(const Mpi& mpi, const char* invoking_file, int invoking_line) const
            {
                int error_code = VecView(Internal(), PETSC_VIEWER_STDOUT_(mpi.GetCommunicator()));
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecView", invoking_file, invoking_line);
            }
            
            
            void Vector::View(const Mpi& mpi, const std::string& output_file,
                              const char* invoking_file, int invoking_line,
                              PetscViewerFormat format) const
            {
                Viewer viewer(mpi, output_file, format, invoking_file, invoking_line);
                
                int error_code = VecView(Internal(), viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecView", invoking_file, invoking_line);
            }
            
            
            void Vector::ViewBinary(const Mpi& mpi,
                                    const std::string& output_file,
                                    const char* invoking_file, int invoking_line) const
            {
                Viewer viewer(mpi, output_file, FILE_MODE_WRITE, invoking_file, invoking_line);
                
                int error_code = VecView(Internal(), viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecView", invoking_file, invoking_line);
                
            }
            
            
            void Vector::Load(const Mpi& mpi, const std::string& input_file, const char* invoking_file, int invoking_line,
                              PetscViewerFormat format) const
            {
                Viewer viewer(mpi, input_file, format, invoking_file, invoking_line);
                
                int error_code = VecLoad(Internal(), viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecLoad", invoking_file, invoking_line);
            }
            
            
            void GatherVector(const Mpi& mpi,
                              const Wrappers::Petsc::Vector& local_parallel_vector,
                              Wrappers::Petsc::Vector& sequential_vector,
                              const char* invoking_file, int invoking_line)
            {
                if (mpi.Nprocessor<int>() == 1)
                {
                    sequential_vector.Copy(local_parallel_vector, __FILE__, __LINE__);
                    
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] Calling GatherVector in sequential is not "
                                                                  "efficient as a Copy is done in the end.\n",
                                                                  mpi, __FILE__, __LINE__);
                }
                else
                {
                    
                    VecScatter vecscat;
                    
                    Vec local_petsc_vector = local_parallel_vector.Internal();
                    Vec sequential_petsc_vector;
                    
                    int error_code = VecScatterCreateToAll(local_petsc_vector, &vecscat, &sequential_petsc_vector);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "VecScatterCreateToAll", invoking_file, invoking_line);
                    
                    error_code = VecScatterBegin(vecscat, local_petsc_vector, sequential_petsc_vector, INSERT_VALUES,
                                                 SCATTER_FORWARD);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "VecScatterBegin", invoking_file, invoking_line);
                    
                    error_code = VecScatterEnd(vecscat, local_petsc_vector, sequential_petsc_vector, INSERT_VALUES,
                                               SCATTER_FORWARD);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "VecScatterEnd", invoking_file, invoking_line);
                    
                    error_code = VecScatterDestroy(&vecscat);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "VecScatterDestroy", invoking_file, invoking_line);
                    
                    sequential_vector.SetFromPetscVec(sequential_petsc_vector, __FILE__, __LINE__);
                }
            }
            
            
            void Vector::SetDoNotDestroyPetscVector()
            {
                do_petsc_destroy_ = false;
            }
            
            
            void Vector::SetFromPetscVec(const Vec& petsc_vector,
                                         const char* invoking_file, int invoking_line)
            {
                // In this specific method alone I can't use syntax sugary provided by the class, as I manipulate
                // a raw Petsc Vec such as the one provided in arguments of SnesFunction.
                const PetscScalar* values;
                int error_code = VecGetArrayRead(petsc_vector, &values);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGetArrayRead", invoking_file, invoking_line);
                
                const auto size = static_cast<std::size_t>(GetProcessorWiseSize(invoking_file, invoking_line));
                
                #ifndef NDEBUG
                {
                    PetscInt petsc_size;
                    error_code = VecGetLocalSize(petsc_vector, &petsc_size);
                
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "VecGetLocalSize", invoking_file, invoking_line);
                    
                    assert(size == static_cast<std::size_t>(petsc_size));
                }
                #endif // NDEBUG
                
                AccessVectorContent<Utilities::Access::read_and_write> content(*this, invoking_file, invoking_line);
                
                for (auto i = 0ul; i < size; ++i)
                    content[static_cast<unsigned int>(i)] = values[i];

                
                error_code = VecRestoreArrayRead(petsc_vector, &values);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecRestoreArrayRead", invoking_file, invoking_line);
            }
            
            
            #ifdef MOREFEM_CHECK_NAN_AND_INF
            void CheckNumericValues(const Mpi& mpi,
                                     const std::string& vector_name,
                                     const Vector& vector,
                                     const char* invoking_file, int invoking_line)
            {
                AccessVectorContent<Utilities::Access::read_only> content(vector, invoking_file, invoking_line);
                
                const auto size = content.GetSize(invoking_file, invoking_line);

                for (unsigned int i = 0u; i < size; ++i)
                {
                    const auto current_value = content.GetValue(i);
                    
                    if (std::isnan(current_value))
                    {
                        std::ostringstream oconv;
                        oconv << "Nan value found in vector " << vector_name << " called on " << invoking_file << ", line "
                        << invoking_line << '.';
                        
                        ::MoReFEM::ExceptionNS::PrintAndAbort(mpi, oconv.str());
                    }
                    
                    if (std::isinf(current_value))
                    {
                        std::ostringstream oconv;
                        oconv << "Inf value found in vector " << vector_name << " called on " << invoking_file << ", line "
                        << invoking_line << '.';
                        ::MoReFEM::ExceptionNS::PrintAndAbort(mpi, oconv.str());
                    }

                }
            }
            #endif // MOREFEM_CHECK_NAN_AND_INF

            #ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
            
            
            namespace // anonymous
            {
                
                
                decltype(auto) mark = Internal::Wrappers::Petsc::CheckUpdateGhostManager::CreateOrGetInstance();
                
                
                std::vector<double> ExtractGhostValues(const Vector& vector,
                                                       const char* invoking_file, int invoking_line)
                {
                    # ifdef NDEBUG
                    std::cout << "MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE should only be defined in debug mode!" << std::endl;
                    exit(EXIT_FAILURE);
                    # endif // NDEBUG
                    
                    Wrappers::Petsc::AccessGhostContent access_ghost_content(vector, invoking_file, invoking_line);
                    
                    const auto Nitem_on_proc = vector.GetProcessorWiseSize(invoking_file, invoking_line);
                    
                    const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();
                    
                    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
                    vector_with_ghost_content(vector_with_ghost, __FILE__, __LINE__);
                    
                    const auto size = static_cast<int>(vector_with_ghost_content.GetSize(invoking_file, invoking_line));
                    assert(size >= Nitem_on_proc);
                    std::vector<double> ghost_values;
                    const auto Nghost = static_cast<std::size_t>(size - Nitem_on_proc);
                    
                    ghost_values.reserve(Nghost);
                    
                    for (auto i = Nitem_on_proc; i < size; ++i)
                        ghost_values.push_back(vector_with_ghost_content.GetValue(static_cast<unsigned int>(i)));
                    
                    assert(ghost_values.size() == Nghost);
                    
                    return ghost_values;
                }
                
                
            } // namespace anonymous
            
            
            #endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
            
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup

