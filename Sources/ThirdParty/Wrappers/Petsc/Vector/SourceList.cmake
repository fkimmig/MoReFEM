target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vector.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AccessGhostContent.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AccessVectorContent.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
