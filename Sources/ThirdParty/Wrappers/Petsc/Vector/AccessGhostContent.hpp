///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 12 Jun 2015 09:53:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HPP_

# include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
# include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {



            /*!
             * \brief The purpose of this class is to enable access to the whole content of the vector, including its ghost.
             *
             * It is really a RAII around Petsc's VecGhostGetLocalForm/VecGhostRestoreLocalForm.
             *
             * The call should be as follow:
             *
             * \code
             *
             *  Vector vector(...); // the vector we want to probe.
             *
             *  { // important: ensure destructor called as soon as possible!
             *      Wrappers::Petsc::AccessGhostContent access_ghost_content(vector, __FILE__, __LINE__);
             *
             *      const Vector& vector_with_ghost = access_ghost_content.GetVectorWithGhost(); // need to be separated
             *                                                                                   // from following line;
             *                                                                                   // don't merge them!
             *
             *      Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
             *          vector_with_ghost_content(vector_with_ghost, __FILE__, __LINE__);
             *
             *      // Hereafter whatever operation for which you needed the content, for instance:
             *      for (auto global_index : local_2_global)
                        ret.push_back(ghost_vector_content.GetValue(static_cast<unsigned int>(global_index)));
             * }
             *
             * \endcode
             *
             */
            class AccessGhostContent
            {

            public:

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] vector Vector for which local access is granted.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 *
                 */
                explicit AccessGhostContent(const Vector& vector, const char* invoking_file, int invoking_line);

                //! Destructor
                ~AccessGhostContent();

                /*!
                 * \brief Return an extended Vector object for which it is possible to access ghost data.
                 */
                Vector GetVectorWithGhost() const;


            private:

                //! Local vector given in input.
                const Vector& vector_without_ghost_;

                //! Local vector with ghost values added.
                Vec petsc_vector_with_ghost_;
            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_ACCESS_GHOST_CONTENT_HPP_
