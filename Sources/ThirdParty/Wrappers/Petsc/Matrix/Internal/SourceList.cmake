target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BaseMatrix.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/BaseMatrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonZeroPattern.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonZeroPattern.hxx" / 
)

