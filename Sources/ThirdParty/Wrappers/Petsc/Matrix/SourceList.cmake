target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Matrix.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Matrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Matrix.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonZeroPattern.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ShellMatrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ShellMatrix.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
