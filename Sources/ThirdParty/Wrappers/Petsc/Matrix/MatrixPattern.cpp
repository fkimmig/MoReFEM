///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Jan 2014 14:15:48 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include <cassert>

#include "Utilities/Numeric/Numeric.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"


namespace MoReFEM
{
   
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            MatrixPattern::MatrixPattern(const std::vector<std::vector<PetscInt>>& non_zero_slots_per_local_row)
            {
                iCSR_.reserve(non_zero_slots_per_local_row.size() + 1);
                
                iCSR_.push_back(0);
                std::size_t iCSR_index = 0;
                
                for (const auto& row : non_zero_slots_per_local_row)
                {
                    iCSR_index += row.size();
                    iCSR_.push_back(static_cast<PetscInt>(iCSR_index));
                    std::move(row.begin(), row.end(), std::back_inserter(jCSR_));
                }
            }
                
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup

