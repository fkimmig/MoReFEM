///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HPP_

# include <memory>

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"

# include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
# include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Mumps.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Gmres.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Instantiations/Umfpack.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"

# include "Utilities/TimeKeep/TimeKeep.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            //! Whether the Petsc solver should print informations about the solve in progress on standard output.
            enum class print_solver_infos { yes, no };

            //! Whether the nonlinear solver should check the convergence.
            enum class check_convergence { yes, no };

            /*!
             * \class doxygen_hide_print_solver_infos_arg
             *
             * \param[in] do_print_solver_infos Whether the Petsc solver should print informations about the solve in
             * progress on standard output.
             */



            /*!
             * \brief This class wraps all the Petsc objects related to solver.
             *
             * So it should be used for SNES, KSP, PC.
             *
             * \internal <b><tt>[internal]</tt></b> In Petsc KSP and PC can be defined independantly or derive from SNES; I chose
             * the latter for the current implementation.
             */

            class Snes final
            {
            public:

                //! Unique smart pointer.
                using unique_ptr = std::unique_ptr<Snes>;

                //! Alias for the type of function that can define the viewer.
                using SNESViewer = PetscErrorCode(*)(SNES, PetscInt, PetscReal, void*);

                //! Alias for SNESFunction (which seemed to have been deprecated in Petsc 3.4...).
                using SNESFunction = PetscErrorCode(*)(SNES, Vec, Vec, void*);

                //! Alias for SNESJacobian (which seemed to have been deprecated in Petsc 3.4...).
                using SNESJacobian = PetscErrorCode(*)(SNES, Vec, Mat, Mat, void*);

                //! Alias for SNESConvergenceTest.
                using SNESConvergenceTestFunction = PetscErrorCode(*)(SNES, PetscInt, PetscReal, PetscReal,
                                                                      PetscReal, SNESConvergedReason*, void*);


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                //! Friendship to helper class.
                // \todo #730 I don't like having to put the list here; improve that!
                friend Instantiations::Mumps;
                friend Instantiations::Gmres;
                friend Instantiations::Umfpack;


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            public:


                /// \name Constructors and destructor.
                ///@{

                /*!
                 * \brief Constructor.
                 */
                Snes(const Mpi& mpi,
                     const std::string& solver,
                     const std::string& preconditioner,
                     unsigned int gmres_restart,
                     double absolute_tolerance,
                     double relative_tolerance,
                     const double step_size_tolerance,
                     unsigned int max_iteration,
                     SNESFunction snes_function,
                     SNESJacobian snes_jacobian,
                     SNESViewer snes_viewer,
                     SNESConvergenceTestFunction snes_convergence_test_function,
                     const char* invoking_file, int invoking_line);


                //! Destructor.
                ~Snes();

                //! Disable copy constructor.
                Snes(const Snes&) = delete;

                //! Disable move constructor.
                Snes(Snes&&) = delete;

                //! Disable copy assignation operator.
                Snes& operator=(const Snes&) = delete;

                //! Disable move assignation operator.
                Snes& operator=(Snes&&) = delete;

                ///@}


                //! Petsc's matrix structure (SAME_NONZERO_PATTERN, SAME_PRECONDITIONER or DIFFERENT_NONZERO_PATTERN).
                MatStructure Structure() const;

                /*!
                 * \brief Solve a non linear problem with Petsc's Snes algorithm.
                 *
                 * \param[in] context Petsc functions provide as last argument of their prototype a void* argument,
                 * which the developer may cat to whatever he wishes. For instance in some VariationalFormulation(s)
                 * such as hyperelastic case there calls to this function; this void* is used to pass the \a this
                 * pointer, thus allowing access to the VariationalFormulation object.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[in] rhs Right hand side.
                 * \param[in] jacobian_matrix Jacobian matrix.
                 * \param[in] preconditioner_matrix Preconditioner matrix.
                 * \param[in,out] solution Solution of the solver. Must be properly allocated before this method call.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[in] do_check_convergence Check the convergence if needed. If it does not converge throw an error.
                 */
                void SolveNonLinear(void* context,
                                    const Vector& rhs,
                                    const Matrix& jacobian_matrix,
                                    const Matrix& preconditioner_matrix,
                                    Vector& solution,
                                    const char* invoking_file, int invoking_line,
                                    check_convergence do_check_convergence = check_convergence::yes);



                /*!
                 * \brief Solve a linear problem with KSP algorithm.
                 *
                 * \internal <b><tt>[internal]</tt></b> Currently only one matrix is given, and is used also as preconditioner. However
                 * Petsc is broader than that; if some day we need to differentiate an overload of KSPSolve
                 * that takes this additional argument can be written.
                 *
                 * \param[in] matrix Matrix of the problem to solve. If matrix hasn't changed since the
                 * previous solve (for instance the case of the elastic problem in which same matrix is used for every
                 * time iteration) you should use the overload without the \a matrix parameter.
                 * \param[in] preconditioner_matrix Preconditioner matrix.
                 * \param[in] rhs Rhs.
                 * \param[in,out] solution It is expected the vector's structure is already correctly initialized
                 * in input.
                 * \copydoc doxygen_hide_print_solver_infos_arg
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 */
                template<class MatrixT>
                std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
                SolveLinear(const MatrixT& matrix,
                            const MatrixT& preconditioner_matrix,
                            const Vector& rhs,
                            Vector& solution,
                            const char* invoking_file, int invoking_line,
                            print_solver_infos do_print_solver_infos = print_solver_infos::yes);


                /*!
                 * \brief Same as above when the matrix has already been set in a previous solved.
                 *
                 * In this case the factoring isn't needlessly recomputed; the last preconditioner is used again.
                 *
                 * \param[in] rhs Rhs.
                 * \param[in,out] solution It is expected the vector's structure is already correctly initialized
                 * in input.
                 * \copydoc doxygen_hide_print_solver_infos_arg
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void SolveLinear(const Vector& rhs,
                                 Vector& solution,
                                 const char* invoking_file, int invoking_line,
                                 print_solver_infos do_print_solver_infos = print_solver_infos::yes);


                /*!
                 * \brief Return the current iteration number (in Newton algorithm).
                 *
                 * \internal <b><tt>[internal]</tt></b> This is a wrapper over SNESGetLinearSolveIterations and not over SNESGetIterationNumber:
                 * the latter wasn't able to distinguish between the very first iteration and the next one
                 * (see the illustration in the example below).
                 *
                 * SNESGetIterationNumber -> 0	SNESGetLinearSolveIterations -> 0
                 * 0 SNES Function Norm is 8.247142112306e-04 and extrema are -2.107849e-06 and 1.421455e-05
                 * SNESGetIterationNumber -> 0	SNESGetLinearSolveIterations -> 1
                 * 1 SNES Function Norm is 5.390047267101e-09 and extrema are -8.618204e-07 and 4.890128e-06
                 * SNESGetIterationNumber -> 1	SNESGetLinearSolveIterations -> 2
                 * 2 SNES Function Norm is 2.416183247353e-11 and extrema are -8.618198e-07 and 4.890128e-06
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Index of the current Snes iteration.
                 */
                unsigned int GetSnesIteration(const char* invoking_file, int invoking_line) const;

                //! Accessor to the reason why the snes converged. Might be nullptr.
                SNESConvergedReason GetSnesConvergenceReason(const char* invoking_file, int invoking_line) const;

                //! Accessor to the type of the SNES.
                std::string GetSnesType(const char* invoking_file, int invoking_line) const;

                /*!
                 * \class doxygen_hide_convergence_reason_without_explanation
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Current status of convergence.
                 *
                 * See overload of current method if you seek details about convergence status (e.g. converged due
                 * to absolute tolerance reached, for instance).
                 */

                /*!
                 * \class doxygen_hide_convergence_reason_with_explanation
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \param[out] explanation Text that explains in few words reason of convergence or divergence.
                 *
                 * \return Current status of convergence.
                 */


                /*!
                 * \brief For non linear solver, get informations about convergence.
                 *
                 * \copydoc doxygen_hide_convergence_reason_without_explanation
                 *
                 */
                convergence_status GetNonLinearConvergenceReason(const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief For non linear solver, get informations about convergence.
                 *
                 * \copydoc doxygen_hide_convergence_reason_with_explanation
                 */
                convergence_status GetNonLinearConvergenceReason(std::string& explanation,
                                                                 const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief For linear solver, get informations about convergence.
                 *
                 * \copydoc doxygen_hide_convergence_reason_without_explanation
                 *
                 */
                convergence_status GetLinearConvergenceReason(const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief For linear solver, get informations about convergence.
                 *
                 * \copydoc doxygen_hide_convergence_reason_with_explanation
                 */
                convergence_status GetLinearConvergenceReason(std::string& explanation,
                                                              const char* invoking_file, int invoking_line) const;

                /*!
                 * \brief Returns the absolute tolerance.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Absolute tolerance.
                 */
                double GetAbsoluteTolerance(const char* invoking_file, int invoking_line) const;

                /*!
                 * \brief Returns the relative tolerance.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Relative tolerance.
                 */
                double GetRelativeTolerance(const char* invoking_file, int invoking_line) const;

                /*!
                 * \brief Returns the maximum number of iterations allowed.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Maximum number of iterations allowed.
                 */
                unsigned int NmaxIteration(const char* invoking_file, int invoking_line) const;



            public:

                //! Access to mpi.
                const Mpi& GetMpi() const;

            private:

                /*!
                 * \brief Get the underlying KSP.
                 *
                 * \return KSP Petsc object (actually a pointer to a Petsc internal class).
                 *
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 *
                 * \internal <b><tt>[internal]</tt></b> There is currently no Ksp class in the wrapper; it is currently intended that a
                 * Snes is used in all cases and if no Newton is truly required the underlying KSP of Snes is
                 * used (Petsc is very hierarchical: Snes then Ksp then PC...).
                 */
                KSP GetKsp(const char* invoking_file, int invoking_line) const;

                //! Likewise for PC.
                PC GetPreconditioner(const char* invoking_file, int invoking_line) const;

                //! Return the type of KSP as a string.
                std::string GetKspType(const char* invoking_file, int invoking_line) const;

                //! Return the type of PC as a string.
                std::string GetPreconditionerType(const char* invoking_file, int invoking_line) const;

                /*!
                 * \brief Access to the Snes function required by non linear Snes solver.
                 *
                 * BEWARE: snes_function_ might have been initialised as nullptr (for instance for purely
                 * linear problems in which author didn't bother to define non-linear related functions)
                 * but the function below assumes it is not such a case. An assert will be issued in case of
                 * an illegal call.
                 *
                 * \return Petsc \a SNESFunction object (or nullptr if only linear case considered).
                 */
                SNESFunction GetSnesFunction() const;

                /*!
                 * \brief Access to the Snes jacobian function required by non linear Snes solver.
                 *
                 * BEWARE: snes_jacobian_ might have been initialised as nullptr (for instance for purely
                 * linear problems in which author didn't bother to define non-linear related functions)
                 * but the function below assumes it is not such a case. An assert will be issued in case of
                 * an illegal call.
                 *
                 * \return Petsc \a SNESJacobian object (or nullptr if only linear case considered).
                 */
                SNESJacobian GetSnesJacobian() const;

                /*!
                 * \brief Access to the Snes viewer function required by non linear Snes solver.
                 *
                 * BEWARE: snes_viewer_ might have been initialised as nullptr (for instance for purely
                 * linear problems in which author didn't bother to define non-linear related functions)
                 * but the function below assumes it is not such a case. An assert will be issued in case of
                 * an illegal call.
                 *
                 * \return Petsc \a SNESViewer object (or nullptr if only linear case considered).
                 */
                SNESViewer GetSnesViewer() const;

                /*!
                 * \brief Access to the Snes test function required by non linear Snes solver.
                 *
                 * BEWARE: snes_convergence_test_function_ might have been initialised as nullptr (for instance for purely
                 * linear problems in which author didn't bother to define non-linear related functions)
                 * but the function below assumes it is not such a case. An assert will be issued in case of
                 * an illegal call.
                 *
                 * \return Petsc \a SNESConvergenceTestFunction object (or nullptr if only linear case considered).
                 */
                SNESConvergenceTestFunction GetSnesConvergenceTestFunction() const;


            private:

                /*!
                 * \brief An helper method which will seek informations in internal static class about convergence
                 * or divergence attained by non linear solve.
                 *
                 * \tparam SNESConvergedReasonT What was given by Petsc's SNESGetConvergedReason() function.
                 *
                 * \return Current status of convergence.
                 */
                template<SNESConvergedReason SNESConvergedReasonT>
                convergence_status GetNonLinearConvergenceReasonHelper() const noexcept;

                /*!
                 * \brief An helper method which will seek informations in internal static class about convergence
                 * or divergence attained by linear solve.
                 *
                 * \tparam KSPConvergedReasonT What was given by Petsc's KSPGetConvergedReason() function.
                 *
                 * \return Current status of convergence.
                 */
                template<KSPConvergedReason KSPConvergedReasonT>
                convergence_status GetLinearConvergenceReasonHelper() const noexcept;


            private:

                //! Access to the object that holds informations specific to the solver chosen.
                const Internal::Wrappers::Petsc::Solver& GetSolver() const noexcept;

                //! Non constant access to the object that holds informations specific to the solver chosen.
                Internal::Wrappers::Petsc::Solver& GetNonCstSolver() noexcept;

            private:

                //! Petsc's underlying object.
                SNES snes_;

                //! Mpi.
                const Mpi& mpi_;

                //! Snes viewer function. Might be nullptr for linear solve.
                SNESFunction snes_function_;

                //! Snes viewer function. Might be nullptr for linear solve.
                SNESJacobian snes_jacobian_;

                //! Snes viewer function. Might be nullptr for linear solve.
                SNESViewer snes_viewer_;

                //! Snes convergence test function. Might be nullptr for linear solve.
                SNESConvergenceTestFunction snes_convergence_test_function_;

                //! Object that holds informations specific to the solver chosen.
                Internal::Wrappers::Petsc::Solver::unique_ptr solver_ = nullptr;

                /*!
                 * \brief Pointer to a string that explains in detail convergence reason.
                 *
                 * This is truly an internal data attribute, to be used only by Get[Non]LinearConvergenceReason() method.
                 *
                 * \internal Do not delete this pointer: it is either unused or contain address of a static string.
                 */
                mutable const std::string* convergence_reason_;

            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_SNES_HPP_
