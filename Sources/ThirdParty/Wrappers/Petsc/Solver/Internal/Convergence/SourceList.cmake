target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/KspConvergenceReason.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SnesConvergenceReason.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/KspConvergenceReason.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/KspConvergenceReason.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SnesConvergenceReason.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SnesConvergenceReason.hxx" / 
)

