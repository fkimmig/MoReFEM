target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Solver.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Solver.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Convergence/SourceList.cmake)
