target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.hxx" / 
)

