target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Petsc.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Print.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Viewer.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Petsc.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Print.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Print.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Viewer.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Solver/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Vector/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Matrix/SourceList.cmake)
