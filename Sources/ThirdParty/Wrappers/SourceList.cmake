target_sources(${MOREFEM_UTILITIES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/Mpi/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Parmetis/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Seldon/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Petsc/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Libmesh/SourceList.cmake)
