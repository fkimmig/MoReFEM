target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Parmetis.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Parmetis.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
