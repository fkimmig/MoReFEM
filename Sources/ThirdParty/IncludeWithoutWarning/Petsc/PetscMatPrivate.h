//! \file 
//
//
//  Petsc.hpp
//
//  Created by Sébastien Gilles on 11/10/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MAT_PRIVATE_H_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MAT_PRIVATE_H_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wcomma")
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
# endif // __clang__

#ifdef MOREFEM_GCC
    PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")
#endif // MOREFEM_GCC

#include "petsc/private/matimpl.h" // to get around MatGetRow() limitations.

PRAGMA_DIAGNOSTIC(pop)


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MAT_PRIVATE_H_
