target_sources(${MOREFEM_UTILITIES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/PetscMat.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PetscSnes.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PetscSys.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PetscVec.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PetscViewer.hpp" / 
)

