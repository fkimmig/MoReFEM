target_sources(${MOREFEM_UTILITIES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/Wrappers/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Scripts/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/IncludeWithoutWarning/SourceList.cmake)
