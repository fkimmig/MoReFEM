target_sources(${MOREFEM_OP}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVariationalOperator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVariationalOperator.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Helper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Helper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperatorIterator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperatorIterator.hxx" / 
)

