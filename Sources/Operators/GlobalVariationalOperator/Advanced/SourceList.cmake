target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DetermineExtendedUnknownList.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DetermineExtendedUnknownList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DetermineExtendedUnknownList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalOperatorTupleItem.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalOperatorTupleItem.hxx" / 
)

