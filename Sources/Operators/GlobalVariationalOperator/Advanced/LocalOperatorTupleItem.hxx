///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:02:25 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HXX_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace GlobalVariationalOperatorNS
        {


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT,
                class LocalVariationalOperatorPtrT
            >
            constexpr Advanced::GeometricEltEnum
            LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>
            ::RefGeomEltType()
            {
                return RefGeomEltTypeT;
            }


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT,
                class LocalVariationalOperatorPtrT
            >
            LocalVariationalOperatorPtrT& LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>
            ::GetNonCstPointer() noexcept
            {
                return local_operator_;
            }


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT,
                class LocalVariationalOperatorPtrT
            >
            bool LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>
            ::IsRelevant() const noexcept
            {
                return local_operator_ != nullptr;
            }


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT,
                class LocalVariationalOperatorPtrT
            >
            typename LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::local_operator_type&
            LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>
            ::GetNonCstLocalOperator() const noexcept
            {
                assert(!(!local_operator_));
                return *local_operator_;
            }


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT,
                class LocalVariationalOperatorPtrT
            >
            constexpr Advanced::OperatorNS::Nature LocalOperatorTupleItem<RefGeomEltTypeT, LocalVariationalOperatorPtrT>
            ::OperatorNature()
            {
                return local_operator_type::OperatorNature();
            }


            template
            <
                Advanced::GeometricEltEnum RefGeomEltTypeT
            >
            constexpr Advanced::GeometricEltEnum
            LocalOperatorTupleItem<RefGeomEltTypeT, std::nullptr_t>
            ::RefGeomEltType()
            {
                return RefGeomEltTypeT;
            }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_TUPLE_ITEM_HXX_
