target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractLocalDofValues.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVariationalOperator.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
