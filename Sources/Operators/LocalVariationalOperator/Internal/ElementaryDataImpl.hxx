///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:56:52 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            inline const Internal::RefFEltNS::RefLocalFEltSpace& ElementaryDataImpl::GetRefLocalFEltSpace() const noexcept
            {
                return ref_felt_space_;
            }


            inline const RefGeomElt& ElementaryDataImpl::GetRefGeomElt() const noexcept
            {
                return GetRefLocalFEltSpace().GetRefGeomElt();
            }


            inline unsigned int ElementaryDataImpl::NnodeRow() const noexcept
            {
                return Nnode_row_;
            }


            inline unsigned int ElementaryDataImpl::NdofRow() const noexcept
            {
                return Ndof_row_;
            }

            inline unsigned int ElementaryDataImpl::NnodeCol() const noexcept
            {
                return Nnode_col_;
            }


            inline unsigned int ElementaryDataImpl::NdofCol() const noexcept
            {
                return Ndof_col_;
            }


            inline unsigned int ElementaryDataImpl::GetGeomEltDimension() const noexcept
            {
                return geom_elt_dimension_;
            }


            inline unsigned int ElementaryDataImpl::NquadraturePoint() const noexcept
            {
                return GetQuadratureRule().NquadraturePoint();
            }


            inline unsigned int ElementaryDataImpl::GetFEltSpaceDimension() const noexcept
            {
                return felt_space_dimension_;
            }


            inline unsigned int ElementaryDataImpl::GetMeshDimension() const noexcept
            {
                return geometric_mesh_region_dimension_;
            }


            inline const LocalFEltSpace& ElementaryDataImpl::GetCurrentLocalFEltSpace() const noexcept
            {
                assert(!(!current_local_felt_space_));
                return *current_local_felt_space_;
            }


            inline const GeometricElt& ElementaryDataImpl::GetCurrentGeomElt() const noexcept
            {
                assert(!(!current_local_felt_space_));
                return current_local_felt_space_->GetGeometricElt();
            }


            inline unsigned int ElementaryDataImpl::NnodeInRefGeomElt() const noexcept
            {
                return Ncoords_in_geom_ref_elt_;
            }


            inline unsigned int ElementaryDataImpl::Nunknown() const noexcept
            {
                return static_cast<unsigned int>(ref_felt_list_.size());
            }

            inline unsigned int ElementaryDataImpl::NtestUnknown() const noexcept
            {
                return static_cast<unsigned int>(test_ref_felt_list_.size());
            }


            inline const std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& ElementaryDataImpl
            ::GetInformationsAtQuadraturePointListForUnknown() const noexcept
            {
                assert(!infos_at_quad_pt_list_for_unknown_.empty());
                return infos_at_quad_pt_list_for_unknown_;
            }


            inline std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& ElementaryDataImpl
            ::GetNonCstInformationsAtQuadraturePointListForUnknown()
            {
                return const_cast<std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>&>(GetInformationsAtQuadraturePointListForUnknown());
            }


            inline const std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& ElementaryDataImpl
            ::GetInformationsAtQuadraturePointListForTestUnknown() const noexcept
            {
                assert(!infos_at_quad_pt_list_for_test_unknown_.empty());
                return infos_at_quad_pt_list_for_test_unknown_;
            }


            inline std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>& ElementaryDataImpl
            ::GetNonCstInformationsAtQuadraturePointListForTestUnknown()
            {
                return const_cast<std::vector<Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint>&>(GetInformationsAtQuadraturePointListForTestUnknown());
            }


            inline const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& ElementaryDataImpl
            ::GetInformationsAtQuadraturePointForUnknown(unsigned int quadrature_pt_index) const noexcept
            {
                assert(quadrature_pt_index < infos_at_quad_pt_list_for_unknown_.size());
                return infos_at_quad_pt_list_for_unknown_[quadrature_pt_index];
            }


            inline const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& ElementaryDataImpl
            ::GetInformationsAtQuadraturePointForTestUnknown(unsigned int quadrature_pt_index) const noexcept
            {
                assert(quadrature_pt_index < infos_at_quad_pt_list_for_test_unknown_.size());
                return infos_at_quad_pt_list_for_test_unknown_[quadrature_pt_index];
            }


            inline const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ElementaryDataImpl
            ::GetRefFEltList() const noexcept
            {
                return ref_felt_list_;
            }


            inline const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ElementaryDataImpl
            ::GetTestRefFEltList() const noexcept
            {
                return test_ref_felt_list_;
            }


            inline void ElementaryDataImpl::SetCurrentLocalFEltSpace(const LocalFEltSpace* local_felt_space)
            {
                current_local_felt_space_ = local_felt_space;
            }


            inline const QuadratureRule& ElementaryDataImpl::GetQuadratureRule() const noexcept
            {
                return quadrature_rule_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ELEMENTARY_DATA_IMPL_HXX_
