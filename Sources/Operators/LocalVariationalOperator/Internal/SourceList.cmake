target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperator.hxx" / 
)

