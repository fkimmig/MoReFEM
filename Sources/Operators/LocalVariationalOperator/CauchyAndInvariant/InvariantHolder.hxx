///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

//  Invariants.hxx
//
//  Created by Sebastien Gilles on 3/20/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_


namespace MoReFEM
{

    namespace // anonymous
    {

        template<unsigned int NinvariantsT, unsigned int DimensionT>
        void UpdateHelper(const LocalVector& cauchy_green_tensor,
                          const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          InvariantHolderNS::Content content,
                          std::array<double, NinvariantsT>& invariants,
                          std::array<LocalVector, NinvariantsT>& invariants_first_derivative,
                          std::array<LocalMatrix, 2>& invariants_second_derivative,
                          bool do_i4_activate,
                          const FiberList<ParameterNS::Type::vector>* fibers);


    } // namespace anonymous



    template<unsigned int NinvariantsT>
    InvariantHolder<NinvariantsT>::InvariantHolder(unsigned int mesh_dimension,
                                                   InvariantHolderNS::Content content)
    : content_(content),
    mesh_dimension_(mesh_dimension)
    {
        int size = 0;

        assert((mesh_dimension >= 1 && mesh_dimension <= 3) && "Invariants can only be created in dimension 1, 2 or 3.");

        switch (mesh_dimension)
        {
            case 1:
                size = 1;
                break;
            case 2:
                size = 3;
                break;
            case 3:
                size = 6;
                break;
        }

        switch (content)
        {
            case InvariantHolderNS::Content::invariants_and_first_and_second_deriv:
                static_assert(std::tuple_size<decltype(invariants_second_derivative_)>::value == 2ul);
                for (unsigned int i = 0u; i < 2u ; ++i)
                {
                    invariants_second_derivative_[i].Resize(size, size);
                }
                [[fallthrough]];
            case InvariantHolderNS::Content::invariants_and_first_deriv:
                static_assert(std::tuple_size<decltype(invariants_first_derivative_)>::value == NinvariantsT);
                for (unsigned int i = 0u; i < NinvariantsT ; ++i)
                {
                    invariants_first_derivative_[i].Resize(size);
                }
                [[fallthrough]];
            case InvariantHolderNS::Content::invariants:
                break;
        }
    }


    template<unsigned int NinvariantsT>
    void InvariantHolder<NinvariantsT>::Update(const LocalVector& cauchy_green_tensor,
                                               const QuadraturePoint& quad_pt,
                                               const GeometricElt& geom_elt)
    {
        const auto mesh_dimension = GetMeshDimension();
        const auto content = GetContent();
        auto& invariants = GetNonCstInvariant();
        auto& invariants_first_derivative = GetNonCstFirstDerivativeWrtCauchyGreen();
        auto& invariants_second_derivative = GetNonCstSecondDerivativeWrtCauchyGreen();

        switch(mesh_dimension)
        {
            case 1u:
                UpdateHelper<NinvariantsT, 1u>(cauchy_green_tensor,
                                               quad_pt,
                                               geom_elt,
                                               content,
                                               invariants, invariants_first_derivative, invariants_second_derivative,
                                               DoI4Activate(),
                                               GetFibers());
                break;
            case 2u:
                UpdateHelper<NinvariantsT, 2u>(cauchy_green_tensor,
                                               quad_pt,
                                               geom_elt,
                                               content,
                                               invariants, invariants_first_derivative, invariants_second_derivative,
                                               DoI4Activate(),
                                               GetFibers());
                break;
            case 3u:
                UpdateHelper<NinvariantsT, 3u>(cauchy_green_tensor,
                                               quad_pt,
                                               geom_elt,
                                               content,
                                               invariants, invariants_first_derivative, invariants_second_derivative,
                                               DoI4Activate(),
                                               GetFibers());
                break;
            default:
            {
                assert(false && "Invariants computation is available only for dimensions 2 and 3.");
                exit(EXIT_FAILURE);
            }
        }

#ifndef NDEBUG
        are_invariant_set_ = true;
#endif // NDEBUG
    }


    namespace // anonymous
    {

        template<unsigned int NinvariantsT, unsigned int DimensionT>
        void UpdateHelper(const LocalVector& cauchy_green_tensor,
                          const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          InvariantHolderNS::Content content,
                          std::array<double, NinvariantsT>& invariants,
                          std::array<LocalVector, NinvariantsT>& invariants_first_derivative,
                          std::array<LocalMatrix, 2>& invariants_second_derivative,
                          bool do_i4_activate,
                          const FiberList<ParameterNS::Type::vector>* fibers)
        {
            using namespace InvariantHolderNS;

            switch(content)
            {
                case Content::invariants_and_first_and_second_deriv:
                    SecondDerivativeInvariant2CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                      invariants_second_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_second_derivative_index::d2I2dCdC)]);
                    SecondDerivativeInvariant3CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                      invariants_second_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_second_derivative_index::d2I3dCdC)]);
                    [[fallthrough]];
                case Content::invariants_and_first_deriv:
                    FirstDerivativeInvariant1CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                     invariants_first_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_first_derivative_index::dI1dC)]);
                    FirstDerivativeInvariant2CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                     invariants_first_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_first_derivative_index::dI2dC)]);
                    FirstDerivativeInvariant3CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                     invariants_first_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_first_derivative_index::dI3dC)]);

                    if (do_i4_activate)
                    {
                        const auto& tau_interpolate_at_quad_point = fibers->GetValue(quad_pt, geom_elt);

                        FirstDerivativeInvariant4CauchyGreen<DimensionT>(cauchy_green_tensor,
                                                                         tau_interpolate_at_quad_point,
                                                                         invariants_first_derivative[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_first_derivative_index::dI4dC)]);
                    }
                    [[fallthrough]];
                case Content::invariants:
                    invariants[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_index::I1)] = Invariant1<DimensionT>(cauchy_green_tensor);
                    invariants[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_index::I2)] = Invariant2<DimensionT>(cauchy_green_tensor);
                    invariants[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_index::I3)] = Invariant3<DimensionT>(cauchy_green_tensor);

                    if (do_i4_activate)
                    {
                        const auto& tau_interpolate_at_quad_point = fibers->GetValue(quad_pt, geom_elt);

                        invariants[EnumUnderlyingType(InvariantHolder<NinvariantsT>::invariants_index::I4)] = Invariant4<DimensionT>(cauchy_green_tensor, tau_interpolate_at_quad_point);
                    }
                    break;
            } // switch
        }
    } //namespace anonymous


    #ifndef NDEBUG
    template<unsigned int NinvariantsT>
    inline void InvariantHolder<NinvariantsT>::Reset()
    {
        are_invariant_set_ = false;
    }
    #endif // NDEBUG


    template<unsigned int NinvariantsT>
    double InvariantHolder<NinvariantsT>::GetInvariant(invariants_index a_invariants_index) const noexcept
    {
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_index) < NinvariantsT);
        return invariants_[EnumUnderlyingType(a_invariants_index)];
    }


    template<unsigned int NinvariantsT>
    const LocalVector& InvariantHolder<NinvariantsT>::GetFirstDerivativeWrtCauchyGreen(invariants_first_derivative_index a_invariants_first_derivative_index) const noexcept
    {
        assert(GetContent() >= InvariantHolderNS::Content::invariants_and_first_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
               "constructor argument if you really need first derivate!");
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_first_derivative_index) < NinvariantsT);

        return invariants_first_derivative_[EnumUnderlyingType(a_invariants_first_derivative_index)];
    }


    template<unsigned int NinvariantsT>
    const LocalMatrix& InvariantHolder<NinvariantsT>::GetSecondDerivativeWrtCauchyGreen(invariants_second_derivative_index a_invariants_second_derivative_index) const noexcept
    {
        assert(GetContent() == InvariantHolderNS::Content::invariants_and_first_and_second_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
               "constructor argument if you really need first derivate!");


        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(a_invariants_second_derivative_index) < 2);

        return invariants_second_derivative_[EnumUnderlyingType(a_invariants_second_derivative_index)];
    }


    template<unsigned int NinvariantsT>
    inline unsigned int InvariantHolder<NinvariantsT>::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    template<unsigned int NinvariantsT>
    inline InvariantHolderNS::Content InvariantHolder<NinvariantsT>::GetContent() const noexcept
    {
        return content_;
    }


    template<unsigned int NinvariantsT>
    std::array<double, NinvariantsT>& InvariantHolder<NinvariantsT>::GetNonCstInvariant() noexcept
    {
        return invariants_;
    }


    template<unsigned int NinvariantsT>
    std::array<LocalVector, NinvariantsT>& InvariantHolder<NinvariantsT>::GetNonCstFirstDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_first_derivative_;
    }

    template<unsigned int NinvariantsT>
    std::array<LocalMatrix, 2>& InvariantHolder<NinvariantsT>::GetNonCstSecondDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_second_derivative_;
    }


    template<unsigned int NinvariantsT>
    void InvariantHolder<NinvariantsT>::SetFibers(const FiberList<ParameterNS::Type::vector>* fibers)
    {
        fibers_ = fibers;
    }


    template<unsigned int NinvariantsT>
    void InvariantHolder<NinvariantsT>::SetI4(bool do_i4_activate)
    {
        do_i4_activate_ = do_i4_activate;
    }


    template<unsigned int NinvariantsT>
    bool InvariantHolder<NinvariantsT>::DoI4Activate() const noexcept
    {
        return do_i4_activate_;
    }


    template<unsigned int NinvariantsT>
    const FiberList<ParameterNS::Type::vector>* InvariantHolder<NinvariantsT>::GetFibers() const noexcept
    {
        return fibers_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
