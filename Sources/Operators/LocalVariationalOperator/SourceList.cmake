target_sources(${MOREFEM_OP}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/BilinearLocalVariationalOperator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BilinearLocalVariationalOperator.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryData.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LinearLocalVariationalOperator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LinearLocalVariationalOperator.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonlinearLocalVariationalOperator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonlinearLocalVariationalOperator.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/CauchyAndInvariant/SourceList.cmake)
