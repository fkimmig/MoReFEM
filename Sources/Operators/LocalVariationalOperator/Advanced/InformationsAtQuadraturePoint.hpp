///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_

# include <memory>
# include <vector>
# include <array>

# include "Utilities/MatrixOrVector.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
# include "Utilities/Containers/EnumClass.hpp"

# include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"

# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
# include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class RefGeomElt;
    class LocalFEltSpace;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Whether a gradient matrix should be allocated for \a InformationsAtQuadraturePoint.
     *
     * This is required for \a LocalVariationalOperator that uses up gradient-based elements. If you're not sure,
     * try 'no' and run in debug mode: an assert will tell you if 'yes' was required.
     */
    enum class AllocateGradientFEltPhi { no, yes };


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {

            /*!
             * \class doxygen_hide_infos_at_quad_pt_arg
             *
             * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature point, such as the
             * geometric and finite element shape function values.
             */

            /*!
             * \brief Stores data related to a given quadrature point, such as the geometric and finite element shape
             * function values.
             */
            class InformationsAtQuadraturePoint final
            : public ::MoReFEM::Crtp::LocalMatrixStorage<InformationsAtQuadraturePoint, 5u>,
            public ::MoReFEM::Crtp::LocalVectorStorage<InformationsAtQuadraturePoint, 5u>
            {

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
                 * \param[in] quadrature_point \a QuadraturePoint for which the helper data are computed.
                 * \param[in] ref_geom_elt \a RefGeomElt considered.
                 * \param[in] ref_felt_list List of reference finite elements (The brand adapted for usage in
                 * \a LocalVariationalOperator).
                 * \param[in] mesh_dimension Dimension of the mesh (so might be higher than dimension of \a ref_geom_elt.
                 */
                explicit InformationsAtQuadraturePoint(const QuadraturePoint& quadrature_point,
                                                       const RefGeomElt& ref_geom_elt,
                                                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                                                       unsigned int mesh_dimension,
                                                       AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

                //! Destructor.
                ~InformationsAtQuadraturePoint() = default;

                //! Copy constructor.
                InformationsAtQuadraturePoint(const InformationsAtQuadraturePoint&) = delete;

                //! Move constructor.
                InformationsAtQuadraturePoint(InformationsAtQuadraturePoint&&) = default;

                //! Copy affectation.
                InformationsAtQuadraturePoint& operator=(const InformationsAtQuadraturePoint&) = delete;

                //! Move affectation.
                InformationsAtQuadraturePoint& operator=(InformationsAtQuadraturePoint&&) = default;

                ///@}


            public:


                //! Get quadrature point informations.
                const QuadraturePoint& GetQuadraturePoint() const noexcept;

                //! Geometric shape function value at each local node of the reference geometric element.
                const LocalVector& GetRefGeometricPhi() const noexcept;

                //! Geometric shape function value at each local node of the reference geometric element.
                double GetRefGeometricPhi(unsigned int local_node_index) const;

                /*!
                 * \brief First derivative of the geometric shape function value at each local node of the
                 * reference geometric element.
                 *
                 * By convention rows stand for the local_node_index considered, and columns for the component against
                 * which derivation is performed.
                 *
                 * \return First derivative of the geometric shape function value at each local node of the
                 * reference geometric element.
                 */
                const LocalMatrix& GetGradientRefGeometricPhi() const noexcept;

                //! Finite element shape function value at each local node of the reference finite element.
                const LocalVector& GetRefFEltPhi() const noexcept;

                /*!
                 * \brief Finite element shape function value at each local node of the reference finite element.
                 *
                 * This is in fact an alias of GetRefFEltPhi().
                 *
                 * \return Finite element shape function value at each local node of the reference finite element.
                 */
                const LocalVector& GetFEltPhi() const noexcept;

                //! Finite element shape function value at each local node of the reference finite element.
                double GetRefFEltPhi(unsigned int local_node_index) const;

                /*!
                 * \brief First derivative of the finite element shape function value at each local node of the
                 * reference finite element.
                 *
                 * By convention rows stand for the local_node_index considered, and columns for the component against
                 * which derivation is performed.
                 *
                 * \return First derivative of the finite element shape function value at each local node of the
                 * reference finite element.
                 */
                const LocalMatrix& GetGradientRefFEltPhi() const noexcept;


                /*!
                 * \brief Compute the attributes that depends on the \a local_felt_space.
                 *
                 * \param[in] local_felt_space \a LocalFEltSpace for which data are computed.
                 */
                void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);


                /*!
                 * \brief Get the first derivative of the finite element shape function value at each node of the
                 * finite element (not the reference one).
                 *
                 * The value returns is the one matching the last LocalFEltSpace associated to the object
                 * through ComputeLocalFEltSpaceData() method.
                 *
                 * By convention rows stand for the local_node_index considered, and columns for the component against
                 * which derivation is performed.
                 *
                 * \return First derivative of the finite element shape function value at each node of the
                 * finite element (not the reference one).
                 */
                const LocalMatrix& GetGradientFEltPhi() const noexcept;

                //! Transpose matrix of GetGradientFEltPhi().
                const LocalMatrix& GetTransposedGradientFEltPhi() const;

                /*!
                 * \brief Get the determinant of the jacobian.
                 *
                 * The value returns is the one matching the last LocalFEltSpace associated to the object
                 * through ComputeLocalFEltSpaceData() method.
                 *
                 * \return Determinant of the jacobian.
                 */
                double GetJacobianDeterminant() const noexcept;


                /*!
                 * \brief Get the absolute value of the determinant of the jacobian.
                 *
                 * The value returns is the one matching the last LocalFEltSpace associated to the object
                 * through ComputeLocalFEltSpaceData() method.
                 *
                 * \return Absolute value of the determinant of the jacobian.
                 *
                 */
                double GetAbsoluteValueJacobianDeterminant() const;

                //! Returns the dimension of the mesh.
                unsigned int GetMeshDimension() const noexcept;

                //! Returns the number of nodes.
                unsigned int Nnode() const noexcept;


            private:

                //! Init reference geometric shape functions.
                void InitRefGeometricShapeFunction(const RefGeomElt& ref_geom_elt);

                //! Init reference finite element shape functions.
                void InitRefFEltShapeFunction(const RefGeomElt& ref_geom_elt,
                                              const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list);

                //! Non constant accessor to determinant.
                double& GetNonCstDeterminant() noexcept;

                /*!
                 * \brief Non constant accessor to current value of deriv_phi_felt_.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method should never be used except in ComputeGradientFEltPhi() method.
                 *
                 * \return Current value of deriv_phi_felt_.
                 */
                LocalMatrix& GetNonCstGradientFEltPhi() noexcept;

                //! Allocate memory for gradient felt phi.
                void ResizeGradientFEltPhi(int Nrow, int Ncol);

                //! Return the sequence [0, Ncomponent - 1].
                const Seldon::Vector<int>& GetComponentSequence() const noexcept;

                //! Whether the gradient of finite element is required or not.
                bool DoAllocateGradientFEltPhi() const;


            private:

                //! Non constant version of GetRefGeometricPhi().
                LocalVector& GetNonCstRefGeometricPhi() noexcept;

                //! Non constant version of GetGradientRefGeometricPhi().
                LocalMatrix& GetNonCstGradientRefGeometricPhi() noexcept;

                //! Non constant version of GetRefFEltPhi().
                LocalVector& GetNonCstRefFEltPhi() noexcept;

                //! Non constant version of GetGradientRefFEltPhi().
                LocalMatrix& GetNonCstGradientRefFEltPhi() noexcept;

                /*!
                 * \brief Non constant access to the inverse of the jacobian matrix.
                 *
                 * \warning This one should not be used outside of the place the calculation occurs; all accessors should
                 * hence be private.
                 *
                 * \return Inverse of the jacobian matrix.
                 */
                LocalMatrix& GetNonCstInverseJacobian() noexcept;

                //! Constant accessor to the helper class to compute jacobian.
                const Advanced::GeomEltNS::ComputeJacobian& GetComputeJacobianHelper() const noexcept;

                //! Non constant accessor to the helper class to compute jacobian.
                Advanced::GeomEltNS::ComputeJacobian& GetNonCstComputeJacobianHelper() noexcept;

            private:

                /// \name Reference quantities
                ///@{

                //! Quadrature point considered in the class.
                const QuadraturePoint& quadrature_point_;

                //! Dimension of the geometric mesh region (might be higher than the one of finite elements here).
                const unsigned int geometric_mesh_region_dimension_;


                ///@}


            private:

                /// \name Quantities dependant of the actual GeometricElt considered.
                ///@{

                //! Determinant of the jacobian matrix.
                double determinant_ = std::numeric_limits<double>::max();


                ///@}


                /*!
                 * \brief Return a sequence from 0 to Ncomponent - 1.
                 *
                 * \internal <b><tt>[internal]</tt></b> Stored here to keep generating it at each call of dPhi().
                 */
                Seldon::Vector<int> Ncomponent_sequence_;

                //! Helper class to compute jacobian.
                Advanced::GeomEltNS::ComputeJacobian::unique_ptr compute_jacobian_helper_ = nullptr;


            private:

                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{


                //! Indexes for local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    inverse_jacobian = 0,
                    deriv_phi_ref_geo = 1,
                    deriv_phi_ref_felt = 2,
                    deriv_phi_felt = 3, // First derivative of the finite element shape function value at each node of the
                                        // finite element (not the reference one).
                    transposed_deriv_phi_felt = 4

                };


                //! Indexes for local vectors.
                enum class LocalVectorIndex : std::size_t
                {
                    phi_ref_geo = 0,
                    phi_ref_felt = 1,
                    cross_product = 2, // allocated only if used!
                    column1 = 3, // allocated only if used!
                    column2 = 4 // allocated only if used!
                };

                ///@}


            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HPP_
