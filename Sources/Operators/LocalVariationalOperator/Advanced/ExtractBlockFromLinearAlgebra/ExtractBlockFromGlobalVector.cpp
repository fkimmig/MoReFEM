///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:41:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractBlockFromLinearAlgebra/ExtractBlockFromGlobalVector.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace LocalVariationalOperatorNS
        {
            
            
            # ifndef NDEBUG
            
            
            ::Seldon::SubVector<LocalVector>
            ExtractBlockFromLocalVector(const Advanced::RefFEltInLocalOperator& ref_felt,
                                        const unsigned int component,
                                        LocalVector& vector)
            {
                const auto& row_range = ref_felt.GetLocalDofIndexList(component);
                
                Wrappers::Seldon::AssertBlockValidity(vector, row_range);
                ::Seldon::SubVector<LocalVector> ret(vector, row_range);
                
                return ret;
            }
            
            
            ::Seldon::SubVector<LocalVector>
            ExtractBlockFromLocalVector(const Advanced::RefFEltInLocalOperator& ref_felt,
                                        LocalVector& vector)
            {
                const auto& row_range = ref_felt.GetLocalDofIndexList();
                Wrappers::Seldon::AssertBlockValidity(vector, row_range);
                ::Seldon::SubVector<LocalVector> ret(vector, row_range);
                
                return ret;
            }
            
            
            # endif // NDEBUG
          
            
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
        

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
