target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromGlobalVector.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromGlobalMatrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromGlobalMatrix.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromGlobalVector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromGlobalVector.hxx" / 
)

