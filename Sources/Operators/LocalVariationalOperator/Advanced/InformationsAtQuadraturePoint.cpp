///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "Geometry/RefGeometricElt/RefGeomElt.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace LocalVariationalOperatorNS
        {
        
        
            namespace // anonymous
            {
                
                
                void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                         const QuadraturePoint& quadrature_point,
                                         LocalVector& phi,
                                         LocalMatrix& dphi);
                
                
            } // namespace anonymous
            
            
            InformationsAtQuadraturePoint
            ::InformationsAtQuadraturePoint(const QuadraturePoint& quadrature_point,
                                            const RefGeomElt& ref_geom_elt,
                                            const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                                            const unsigned int geometric_mesh_region_dimension,
                                            AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            : quadrature_point_(quadrature_point),
            geometric_mesh_region_dimension_(geometric_mesh_region_dimension)
            {
                compute_jacobian_helper_ =
                    std::make_unique<Advanced::GeomEltNS::ComputeJacobian>(geometric_mesh_region_dimension);
                
                InitRefGeometricShapeFunction(ref_geom_elt);
                InitRefFEltShapeFunction(ref_geom_elt, ref_felt_list);
                
                const auto& deriv_ref_phi_felt = GetGradientRefFEltPhi();
                
                if (do_allocate_gradient_felt_phi == AllocateGradientFEltPhi::yes)
                {
                   ResizeGradientFEltPhi(deriv_ref_phi_felt.GetM(),
                                         deriv_ref_phi_felt.GetN());
                }
                
                Ncomponent_sequence_ =
                Wrappers::Seldon::ContiguousSequence(0, static_cast<int>(ref_geom_elt.GetDimension()));
                
                GetNonCstInverseJacobian().Resize(static_cast<int>(geometric_mesh_region_dimension),
                                                  static_cast<int>(geometric_mesh_region_dimension));
                
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_deriv_phi_felt)>().Resize(deriv_ref_phi_felt.GetN(),
                                                                                                         deriv_ref_phi_felt.GetM());
                
                {
                    const auto ref_geom_elt_dimension = ref_geom_elt.GetDimension();
                    
                    if (ref_geom_elt_dimension == 2 && ref_geom_elt_dimension != GetMeshDimension())
                    {
                        auto& col1 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column1)>();
                        auto& col2 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column2)>();
                        auto& cross_product = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::cross_product)>();
                        
                        col1.Resize(3);
                        col2.Resize(3);
                        cross_product.Resize(3);
                    }
                }
            }
            
            
            void InformationsAtQuadraturePoint::ResizeGradientFEltPhi(int Nrow, int Ncol)
            {
                GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>().Resize(Nrow, Ncol);
            }
            
            
            void InformationsAtQuadraturePoint::InitRefGeometricShapeFunction(const RefGeomElt& ref_geom_elt)
            {
                const unsigned int dimension = ref_geom_elt.GetDimension();
                const unsigned int Ngeometric_coords = ref_geom_elt.Ncoords();
                const auto& quad_pt = GetQuadraturePoint();
                
                const int int_dimension = static_cast<int>(dimension);
                const int int_Ngeometric_coords = static_cast<int>(Ngeometric_coords);
                
                assert(Ngeometric_coords > 0u);
                
                auto& phi_ref_geo = GetNonCstRefGeometricPhi();
                auto& deriv_phi_ref_geo = GetNonCstGradientRefGeometricPhi();
                
                phi_ref_geo.Resize(int_Ngeometric_coords);
                deriv_phi_ref_geo.Resize(int_Ngeometric_coords, int_dimension);
                
                for(unsigned int local_node_index = 0u; local_node_index < Ngeometric_coords; ++local_node_index)
                {
                    const int int_local_node_index = static_cast<int>(local_node_index);
                    
                    phi_ref_geo(int_local_node_index) = ref_geom_elt.ShapeFunction(local_node_index, quad_pt);
                    
                    for(unsigned int component = 0u; component < dimension; ++component)
                    {
                        const int int_component = static_cast<int>(component);
                        
                        deriv_phi_ref_geo(int_local_node_index, int_component)
                        = ref_geom_elt.FirstDerivateShapeFunction(local_node_index, component, quad_pt);
                    }
                }
            }
            
            
            void InformationsAtQuadraturePoint
            ::InitRefFEltShapeFunction(const RefGeomElt& ref_geom_elt,
                                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
            {
                const int Nnode =
                std::accumulate(ref_felt_list.cbegin(), ref_felt_list.cend(),
                                0,
                                [](int sum, const Advanced::RefFEltInLocalOperator::const_unique_ptr& ref_felt_ptr)
                                {
                                    if (!(!ref_felt_ptr))
                                        return sum + static_cast<int>(ref_felt_ptr->Nnode());
                                    else
                                        return sum;
                                });
                
                assert(Nnode > 0);
                
                const int dimension =  static_cast<int>(ref_geom_elt.GetDimension());
                
                auto& phi_ref_felt = GetNonCstRefFEltPhi();
                auto& deriv_phi_ref_felt = GetNonCstGradientRefFEltPhi();
                
                phi_ref_felt.Resize(Nnode);
                deriv_phi_ref_felt.Resize(Nnode, dimension);
                
                const auto& quad_pt = GetQuadraturePoint();
                
                for (const auto& ref_felt_ptr : ref_felt_list)
                {
                    assert(!(!ref_felt_ptr));
                    FillPhiAndDerivates(*ref_felt_ptr,
                                        quad_pt,
                                        phi_ref_felt,
                                        deriv_phi_ref_felt);
                }
            }
            
            
            
            const LocalMatrix& InformationsAtQuadraturePoint
            ::GetTransposedGradientFEltPhi() const
            {
                decltype(auto) ret = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_deriv_phi_felt)>();
                Wrappers::Seldon::Transpose(GetGradientFEltPhi(), ret);
                return ret;
            }
            
            
            void InformationsAtQuadraturePoint::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
            {
                const auto& geom_elt = local_felt_space.GetGeometricElt();
                
                const auto& quad_pt = GetQuadraturePoint();
                
                auto& jacobian = GetNonCstComputeJacobianHelper().Compute(geom_elt, quad_pt);
                
                const unsigned int geometric_mesh_region_dimension = GetMeshDimension();
                
                if (geometric_mesh_region_dimension == geom_elt.GetDimension())
                {
                    if (DoAllocateGradientFEltPhi())
                    {
                        // Compute inverse of the jacobian.
                        auto& inverse_jacobian = GetNonCstInverseJacobian();
                        
                        Wrappers::Seldon::ComputeInverseSquareMatrix(jacobian,
                                                                     inverse_jacobian,
                                                                     GetNonCstDeterminant());
                        
                        Seldon::Mlt(GetGradientRefFEltPhi(), inverse_jacobian, GetNonCstGradientFEltPhi());
                    }
                    else
                        GetNonCstDeterminant() = Wrappers::Seldon::ComputeDeterminant(jacobian);
                }
                else // Case in which a geometric with lower dimension than the mesh is considered.
                {
                    const unsigned int geom_elt_dimension = geom_elt.GetDimension();
                    assert(geometric_mesh_region_dimension > geom_elt_dimension);
                    
                    switch(geom_elt_dimension)
                    {
                        case 2u:
                        {
                            auto& col1 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column1)>();
                            auto& col2 = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::column2)>();
                            auto& cross_product = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::cross_product)>();
                            
                            Seldon::GetCol(jacobian, 0, col1);
                            Seldon::GetCol(jacobian, 1, col2);
                            Wrappers::Seldon::CrossProduct(col1, col2, cross_product);
                            
                            GetNonCstDeterminant() = std::sqrt(NumericNS::Square(cross_product(0))
                                                               + NumericNS::Square(cross_product(1))
                                                               + NumericNS::Square(cross_product(2)));
                            
                            break;
                        }
                        case 1u:
                        {
                            GetNonCstDeterminant() = std::sqrt(NumericNS::Square(jacobian(0, 0)) + NumericNS::Square(jacobian(1, 0)));
                            break;
                        }
                        case 0u:
                        {
                            GetNonCstDeterminant() = std::fabs(jacobian(0, 0));
                            break;
                        }
                        default:
                        {
                            assert(false && "Should never happen!");
                            break;
                        }
                    } // switch
                }
            }
            
                 
            
            namespace // anonymous
            {
                
                
                void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                         const QuadraturePoint& quadrature_point,
                                         LocalVector& phi,
                                         LocalMatrix& dphi)
                {
                    const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
                    
                    const unsigned int Ncomponent = basic_ref_felt.GetTopologyDimension();
                    const unsigned int Nnode = ref_felt.Nnode();
                    
                    // It's this line that make the difference between the two reference finite elements (if there
                    // are 2 of course...)
                    const Seldon::Vector<int>& node_position_list_in_matrix = ref_felt.GetLocalNodeIndexList();
                    
                    assert(node_position_list_in_matrix.GetSize() > 0);
                    assert(node_position_list_in_matrix.GetSize() == static_cast<int>(Nnode));
                    
                    for (unsigned int local_node_index = 0; local_node_index < Nnode; ++local_node_index)
                    {
                        const int node_position_in_matrix = node_position_list_in_matrix(static_cast<int>(local_node_index));
                        
                        phi(node_position_in_matrix) = basic_ref_felt.ShapeFunction(local_node_index, quadrature_point);
                        
                        for(unsigned int component = 0u; component < Ncomponent; ++component)
                        {
                            dphi(node_position_in_matrix, static_cast<int>(component))
                            = basic_ref_felt.FirstDerivateShapeFunction(local_node_index, component, quadrature_point);
                        }
                    }
                }
                
                
            } // namespace anonymous
        
        
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
