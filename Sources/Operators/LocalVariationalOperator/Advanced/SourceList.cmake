target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradientDisplacementMatrix.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InformationsAtQuadraturePoint.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradientDisplacementMatrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradientDisplacementMatrix.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InformationsAtQuadraturePoint.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InformationsAtQuadraturePoint.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromLinearAlgebra/SourceList.cmake)
