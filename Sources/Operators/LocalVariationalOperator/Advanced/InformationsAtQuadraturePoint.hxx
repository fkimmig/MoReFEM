///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:09:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline const QuadraturePoint& InformationsAtQuadraturePoint::GetQuadraturePoint() const noexcept
            {
                return quadrature_point_;
            }


            inline const LocalVector& InformationsAtQuadraturePoint::GetRefGeometricPhi() const noexcept
            {
                return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_geo)>();
            }


            inline LocalVector& InformationsAtQuadraturePoint::GetNonCstRefGeometricPhi() noexcept
            {
                return const_cast<LocalVector&>(GetRefGeometricPhi());
            }


            inline double InformationsAtQuadraturePoint::GetRefGeometricPhi(unsigned int local_node_index) const
            {
                const int index = static_cast<int>(local_node_index);
                assert(index < GetRefGeometricPhi().GetSize());
                return GetRefGeometricPhi()(index);
            }


            inline const LocalMatrix& InformationsAtQuadraturePoint::GetGradientRefGeometricPhi() const noexcept
            {
                return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_geo)>();
            }


            inline LocalMatrix& InformationsAtQuadraturePoint::GetNonCstGradientRefGeometricPhi() noexcept
            {
                return const_cast<LocalMatrix&>(GetGradientRefGeometricPhi());
            }


            inline const LocalVector& InformationsAtQuadraturePoint::GetRefFEltPhi() const noexcept
            {
                return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_felt)>();
            }


            inline const LocalVector& InformationsAtQuadraturePoint::GetFEltPhi() const noexcept
            {
                return GetRefFEltPhi();
            }


            inline LocalVector& InformationsAtQuadraturePoint::GetNonCstRefFEltPhi() noexcept
            {
                return const_cast<LocalVector&>(GetRefFEltPhi());
            }


            inline double InformationsAtQuadraturePoint::GetRefFEltPhi(unsigned int local_node_index) const
            {
                const int index = static_cast<int>(local_node_index);
                assert(index < GetRefFEltPhi().GetSize());
                return GetRefFEltPhi()(index);
            }


            inline const LocalMatrix& InformationsAtQuadraturePoint::GetGradientRefFEltPhi() const noexcept
            {
                return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_felt)>();;
            }


            inline LocalMatrix& InformationsAtQuadraturePoint::GetNonCstGradientRefFEltPhi() noexcept
            {
                return const_cast<LocalMatrix&>(GetGradientRefFEltPhi());
            }


            inline double& InformationsAtQuadraturePoint::GetNonCstDeterminant() noexcept
            {
                return determinant_;
            }


            inline LocalMatrix& InformationsAtQuadraturePoint::GetNonCstGradientFEltPhi() noexcept
            {
                return const_cast<LocalMatrix&>(GetGradientFEltPhi());
            }


            inline const LocalMatrix& InformationsAtQuadraturePoint
            ::GetGradientFEltPhi() const noexcept
            {
                decltype(auto) ret = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>();
                assert(ret.GetM() != 0
                       && "If not fulfilled, it's likely the operator what calls current object set "
                       "AllocateGradientFEltPhi::no in its constructor; try replace it by 'yes'.");
                return ret;
            }


            inline const Seldon::Vector<int>& InformationsAtQuadraturePoint::GetComponentSequence() const noexcept
            {
                return Ncomponent_sequence_;
            }


            inline double InformationsAtQuadraturePoint::GetJacobianDeterminant() const noexcept
            {
                return determinant_;
            }


            inline double InformationsAtQuadraturePoint::GetAbsoluteValueJacobianDeterminant() const
            {
                return std::fabs(GetJacobianDeterminant());
            }


            inline unsigned int InformationsAtQuadraturePoint::GetMeshDimension() const noexcept
            {
                return geometric_mesh_region_dimension_;
            }


            inline bool InformationsAtQuadraturePoint::DoAllocateGradientFEltPhi() const
            {
                // Do not use the accessor GetGradientFEltPhi() here, as it asserts number of rows is not null...
                return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>().GetM() != 0;
            }


            inline LocalMatrix& InformationsAtQuadraturePoint::GetNonCstInverseJacobian() noexcept
            {
                return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::inverse_jacobian)>();
            }


            inline unsigned int InformationsAtQuadraturePoint::Nnode() const noexcept
            {
                return static_cast<unsigned int>(GetFEltPhi().GetSize());
            }



            inline const Advanced::GeomEltNS::ComputeJacobian& InformationsAtQuadraturePoint
            ::GetComputeJacobianHelper() const noexcept
            {
                assert(!(!compute_jacobian_helper_));
                return *compute_jacobian_helper_;
            }


            inline Advanced::GeomEltNS::ComputeJacobian& InformationsAtQuadraturePoint
            ::GetNonCstComputeJacobianHelper() noexcept
            {
                return const_cast<Advanced::GeomEltNS::ComputeJacobian&>(GetComputeJacobianHelper());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATIONS_AT_QUADRATURE_POINT_HXX_
