///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Sep 2014 13:34:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HPP_


# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"

# include "Geometry/GeometricElt/GeometricElt.hpp"

# include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

# include "Parameters/Parameter.hpp"

# include "Operators/Enum.hpp"
# include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"
# include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    template
    <
        class DerivedT,
        class LocalVariationalOperatorT,
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT
    >
    class GlobalParameterOperator;


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            template
            <
                class DerivedT,
                Advanced::OperatorNS::Nature NatureT,
                class TupleT
            >
            class GlobalVariationalOperator;



            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            struct LocalVariationalOperatorIterator;


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        /*!
         * \brief This object holds all the data required to compute the elementary matrix or vector and the resulting one.
         *
         *
         * \attention the values inside the local matrices and vectors may change frequently, but the allocation does
         * not: the size of the matrices and vectors is fixed early in the program and is not modified until
         * the deallocation when the program is done.
         *
         * \tparam MatrixTypeT Type of the elementary matrix, or std::false_type if irrelevant.
         * \tparam VectorTypeT Type of the elementary vector, or std::false_type if irrelevant.
         *
         */
        template
        <
            OperatorNS::Nature OperatorNatureT,
            class MatrixTypeT,
            class VectorTypeT
        >
        class ElementaryData;


        /*!
         * \class doxygen_hide_elementary_data_unknown_and_test_unknown_param
         *
         * \param[in] unknown_storage List of unknown/numbering subset pairs involved in the current local operator.
         * There can be two at most; their relative ordering fix the structure of the local matrix (but it should be
         * completely transparent for the user anyway: the library takes care of fetching the appropriate elements
         * through RefFEltInLocalOperator class).
         * \param[in] test_unknown_storage Same as \a unknown_storage for test functions.
         */


        /*!
         * \brief Specialization of ElementaryData for vectors (i.e. the one used in linear form operators).
         */
        template
        <
            class VectorTypeT
        >
        class ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>
        : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
        public Internal::LocalVariationalOperatorNS::ElementaryDataStorage
        <
            ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>,
            IsMatrixOrVector::vector,
            VectorTypeT
        >
        {
        private:

            //! Convenient alias.
            using self = ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>;

            //! Alias for parent.
            using vector_storage_parent =
                Internal::LocalVariationalOperatorNS::ElementaryDataStorage
                <
                    self,
                    IsMatrixOrVector::vector,
                    VectorTypeT
                >;

        public:

            //! Alias to type of matrix (std::false_type if irrelevant).
            using matrix_type = std::false_type;

            //! Alias to type of vector (std::false_type if irrelevant).
            using vector_type = VectorTypeT;


            //! Friendship to GlobalVariationalOperator, one of the two classes allowed to build such an object.
//            template
//            <
//                class DerivedT,
//                class LocalVariationalOperatorT
//            >
//            friend class ::MoReFEM::GlobalVariationalOperator;

            //! Friendship to GlobalParameterOperator, one of the two classes allowed to build such an object.
            template
            <
                class DerivedT,
                class LocalVariationalOperatorT,
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            friend class ::MoReFEM::GlobalParameterOperator;

            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;


        private:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
             * list of unknowns involved for the operator related to this ElementaryData will provide all the data
             * required to build RefFEltInLocalOperator objects that allow easy navigation through the
             * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the required
             * data).
             * \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
             * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
             * \param[in] felt_space_dimension Dimension considered in the finite element space.
             * \param[in] geom_mesh_region_dimension Dimension in the geometric mesh region.
             *  Must be equal or higher than felt_space_dimension.
             * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
             */
            explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                    const QuadratureRule& quadrature_rule,
                                    const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                    const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                    unsigned int felt_space_dimension,
                                    unsigned int geom_mesh_region_dimension,
                                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        public:

            //! Destructor.
            ~ElementaryData() = default;

            //! Copy constructor.
            ElementaryData(const ElementaryData&) = delete;

            //! Move constructor.
            ElementaryData(ElementaryData&&) = default;

            //! Copy affectation.
            ElementaryData& operator=(const ElementaryData&) = delete;

            //! Move affectation.
            ElementaryData& operator=(ElementaryData&&) = default;

            ///@}

            //! Allocate the elementary vector.
            void Allocate();

            //! Zero the elementary vector.
            void Zero();

        };


        /*!
         * \brief Specialization of ElementaryData for matrices (i.e. the one used in bilinear form operators).
         */
        template
        <
            class MatrixTypeT
        >
        class ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>
        : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
        public Internal::LocalVariationalOperatorNS::ElementaryDataStorage
        <
            ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>,
            IsMatrixOrVector::matrix,
            MatrixTypeT
        >
        {
        private:

            //! Convenient alias.
            using self = ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>;

            //! Alias for parent.
            using matrix_storage_parent =
                Internal::LocalVariationalOperatorNS::ElementaryDataStorage
                <
                    self,
                    IsMatrixOrVector::matrix,
                    MatrixTypeT
                >;

        public:

            //! Alias to type of matrix (std::false_type if irrelevant).
            using matrix_type = MatrixTypeT;

            //! Alias to type of vector (std::false_type if irrelevant).
            using vector_type = std::false_type;


            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;

        private:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
             * list of unknowns involved for the operator related to this ElementaryData will provide all the data
             * required to build RefFEltInLocalOperator objects that allow easy navigation through the
             * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the required
             * data).
             * \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
             * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
             * \param[in] felt_space_dimension Dimension considered in the finite element space.
             * \param[in] geom_mesh_region_dimension Dimension in the geometric mesh region.
             *  Must be equal or higher than felt_space_dimension.
             * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
             */
            explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                    const QuadratureRule& quadrature_rule,
                                    const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                    const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                    unsigned int felt_space_dimension,
                                    unsigned int geom_mesh_region_dimension,
                                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        public:

            //! Destructor.
            ~ElementaryData() = default;

            //! Copy constructor.
            ElementaryData(const ElementaryData&) = delete;

            //! Move constructor.
            ElementaryData(ElementaryData&&) = default;

            //! Affectation.
            ElementaryData& operator=(const ElementaryData&) = delete;

            //! Affectation.
            ElementaryData& operator=(ElementaryData&&) = default;

            ///@}

            //! Allocate the elementary matrix.
            void Allocate();

            //! Zero the elementary matrix.
            void Zero();


        };


        /*!
         * \brief Specialization of ElementaryData for matrices and vectors (i.e. the one used in nonlinear form operators).
         */
        template
        <
            class MatrixTypeT,
            class VectorTypeT
        >
        class ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>
        : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
        public Internal::LocalVariationalOperatorNS::ElementaryDataStorage
        <
            ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
            IsMatrixOrVector::vector,
            VectorTypeT
        >,
        public Internal::LocalVariationalOperatorNS::ElementaryDataStorage
        <
            ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
            IsMatrixOrVector::matrix,
            MatrixTypeT
        >
        {

        private:

            //! Convenient alias.
            using self = ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>;

            //! Alias for parent.
            using vector_storage_parent =
            Internal::LocalVariationalOperatorNS::ElementaryDataStorage
                <
                    self,
                    IsMatrixOrVector::vector,
                    VectorTypeT
                >;

            //! Alias for parent.
            using matrix_storage_parent =
                Internal::LocalVariationalOperatorNS::ElementaryDataStorage
                <
                    self,
                    IsMatrixOrVector::matrix,
                    MatrixTypeT
                >;
        public:

            //! Alias to type of matrix (std::false_type if irrelevant).
            using matrix_type = MatrixTypeT;

            //! Alias to type of vector (std::false_type if irrelevant).
            using vector_type = VectorTypeT;


            template
            <
                class TupleT,
                std::size_t I,
                std::size_t TupleSizeT,
                Advanced::OperatorNS::Nature NatureT
            >
            friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;


        private:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
             * list of unknowns involved for the operator related to this ElementaryData will provide all the data
             * required to build RefFEltInLocalOperator objects that allow easy navigation through the
             * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the required
             * data).
             * \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
             * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
             * \param[in] felt_space_dimension Dimension considered in the finite element space.
             * \param[in] geom_mesh_region_dimension Dimension in the geometric mesh region.
             *  Must be equal or higher than felt_space_dimension.
             * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
             */
            explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                    const QuadratureRule& quadrature_rule,
                                    const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                    const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                    unsigned int felt_space_dimension,
                                    unsigned int geom_mesh_region_dimension,
                                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        public:

            //! Destructor.
            ~ElementaryData() = default;

            //! Copy constructor.
            ElementaryData(const ElementaryData&) = delete;

            //! Move constructor.
            ElementaryData(ElementaryData&&) = default;

            //! Affectation.
            ElementaryData& operator=(const ElementaryData&) = delete;

            //! Affectation.
            ElementaryData& operator=(ElementaryData&&) = default;

            ///@}

            //! Allocate the elementary matrix and vector.
            void Allocate();

            //! Zero the elementary matrix and vector.
            void Zero();

        };


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/ElementaryData.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ELEMENTARY_DATA_HPP_
