target_sources(${MOREFEM_OP}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataStorage.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknownAndTestUnknownList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknownAndTestUnknownList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix/SourceList.cmake)
