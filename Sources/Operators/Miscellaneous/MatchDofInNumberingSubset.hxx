///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Jan 2016 10:15:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HXX_
# define MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HXX_


namespace MoReFEM
{


    inline const std::vector<std::vector<unsigned int>>& MatchDofInNumberingSubset::GetData() const noexcept
    {
        return match_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HXX_
