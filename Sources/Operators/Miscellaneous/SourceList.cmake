target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FindCoordsOfGlobalVector.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatchDofInNumberingSubset.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FindCoordsOfGlobalVector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FindCoordsOfGlobalVector.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatchDofInNumberingSubset.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MatchDofInNumberingSubset.hxx" / 
)

