///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Jan 2016 10:15:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HPP_
# define MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    /*!
     * \class doxygen_hide_MatchDofInNumberingSubset_arg
     *
     * \param[in] match_dof_in_numbering_subset_operator This operator is there to match the dofs between two different
     * \a GlobalVector. See a much more detailed description in \a MatchDofInNumberingSubset.
     */


    /*!
     * \brief This operator is used for instance if you need to match the content of two global vectors.
     *
     * For instance let's consider the following case:
     * . A global vector which contains the displacement on a given finite element space.
     * . Another global vector which contains a scalar quantity on the same finite element space.
     *
     * The new operator matches them, i.e. is able to tell which dof(s) on the second vector are on the same location
     * as the dofs in the first one.
     *
     * It is actually the numbering subsets that are conveyed in the constructor; hence the same operator may be used
     * for two different global vectors in the same numbering subset (it is obtained from GlobalVector::GetNumberingSubset()
     * method).
     *
     * Contrary to what I usually do in MoReFEM, indexes are manipulated directly here, for efficiency reasons.
     *
     * Here is how to use it
     * \code
     * GlobalVector displacement(...); // defined earlier on FEltSpace felt_space.
     * GlobalVector scalar_unknown(...); // defined earlier on same FEltSpace felt_space.
     *
     * MatchDofInNumberingSubset match_dof(felt_space,
     *                                     displacement.GetNumberingSubset(),
     *                                     scalar_unknown.GetNumberingSubset());
     *
     * Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> disp_content(displacement, __FILE__, __LINE__);
     * Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> scalar_content(displacement, __FILE__, __LINE__);
     *
     * decltype(auto) match = match_dof.GetData();
     *
     * const auto Ndof_in_source = disp_content.GetSize(__FILE__, __LINE__);
     * for (auto i = 0; i < Ndof_in_source; ++i)
     * {
     *     const auto& dof_list_in_target = match[i];
     *     assert(dof_list_in_target.size() == 1 && "In current example target is scalar...");
     *     const auto scalar_value = scalar_content.GetValue(dof_list_in_target.back(), __FILE__, __LINE__);
     *     ...
     * }
     * \endcode
     *
     * \attention This operator is at the moment somewhat limited: it expects that both numbering subsets cover
     * exactly the same node bearers in the finite element space and that the numbering subsets cover exactly one
     * unknown in the finite element space.
     *
     */
    class MatchDofInNumberingSubset final
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = MatchDofInNumberingSubset;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

    public:

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit MatchDofInNumberingSubset(const FEltSpace& felt_space,
                                           const NumberingSubset& source_numbering_subset,
                                           const NumberingSubset& target_numbering_subset);

        //! Destructor.
        ~MatchDofInNumberingSubset() = default;

        //! Copy constructor.
        MatchDofInNumberingSubset(const MatchDofInNumberingSubset&) = delete;

        //! Move constructor.
        MatchDofInNumberingSubset(MatchDofInNumberingSubset&&) = delete;

        //! Copy affectation.
        MatchDofInNumberingSubset& operator=(const MatchDofInNumberingSubset&) = delete;

        //! Move affectation.
        MatchDofInNumberingSubset& operator=(MatchDofInNumberingSubset&&) = delete;

        ///@}

        /*!
         * \brief Accessor to the matching data.
         *
         * See explanation in the class documentation to see how to use it.
         *
         * \return Matching data.
         */
        const std::vector<std::vector<unsigned int>>& GetData() const noexcept;

    private:

        /*!
         * \brief Store the match of processor-wise dof indexes.
         *
         * . The index of the vector stands for the processor-wise index of the source.
         * . The inner vector gives away all the processor-wise index(es) in the target numbering subset.
         */
        std::vector<std::vector<unsigned int>> match_;

    };


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/Miscellaneous/MatchDofInNumberingSubset.hxx"


#endif // MOREFEM_x_OPERATORS_x_MISCELLANEOUS_x_MATCH_DOF_IN_NUMBERING_SUBSET_HPP_
