target_sources(${MOREFEM_OP}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/GlobalParameterOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LocalParameterOperator/SourceList.cmake)
