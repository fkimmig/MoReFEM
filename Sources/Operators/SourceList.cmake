target_sources(${MOREFEM_OP}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/ParameterOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Miscellaneous/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/GlobalVariationalOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ConformInterpolator/SourceList.cmake)
