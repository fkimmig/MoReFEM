target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/LocalLagrangianInterpolator.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/LagrangianInterpolator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LagrangianInterpolator.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalLagrangianInterpolator.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalLagrangianInterpolator.hxx" / 
)

