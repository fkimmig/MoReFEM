target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationData.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SourceOrTargetData.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationData.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SourceOrTargetData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SourceOrTargetData.hxx" / 
)

