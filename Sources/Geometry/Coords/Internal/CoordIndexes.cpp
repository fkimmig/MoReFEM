///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/Coords/Internal/CoordIndexes.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace CoordsNS
        {
        
        
            void CoordIndexes::SetFileIndex(unsigned int value) noexcept
            {
                assert(file_index_ == NumericNS::UninitializedIndex<unsigned int>() && "Should be assigned only once!");
                file_index_ = value;
            }
            

            void CoordIndexes::SetPositionInOriginalCoordList(unsigned int value) noexcept
            {
                assert(position_in_original_coord_list_ == NumericNS::UninitializedIndex<unsigned int>()
                       && "Should be assigned only once!");
                position_in_original_coord_list_ = value;
                
                assert(position_in_reduced_coord_list_ == NumericNS::UninitializedIndex<unsigned int>()
                       && "Should also not have been assigned before!");

                position_in_reduced_coord_list_ = value;
            }
            

            void CoordIndexes::SetPositionInReducedCoordList(unsigned int value) noexcept
            {
                assert(was_set_position_in_reduced_coord_list_already_called_ == false
                       && "Should be assigned only once!");
                
                #ifndef NDEBUG
                was_set_position_in_reduced_coord_list_already_called_ = true;
                #endif // NDEBUG
                
                position_in_reduced_coord_list_ = value;
            }

        
        } // namespace CoordsNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
