///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Jan 2014 17:51:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_



namespace MoReFEM
{


    template<class T>
    Coords::Coords(T&& array, const double space_unit)
    : parent(array, space_unit)
    {
        #ifndef NDEBUG
        GetNonCstNobjects()++;
        #endif // NDEBUG
    }


    inline unsigned int Coords::GetIndex() const noexcept
    {
        return GetCoordIndexes().GetFileIndex();
    }


    inline void Coords::SetIndex(unsigned int index) noexcept
    {
        GetNonCstCoordIndexes().SetFileIndex(index);
    }


    inline void Coords::SetMeshLabel(const MeshLabel::const_shared_ptr& mesh_label)
    {
        mesh_label_ = mesh_label;
    }


    inline MeshLabel::const_shared_ptr Coords::GetMeshLabelPtr() const
    {
        return mesh_label_;
    }



    inline bool operator<(const Coords& lhs, const Coords& rhs)
    {
        return lhs.GetIndex() < rhs.GetIndex();
    }


    inline bool operator==(const Coords& lhs, const Coords& rhs)
    {
        # ifndef NDEBUG
        if (lhs.GetIndex() == rhs.GetIndex())
        {
            assert("Coordinates should also be the same, even if for efficiency reason I stick to index comparison!"
                   && Distance(lhs, rhs) < 1.e-12);
        }
        # endif // NDEBUG

        return lhs.GetIndex() == rhs.GetIndex();
    }



    template<bool DoPrintIndexT>
    void WriteEnsightFormat(const Coords& point, std::ostream& stream)
    {
        if (DoPrintIndexT)
            stream << std::setw(8) << point.GetIndex();

        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.x();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.y();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.z();
        stream << std::endl;
    }



    template<class FloatT>
    void WriteMeditFormat(const unsigned int dimension,
                          const Coords& point,
                          int lm_mesh_index)
    {

        int label_index(0);

        auto label_ptr = point.GetMeshLabelPtr();
        if (label_ptr)
            label_index = static_cast<int>(label_ptr->GetIndex());

        switch(dimension)
        {
            case 1:
                GmfSetLin(lm_mesh_index, GmfVertices,
                          point.x(),
                          0.,
                          label_index);
                break;
            case 2:
                GmfSetLin(lm_mesh_index, GmfVertices,
                          point.x(),
                          point.y(),
                          label_index);
                break;
            case 3:
                GmfSetLin(lm_mesh_index, GmfVertices,
                          point.x(),
                          point.y(),
                          point.z(),
                          label_index);
                break;
            default:
                assert(false && "Dimension should be 1, 2, or 3!");
                break;
        }
    }


    inline InterfaceNS::Nature Coords::GetInterfaceNature() const
    {
        return interface_nature_;
    }


    inline const Internal::CoordsNS::CoordIndexes& Coords::GetCoordIndexes() const noexcept
    {
        return coord_indexes_;
    }


    inline Internal::CoordsNS::CoordIndexes& Coords::GetNonCstCoordIndexes() noexcept
    {
        return const_cast<Internal::CoordsNS::CoordIndexes&>(GetCoordIndexes());
    }



    template<MpiScale MpiScaleT>
    inline unsigned int Coords::GetPositionInCoordsListInMesh() const noexcept
    {
        switch(MpiScaleT)
        {
            case MpiScale::program_wise:
                return GetCoordIndexes().GetPositionInOriginalCoordList();
            case MpiScale::processor_wise:
                return GetCoordIndexes().GetPositionInReducedCoordList();
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    template<MpiScale MpiScaleT>
    void Coords::SetPositionInCoordsListInMesh(unsigned int position) noexcept
    {
        switch(MpiScaleT)
        {
            case MpiScale::program_wise:
                return GetNonCstCoordIndexes().SetPositionInOriginalCoordList(position);
            case MpiScale::processor_wise:
                return GetNonCstCoordIndexes().SetPositionInReducedCoordList(position);
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    inline void Coords::AddDomainContainingCoords(unsigned int domain_unique_id)
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of your "
               "model. See the TestDomainListInCoords model for example.");
        domain_list_.insert(domain_unique_id);
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup

#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_COORDS_HXX_
