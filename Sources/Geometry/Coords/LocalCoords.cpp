///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Mar 2017 22:51:37 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <iostream>
#include <limits>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Containers/Print.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{
    
    
    LocalCoords::LocalCoords(std::initializer_list<double>&& coor)
    : coordinate_list_(std::move(coor))
    { }
    
    
    LocalCoords::~LocalCoords() = default;


    void LocalCoords::Print(std::ostream& out) const
    {
        Utilities::PrintContainer(coordinate_list_, out);
    }


    
    std::vector<unsigned int> ExtractMismatchedComponentIndexes(const LocalCoords& coords1,
                                                                const LocalCoords& coords2)
    {
        const auto& component_coordinates1 = coords1.GetCoordinates();
        const auto& component_coordinates2 = coords2.GetCoordinates();
        
        const std::size_t Ncomponent = component_coordinates1.size();
        assert(Ncomponent == component_coordinates2.size());
        
        std::vector<unsigned int> ret;
        
        for (std::size_t i = 0ul; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1[i], component_coordinates2[i]))
                ret.push_back(static_cast<unsigned int>(i));
        }
        
        return ret;

    }
    
    
    unsigned int ExtractMismatchedComponentIndex(const LocalCoords& coords1,
                                                 const LocalCoords& coords2)
    {
        const auto& component_coordinates1 = coords1.GetCoordinates();
        const auto& component_coordinates2 = coords2.GetCoordinates();
        
        const std::size_t Ncomponent = component_coordinates1.size();
        assert(Ncomponent == component_coordinates2.size());
        
        for (std::size_t i = 0ul; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1[i], component_coordinates2[i]))
            {
                #ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractMismatchedComponentIndexes(coords1, coords2);
                assert("When the current function is called it is implicitly expected to be for an edge of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1ul);
                assert(output_more_generic_function.back() == static_cast<unsigned int>(i));
                #endif // NDEBUG
                
                return static_cast<unsigned int>(i);
            }
        }
        
        assert(false);
        return NumericNS::UninitializedIndex<unsigned int>();
    }



    std::vector<unsigned int> ExtractIdenticalComponentIndexes(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1ul);
        std::vector<std::vector<double>> coords_component_list;
        
        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());
        
        const auto& last_coord = coords_component_list.back();

        const unsigned int Ncomponent = static_cast<unsigned int>(coords_component_list.back().size());
        
        #ifndef NDEBUG
        for (auto coords_component : coords_component_list)
            assert(static_cast<unsigned int>(coords_component.size()) == Ncomponent);
        #endif // NDEBUG
        
        std::vector<unsigned int> ret;
        
        for (unsigned int i = 0u; i < Ncomponent; ++i)
        {
            bool still_identical = true;
            
            for (unsigned int j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j][i], last_coord[i]))
                    still_identical = false;
            }
            
            if (still_identical)
                ret.push_back(i);
        }
            
        return ret;
        
    }
    
    
    std::pair<unsigned int, double> ExtractIdenticalComponentIndex(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1ul);
        std::vector<std::vector<double>> coords_component_list;
        
        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());
        
        const auto& last_coord = coords_component_list.back();
        
        const unsigned int Ncomponent = static_cast<unsigned int>(coords_component_list.back().size());
        
        #ifndef NDEBUG
        for (auto coords_component : coords_component_list)
            assert(static_cast<unsigned int>(coords_component.size()) == Ncomponent);
        #endif // NDEBUG
        
        for (unsigned int i = 0u; i < Ncomponent; ++i)
        {
            bool still_identical = true;
            
            for (unsigned int j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j][i], last_coord[i]))
                    still_identical = false;
            }
            
            if (still_identical)
            {
                #ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractIdenticalComponentIndexes(coords);
                assert("When the current function is called it is implicitly expected to be for a face of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1ul);
                assert(output_more_generic_function.back() == i);
                #endif // NDEBUG
                return std::make_pair(i, last_coord[i]);
            }
        }
        
        assert(false);
        return std::make_pair(NumericNS::UninitializedIndex<unsigned int>(), std::numeric_limits<double>::lowest());

    }
    
    
    
    bool operator==(const LocalCoords& coords1, const LocalCoords& coords2)
    {
        const auto& components1 = coords1.GetCoordinates();
        const auto& components2 = coords2.GetCoordinates();

        const std::size_t Ncomponent = components1.size();
        assert(components1.size() == components2.size());
        
        for (std::size_t i = 0ul; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(components1[i], components2[i]))
                return false;
        }
        
        return true;
    }
    
    
    double Distance(const LocalCoords& point1, const LocalCoords& point2)
    {
        double sum = 0.;
        
        assert(point1.GetDimension() == point2.GetDimension());
        const auto dimension = point1.GetDimension();
        
        for (auto i = 0u; i < dimension; ++i)
            sum += NumericNS::Square(point1[i] - point2[i]);
        
        return std::sqrt(sum);
        
    }


} // namespace MoReFEM



namespace std
{


    std::ostream& operator<<(std::ostream& stream, const MoReFEM::LocalCoords& point)
    {
        point.Print(stream);
        return stream;
    }

    
} // namespace std


/// @} // addtogroup GeometryGroup
