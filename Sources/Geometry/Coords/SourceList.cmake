target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coords.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Coords.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Coords.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalCoords.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SpatialPoint.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
