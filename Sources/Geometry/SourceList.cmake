target_sources(${MOREFEM_GEOMETRY}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/Mesh/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Interpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/RefGeometricElt/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/GeometricElt/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Coords/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Domain/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Interfaces/SourceList.cmake)
