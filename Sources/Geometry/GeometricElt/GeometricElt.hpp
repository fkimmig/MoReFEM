///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HPP_

# include <string>
# include <array>
# include <set>
# include <vector>
# include <iosfwd>
# include <memory>

# include "ThirdParty/Wrappers/Libmesh/Libmesh.hpp"

# include "Utilities/Exceptions/Exception.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Domain/MeshLabel.hpp"
# include "Geometry/Interfaces/Instances/Vertex.hpp"
# include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
# include "Geometry/Interfaces/Instances/OrientedFace.hpp"
# include "Geometry/Interfaces/Instances/Volume.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
# include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================

    class GeometricMeshRegion;
    class Domain;
    class RefGeomElt;
    class LocalCoords;


    namespace PostProcessingNS
    {


        class OutputDeformedMesh;


    } // namespace PostProcessingNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================





    /*!
     * \brief Generic class handling geometric element.
     *
     * \attention By design a GeometricElt is automatically related to a GeometricMeshRegion object.
     *
     * This abstract class is meant to be used polymorphically: all geometrical geometric elements should derive from
     * this one. As a matter of fact, the dependancy is not direct: a class in Internal::MeshNS namespace named TGeometricElt
     * defines all the pure virtual here (TGeometricElt stands for "templatized geometric element").
     *
     * GeometricElt objects are expected to be defined within a mesh context: for instance their index_ is the
     * index of the current GeometricElt is used to label the geometric element within the mesh.
     *
     * If you need to use a type rather than a specific geometric element (for instance in the mesh you have to
     * store in a list the different types of geometric element involved) use rather RefGeomElt.
     */
    class GeometricElt : public std::enable_shared_from_this<GeometricElt>
    {
    public:

        //! Alias for unique pointer.
        using unique_ptr = std::unique_ptr<GeometricElt>;

        //! Alias for shared pointer.
        using shared_ptr = std::shared_ptr<GeometricElt>;

        //! Alias for vector of pointers
        using vector_shared_ptr = std::vector<shared_ptr>;


        //! Friendship to allow call to SetCoordsList().
        friend class PostProcessingNS::OutputDeformedMesh;


        /// \name Special members.
        ///@{


        /*!
         * \class doxygen_hide_geom_elt_mesh_id_constructor_arg
         *
         * \param[in] mesh_unique_id Unique id of the mesh for which the \a GeometricElt is built.
         */

        /*!
         * \class doxygen_hide_geom_elt_mesh_coords_list_constructor_arg
         *
         * \param[in] mesh_coords_list List of all \a Coords known by the mesh. \a coords argument will specify the
         * position in this vector of the relevant values for the current \a GeometricElt.
         */


        /*!
         * \class doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_constructor_arg
         * \param[in] coords_indexes Indexes of the \a Coords to pick from \a mesh_coords_list.
         */


        /*!
         * \brief Minimal constructor: only the GeometricMeshRegion unique id is given.
         *
         * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
         */
        explicit GeometricElt(unsigned int mesh_unique_id);

        /*!
         * \brief Constructor from vector.
         *
         * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
         * \param[in] index Index associated to the geometric element in the mesh.
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         */
        explicit GeometricElt(unsigned int mesh_unique_id,
                              const Coords::vector_unique_ptr& mesh_coords_list,
                              unsigned int index,
                              std::vector<unsigned int>&& coords_indexes);

        /*!
         * \brief Constructor from vector; index is left undefined.
         *
         * \copydoc doxygen_hide_geom_elt_mesh_id_constructor_arg
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * Index should be specified through dedicated method #SetIndex
         */
        explicit GeometricElt(unsigned int mesh_unique_id,
                              const Coords::vector_unique_ptr& mesh_coords_list,
                              std::vector<unsigned int>&& coords_indexes);

        //! Destructor
        virtual ~GeometricElt();

        //! Recopy constructor - disabled.
        explicit GeometricElt(const GeometricElt&) = delete;

        //! Move constructor.
        explicit GeometricElt(GeometricElt&&) = delete;

        //! Copy affectation.
        GeometricElt& operator=(const GeometricElt&) = delete;

        //! Move affectation.
        GeometricElt& operator=(GeometricElt&&) = delete;



        ///@}



        /*!
         * \class doxygen_hide_geometric_elt_Ncoords_method
         *
         * \brief Get the number of \a Coords object required to characterize completely a GeometricElt of this type.
         *
         * For instance 27 for an Hexahedron27.
         *
         * \return Number of \a Coords object required to characterize completely a GeometricElt of this type.
         */

        //! \copydoc doxygen_hide_geometric_elt_Ncoords_method
        virtual unsigned int Ncoords() const = 0;

        //! Get the number of vertice.
        virtual unsigned int Nvertex() const = 0;

        //! Get the number of edges.
        virtual unsigned int Nedge() const = 0;

        //! Get the number of faces.
        virtual unsigned int Nface() const = 0;

        //! Get the dimension of the geometric element.
        virtual unsigned int GetDimension() const = 0;

        //! Get the identifier of the geometric element.
        virtual Advanced::GeometricEltEnum GetIdentifier() const = 0;

        //! Get a reference to the reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const = 0;

        //! Get the string name of the geometric element (e.g. 'Triangle3', 'Segment2', etc...).
        virtual const std::string GetName() const = 0;

        //! Set the index.
        void SetIndex(unsigned int index);

        //! Get the index.
        inline unsigned int GetIndex() const;

        /*!
         * \brief Get the identifier of the mesh.
         *
         * You shouldn't have to use this: it is used mostly to implement the comparison operators.
         *
         * \return Index that tags the mesh.
         */
        unsigned int GetMeshIdentifier() const;

        //! Set the label.
        void SetMeshLabel(const MeshLabel::const_shared_ptr& mesh_label);

        //! Get the label.
        MeshLabel::const_shared_ptr GetMeshLabelPtr() const;

        //! Get the coords as a vector.
        const Coords::vector_raw_ptr& GetCoordsList() const;

        //! Get the ith coords index.
        const Coords& GetCoord(unsigned int i) const;

        /*!
         * \brief Return the list of vertice.
         *
         * \return The list of vertice (as smart pointers).
         */
        const Vertex::vector_shared_ptr& GetVertexList() const;

        /*!
         * \brief Return the list of edges.
         *
         * This method assumes the edges were already built by the mesh; if not an exception is thrown.
         *
         * \return The list of edges (as smart pointers).
         */
        const OrientedEdge::vector_shared_ptr& GetOrientedEdgeList() const;

        /*!
         * \brief Return the list of faces.
         *
         * This method assumes the faces were already built by the mesh; if not an exception is thrown.
         *
         * \return The list of faces (as smart pointers).
         */
        const OrientedFace::vector_shared_ptr& GetOrientedFaceList() const;

        /*!
         * \brief Return the volume (as an interface).
         *
         * This method assumes the volume were already built by the mesh; if not an exception is thrown.
         *
         * \return The volume interface. Might be nullptr!
         */
        Volume::shared_ptr GetVolumePtr() const;

        /*!
         * \brief Set the list of vertice.
         *
         */
        void SetVertexList(Vertex::vector_shared_ptr&& vertex_list);

        /*!
         * \brief Set the list of edges.
         *
         */
        void SetOrientedEdgeList(OrientedEdge::vector_shared_ptr&& edge_list);

        /*!
         * \brief Set the list of faces.
         *
         */
        void SetOrientedFaceList(OrientedFace::vector_shared_ptr&& face_list);

        /*!
         * \brief Set the volume interface.
         *
         * \param[in] volume New value to set.
         *
         */
        void SetVolume(const Volume::shared_ptr& volume);


        /*!
         * \class doxygen_hide_geom_elt_build_interface_ptr
         *
         * \param[in] geom_elt_ptr Pointer to current \a GeometricElt; must point to same object as this. This is
         * rather clumsy, but I lack the time currently to fix something that is clumsy but ultimately not broken.
         */



        /*!
         * \brief Build a new interface, regardless of the orientation.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] interface_list List of \a InterfaceT under construction when current method is called.
         * If interface is not yet there it is added at the end of the method call.
         */
        template<class InterfaceT>
        void BuildInterface(const GeometricElt* geom_elt_ptr,
                            typename InterfaceT::InterfaceMap& interface_list);


    public:

        //! \name Shape function methods.
        //@{

        //! Return the value of the i-th shape function on given point.
        virtual double ShapeFunction(unsigned int index_node, const LocalCoords& local_coords) const = 0;

        //! Return the value of the icoor-th derivative of the i-th shape function on given point.
        virtual double FirstDerivateShapeFunction(unsigned int index_node, unsigned int index_coor,
                                                  const LocalCoords& local_coords) const = 0;

        //! Return the value of the (icoor, jcoor)-th second derivative of the i-th shape function on given point.
        virtual double SecondDerivateShapeFunction(unsigned int index_node,
                                                   unsigned int index_coor1,
                                                   unsigned int index_coor2,
                                                   const LocalCoords& local_coords) const = 0;

        //@}



    private:


        /*!
         * \brief Build the list of all vertices that belongs to the geometric element.
         *
         * \param[in,out] existing_list List of all vertice that have been built so far for the enclosing geometric
         * mesh. If a vertex already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         *
         */
        virtual void BuildVertexList(const GeometricElt* geom_elt_ptr,
                                     Vertex::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all edges that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all edges that have been built so far for the enclosing geometric
         * mesh. If an edge already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
        */
        virtual void BuildEdgeList(const GeometricElt* geom_elt_ptr,
                                   Edge::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all faces that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all faces that have been built so far for the enclosing geometric
         * mesh. If a face already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
         */
        virtual void BuildFaceList(const GeometricElt* geom_elt_ptr,
                                   Face::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all faces that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all faces that have been built so far for the enclosing geometric
         * mesh. If a face already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
         * \internal <b><tt>[internal]</tt></b> As there is at most one volume by GeometricElt, and this volume can't therefore be shared
         * with others geometric elements, we could have handled it differently from other interfaces. But the
         * little efficiency we could have gained would have to be paid with maintainance cost; hence the choice
         * to use a same mechanism for all the itnerfaces.
         */
        virtual void BuildVolumeList(const GeometricElt* geom_elt_ptr,
                                     Volume::InterfaceMap& existing_list) = 0;



    public:

        //! Ensight-related methods.
        //@{

        /*!
         * \brief Get the ensight name of the geometric element (e.g. 'tria3', 'bar2', etc...)
         *
         * An exception is thrown if Medit format is not supported.
         *
         * \return Ensight name (acts as an identifier for Ensight).
         */
        virtual const std::string& GetEnsightName() const = 0;


        /*!
         * \brief Write the geometric element in Ensight format.
         *
         * \param[in,out] stream Stream to which the GeometricElt is written.
         * \param[in] do_print_index True if the geometric element is preceded by its internal index.
         */
        virtual void WriteEnsightFormat(std::ostream& stream, bool do_print_index) const = 0;

        //@}


    public:

        //! Medit-related methods.
        //@{

        /*!
         * \brief Get the identifier Medit use to tag the geometric element.
         *
         * An exception is thrown if Medit format is not supported.
         *
         * \return Identifier.
         */
        virtual GmfKwdCod GetMeditIdentifier() const = 0;


        /*!
         * \brief Write the geometric elementin Medit format.
         *
         * \param[in] mesh_index Index that was returned by Medit when the output file was opened.
         * \param[in] processor_wise_reindexing Medit requires the indexes run from 1 to Ncoord on each processor;
         * this container maps the Coords index to the processor-wise one to write in the Medit file.
         *
         * The method is indeed an underlying call to the Medit API.
         */
        virtual void WriteMeditFormat(int mesh_index,
                                      const std::unordered_map<unsigned int, int>& processor_wise_reindexing) const = 0;


        /*!
         * \class doxygen_hide_geometric_elt_read_medit_method
         *
         * \brief Read the geometric element in Medit format.
         *
         * \param[in] libmesh_mesh_index Index that was returned by Medit when the input file was opened.
         * \param[in] Ncoord_in_mesh Total number of coord in the mesh. Used for a check upon file validity.
         * \param[out] reference_index Reference of the label, as libmesh int index. The calling function has the
         * responsability to use (or not) this value and Set the label pointer in GeometricElt object.
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_constructor_arg
         *
         * The method is indeed an underlying call to the Medit API.
         */

        //! \copydoc doxygen_hide_geometric_elt_read_medit_method
        virtual void ReadMeditFormat(const Coords::vector_unique_ptr& mesh_coords_list,
                                     int libmesh_mesh_index,
                                     unsigned int Ncoord_in_mesh,
                                     int& reference_index) = 0;

    protected:


        /*!
         * \brief Set the coord list from the vector given in input.
         *
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * Beware: an error will be raised if the input vector size is not what is expected !
         * Note: it is not passed by reference of purpose, to let compiler optimize it by copy elision
         */
        void SetCoordsList(const Coords::vector_unique_ptr& mesh_coords_list,
                           std::vector<unsigned int>&& coords_indexes);


        /*!
         * \brief Tells the object edges or faces have been built.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         */
        template<InterfaceNS::Nature InterfaceT>
        void SetInterfaceListBuilt();




    private:

        /*!
         * \brief Identifier of the mesh.
         */
        const unsigned int mesh_identifier_;

        /*!
         * \brief Index associated to the current geometric element.
         *
         * This index is assumed to be unique for a given GeometricMeshRegion: no other geometric element should bear
         * the same index (GeometricMeshRegion enforces that when the mesh is read).
         *
         * This uniqueness is important: indexes are used to compare elements, and also to define a hash
         * function when GeometricElt are put in an unordered_map.
         *
         * However, a check upon the mesh is also required: same index might be used by two different meshes for
         * two unrelated geometric elements.
         */
        unsigned int index_ = NumericNS::UninitializedIndex<decltype(index_)>();

        //! Surface reference to which the geometric element belongs to.
        MeshLabel::const_shared_ptr mesh_label_ = nullptr;

        //! Coords that belong to the geometric element.
        Coords::vector_raw_ptr coords_list_;

        //! List of vertice.
        Vertex::vector_shared_ptr vertex_list_;

        //! List of edges.
        OrientedEdge::vector_shared_ptr oriented_edge_list_;

        //! List of faces.
        OrientedFace::vector_shared_ptr oriented_face_list_;

        //! Volume (as an interface).
        Volume::shared_ptr volume_ = nullptr;

        /*!
         * \brief Knows whether a given type of interface has been built or not.
         */
        InterfaceNS::Nature higher_interface_type_built_ = InterfaceNS::Nature::undefined;

    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * The underlying index is the criterion used here: two geometric elements are equal if their index is the same.
     */
    bool operator==(const GeometricElt& lhs, const GeometricElt& rhs);


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * As for operator==, underlying index is used to perform the comparison.
     */
    bool operator<(const GeometricElt& lhs, const GeometricElt& rhs);



    namespace GeometricEltNS
    {


        namespace SortingCriterion
        {


            //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
            template<class StrictOrderingOperatorT = Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>>
            struct SurfaceRef
            {
                //! Call the appropriate method of GeometricElt that returns the label.
                inline static MeshLabel::const_shared_ptr Value(const GeometricElt::shared_ptr& ptr);

                //! Operator used for the sorting criterion.
                using StrictOrderingOperator = StrictOrderingOperatorT;
            };


            //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
            template<class StrictOrderingOperatorT = std::less<unsigned int> >
            struct Dimension
            {
                //! Call the appropriate method of GeometricElt that returns the dimension.
                inline static unsigned int Value(const GeometricElt::shared_ptr& ptr);

                //! Operator used for the sorting criterion.
                using StrictOrderingOperator = StrictOrderingOperatorT;
            };

            //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
            template<class StrictOrderingOperatorT = std::less<Advanced::GeometricEltEnum>>
            struct Type
            {
                //! Call the appropriate method of GeometricElt that returns the type of geometric element.
                inline static Advanced::GeometricEltEnum Value(const GeometricElt::shared_ptr& ptr);

                //! Operator used for the sorting criterion.
                using StrictOrderingOperator = StrictOrderingOperatorT;
            };


        } // namespace SortingCriterion


    } // namespace GeometricEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



namespace std
{


    //! Provide hash function for GeometricElt::shared_ptr.
    template<>
    struct hash<MoReFEM::GeometricElt::shared_ptr>
    {
    public:

        //! Overload.
        std::size_t operator()(const MoReFEM::GeometricElt::shared_ptr& geo_ptr) const
        {
            assert(!(!geo_ptr));
            return std::hash<unsigned int>()(geo_ptr->GetIndex());
        }
    };


}




# include "Geometry/GeometricElt/GeometricElt.hxx"

#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HPP_
