///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HXX_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HXX_



namespace MoReFEM
{


    inline const Coords::vector_raw_ptr& GeometricElt::GetCoordsList() const
    {
        return coords_list_;
    }


    inline unsigned int GeometricElt::GetIndex() const
    {
        if (index_ == NumericNS::UninitializedIndex<decltype(index_)>())
            throw Exception("GeometricElt not properly initialized: index got default value", __FILE__, __LINE__);

        return index_;
    }


    inline unsigned int GeometricElt::GetMeshIdentifier() const
    {
        assert(mesh_identifier_ != NumericNS::UninitializedIndex<decltype(mesh_identifier_)>());
        return mesh_identifier_;
    }


    inline const Vertex::vector_shared_ptr& GeometricElt::GetVertexList() const
    {
        return vertex_list_;
    }


    inline const OrientedEdge::vector_shared_ptr& GeometricElt::GetOrientedEdgeList() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::edge)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("edge", __FILE__, __LINE__);

        return oriented_edge_list_;
    }


    inline const OrientedFace::vector_shared_ptr& GeometricElt::GetOrientedFaceList() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::face)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("face", __FILE__, __LINE__);

        return oriented_face_list_;
    }


    inline Volume::shared_ptr GeometricElt::GetVolumePtr() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::volume)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("volume", __FILE__, __LINE__);

        assert((!(!volume_)) || GetDimension() < 3u);

        return volume_;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::vertex>()
    {
        higher_interface_type_built_ = InterfaceNS::Nature::vertex;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::edge>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::vertex)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("edge", "vertex", __FILE__, __LINE__);

        higher_interface_type_built_ = InterfaceNS::Nature::edge;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::face>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::edge)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("face", "edge", __FILE__, __LINE__);

        higher_interface_type_built_ = InterfaceNS::Nature::face;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::volume>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::face)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("volume", "face", __FILE__, __LINE__);

        higher_interface_type_built_ = InterfaceNS::Nature::volume;
    }


    inline void GeometricElt::SetVertexList(Vertex::vector_shared_ptr&& vertex_list)
    {
        vertex_list_ = std::move(vertex_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::vertex>();
    }


    inline void GeometricElt::SetOrientedEdgeList(OrientedEdge::vector_shared_ptr&& oriented_edge_list)
    {
        oriented_edge_list_ = std::move(oriented_edge_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::edge>();
    }


    inline void GeometricElt::SetOrientedFaceList(OrientedFace::vector_shared_ptr&& face_list)
    {
        oriented_face_list_ = std::move(face_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::face>();
    }


    inline void GeometricElt::SetVolume(const Volume::shared_ptr& volume_ptr)
    {
        volume_ = volume_ptr; // might be nullptr
        SetInterfaceListBuilt<InterfaceNS::Nature::volume>();
    }



    inline void GeometricElt::SetMeshLabel(const MeshLabel::const_shared_ptr& label)
    {
        mesh_label_ = label;
    }


    inline MeshLabel::const_shared_ptr GeometricElt::GetMeshLabelPtr() const
    {
        return mesh_label_;
    }


    inline void GeometricElt::SetIndex(unsigned int index)
    {
        index_ = index;
    }


    inline bool operator<(const GeometricElt& element1, const GeometricElt& element2)
    {
        assert(element1.GetMeshIdentifier() == element2.GetMeshIdentifier()
               && "At the moment I consider it doesn't make sense to compare GeometricElt from two "
               "different GeometricMeshRegion objects.");

        return element1.GetIndex() < element2.GetIndex();
    }


    inline bool operator==(const GeometricElt& element1, const GeometricElt& element2)
    {
        assert(element1.GetMeshIdentifier() == element2.GetMeshIdentifier()
               && "At the moment I consider it doesn't make sense to compare GeometricElt from two "
               "different GeometricMeshRegion objects.");

        return element1.GetIndex() == element2.GetIndex();
    }


    namespace GeometricEltNS
    {


        namespace SortingCriterion
        {


            template<class StrictOrderingOperatorT>
            inline MeshLabel::const_shared_ptr SurfaceRef<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
            {
                return ptr->GetMeshLabelPtr();
            }



            template<class StrictOrderingOperatorT>
            inline unsigned int Dimension<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
            {
                return ptr->GetDimension();
            };


            template<class StrictOrderingOperatorT>
            inline Advanced::GeometricEltEnum Type<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
            {
                return ptr->GetIdentifier();
            }


        } // namespace SortingCriterion


    } // namespace GeometricEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_GEOMETRIC_ELT_HXX_
