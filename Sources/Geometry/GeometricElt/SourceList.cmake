target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GeometricElt.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GeometricElt.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricElt.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
