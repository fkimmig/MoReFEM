target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeJacobian.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FreeFunctions.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltEnum.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltFactory.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TGeometricElt.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TGeometricElt.hxx" / 
)

