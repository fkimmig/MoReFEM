target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegion.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ComputeColoring.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegion.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegion.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
