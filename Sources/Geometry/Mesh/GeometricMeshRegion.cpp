///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <numeric>
#include <unordered_map>
#ifndef NDEBUG
# include <algorithm>
#endif // NDEBUG

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PointerComparison.hpp"

#include "Core/Enum.hpp"

#include "Geometry/Mesh/GeometricMeshRegion.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"
#include "Geometry/Mesh/Internal/Format/Ensight.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"

#include "Geometry/RefGeometricElt/Instances/Triangle/Triangle3.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"


namespace MoReFEM
{
    
    
    const std::string& GeometricMeshRegion::ClassName()
    {
        static std::string ret("GeometricMeshRegion");
        return ret;
    }
    
    
    GeometricMeshRegion::~GeometricMeshRegion() = default;

    
    GeometricMeshRegion::GeometricMeshRegion(const unsigned int unique_id,
                                             const std::string& mesh_file,
                                             unsigned int dimension,
                                             MeshNS::Format format,
                                             const double space_unit,
                                             GeometricMeshRegion::BuildEdge do_build_edge,
                                             GeometricMeshRegion::BuildFace do_build_face,
                                             GeometricMeshRegion::BuildVolume do_build_volume,
                                             GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals)
    : unique_id_parent(unique_id),
    dimension_(dimension),
    space_unit_(space_unit)
    {
        GeometricElt::vector_shared_ptr unsort_element_list;
        Coords::vector_unique_ptr coords_list;
        MeshLabel::vector_const_shared_ptr mesh_label_list;
        
        Read(mesh_file,
             format,
             space_unit,
             unsort_element_list,
             coords_list,
             mesh_label_list);
        
        Construct(std::move(unsort_element_list),
                  std::move(coords_list),
                  std::move(mesh_label_list),
                  do_build_edge,
                  do_build_face,
                  do_build_volume,
                  do_build_pseudo_normals);
    }
    
    
    GeometricMeshRegion::GeometricMeshRegion(const unsigned int unique_id,
                                             unsigned int dimension,
                                             const double space_unit,
                                             GeometricElt::vector_shared_ptr&& unsort_element_list,
                                             Coords::vector_unique_ptr&& coords_list,
                                             MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                                             GeometricMeshRegion::BuildEdge do_build_edge,
                                             GeometricMeshRegion::BuildFace do_build_face,
                                             GeometricMeshRegion::BuildVolume do_build_volume,
                                             GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals)
    : unique_id_parent(unique_id),
    dimension_(dimension),
    space_unit_(space_unit)
    {
        Construct(std::move(unsort_element_list),
                  std::move(coords_list),
                  std::move(mesh_label_list),
                  do_build_edge,
                  do_build_face,
                  do_build_volume,
                  do_build_pseudo_normals);
    }
    
    
    void GeometricMeshRegion::ReduceLabelList()
    {
        auto& label_list = GetNonCstLabelList();
        
        label_list.clear();
        
        Utilities::PointerComparison::Set<MeshLabel::const_shared_ptr> label_to_keep;
        
        const auto& geometric_elt_list = GetGeometricEltList();
        
        for (const auto& geometric_elt_ptr : geometric_elt_list)
        {
            assert(!(!geometric_elt_ptr));
            
            label_to_keep.insert(geometric_elt_ptr->GetMeshLabelPtr());
            
            const auto& coords_list_of_geometric_element = geometric_elt_ptr->GetCoordsList();
            
            for (const auto& coord_ptr : coords_list_of_geometric_element)
            {
                assert(!(!coord_ptr));
                label_to_keep.insert(coord_ptr->GetMeshLabelPtr());
            }
        }
        
        label_list.resize(label_to_keep.size());
        std::move(label_to_keep.begin(), label_to_keep.end(),
                  label_list.begin());
    }
    
    
    namespace // anonymous
    {
        
        
        /*!
         * \brief Return list of the indexes of the \a Coords in the list.
         */
        std::vector<unsigned int> GenerateIndexList(const Coords::vector_raw_ptr& coords_list)
        {
            std::vector<unsigned int> ret(coords_list.size());
            const auto end_coords_list = coords_list.cend();
            
            std::transform(coords_list.cbegin(),
                           end_coords_list,
                           ret.begin(),
                           [](const auto& coords_ptr)
                           {
                               assert(!(!coords_ptr));
                               return coords_ptr->GetIndex();
                           });
            
            assert(ret.size() == coords_list.size());
            
            Utilities::EliminateDuplicate(ret); // also sort the list
            assert(ret.size() == coords_list.size()
                   && "Input list should not include duplicate elements.");
            
            return ret;
        }
        
        
        /*!
         * \brief Create the new unique_ptr list given the indexes to cherry-pick in the original one.
         */
        Coords::vector_unique_ptr GenerateReducedList(const std::vector<unsigned int>& index_list,
                                                      Coords::vector_unique_ptr& list_before_reduction)
        {
            Coords::vector_unique_ptr ret;
            
            const auto begin_index_list = index_list.cbegin();
            const auto end_index_list = index_list.cend();
            
            const auto size = list_before_reduction.size();
            
            for (auto i = 0ul; i < size; ++i)
            {
                auto& coords_ptr = list_before_reduction[i];
                
                if (!coords_ptr)
                    continue;
                
                const auto coords_id = coords_ptr->GetIndex();
                
                if (std::binary_search(begin_index_list,
                                       end_index_list,
                                       coords_id))
                {
                    ret.emplace_back(std::move(coords_ptr));
                    list_before_reduction[i] = nullptr;
                }
            }
            
            assert(ret.size() == index_list.size() && "That's the whole point of the function!");
            
            assert(std::none_of(ret.cbegin(),
                                ret.cend(),
                                Utilities::IsNullptr<Coords::unique_ptr>));
            
            return ret;
        }

        
    } // namespace anonymous
    
    
    
    void GeometricMeshRegion::SetReducedCoordsList(const Wrappers::Mpi& mpi,
                                                   Coords::vector_raw_ptr&& processor_wise_coords_list,
                                                   Coords::vector_raw_ptr&& ghosted_coords_list)
    {
        auto& program_wise_coords_list = GetNonCstProcessorWiseCoordsList(); // still encompasses all program-wise
        // \a Coords at this stage.
        
        const auto Nprogram_wise_coords = static_cast<unsigned int>(program_wise_coords_list.size());
        
        // Here I need to keep only the \a Coords related to the processor; but there are hoops to jump to take
        // care or the nature of the pointer (raw or unique).
        // It's not very efficient, but done only once in the code.
        
        auto processor_wise_coords_id_list = GenerateIndexList(processor_wise_coords_list);
        
        const auto Nproc = mpi.Nprocessor<unsigned int>();
        
        // Prepare the list of \a Coords already considered processor-wise on lower ranks; this will be helpful
        // whenever we want to count how many \a Coords are in a \a Domain without counting some twice.
        std::vector<unsigned int> already_found_on_lower_ranks;
        
        if (Nproc > 1u)
        {
            const auto rank = mpi.GetRank<unsigned int>();
        
            if (rank == 0)
            {
                mpi.SendContainer(1,
                                  processor_wise_coords_id_list);
            }
            else if (rank < Nproc - 1u)
            {
                already_found_on_lower_ranks = mpi.Receive<unsigned int>(rank - 1u, Nprogram_wise_coords);
                
                std::vector<unsigned int> to_send = already_found_on_lower_ranks;
                to_send.insert(to_send.end(), processor_wise_coords_id_list.begin(), processor_wise_coords_id_list.end());
                
                Utilities::EliminateDuplicate(to_send);
                
                mpi.SendContainer(rank + 1u,
                                  to_send);
            }
            else
                already_found_on_lower_ranks = mpi.Receive<unsigned int>(rank - 1u, Nprogram_wise_coords);
        }
        
        auto ghosted_coords_id_list = GenerateIndexList(ghosted_coords_list);
        
        #ifndef NDEBUG
        {
            std::vector<unsigned int> intersection;
            std::set_intersection(processor_wise_coords_id_list.begin(),
                                  processor_wise_coords_id_list.end(),
                                  ghosted_coords_id_list.begin(),
                                  ghosted_coords_id_list.end(),
                                  std::back_inserter(intersection));
            
            assert(intersection.empty() && "Ghosted coords list is assumed not to encompass Coords already handled "
                   "by processor-wise list.");
            
        }
        #endif // NDEBUG
        
        
        
        auto processor_wise_coords_unique_ptr_list = GenerateReducedList(processor_wise_coords_id_list, program_wise_coords_list);
        SetGhostedCoordsList(GenerateReducedList(ghosted_coords_id_list, program_wise_coords_list));
        
        const auto Nprocessor_wise_coords = processor_wise_coords_unique_ptr_list.size();
        
        
        
        const auto begin_already_found_on_lower_ranks = already_found_on_lower_ranks.cbegin();
        const auto end_already_found_on_lower_ranks = already_found_on_lower_ranks.cend();
        
        auto index = 0ul;
        for (; index < Nprocessor_wise_coords; ++index)
        {
            auto& coords_ptr = processor_wise_coords_unique_ptr_list[static_cast<std::size_t>(index)];
            assert(!(!coords_ptr));
            auto& coords = *coords_ptr;
            coords.SetPositionInCoordsListInMesh<MpiScale::processor_wise>(static_cast<unsigned int>(index));
            
            const auto current_coords_index = coords.GetIndex();
            
            const auto it_lowest_rank =
                std::find(begin_already_found_on_lower_ranks,
                          end_already_found_on_lower_ranks,
                          current_coords_index);
            
            coords.SetLowestProcessor(it_lowest_rank == end_already_found_on_lower_ranks);
        }
        
        // By convention, ghost coords indexing follow the same indexing (i.e. first ghosted Coords gets
        // Nprocessor_wise_coords index).
        decltype(auto) ghosted_coords_unique_ptr_list = GetGhostedCoordsList();
        
        for (const auto& coords_ptr : ghosted_coords_unique_ptr_list)
        {
            assert(!(!coords_ptr));
            // index is the same as in previous loop!
            coords_ptr->SetPositionInCoordsListInMesh<MpiScale::processor_wise>(static_cast<unsigned int>(index++));
        }
        
        std::swap(program_wise_coords_list, processor_wise_coords_unique_ptr_list);
    }

    
    void GeometricMeshRegion::ShrinkToProcessorWise(const Wrappers::Mpi& mpi,
                                                    const GeometricElt::vector_shared_ptr& processor_wise_geo_element,
                                                    Coords::vector_raw_ptr&& processor_wise_coords_list,
                                                    Coords::vector_raw_ptr&& ghosted_coords_list,
                                                    unsigned int Nprocessor_wise_vertex,
                                                    unsigned int Nprocessor_wise_edge,
                                                    unsigned int Nprocessor_wise_face,
                                                    unsigned int Nprocessor_wise_volume)
    {
        geometric_elt_list_.Clear();
        geometric_elt_list_.Init(processor_wise_geo_element, false);

        SetReducedCoordsList(mpi,
                             std::move(processor_wise_coords_list),
                             std::move(ghosted_coords_list));

        ReduceLabelList();

        Nprocessor_wise_vertex_ = Nprocessor_wise_vertex;
        Nedge_ = Nprocessor_wise_edge;
        Nface_ = Nprocessor_wise_face;
        Nvolume_ = Nprocessor_wise_volume;
    }
    
    
    void GeometricMeshRegion::Read(const std::string& mesh_file,
                                   MeshNS::Format format,
                                   const double space_unit,
                                   GeometricElt::vector_shared_ptr& unsort_element_list,
                                   Coords::vector_unique_ptr& coords_list,
                                   MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {        
        unsigned int dimension_read = NumericNS::UninitializedIndex<unsigned int>();
        
        
        switch(format)
        {
            case MeshNS::Format::Medit:
                Internal::MeshNS::FormatNS::Medit::ReadFile(GetUniqueId(),
                                                            mesh_file,
                                                            space_unit,
                                                            dimension_read,
                                                            unsort_element_list,
                                                            coords_list,
                                                            mesh_label_list);
                                                            
                break;
            case MeshNS::Format::Ensight:
                
                Internal::MeshNS::FormatNS::Ensight::ReadFile(GetUniqueId(),
                                                              mesh_file,
                                                              space_unit,
                                                              dimension_read,
                                                              unsort_element_list,
                                                              coords_list,
                                                              mesh_label_list);
                
                break;
            case MeshNS::Format::VTK_PolygonalData:
                assert(false);
                break;
            case MeshNS::Format::End:
                assert(false);
                break;
        }
        
        
        if (dimension_read < GetDimension())
            throw Exception("Dimension given in the input file is higher than the one read in the mesh file " + mesh_file,
                            __FILE__, __LINE__);

      
    }
    
    
    void GeometricMeshRegion::PrintGeometricEltsInLabel(std::ostream& out) const
    {
        const auto& label_list = GetLabelList();
        
        for (const auto& surf : label_list)
        {
            out  << "Surface " << surf->GetIndex() << " -> " << std::endl;
            auto geo_elements_in_surf = GeometricEltListInLabel(surf);
            
            {
                namespace sc = GeometricEltNS::SortingCriterion;
                std::stable_sort(geo_elements_in_surf.begin(), geo_elements_in_surf.end(),
                                 Utilities::Sort<GeometricElt::shared_ptr, sc::Type<>>);
            }
            
            unsigned int Nsame_geo_element = 0u;
            Advanced::GeometricEltEnum id(Advanced::GeometricEltEnum::Undefined);
            
            std::string name;
            
            for (auto geo_element : geo_elements_in_surf)
            {
                if (geo_element->GetIdentifier() == id)
                    ++Nsame_geo_element;
                else
                {
                    if (id != Advanced::GeometricEltEnum::Undefined)
                        out << '\t' << Nsame_geo_element << ' ' << name << std::endl;
                    
                    name = geo_element->GetName();
                    
                    Nsame_geo_element = 1u;
                    id = geo_element->GetIdentifier();
                }
            }
            
            if (Nsame_geo_element)
                out << '\t' << Nsame_geo_element << ' ' << name << std::endl;
        }
    }
    
    
    # ifndef NDEBUG
    void GeometricMeshRegion::CheckGeometricEltOrdering() const
    {
        auto copy_geometric_elt_list = GetGeometricEltList();
        
        namespace sc = GeometricEltNS::SortingCriterion;
        
        std::stable_sort(copy_geometric_elt_list.begin(), copy_geometric_elt_list.end(),
                         Utilities::Sort<GeometricElt::shared_ptr, sc::Dimension<>, sc::Type<>, sc::SurfaceRef<>>);
        
        assert(copy_geometric_elt_list == GetGeometricEltList());
    }
    # endif // NDEBUG
    
    
    
    namespace // anonymous
    {
        
        
        /*!
         * \brief Helper function to create a vector of geometric elements given the position of the
         * first element in it and the number of elements considered.
         *
         * \param[in] pair Pair <first_element, Nelt>
         * \param[in] geometric_elt_list List of all geometric elements that belongs to the mesh.
         * GeometricMeshRegion::geometric_elt_list_ is expected here.
         *
         * \return Pair <begin, end> that returns all the geometric elements meeting the requirements.
         */
        GeometricMeshRegion::subset_range SubsetRange(const std::pair<unsigned int, unsigned int>& pair,
                                                      const Internal::MeshNS::GeometricEltList& geometric_elt_list)
        {
            unsigned int first_element, Nelt;
            std::tie(first_element, Nelt) = pair;
            
            const auto& vector_geometric_element = geometric_elt_list.All();
            assert(first_element + Nelt <= vector_geometric_element.size());
            
            using difference_type = GeometricMeshRegion::iterator_geometric_element::difference_type;
            
            GeometricMeshRegion::iterator_geometric_element it_begin =
            vector_geometric_element.cbegin() + static_cast<difference_type>(first_element);
            GeometricMeshRegion::iterator_geometric_element it_end = it_begin + static_cast<difference_type>(Nelt);
            
            GeometricMeshRegion::subset_range ret({it_begin, it_end});
            
            return ret;
        }
        
        
        
        
    } // namespace anonymous
    
    
    
    GeometricMeshRegion::subset_range GeometricMeshRegion::GetSubsetGeometricEltList(const RefGeomElt& geometric_type) const
    {
        auto pair = geometric_elt_list_.GetLocationInEltList(geometric_type);
        return SubsetRange(pair, geometric_elt_list_);
    }
    
    
    GeometricMeshRegion::subset_range GeometricMeshRegion::GetSubsetGeometricEltList(const RefGeomElt& geometric_type,
                                                                                     unsigned int label_index) const
    {
        std::pair<unsigned int, unsigned int> position_of_subset;
        if (geometric_elt_list_.GetLocationInEltList(geometric_type, label_index, position_of_subset))
            return SubsetRange(position_of_subset, geometric_elt_list_);
        
        // If none, return a pair of end iterators.
        auto end = geometric_elt_list_.All().cend();
        
        return { end, end };
    }
    
    
    GeometricMeshRegion::subset_range GeometricMeshRegion::GetSubsetGeometricEltList(unsigned int dimension) const
    {
        auto pair = geometric_elt_list_.GetLocationInEltList(dimension);
        return SubsetRange(pair, geometric_elt_list_);
    }
     
    
    template<>
    unsigned int GeometricMeshRegion::DetermineNInterface<Volume>() const
    {
        const auto& geometric_elt_list = GetGeometricEltList();
        
        unsigned int count = 0;
        
        for (auto geometric_elt_ptr : geometric_elt_list)
        {
            assert(!(!geometric_elt_ptr));
            
            if (geometric_elt_ptr->GetVolumePtr() != nullptr)
                ++count;
        }
        
        return count;
    }
    
    
    void WriteInterfaceList(const std::pair<unsigned int, std::string>& mesh_infos)
    {        
        std::ofstream out;
        
        decltype(auto) mesh_output_directory = mesh_infos.second;
        
        assert(FilesystemNS::Folder::DoExist(mesh_output_directory) && "Assert because it should be guaranteed by Model");
        
        std::string filename = mesh_output_directory + "/interfaces.hhdata";
        
        decltype(auto) mesh = Internal::MeshNS::GeometricMeshRegionManager::GetInstance().GetMesh(mesh_infos.first);
        
        FilesystemNS::File::Create(out, filename, __FILE__, __LINE__);
        
        Internal::MeshNS::ComputeInterfaceListInMesh mesh_interface_list(mesh);
        mesh_interface_list.Print(out);
    }
    
    
    template<>
    void GeometricMeshRegion::Write<MeshNS::Format::Ensight>(const std::string& mesh_file) const
    {
        return Internal::MeshNS::FormatNS::Ensight::WriteFile(*this,
                                                              mesh_file);
    }
    
    
    template<>
    void GeometricMeshRegion::Write<MeshNS::Format::Medit>(const std::string& mesh_file) const
    {
        constexpr int version = 2; // namely double precision - no weird stuff to handle very big files not handled
                                   // afterwards by Ensight.
        
        return Internal::MeshNS::FormatNS::Medit::WriteFile(*this,
                                                              mesh_file,
                                                              version);
    }
    
    
    template<>
    void GeometricMeshRegion::Write<MeshNS::Format::VTK_PolygonalData>(const std::string& mesh_file) const
    {
        return Internal::MeshNS::FormatNS::VTK_PolygonalData::WriteFile(*this,
                                                                        mesh_file);
    }
    
    
    void GeometricMeshRegion::Construct(GeometricElt::vector_shared_ptr&& a_unsort_element_list,
                                        Coords::vector_unique_ptr&& a_coords_list,
                                        MeshLabel::vector_const_shared_ptr&& a_mesh_label_list,
                                        GeometricMeshRegion::BuildEdge do_build_edge,
                                        GeometricMeshRegion::BuildFace do_build_face,
                                        GeometricMeshRegion::BuildVolume do_build_volume,
                                        GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals)

    {
        {
            auto& coords_list = GetNonCstProcessorWiseCoordsList();
            assert(coords_list.empty());
            coords_list = std::move(a_coords_list);
            
            unsigned int index = 0u;
            
            for (auto& coords_ptr : coords_list)
            {
                assert(!(!coords_ptr));
                coords_ptr->SetPositionInCoordsListInMesh<MpiScale::program_wise>(index++);
            }

        }
        
        {
            auto& mesh_label_list = GetNonCstLabelList();
            assert(mesh_label_list.empty());
            mesh_label_list = std::move(a_mesh_label_list);
            
            std::sort(mesh_label_list.begin(), mesh_label_list.end(),
                      Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());
        }
        
        geometric_elt_list_.Init(a_unsort_element_list, false);
        
        Vertex::InterfaceMap vertex_interface_list;
        Edge::InterfaceMap edge_interface_list;
        Face::InterfaceMap face_interface_list;
        Volume::InterfaceMap volume_interface_list;
        
        BuildInterface<Vertex>(vertex_interface_list);
        Nprogram_wise_vertex_ = Nvertex();
        
        if (do_build_edge == BuildEdge::yes)
            BuildInterface<Edge>(edge_interface_list);
        
        if (do_build_face == BuildFace::yes)
            BuildInterface<Face>(face_interface_list);
        
        if (do_build_volume == BuildVolume::yes)
            BuildInterface<Volume>(volume_interface_list);
        
        if (do_build_pseudo_normals == BuildPseudoNormals::yes)
        {
            assert(do_build_edge == BuildEdge::yes);
            assert(do_build_face == BuildFace::yes);
            
            // For now I take all the labels as we are still not able to really do it thanks to a given domain at this point.
            std::vector<unsigned int> label_list_index;
            
            const auto& mesh_label_list = GetLabelList();
            
            const unsigned int mesh_label_list_size = static_cast<unsigned int>(mesh_label_list.size());
            
            assert(std::none_of(mesh_label_list.cbegin(),
                                mesh_label_list.cend(),
                                Utilities::IsNullptr<MeshLabel::const_shared_ptr>));
            
            for (unsigned int i = 0u; i < mesh_label_list_size; ++i)
            {
                const auto& mesh_label = *mesh_label_list[i];
                label_list_index.push_back(mesh_label.GetIndex());
            }
            
            ComputePseudoNormals(label_list_index,
                                 vertex_interface_list,
                                 edge_interface_list,
                                 face_interface_list);
        }
    }
    
    
    void GeometricMeshRegion::ComputePseudoNormals(const std::vector<unsigned int>& label_list_index,
                                                   const Vertex::InterfaceMap& vertex_interface_list,
                                                   const Edge::InterfaceMap& edge_interface_list,
                                                   const Face::InterfaceMap& face_interface_list)
    {
        const unsigned int label_index_list_size = static_cast<unsigned int>(label_list_index.size());
        
        for (unsigned int label_index = 0 ; label_index < label_index_list_size ; ++label_index)
        {            
            decltype(auto) geometric_type =
            Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
            
            auto subset = GetSubsetGeometricEltList(geometric_type,
                                                    label_list_index[label_index]);
            
            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);
                
                auto begin = face_interface_list.cbegin();
                auto end = face_interface_list.cend();
                
                geom_elt.GetOrientedFaceList();
                
                const auto it = std::find_if(begin,
                                             end,
                                             [&geom_elt](const auto& face)
                                             {
                                                 assert(!(!face.first));
                                                 
                                                 const auto& face_coord = face.first->GetVertexCoordsList();
                                                 const auto& geom_elt_coord = geom_elt.GetCoordsList();
                                                 const auto face_size = face_coord.size();
                                                 const auto geom_elt_size = geom_elt_coord.size();
                                                 
                                                 assert(geom_elt_size == face_size);
                                                 
                                                 int found = 0;
                                                 
                                                 for (unsigned int j = 0u; j < face_size; ++j)
                                                 {
                                                     auto& face_item = *face_coord[j];
                                                     
                                                     for (unsigned int k = 0u; k < geom_elt_size; ++k)
                                                     {
                                                         auto& geom_elt_item = *geom_elt_coord[k];
                                                         
                                                         if (face_item == geom_elt_item)
                                                         {
                                                             ++found;
                                                         }
                                                     }
                                                 }
                                                 
                                                 return found == 3 ? true : false;
                                             });
                
                if (!(it == end))
                    it->first->ComputePseudoNormal(it->second);
                else
                    throw Exception("A face was not found in the geometric elements. This should not happen.", __FILE__, __LINE__);
            }
        }
        
        //std::cout << Face::NumberOfFacesPseudoNormalComputed() << '\t' << "pseudo-normals for Faces." << std::endl;
        
        for (unsigned int label_index = 0 ; label_index < label_index_list_size ; ++label_index)
        {
            decltype(auto) geometric_type =
            Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
            
            auto subset = GetSubsetGeometricEltList(geometric_type,
                                                    label_list_index[label_index]);
            
            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);
                
                const auto& edge_list = geom_elt.GetOrientedEdgeList();
                
                const auto edge_list_size = edge_list.size();
                
                for (unsigned int j = 0 ; j < edge_list_size ; ++j)
                {
                    auto& edge = *edge_list[j];
                    auto it = edge_interface_list.find(edge_list[j]->GetUnorientedInterfacePtr());
                    assert(it != edge_interface_list.cend());
                    const auto& geom_elemts = it->second;
                    edge.GetNonCstUnorientedInterface().ComputePseudoNormal(geom_elemts);
                }
            }
        }
        
        //std::cout << Edge::NumberOfEdgesPseudoNormalComputed() << '\t' << "pseudo-normals for Edges." << std::endl;
        
        for (unsigned int label_index = 0 ; label_index < label_index_list_size ; ++label_index)
        {
            decltype(auto) geometric_type =
            Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
            
            auto subset = GetSubsetGeometricEltList(geometric_type,
                                                    label_list_index[label_index]);
            
            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);
                    
                const auto& vertex_list = geom_elt.GetVertexList();
                
                const auto vertex_list_size = vertex_list.size();
                
                for (unsigned int j = 0 ; j < vertex_list_size ; ++j)
                {
                    auto& vertex = *vertex_list[j];
                    auto it = vertex_interface_list.find(vertex_list[j]);
                    assert(it != vertex_interface_list.cend());
                    const auto& geom_elemts = it->second;
                    vertex.ComputePseudoNormal(geom_elemts);
                }
            }
        }
    }
    
    
    void GeometricMeshRegion::SetGhostedCoordsList(Coords::vector_unique_ptr&& list)
    {
        ghosted_coords_list_ = std::move(list);
    }

    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
