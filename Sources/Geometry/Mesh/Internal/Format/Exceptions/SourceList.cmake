target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Medit.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format_fwd.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Medit.hpp" / 
)

