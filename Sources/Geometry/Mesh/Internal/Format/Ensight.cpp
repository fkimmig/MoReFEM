///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <fstream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iostream>


#include "Utilities/Containers/Sort.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/GeometricMeshRegion.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Coords/Internal/Factory.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace MeshNS
        {
            
            
            namespace FormatNS
            {
                
                
                const std::string& Informations<::MoReFEM::MeshNS::Format::Ensight>::Name()
                {
                    static std::string ret("Ensight");
                    return ret;
                }
                
                
                const std::string& Informations<::MoReFEM::MeshNS::Format::Ensight>::Extension()
                {
                    static std::string ret("geo");
                    return ret;
                }
                
                             
                
                namespace Ensight
                {

                
                    // Declarations here; definitions (and comments) in a namespace at the end of this file
                    namespace // anonymous
                    {

                        namespace FormatExceptionNS = ::MoReFEM::ExceptionNS::Format;

                        namespace EnsightExceptionNS = FormatExceptionNS::Ensight;

                        
                        enum class CoordOrElt
                        {
                            coord,
                            element
                        };
                        
                        
                        enum class CoordOrEltStatus
                        {
                            offOrAssign, // at the moment, no reason to make a difference between both
                            given,
                            ignore
                        };
                        
                        
                        CoordOrEltStatus DetermineNodeOrEltStatus(std::ifstream& file_in,
                                                                  CoordOrElt CoordOrElt,
                                                                  const std::string& mesh_file);
                        
                        
                        unsigned int ReadNumberCoords(std::ifstream& file_in);
                        
                        template<CoordOrEltStatus CoordOrEltStatusT>
                        void ReadCoordsHelper(std::istream& file_in,
                                              double space_unit,
                                              Coords::vector_unique_ptr& coord_list);
                        
                        void ReadCoords(std::ifstream& file_in,
                                        const std::string& mesh_file,
                                        double space_unit,
                                        CoordOrEltStatus coord_or_elt_status,
                                        Coords::vector_unique_ptr& coord_list);
                        
                        template<CoordOrEltStatus CoordOrEltStatusT>
                        void ReadBlockHelper(std::istream& file_in,
                                             const std::string& geometric_elt_name,
                                             std::map<unsigned int, GeometricElt::vector_shared_ptr>& geometric_elt_list,
                                             const Coords::vector_unique_ptr& coords_list,
                                             const MeshLabel::const_shared_ptr& label);
                        
                        MeshLabel::const_shared_ptr EnsightReadLabel(unsigned int mesh_id,
                                                                     GeometricElt::vector_shared_ptr& geometric_elt_list,
                                                                     const Coords::vector_unique_ptr& coords_list,
                                                                     const std::string& mesh_file,
                                                                     std::istream& stream, CoordOrEltStatus elementStatus);
                        
                        void FlushBlock(std::ostream& file_out,
                                        const std::map<std::string, GeometricElt::vector_shared_ptr>& current_block,
                                        const MeshLabel::const_shared_ptr& current_label,
                                        bool do_print_index,
                                        unsigned int& part_index);
                        
                        void WriteLabels(std::ostream& file_out,
                                         const GeometricElt::vector_shared_ptr& geometric_elt_list,
                                         bool do_print_index);
                        
                    } // anonymous namespace
                    
                    
                    
                    void ReadFile(const unsigned int mesh_id,
                                  const std::string& mesh_file,
                                  double space_unit,
                                  unsigned int& dimension,
                                  GeometricElt::vector_shared_ptr& unsort_geom_element_list,
                                  Coords::vector_unique_ptr& coords_list,
                                  MeshLabel::vector_const_shared_ptr& mesh_label_list)
                    {
                        dimension = 3; // Ensight read all its coords as dimension 3.
                        
                        std::ifstream file_in(mesh_file);
                        
                        if (!file_in)
                            throw FormatExceptionNS::UnableToOpenFile(mesh_file, __FILE__, __LINE__);
                        
                        std::cout << "Reading from file "<< mesh_file << std::endl;
                        
                        // Skip the first two lines
                        std::streamsize maxStreamSize = std::numeric_limits<std::streamsize>::max();
                        
                        file_in.ignore(maxStreamSize, '\n');
                        file_in.ignore(maxStreamSize, '\n');
                        
                        // Determine coord and element status with next two lines
                        CoordOrEltStatus coord_or_elt_status = DetermineNodeOrEltStatus(file_in, CoordOrElt::coord, mesh_file);
                        CoordOrEltStatus elementStatus = DetermineNodeOrEltStatus(file_in, CoordOrElt::element, mesh_file);
                        
                        // Skip next line (which is "coordinates")
                        file_in.ignore(maxStreamSize, '\n');
                        
                        // Read all the coords
                        // Check whether the mesh is actually 2D or 3D (Ensight format is always three-dimensional,
                        // with third component being always 0 for 2D problems)
                        ReadCoords(file_in, mesh_file, space_unit, coord_or_elt_status, coords_list);
                        
                        for (const auto& coords_ptr : coords_list)
                        {
                            assert(!(!coords_ptr));
                            coords_ptr->SetInterfaceNature(::MoReFEM::InterfaceNS::Nature::vertex);  // \todo #248: Not always true with P2 mesh!
                        }
                        
                        // Now read all the geometric geometric elements blocks
                        while (file_in)
                            mesh_label_list.push_back(EnsightReadLabel(mesh_id,
                                                                       unsort_geom_element_list,
                                                                       coords_list,
                                                                       mesh_file,
                                                                       file_in,
                                                                       elementStatus));
                        
                        assert(file_in.eof());
                        
                        
                    }
                    
                    
                    void WriteFile(const GeometricMeshRegion& mesh,
                                   const std::string& mesh_file)
                    {
                        std::ofstream file_out(mesh_file);
                        
                        if (!file_out)
                            throw FormatExceptionNS::UnableToOpenFile(mesh_file, __FILE__, __LINE__);
                        
                        std::cout << "Writing to file "<< mesh_file << std::endl;
                        
                        // Write the first two lines
                        for (unsigned int i = 0u; i < 2u; ++i)
                            file_out << "Geometry file\n";
                        
                        // Write coords and element related lines
                        file_out << "node id assign\n";
                        file_out << "element id assign\n";
                        
                        // Fifth line is "coordinates"
                        file_out << "coordinates\n";
                        
                        // Sixth one is the number of coords; respect the Ensight convention of width of 8
                        decltype(auto) coords_list = mesh.GetProcessorWiseCoordsList();
                        Coords::vector_unique_ptr::size_type Ncoords = coords_list.size();
                        file_out << std::setw(8) << Ncoords << std::endl;
                        
                        // Now write all coords
                        for (unsigned int i = 0u; i < Ncoords; ++i)
                            WriteEnsightFormat<false>(*coords_list[i], file_out);
                        
                        // Finally write the labels. Ensure first the geometric elements are sort the right way!
                        GeometricElt::vector_shared_ptr list_to_sort = mesh.GetGeometricEltList();
                        
                        namespace sc = GeometricEltNS::SortingCriterion;
                        std::stable_sort(list_to_sort.begin(), list_to_sort.end(),
                                         Utilities::Sort<GeometricElt::shared_ptr, sc::SurfaceRef<>>);
                        
                        WriteLabels(file_out, list_to_sort, false);
                    }
                    
                    
                    
                    // Definitions
                    namespace // anonymous
                    {
                        
                        
                        /*
                         * \brief Utility to read third and fourth lines of the mesh file.
                         *
                         * The expected format for them is:
                         * node id *status*
                         * element id *status*
                         * where status is among the following: off/given/assign/ignore.
                         *
                         * \param[in] line Line considered.
                         * \param[in] CoordOrElt Whether we are considering the line concerning the coord or the element.
                         * \param[in] mesh_file Name of the mesh file (only for logging purposes).
                         */
                        CoordOrEltStatus DetermineNodeOrEltStatus(std::ifstream& file_in,
                                                                  CoordOrElt coord_or_element,
                                                                  const std::string& mesh_file)
                        {
                            std::string buf;
                            std::string expected;
                            unsigned int line_index;
                            
                            // First string should be either "node" or "element"
                            getline(file_in, buf, ' ');
                            
                            switch (coord_or_element)
                            {
                                case CoordOrElt::coord:
                                    expected = "node";
                                    line_index = 3u;
                                    break;
                                case CoordOrElt::element:
                                    expected = "element";
                                    line_index = 4u;
                                    break;
                            }
                            
                            // Ignore GCC warning about uninitialized dimension_read (should another format be added to the code
                            // that another warning would say the switch doesn't cover all cases).
                            PRAGMA_DIAGNOSTIC(push)
                            #ifdef MOREFEM_GCC
                            PRAGMA_DIAGNOSTIC(ignored "-Wmaybe-uninitialized")
                            #endif // MOREFEM_GCC
                            
                            if (buf != expected)
                                throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file,
                                                                                   line_index,
                                                                                   __FILE__, __LINE__);
                            
                            // Second string should be id
                            getline(file_in, buf, ' ');
                            expected = "id";
                            
                            if (buf != expected)
                                throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file,
                                                                                   line_index,
                                                                                   __FILE__, __LINE__);
                            
                            // Third string should be among <off/given/assign/ignore>
                            getline(file_in, buf);
                            
                            if (buf == "off" || buf == "assign")
                                return CoordOrEltStatus::offOrAssign;
                            
                            if (buf == "given")
                                return CoordOrEltStatus::given;
                            
                            if (buf == "ignore")
                                return CoordOrEltStatus::ignore;
                            
                            throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file, line_index, __FILE__, __LINE__);
                            
                            PRAGMA_DIAGNOSTIC(pop)
                        }
                        
                        
                        // Get the number of coords (sixth line)
                        unsigned int ReadNumberCoords(std::ifstream& file_in)
                        {
                            std::string line;
                            getline(file_in, line);
                            return static_cast<unsigned int>(std::stoul(line));
                        }
                        
                        
                        /*
                         * \brief Read an ensight line that describes a coord and store it
                         *
                         * \tparam CoordOrEltStatusT Status of the nodes read, read earlier in Ensight file
                         *
                         * For coord lines, the format is:
                         *     - *ID* *X* *Y* *Z* for status = ignore or given
                         *     - *X* *Y* *Z* for status = off or assign
                         *
                         * \param[in,out] file_in Stream being read; function stops when its status is fail (ie last correct line read)
                         * \param[out] coord_list List of all coords read
                         */
                        template<CoordOrEltStatus CoordOrEltStatusT>
                        void ReadCoordsHelper(std::istream& file_in, const double space_unit, Coords::vector_unique_ptr& coord_list)
                        {
                            unsigned int index = 1u; // Ensight begins its numeration at 1
                            unsigned int ensight_index; // Ensight index, which will be hanled differently depending on template parameter
                            
                            while (true)
                            {
                                if (CoordOrEltStatusT == CoordOrEltStatus::ignore || CoordOrEltStatusT == CoordOrEltStatus::given) // compile time resolution
                                    file_in >> ensight_index;
                                
                                auto&& point_ptr = Internal::CoordsNS::Factory::FromStream(3u, file_in, space_unit);
                                auto& point = *point_ptr;
                                
                                if (!file_in)
                                    return;
                                
                                if (CoordOrEltStatusT == CoordOrEltStatus::given) // compile time resolution
                                    point.SetIndex(ensight_index);
                                else
                                    point.SetIndex(index++);
                                
                                coord_list.emplace_back(std::move(point_ptr));
                            }
                        }
                        
                        
                        /*
                         * \brief Read the coords
                         *
                         * \param[in,out] fileStream Stream that is reading the input file. Reading goes on until the item read
                         * is not a coord; the status of the stream is cleant so that reading can continue (file doesn't end
                         * with coords...)
                         * \param[in] mesh_file Name of the mesh file (only for logging purposes)
                         * \param{in] coord_or_elt_status Status of the coord. For 'ignore' and 'given' an additional column (the id) has to be read
                         * \param[out] coord_list List of all coords read
                         *
                         *
                         */
                        void ReadCoords(std::ifstream& file_in, const std::string& mesh_file, const double space_unit, CoordOrEltStatus coord_or_elt_status,
                                        Coords::vector_unique_ptr& coord_list)
                        {
                            assert(coord_list.empty());
                            auto Ncoords = ReadNumberCoords(file_in);
                            coord_list.reserve(Ncoords);
                            
                            switch(coord_or_elt_status)
                            {
                                case CoordOrEltStatus::offOrAssign:
                                    ReadCoordsHelper<CoordOrEltStatus::offOrAssign>(file_in, space_unit, coord_list);
                                    break;
                                case CoordOrEltStatus::ignore:
                                    ReadCoordsHelper<CoordOrEltStatus::ignore>(file_in, space_unit, coord_list);
                                    break;
                                case CoordOrEltStatus::given:
                                    ReadCoordsHelper<CoordOrEltStatus::given>(file_in, space_unit, coord_list);
                                    break;
                            }
                            
                            if (Ncoords != coord_list.size())
                                throw EnsightExceptionNS::InvalidCoords(mesh_file, Ncoords, static_cast<unsigned int>(coord_list.size()), __FILE__, __LINE__);
                            
                            file_in.clear(); // reset the state, which was by construction failbit
                            
                            std::cout << Ncoords << " coords read in " << mesh_file << '\n';
                        }
                        
                        
                        
                        /*
                         * \brief Read an ensight line that describes an element of a block and store it
                         *
                         * \tparam CoordOrEltStatusT Status of the elements read, read earlier in Ensight file
                         *
                         * For element, it is:
                         *     - *ID* *coord* *coord* ... for status = ignore or given
                         *     - *coord* *coord* ... for status = off or assign
                         *
                         * \param[in,out] file_in Stream being read; function stops when its status is fail (ie last correct line read)
                         * \param[in] geometric_elt_name Name of the geometric element being read.
                         * \param[out] geometric_elt_list Key is the dimension, value is the list of elements.
                         * \param[in] Label::shared_ptr label Label to which the geometric elements in the block belong to.
                         *
                         */
                        template<CoordOrEltStatus CoordOrEltStatusT>
                        void ReadBlockHelper(const unsigned int mesh_unique_id,
                                             const std::string& geometric_elt_name,
                                             GeometricElt::vector_shared_ptr& geometric_elt_list,
                                             const Coords::vector_unique_ptr& coords_list,
                                             const MeshLabel::const_shared_ptr& label,
                                             std::istream& file_in)
                        {
                            // First one is used in the cases the file doesn't give indexes, and second one if on the contrary
                            // it does, in which case we keep Ensight internal convention.
                            static unsigned int assigned_ensight_index = 1u;
                            unsigned int read_ensight_index;
                            
                            const auto& geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::GetInstance();
                           
                            while (true)
                            {
                                if (CoordOrEltStatusT == CoordOrEltStatus::ignore || CoordOrEltStatusT == CoordOrEltStatus::given) // static if
                                    file_in >> read_ensight_index;
                                
                                GeometricElt::shared_ptr geometric_element(geometric_elt_factory.CreateFromEnsightName(mesh_unique_id,
                                                                                                                       coords_list,
                                                                                                                       geometric_elt_name,
                                                                                                                       file_in));
                                
                                // Exit immediately the function if stream state isn't good.
                                if (!file_in)
                                    return;
                                
                                assert("If stream is correct geometric elementshould be well defined" && geometric_element);
                                geometric_element->SetMeshLabel(label);
                                
                                switch(CoordOrEltStatusT) // choice at compile time
                                {
                                    case CoordOrEltStatus::given:
                                        geometric_element->SetIndex(read_ensight_index);
                                        break;
                                    case CoordOrEltStatus::offOrAssign:
                                    case CoordOrEltStatus::ignore:
                                        geometric_element->SetIndex(assigned_ensight_index++);
                                        break;
                                }
                                
                                geometric_elt_list.push_back(geometric_element);
                            }
                        }
                        
                        
                        /*
                         * \brief Read a block that defines a label.
                         *
                         * \param[in,out] geometric_elt_list The list of all geometric elements read in Ensight file so far, in input. Files related
                         * to label block being read are added in output.
                         * \param[in,out] stream Input stream from which the block is read.
                         * \param[in] mesh_file Ensight file being read (used only in case of error).
                         * \param[in] elementStatus How the indexes are handled in Ensight format (read earlier in the Ensight file).
                         *
                         * \return Pointer to a newly created object; nullptr if the block couldn't be read properly.
                         *
                         * An exception is thrown if any reading from the stream fails, except in the case the end of file was reached.
                         */
                        MeshLabel::const_shared_ptr EnsightReadLabel(const unsigned int mesh_id,
                                                                     GeometricElt::vector_shared_ptr& geometric_elt_list,
                                                                     const Coords::vector_unique_ptr& coords_list,
                                                                     const std::string& mesh_file,
                                                                     std::istream& stream, CoordOrEltStatus elementStatus)
                        {
                            unsigned int index = NumericNS::UninitializedIndex<unsigned int>();
                            std::string description;
                            
                            {
                                // First line should be "part " # where # is an index
                                std::string buf;
                                stream >> buf;
                                if (buf != "part")
                                {
                                    // If the end of Ensight file was reached, no problem
                                    if (stream.eof())
                                        return nullptr;
                                    
                                    throw EnsightExceptionNS::InvalidLabelBlock(mesh_file, buf, __FILE__, __LINE__);
                                }
                                
                                // Read index
                                stream >> index;
                            }
                            
                            {
                                // Second one should be a description string. Beware: finishing '\n' still in the buffer!
                                stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                                getline(stream, description);
                            }
                            
                            auto current_label = std::make_shared<MeshLabel>(mesh_id,
                                                                             index,
                                                                             std::move(description));
                            
                            const auto& geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::GetInstance();
                            
                            // In the following, we should get one block per geometric geometric elementpresent in the label
                            // Format is:
                            // - one line for the name of the geometric element considered.
                            // - one line to tell how many geometric elements are expected.
                            // - and then a line per geometric element of this kind in the label.
                            while (stream)
                            {
                                // Description of the geometric geometric elementthat will be read in Ensight format(e.g. "tria3"), and the number of objects considered
                                auto begin_block_position = stream.tellg();
                                std::string geometric_elt_name;
                                getline(stream, geometric_elt_name);
                                
                                // Exit the loop if end of file is reached
                                if (stream.eof())
                                    return current_label;
                                
                                if (geometric_elt_name.substr(0, 4) == "part")
                                {
                                    // Means we are beginning to scrutinize a new label
                                    stream.seekg(begin_block_position);
                                    return current_label;
                                }
                                else if (!geometric_elt_factory.IsEnsightName(geometric_elt_name))
                                    throw ::MoReFEM::ExceptionNS::Factory::GeometricElt
                                    ::InvalidEnsightGeometricEltName(geometric_elt_name,
                                                                     __FILE__, __LINE__);
                                else
                                {
                                    // Fourth line should be the number of geometric geometric elements described in the block
                                    unsigned int NgeometricElt;
                                    stream >> NgeometricElt;
                                    
                                    if (!stream)
                                        throw EnsightExceptionNS::InvalidNumberOfGeometricElts(mesh_file, __FILE__, __LINE__);
                                    
                                    // Next read the geometric geometric elements until the stream fails to do so
                                    const std::size_t initial_size = geometric_elt_list.size();
                                    
                                    switch(elementStatus)
                                    {
                                        case CoordOrEltStatus::offOrAssign:
                                            ReadBlockHelper<CoordOrEltStatus::offOrAssign>(mesh_id,
                                                                                           geometric_elt_name,
                                                                                           geometric_elt_list,
                                                                                           coords_list,
                                                                                           current_label,
                                                                                           stream);
                                            break;
                                        case CoordOrEltStatus::ignore:
                                            ReadBlockHelper<CoordOrEltStatus::ignore>(mesh_id,
                                                                                      geometric_elt_name,
                                                                                      geometric_elt_list,
                                                                                      coords_list,
                                                                                      current_label,
                                                                                      stream);
                                            break;
                                        case CoordOrEltStatus::given:
                                            ReadBlockHelper<CoordOrEltStatus::given>(mesh_id,
                                                                                     geometric_elt_name,
                                                                                     geometric_elt_list,
                                                                                     coords_list,
                                                                                     current_label,
                                                                                     stream);
                                            break;
                                    }
                                    
                                    if (initial_size + NgeometricElt != geometric_elt_list.size())
                                        throw EnsightExceptionNS::BadNumberOfEltsInLabel(mesh_file,
                                                                                                   index,
                                                                                                   description,
                                                                                                   geometric_elt_name,
                                                                                                   NgeometricElt,
                                                                                                   static_cast<unsigned int>(geometric_elt_list.size() - initial_size),
                                                                                                   __FILE__, __LINE__);
                                    
                                    stream.clear(); // reset the state, which was by construction failbit at this point
                                }
                            }
                            
                            return current_label;
                        }
                        
                        
                        /*!
                         * \brief Flush to output file the previous block read.
                         *
                         * This is intended to be an helper function of #WriteLabels defined below.
                         *
                         * \param[in,out] part_index Ensight writes parts numbered from 1 to Npart (DO NOT PUT 0: it leads to
                         * a segmentation faults when loading Ensight..). So this is an increased index, as we can't rely upon mesh label
                         * for this (Medit meshes labels may start at 0).
                         */
                        void FlushBlock(std::ostream& file_out,
                                        const std::map<std::string, GeometricElt::vector_shared_ptr>& current_block,
                                        const MeshLabel::const_shared_ptr& current_label,
                                        bool do_print_index,
                                        unsigned int& part_index)
                        {
                            if (!current_block.empty())
                            {
                                assert(current_label);
                                file_out << "part" << std::setw(8) << part_index++ << '\n';
                                file_out << current_label->GetDescription() << '\n';
                                
                                for (const auto& pair : current_block)
                                {
                                    file_out << pair.first << '\n';
                                    const auto& local_geometric_elt_list = pair.second;
                                    file_out << std::setw(8) << local_geometric_elt_list.size() << '\n';
                                    
                                    for (auto block_geom_elt : local_geometric_elt_list) // all geometric elements of the same geometric_type and same label
                                        block_geom_elt->WriteEnsightFormat(file_out, do_print_index);
                                }
                            }
                        }
                        
                        
                        /*
                         * /brief Write the part of Ensight file dedicated to labels
                         *
                         * \param[in] file_out The output stream to which the data are written
                         * \param[in] geometric_elt_list List of all geometric elements in the mesh. These geometric elements will be sort by label
                         * and types before being written (Ensight format is a list of blocks, with geometrical geometric elements put together
                         * in a block.
                         * So for instance in a block we'll have first all triangles3, then all tetra4, etc...
                         * \param[in] do_print_index True if the geometric elementis preceded by its internal index
                         */
                        void WriteLabels(std::ostream& file_out, const GeometricElt::vector_shared_ptr& geometric_elt_list,
                                         bool do_print_index)
                        {
                            MeshLabel::const_shared_ptr current_label(nullptr);
                            std::map<std::string, GeometricElt::vector_shared_ptr> current_block; // key is the geometric element considered
                            
                            unsigned int part_index = 1u;
                            
                            for (auto geometric_elt_ptr: geometric_elt_list)
                            {
                                assert(!(!geometric_elt_ptr));
                                const auto& geometric_element = *geometric_elt_ptr;
                                
                                if (geometric_element.GetMeshLabelPtr() != current_label)
                                {
                                    // Write to file the previous label block (if any)
                                    FlushBlock(file_out, current_block, current_label, do_print_index, part_index);
                                    
                                    // Prepare next block
                                    current_block.clear();
                                    
                                    // Beginning a new label block
                                    current_label = geometric_element.GetMeshLabelPtr();
                                }
                                
                                current_block[geometric_element.GetEnsightName()].push_back(geometric_elt_ptr);
                            }
                            
                            // Flush the last block in memory once the loop is done
                            FlushBlock(file_out, current_block, current_label, do_print_index, part_index);
                        }
                        
                    } // anonymous namespace
                    
                    
                } // namespace Ensight
                
                
            } // namespace FormatNS
            
            
        } // namespace MeshNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
