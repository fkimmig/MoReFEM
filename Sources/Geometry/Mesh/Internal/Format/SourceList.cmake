target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Medit.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VTK_PolygonalData.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Ensight.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Format.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Medit.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Medit.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/VTK_PolygonalData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VTK_PolygonalData.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Dispatch/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
