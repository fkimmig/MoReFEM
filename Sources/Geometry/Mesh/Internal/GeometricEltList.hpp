///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Apr 2013 15:06:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HPP_


# include <unordered_map>
# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            /*!
             * \brief Type useful to access quickly elements in the GeometricEltList class.
             *
             * Key of the map is index of the label.
             * Value of the map is a pair which elements are:
             *  - first: starting index to the elements having this label.
             *  - second: the number of elements having this label.
             */
            using geom_elt_per_label_id_type =
                std::unordered_map
                <
                    unsigned int,
                    std::pair<unsigned int, unsigned int>
                >;


            /*!
             * \brief Convenient alias over the underlying storage of the GeometricElt in GeometricEltList.
             *
             * See geometric_elt_lookup_helper_ of GeometricEltList for an explanation of the content of this
             * ugly container.
             *
             */
            using geom_elt_lookup_type =
                std::unordered_map
                <
                    MoReFEM::Advanced::GeometricEltEnum,
                    geom_elt_per_label_id_type
                >;


            /*!
             * \brief Helper class designed to be used within GeometricMeshRegion to store all the elements.
             *
             * GeometricElts need to be stored efficiently, but we also need to be able to access them quickly.
             *
             * The current class intends to be in charge of all these operations.
             *
             * The storage of GeometricElt (in data_) is done the following way:
             * . Criterion 1 is the dimension.
             * . Criterion 2 is the element type ("Triangle3", "Hexa6", etc...)
             * . Criterion 3 is the label to which the element belongs. So all triangles for a label which
             * index is *i* will be packed together.
             */
            class GeometricEltList final
            {


            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit GeometricEltList() = default;

                //! Destructor.
                ~GeometricEltList() = default;

                //! Copy constructor.
                GeometricEltList(const GeometricEltList&) = delete;

                //! Move constructor.
                GeometricEltList(GeometricEltList&&) = delete;

                //! Copy affectation.
                GeometricEltList& operator=(const GeometricEltList&) = delete;

                //! Move affectation.
                GeometricEltList& operator=(GeometricEltList&&) = delete;


                ///@}

                //! Clear the object.
                void Clear();

                //! Initialize from a list of geometric elements (a priori not yet sort).
                void Init(const GeometricElt::vector_shared_ptr& unsort_list, bool do_print);

                //! Number of geometric elements.
                unsigned int NgeometricElt() const;

                //! Number of geometric elements of a given dimension.
                unsigned int NgeometricElt(unsigned int dimension) const;

                /*!
                 * \brief Access to the list of all geometric elements.
                 *
                 * \return All geometric elements as a vector of smart pointers.
                 */
                const GeometricElt::vector_shared_ptr& All() const noexcept;

                /*!
                 * \brief Obtains the position in the internal storage of all GeometricElt that shares the same dimension.
                 *
                 * \param[in] dimension Dimension of the sought \a GeometricElt.
                 *
                 * \return Pair <first index, number of geometric elements that share the same dimension>.
                 */
                const std::pair<unsigned int, unsigned int>& GetLocationInEltList(unsigned int dimension) const;


                /*!
                 * \brief Obtains the position in the internal storage of all GeometricElt that shares the type
                 * of RefGeomElt.
                 *
                 * \param[in] geometric_type Type of the geometric element requested.
                 *
                 * \return   Pair <first index, number of geometric elements that share the same type, regardless
                 * of the label type>
                 *
                 * The elements themselves can be retrieved afterwards by the [] operator:
                 * \code
                 * GeometricEltList list;
                 * ... definition ...
                 * RefGeomElt geo_type(new Triangle3);
                 *
                 * auto all_elements_of_given_type = list.GetLocationInEltList(geo_type);
                 * unsigned int first_element = all_elements_of_given_type.first;
                 * unsigned int last_element = first_element + all_elements_of_given_type.second - 1u ;
                 * for (unsigned int i = first_element; i <= last_element; ++i)
                 *   ... access to each element through list[i] ...
                 * \endcode
                 *
                 *
                 */
                std::pair<unsigned int, unsigned int> GetLocationInEltList(const RefGeomElt& geometric_type) const;


                /*!
                 * \brief Same as the namesake method except we consider only elements that belongs to a given label.
                 *
                 * \param[in] geometric_type Type of the geometric element requested.
                 * \param[in] label_index Index of the label considered.
                 * \param[out] out Pair <first index, number of geometric elements that share the same type and label
                 *
                 * \return True if at least one element match the request, false if the label doesn't exist for the given
                 * geometric element type.
                 * In case the geometric type doesn't exist, an exception is thrown (because it highlights a discrepancy
                 * somewhere in the code; for the label it is different as we didn't enforce in detail how they are
                 * partitioned between all geometric element types).
                 *
                 */
                 bool GetLocationInEltList(const RefGeomElt& geometric_type,
                                           unsigned int label_index,
                                           std::pair<unsigned int, unsigned int>& out) const;


                /*!
                 * \brief Returns the pair (first index)in each label for a given type of geometric element.
                 *
                 * \param[in] geometric_type Instance of the type of geometric element considered (the default one is fine: only identified is important here).
                 *
                 * \return Key of the unordered map is the index of the label. Value is a pair: first value of which
                 * is the first index in the list and second value is the number of elements that share both the element type
                 * and the label.
                 */
                const geom_elt_per_label_id_type&
                GetLocationInEltListPerLabel(const RefGeomElt& geometric_type) const;


                /*!
                 * \brief Returns all the types of geometric elements in the object.
                 *
                 * \return Vector which include all RefGeomElts found in the object.
                 */
                RefGeomElt::vector_shared_ptr BagOfEltType() const;

                /*!
                 * \brief Same as namesake methods except we limit ourselves to a given dimension.
                 *
                 * \param[in] dimension Only objects of this dimension will be considered.
                 *
                 * \return Vector which include all RefGeomElts of a given dimension found in the object.
                 */
                RefGeomElt::vector_shared_ptr BagOfEltType(unsigned int dimension) const;


                /*!
                 * \brief Returns a list of all elements that belongs to the same label.
                 *
                 * \param[in] label Smart pointer standing for the label.
                 *
                 * \return The list as a vector of smart pointers.
                 */
                GeometricElt::vector_shared_ptr GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const;


                //! \copydoc doxygen_hide_geometric_mesh_region_get_geom_elt_from_index_1
                const GeometricElt& GetGeometricEltFromIndex(unsigned int index) const;


                //! \copydoc doxygen_hide_geometric_mesh_region_get_geom_elt_from_index_2
                const GeometricElt& GetGeometricEltFromIndex(unsigned int index,
                                                             const RefGeomElt& ref_geom_elt) const;



            private:

                /*!
                 * \brief The actual underlying data.
                 *
                 * Detail of the layout is given in the class header documentation.
                 */
                GeometricElt::vector_shared_ptr data_;


                /*!
                 * \brief A minimalist container to be able to fetch quickly all the geometric elements of a given kind.
                 *
                 * Key of the first map: identifier of the GeometricElt. (for instance 'Triangle3').
                 * Value of the first map is a second map.
                 * Key of the second map is index of the label.
                 * Value of the second map is a pair which elements are:
                 *  - first: starting index to the elements having this label
                 *  - second: the number of elements having this label.
                 *
                 * This quite ugly container is supposed to be purely internal; it shouldn't be left accessible to
                 * the public interface!
                 *
                 */
                geom_elt_lookup_type geometric_elt_lookup_helper_;


                //! Key is the dimension, value is the pair <First index, number of elements>.
                std::map<unsigned int, std::pair<unsigned int, unsigned int>> dimension_accessor_;


            };


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/GeometricEltList.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_ELT_LIST_HPP_
