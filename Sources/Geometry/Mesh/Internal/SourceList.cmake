target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegionManager.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Coloring.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeInterfaceListInMesh.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricEltList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegionManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GeometricMeshRegionManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormalsManager.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
