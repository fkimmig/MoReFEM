target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DistanceFromMesh.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DistanceFromMesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DistanceFromMesh.hxx" / 
)

