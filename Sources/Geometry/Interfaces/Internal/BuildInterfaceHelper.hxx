///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_



namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {



            template<class InterfaceT, class TopologyT>
            typename InterfaceT::vector_shared_ptr Build<InterfaceT, TopologyT>
            ::Perform(const GeometricElt* geom_elt_ptr,
                      const Coords::vector_raw_ptr& coords_in_geometric_elt,
                      typename InterfaceT::InterfaceMap& already_existing_interface_list)
            {
                constexpr bool is_relevant = (Ninterface() > 0);

                return PerformHelper(std::integral_constant<bool, is_relevant>(),
                                     geom_elt_ptr,
                                     coords_in_geometric_elt,
                                     already_existing_interface_list);
            }



            template<class InterfaceT, class TopologyT>
            typename InterfaceT::vector_shared_ptr Build<InterfaceT, TopologyT>
            ::PerformHelper(std::true_type,
                            const GeometricElt* geom_elt_ptr,
                            const Coords::vector_raw_ptr& coords_in_geometric_elt,
                            typename InterfaceT::InterfaceMap& already_existing_interface_list)
            {
                typename InterfaceT::vector_shared_ptr ret;
                constexpr unsigned int Ninterface = Build<InterfaceT, TopologyT>::Ninterface();

                for (unsigned int i = 0u; i < Ninterface; ++i)
                {
                    auto item_ptr = Impl::CreateNewInterface<InterfaceT, TopologyT>::Perform(coords_in_geometric_elt, i);

                    // Look whether the interface already existed; the criterion is to match or not the coord list
                    // (if 2 interfaces get the same list of coord then they are actually the same).
                    auto it = already_existing_interface_list.find(item_ptr);

                    if (it == already_existing_interface_list.cend())
                    {
                        // Give a unique index to the new interface: the number inside the container so far.
                        item_ptr->SetIndex(static_cast<unsigned int>(already_existing_interface_list.size()));

                        ret.push_back(item_ptr);
                        std::vector<const GeometricElt*> vector_raw;
                        vector_raw.push_back(geom_elt_ptr);
                        already_existing_interface_list.insert({item_ptr, vector_raw});
                    }
                    else
                    {
                        ret.push_back((*it).first); // put in the container the already existing interface.
                        (*it).second.push_back(geom_elt_ptr);
                    }
                }


                return ret;
            }


            template<class InterfaceT, class TopologyT>
            typename InterfaceT::vector_shared_ptr Build<InterfaceT, TopologyT>
            ::PerformHelper(std::false_type,
                            const GeometricElt* geom_elt_ptr,
                            const Coords::vector_raw_ptr& coords_in_geometric_elt,
                            typename InterfaceT::InterfaceMap& already_existing_interface_list)
            {
                static_cast<void>(coords_in_geometric_elt);
                static_cast<void>(already_existing_interface_list);
                static_cast<void>(geom_elt_ptr);
                typename InterfaceT::vector_shared_ptr ret;
                return ret;
            }



            template<class InterfaceT, class TopologyT>
            constexpr unsigned int Build<InterfaceT, TopologyT>::Ninterface()
            {
                return MoReFEM::InterfaceNS::Ninterface<TopologyT, InterfaceT::StaticNature()>::Value();
            }


            namespace Impl
            {


                template<class TopologyT>
                Vertex::shared_ptr CreateNewInterface<Vertex, TopologyT>::Perform(const Coords::vector_raw_ptr& coords_in_geometric_elt,
                                                                                  unsigned int local_vertex_index)
                {
                    const std::size_t index = static_cast<std::size_t>(local_vertex_index);
                    assert(index < coords_in_geometric_elt.size());

                    return std::make_shared<Vertex>(coords_in_geometric_elt[index]);
                }


                template<class TopologyT>
                Edge::shared_ptr CreateNewInterface<Edge, TopologyT>::Perform(const Coords::vector_raw_ptr& coords_in_geometric_elt,
                                                                              unsigned int local_edge_index)
                {
                    const auto& local_edge = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetEdge(local_edge_index);

                    return std::make_shared<Edge>(local_edge, coords_in_geometric_elt);
                }


                template<class TopologyT>
                Face::shared_ptr CreateNewInterface<Face, TopologyT>::Perform(const Coords::vector_raw_ptr& coords_in_geometric_elt,
                                                                              unsigned int local_face_index)
                {
                    const auto& local_face = RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index);

                    return std::make_shared<Face>(local_face, coords_in_geometric_elt);
                }


            } // namespace Impl


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_HELPER_HXX_
