target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceHelper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceListHelper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BuildInterfaceListHelper.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeOrientation.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeOrientation.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Orientation.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Orientation.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TInterface.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TInterface.hxx" / 
)

