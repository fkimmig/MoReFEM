target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Edge.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Face.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Volume.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Edge.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Edge.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Face.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Face.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/OrientedEdge.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OrientedEdge.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/OrientedFace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OrientedFace.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vertex.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Volume.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Volume.hxx" / 
)

