/*!
* \defgroup GeometryGroup Geometry
*
* \brief This module encompass purely geometric objects of the code, such as the mesh, geometric elements, domains and
* so forth.
*
*
*/


/// \addtogroup GeometryGroup
///@{

/// \namespace MoReFEM::DomainNS
/// \brief Namespace that enclose stuff related to Domain.

/// \namespace MoReFEM::Internal::MeshNS
/// \brief Namespace that enclose internals related to meshes.

/// \namespace MoReFEM::Internal::MeshNS::FormatNS
/// \brief Namespace that enclose internals related to the different file format supported for meshes.

/// \namespace MoReFEM::Internal::MeshNS::FormatNS::Medit
/// \brief Namespace that enclose internals related to Medit file format.

/// \namespace MoReFEM::Internal::MeshNS::FormatNS::Medit::Dispatch
/// \brief Namespace that enclose free functions used to dispatch Mesh methods depending on the format employed.

/// \namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight
/// \brief Namespace that enclose internals related to Medit file format.

/// \namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight::Dispatch
/// \brief Namespace that enclose free functions used to dispatch Mesh methods depending on the format employed.



/// \namespace MoReFEM::Internal::ColoringNS
/// \brief Namespace that enclose internals related to coloring of a mesh (in which two connected vertices must not
/// share the same color; this is useful for shared memory parallelism).



///@} // addtogroup


/*!
 * \class doxygen_hide_space_unit_arg
 *
 * \param[in] space_unit The unit of the mesh, in meters.
 */

