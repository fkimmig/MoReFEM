///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Aug 2014 14:43:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HXX_
# define MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace DomainNS
        {


            inline bool IsObjectInGeometricMeshRegion(const GeometricElt& geometric_elt,
                                                      unsigned int geometric_mesh_region_identifier)
            {
                return geometric_elt.GetMeshIdentifier() == geometric_mesh_region_identifier;
            }



            inline bool IsMeshLabelInList(const GeometricElt& geometric_elt,
                                          const MeshLabel::vector_const_shared_ptr& mesh_label_list)
            {
                return std::binary_search(mesh_label_list.cbegin(),
                                          mesh_label_list.cend(),
                                          geometric_elt.GetMeshLabelPtr(),
                                          Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());
            }


        } // namespace DomainNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HXX_
