target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Domain.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DomainManager.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MeshLabel.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/DomainManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DomainManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MeshLabel.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MeshLabel.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
