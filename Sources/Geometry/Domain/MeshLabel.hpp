///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HPP_
# define MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HPP_


# include <cassert>
# include <string>
# include <memory>
# include <vector>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricMeshRegion;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Depiction of a given label.
     *
     * This is mostly just a couple unique_id/description; the expected modus operandi is that a (smart) pointer to
     * the MeshLabel object will be passed to each element that belongs to it (geometric elements, edges, etc...)
     *
     * So MeshLabels objects should always be created through dynamic allocation:
     * \code
     * auto my_surface = std::make_shared<MeshLabel>(mesh, 42, "Left ventricle");
     * \endcode
     *
     * \attention If for some reason we need to know quickly and often all the geometric elements in a label,
     * this data should be stored as a vector of WEAK pointers, to avoid circular definitions.
     */
    class MeshLabel final
    {
    public:

        //! Typical smart pointer associated to the object.
        using const_shared_ptr = std::shared_ptr<const MeshLabel>;

        //! Vector of smart pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Name of the class (required by unique_id Crtp).
        static const std::string& ClassName();


    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor
         *
         * \param[in] geometric_mesh_region_id Identifier of the \a GeometricMeshRegion to which the label belongs to.
         * \param[in] index Identifier related to the label. This identifier should be unique for a given
         * \a GeometricMeshRegion (i.e. that a given mesh should get at most one MeshLabel object with a given
         * unique id).
         * \internal <b><tt>[internal]</tt></b> Crtp UniqueId can't be used used as same integer might be used in
         * different meshes.
         * \param[in] description Description of the label. Might be empty.
         *
         */
        explicit MeshLabel(unsigned int geometric_mesh_region_id,
                           unsigned int index,
                           std::string&& description);

        //! Destructor.
        ~MeshLabel();

        //! Copy constructor.
        MeshLabel(const MeshLabel&) = delete;

        //! Move constructor.
        MeshLabel(MeshLabel&&) = delete;

        //! Copy affectation.
        MeshLabel& operator=(const MeshLabel&) = delete;

        //! Move affectation.
        MeshLabel& operator=(MeshLabel&&) = delete;



        ///@}

        //! Get the description
        const std::string& GetDescription() const noexcept;

        //! Get the mesh identifier.
        unsigned int GetMeshIdentifier() const noexcept;

        //! Get the index.
        unsigned int GetIndex() const noexcept;


    private:

        //! Identifier of the mesh for which the label is defined.
        const unsigned int mesh_identifier_;

        /*!
         * \brief Identifier related to the label.
         *
         * This identifier should be unique for a given
         * \a GeometricMeshRegion (i.e. that a given mesh should get at most one MeshLabel object with a given
         * unique id).
         * \internal <b><tt>[internal]</tt></b> Crtp UniqueId can't be used used as same integer might be used in
         * different meshes.
         */
        const unsigned int index_;

        //! Description
        std::string description_;

    };


    /*!
     * \brief Operator== : two \a MeshLabel are equal if their unique identifiers (returned by GetUniqueId()) are the same.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return True if lhs.GetUniqueId() == rhs.GetUniqueId().
     */
    inline bool operator==(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;


    /*!
     * \brief Operator< : two \a MeshLabel are sort by comparing their unique identifiers (returned by GetUniqueId()).
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return True if lhs.GetUniqueId() < rhs.GetUniqueId().
     */
    bool operator<(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;


    /*!
     * \brief Operator!= : two \a MeshLabel are different if their unique identifiers (returned by GetUniqueId()) are not the same.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return True if lhs.GetUniqueId() != rhs.GetUniqueId().
     */
    inline bool operator!=(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Domain/MeshLabel.hxx"


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_MESH_LABEL_HPP_
