target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.hxx" / 
)

