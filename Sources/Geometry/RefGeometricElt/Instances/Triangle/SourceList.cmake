target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Triangle3.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Triangle6.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Triangle3.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Triangle6.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
