target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Segment2.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Segment3.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Segment2.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Segment3.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
