target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron10.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron4.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron10.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron10.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron4.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tetrahedron4.hxx" / 
)

