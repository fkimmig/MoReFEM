target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron20.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron27.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron8.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron20.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron27.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Hexahedron8.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Topology/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Format/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
