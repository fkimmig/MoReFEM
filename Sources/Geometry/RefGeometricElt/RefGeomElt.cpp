///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 10:04:38 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/RefGeometricElt/RefGeomElt.hpp"



namespace MoReFEM
{


    RefGeomElt::~RefGeomElt() = default;

      


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
