///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 19 Mar 2014 11:43:44 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HXX_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            template<class DerivedT, class ShapeFunctionTraitsT, class TopologyT>
            constexpr unsigned int RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::NshapeFunction()
            {
                return static_cast<unsigned int>(ShapeFunctionTraitsT::ShapeFunctionList().size());
            }


            template<class DerivedT, class ShapeFunctionTraitsT, class TopologyT>
            constexpr unsigned int RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::Ncoordinates()
            {
                return TopologyT::Nderivate_component_;
            }


            template<class TopologyT>
            LocalCoords ComputeBarycenter()
            {
                const auto& coords_list= TopologyT::GetVertexLocalCoordsList();
                const unsigned int dimension = TopologyT::dimension;

                const double inv_Ncoords = 1. / static_cast<double>(coords_list.size());

                std::vector<double> barycenter_coords(dimension, 0.);

                for (unsigned int i = 0u; i < dimension; ++i)
                {
                    double& current_barycenter_coord = barycenter_coords[i];

                    for (const auto& coords : coords_list)
                        current_barycenter_coord += inv_Ncoords * coords[i];
                }

                return LocalCoords(barycenter_coords);
            }


            template<class DerivedT, class ShapeFunctionTraitsT, class TopologyT>
            const LocalCoords& RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::GetBarycenter()
            {
                static LocalCoords barycenter = ComputeBarycenter<TopologyT>();
                return barycenter;
            }


            template<class DerivedT, class ShapeFunctionTraitsT, class TopologyT>
            double RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>
            ::SecondDerivateShapeFunction(unsigned int i, unsigned int icoor, unsigned int jcoor,
                                          const LocalCoords& local_coords)
            {
                auto Ncoor = ShapeFunctionTraitsT::Nderivate_component_;
                assert(i < NshapeFunction() && icoor < Ncoor && jcoor < Ncoor);
                return ShapeFunctionTraitsT::SecondDerivateShapeFunctionList()[(i * Ncoor + icoor) * Ncoor + jcoor](local_coords);
            }


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_REF_GEOM_ELT_IMPL_HXX_
