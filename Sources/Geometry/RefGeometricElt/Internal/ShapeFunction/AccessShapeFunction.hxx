///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 12:26:00 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HXX_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HXX_


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {


            namespace Crtp
            {


                template<class DerivedT>
                double AccessShapeFunction<DerivedT>::ShapeFunction(unsigned int local_node_index,
                                                                    const LocalCoords& local_coords)
                {
                    const auto& shape_function_list = DerivedT::ShapeFunctionList();

                    const std::size_t index = static_cast<std::size_t>(local_node_index);
                    assert(index < shape_function_list.size());

                    return shape_function_list[index](local_coords);
                }


                template<class DerivedT>
                double AccessShapeFunction<DerivedT>
                ::FirstDerivateShapeFunction(unsigned int local_node_index,
                                             unsigned int component,
                                             const LocalCoords& local_coords)
                {
                    const auto& gradient_shape_function_list = DerivedT::FirstDerivateShapeFunctionList();
                    auto Ncoor = DerivedT::Nderivate_component_;

                    const std::size_t index = static_cast<std::size_t>(local_node_index * Ncoor + component);
                    assert(index < gradient_shape_function_list.size());
                    return gradient_shape_function_list[index](local_coords);
                }


            } // namespace Crtp


        } // namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_SHAPE_FUNCTION_x_ACCESS_SHAPE_FUNCTION_HXX_
