target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumTopology.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/EnumTopology.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalData.hxx" / 
)

