target_sources(${MOREFEM_PARAM}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FromFile.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FromFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FromFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Functor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Functor.hxx" / 
)

