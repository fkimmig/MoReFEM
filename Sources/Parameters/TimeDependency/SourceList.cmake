target_sources(${MOREFEM_PARAM}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/None.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeDependency.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeDependency.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
