target_sources(${MOREFEM_PARAM}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ParameterType.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Parameter.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Parameter.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ParameterAtDof.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ParameterAtQuadraturePoint.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ParameterType.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ParameterType.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeDependency/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InitParameterFromInputData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
