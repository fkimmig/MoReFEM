target_sources(${MOREFEM_PARAM}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InitParameterFromInputData.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitParameterFromInputData.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
