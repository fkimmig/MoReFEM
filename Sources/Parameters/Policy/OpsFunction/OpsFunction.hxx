///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 16:38:28 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_OPS_FUNCTION_x_OPS_FUNCTION_HXX_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_OPS_FUNCTION_x_OPS_FUNCTION_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            OpsFunction<TypeT, SettingsSpatialFunctionT>::OpsFunction(const Domain& ,
                                                                      storage_type lua_function)
            : lua_function_(lua_function)
            { }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            typename OpsFunction<TypeT, SettingsSpatialFunctionT>::return_type
            OpsFunction<TypeT, SettingsSpatialFunctionT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                             const GeometricElt& geom_elt) const
            {
                const auto coords_type = SettingsSpatialFunctionT::GetCoordsType();

                switch(coords_type)
                {
                    case CoordsType::local:
                    {
                        return lua_function_(local_coords.GetValueOrZero(0),
                                             local_coords.GetValueOrZero(1),
                                             local_coords.GetValueOrZero(2));
                    }
                    case CoordsType::global:
                    {
                        auto& coords = GetNonCstWorkCoords();
                        Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, coords);
                        return lua_function_(coords.x(), coords.y(), coords.z());
                    }
                }

                assert(false && "Should never happen as all possible values of the enum class are addressed in the "
                       "switch.");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            typename OpsFunction<TypeT, SettingsSpatialFunctionT>::return_type
            OpsFunction<TypeT, SettingsSpatialFunctionT>::GetAnyValueFromPolicy() const
            {
                return lua_function_(0., 0., 0.);
            }



            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            [[noreturn]] typename OpsFunction<TypeT, SettingsSpatialFunctionT>::return_type
            OpsFunction<TypeT, SettingsSpatialFunctionT>::GetConstantValueFromPolicy() const
            {
                assert(false && "An ops function should yield IsConstant() = false!");
                exit(-1);
            }


            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            bool OpsFunction<TypeT, SettingsSpatialFunctionT>::IsConstant() const
            {
                return false;
            }

            template<ParameterNS::Type TypeT, class SettingsSpatialFunctionT>
            void OpsFunction<TypeT, SettingsSpatialFunctionT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Given by the Ops function given in the input parameter file." << std::endl;
            }


            template
            <
                ParameterNS::Type TypeT,
                class SettingsSpatialFunctionT
            >
            inline SpatialPoint& OpsFunction<TypeT, SettingsSpatialFunctionT>::GetNonCstWorkCoords() const noexcept
            {
                return work_coords_;
            }


            template
            <
                ParameterNS::Type TypeT,
                class SettingsSpatialFunctionT
            >
            inline void OpsFunction<TypeT, SettingsSpatialFunctionT>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false);
                exit(EXIT_FAILURE);
            }


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_OPS_FUNCTION_x_OPS_FUNCTION_HXX_
