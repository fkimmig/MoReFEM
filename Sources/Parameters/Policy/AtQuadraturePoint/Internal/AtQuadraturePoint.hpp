///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Jun 2015 15:48:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HPP_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            namespace AtQuadraturePointNS
            {


                /*!
                 * \brief Structure that holds both a value at a quadrature point and the last time it was modified.
                 *
                 * \tparam StorageValueTypeT Type of the value stored.
                 */
                template<class StorageValueTypeT>
                struct ValueHolder
                {


                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] value to store.
                     * \param[in] time_manager_Ntime_modified Index that specify the last time the value was modified.
                     * (was obtained through TimeManager::NtimeModified() method).
                     */
                    ValueHolder(StorageValueTypeT value, unsigned int time_manager_Ntime_modified);

                    //! Value at a given quadrature point.
                    StorageValueTypeT value;

                    //! Index that specifies when it was last updated (it is produced by TimeManager::NtimesModified()).
                    unsigned int last_update_index;

                };


            } // namespace AtQuadraturePointNS


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Policy/AtQuadraturePoint/Internal/AtQuadraturePoint.hxx"


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_QUADRATURE_POINT_x_INTERNAL_x_AT_QUADRATURE_POINT_HPP_
