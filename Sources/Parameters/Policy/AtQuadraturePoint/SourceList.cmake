target_sources(${MOREFEM_PARAM}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
