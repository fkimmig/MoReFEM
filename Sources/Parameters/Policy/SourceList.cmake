target_sources(${MOREFEM_PARAM}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/PiecewiseConstantByDomain/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/AtDof/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/OpsFunction/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Constant/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint/SourceList.cmake)
