target_sources(${MOREFEM_PARAM}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/AtDof.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AtDof.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
