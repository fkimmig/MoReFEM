target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EmptyString.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/String.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/EmptyString.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/String.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/String.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Traits.hpp" / 
)

