target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Definitions.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedOps.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Base.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Base.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Definitions.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedOps.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OpsFunction.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OpsFunction.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
