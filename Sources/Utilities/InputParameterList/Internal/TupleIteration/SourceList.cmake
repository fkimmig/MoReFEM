target_sources(${MOREFEM_UTILITIES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/TupleIteration.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TupleIteration.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Traits/SourceList.cmake)
