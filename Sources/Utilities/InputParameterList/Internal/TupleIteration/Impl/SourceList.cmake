target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/PrepareDefaultEntry.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/OpsType.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PrepareDefaultEntry.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/StaticIf.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/StaticIf.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TupleItem.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TupleItem.hxx" / 
)

