target_sources(${MOREFEM_UTILITIES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/ExtractParameter/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TupleIteration/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Subtuple/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ManualParsing/SourceList.cmake)
