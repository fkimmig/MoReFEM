///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Aug 2013 11:06:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_OPS_FUNCTION_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_OPS_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {



            template<typename ReturnTypeT, typename ...Args>
            OpsFunction<ReturnTypeT(Args...)>::OpsFunction()
            : ops_(nullptr)
            { }


            template<typename ReturnTypeT, typename ...Args>
            OpsFunction<ReturnTypeT(Args...)>::OpsFunction(const std::string& fullname,
                                                           ExtendedOps* ops)
            : fullname_(fullname),
            ops_(ops)
            { }


            template<typename ReturnTypeT, typename ...Args>
            inline ReturnTypeT OpsFunction<ReturnTypeT(Args...)>::operator()(Args... args) const
            {
                assert(!fullname_.empty());
                assert(!(!ops_));

                return ops_->Apply(fullname_, args...);
            }


            template<typename ReturnTypeT, typename ...Args>
            const std::string& OpsFunction<ReturnTypeT(Args...)>::GetKey() const
            {
                return fullname_;
            }


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_OPS_FUNCTION_HXX_
