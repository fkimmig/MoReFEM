///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 15:14:32 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXCEPTIONS_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXCEPTIONS_x_INPUT_PARAMETER_LIST_HPP_

# include <vector>
# include <sstream>
# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            namespace ExceptionNS
            {


                //! Generic class
                class Exception : public MoReFEM::Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] msg Message
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    Exception(const Exception&) = default;

                    //! Move constructor.
                    Exception(Exception&&) = default;

                    //! Copy affectation.
                    Exception& operator=(const Exception&) = default;

                    //! Move affectation.
                    Exception& operator=(Exception&&) = default;


                    //! Destructor
                    virtual ~Exception() override;

                };



                /*!
                 * \brief Thrown when an input parameter appears twice in the input parameter file.
                 */
                class DuplicateInInputFile final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] filename Name of the input file from which input parameters were read.
                     * \param[in] section Section in the input file.
                     * \param[in] variable_list All variables defined in the section of this input file.
                     * When exception is thrown we know there are duplicates but we till don't know
                     * which; it will be determined here.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit DuplicateInInputFile(const std::string& filename,
                                                  const std::string& section,
                                                  const std::vector<std::string>& variable_list,
                                                  const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    DuplicateInInputFile(const DuplicateInInputFile&) = default;

                    //! Move constructor.
                    DuplicateInInputFile(DuplicateInInputFile&&) = default;

                    //! Copy affectation.
                    DuplicateInInputFile& operator=(const DuplicateInInputFile&) = default;

                    //! Move affectation.
                    DuplicateInInputFile& operator=(DuplicateInInputFile&&) = default;


                    //! Destructor
                    virtual ~DuplicateInInputFile();

                };


                /*!
                 * \brief Thrown when a same key (section/name) appears twice in the tuple elements.
                 */
                class DuplicateInTuple final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] key The key found at least twice.
                     * Key is section.name is section is not empty and name otherwise.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit DuplicateInTuple(const std::string& key,
                                              const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    DuplicateInTuple(const DuplicateInTuple&) = default;

                    //! Move constructor.
                    DuplicateInTuple(DuplicateInTuple&&) = default;

                    //! Copy affectation.
                    DuplicateInTuple& operator=(const DuplicateInTuple&) = default;

                    //! Move affectation.
                    DuplicateInTuple& operator=(DuplicateInTuple&&) = default;


                    //! Destructor
                    virtual ~DuplicateInTuple();

                };



                /*!
                 * \brief Thrown when an input parameter in the input parameter file is not related to an
                 * element in the tuple list.
                 */
                class UnboundInputParameter final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] filename Name of the input file from which input parameters were read.
                     * \param[in] section Section in the input file.
                     * \param[in] variable Variable read in the input file.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit UnboundInputParameter(const std::string& filename,
                                                   const std::string& section,
                                                   const std::string& variable,
                                                   const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    UnboundInputParameter(const UnboundInputParameter&) = default;

                    //! Move constructor.
                    UnboundInputParameter(UnboundInputParameter&&) = default;

                    //! Copy affectation.
                    UnboundInputParameter& operator=(const UnboundInputParameter&) = default;

                    //! Move affectation.
                    UnboundInputParameter& operator=(UnboundInputParameter&&) = default;


                    //! Destructor
                    virtual ~UnboundInputParameter();

                };



                /*!
                 * \brief Thrown when an input parameter in the input parameter file is not related to an
                 * element in the tuple list.
                 *
                 * \tparam SubTupleT Tuple type which includes several input parameters which vectors should
                 * have been the same size.
                 *
                 */
                template<class SubTupleT>
                class MismatchedVectorSize final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                     */
                    explicit MismatchedVectorSize(const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    MismatchedVectorSize(const MismatchedVectorSize&) = default;

                    //! Move constructor.
                    MismatchedVectorSize(MismatchedVectorSize&&) = default;

                    //! Copy affectation.
                    MismatchedVectorSize& operator=(const MismatchedVectorSize&) = default;

                    //! Move affectation.
                    MismatchedVectorSize& operator=(MismatchedVectorSize&&) = default;


                    //! Destructor
                    virtual ~MismatchedVectorSize();

                };



                /*!
                 * \brief Thrown when MPI has not been initialized properly.
                 *
                 */
                class MpiNotInitialized final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit MpiNotInitialized(const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    MpiNotInitialized(const MpiNotInitialized&) = default;

                    //! Move constructor.
                    MpiNotInitialized(MpiNotInitialized&&) = default;

                    //! Copy affectation.
                    MpiNotInitialized& operator=(const MpiNotInitialized&) = default;

                    //! Move affectation.
                    MpiNotInitialized& operator=(MpiNotInitialized&&) = default;


                    //! Destructor
                    virtual ~MpiNotInitialized();

                };


                /*!
                 * \brief Thrown when a folder specified in the input file doesn't exist whereas it was
                 * expected to.
                 *
                 */
                class FolderDoesntExist final : public Exception
                {
                public:

                    /*!
                     * \brief Constructor with simple message
                     *
                     * \param[in] folder Name of the folder.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit FolderDoesntExist(const std::string& folder, const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    FolderDoesntExist(const FolderDoesntExist&) = default;

                    //! Move constructor.
                    FolderDoesntExist(FolderDoesntExist&&) = default;

                    //! Copy affectation.
                    FolderDoesntExist& operator=(const FolderDoesntExist&) = default;

                    //! Move affectation.
                    FolderDoesntExist& operator=(FolderDoesntExist&&) = default;


                    //! Destructor
                    virtual ~FolderDoesntExist();

                };



            } // namespace ExceptionNS


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Exceptions/InputParameterList.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_EXCEPTIONS_x_INPUT_PARAMETER_LIST_HPP_
