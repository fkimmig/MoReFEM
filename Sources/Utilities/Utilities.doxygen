/*!
 * \defgroup UtilitiesGroup Utilities
 * 
 * \brief Module to encompass handly side functionalities (printing content of a STL container, creating a file in filesystem, 
 * and so forth...)
 * 
 * This module is compiled along with ThirdParty module in Utilities library. 
 * 
 * Utilities groups all the miscellaneous tools that aren't very tied to MoReFEM core; the idea is that in a 
 * completely unrelated project most of it could remain handy.
 * 
 * It is there for instance that you may found utilities to check whether a real is zero or not, another one to
 * create a file in the filesystem, yet another one to print easily the contain of a STL container.
 */



/// \addtogroup UtilitiesGroup
///@{

/// \namespace MoReFEM::Utilities
/// \brief Namespace that enclose most of the Utilities (some aren't for conveniency, such as NumericNS or FilesystemNS).

/// \namespace MoReFEM::Crtp
/// \brief Namespace that enclose some generic CRTP ("Curiously recurrent template pattern").


/// \namespace MoReFEM::NumericNS
/// \brief Namespace that enclose numeric utilities (such as IsZero() or AreEqual() used to compared floating-points).

/// \namespace MoReFEM::FilesystemNS
/// \brief Namespace that enclose filesystem utilities (check a folder do exist, remove a file, and so forth...)

    
    /// \namespace MoReFEM::FilesystemNS::File
    /// \brief Subnamespace to deal with files.
    
    
    /// \namespace MoReFEM::FilesystemNS::Folder
    /// \brief Subnamespace to deal with folders.

    
///@}



/*!
 * \class doxygen_hide_invoking_file_and_line
 *
 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
 * \param[in] invoking_line Line on which the function or class was invoked; usually __LINE__.
 */


/*!
 * \class doxygen_hide_alias_self
 *
 * \brief Alias to the type of the class.
 */


/*!
 * \class doxygen_hide_alias_unique_ptr
 *
 * \brief Alias to a std::unique_ptr to a non constant object.
 */


/*!
 * \class doxygen_hide_alias_const_unique_ptr
 *
 * \brief Alias to a std::unique_ptr to a const object.
 */

