target_sources(${MOREFEM_UTILITIES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/MatrixOrVector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Miscellaneous.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Mpi/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/UniqueId/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Pragma/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Filesystem/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeKeep/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Numeric/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputParameterList/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Singleton/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Environment/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LinearAlgebra/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/String/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Containers/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Mutex/SourceList.cmake)
