///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Mar 2015 17:20:59 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <cstdlib>
#include <sstream>

#include "Utilities/Environment/EnvironmentVariable.hpp"
#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/String/String.hpp"



namespace MoReFEM
{
    
    
    namespace Utilities
    {
        
        
        namespace EnvironmentNS
        {
            
            
            bool DoExist(const char* variable)
            {
                return std::getenv(variable) != nullptr;
            }
            
        
            std::string GetEnvironmentVariable(const char* variable, const char* invoking_file, int invoking_line)
            {
                auto value = std::getenv(variable);
                
                if (value == nullptr)
                {
                    std::ostringstream oconv;
                    oconv << "Environment variable " << variable << " is not defined!";
                    throw Exception(oconv.str(), invoking_file, invoking_line);
                }
                
                return std::string(value);
            }
            
            
            
            std::string SubstituteValues(std::string string)
            {
                
                auto begin_env_pos = string.find("${");
                auto end_env_pos = string.find("}");
                
                while (end_env_pos != std::string::npos && begin_env_pos < end_env_pos)
                {
                    std::string environment_variable_name_with_braces =
                        string.substr(begin_env_pos, end_env_pos - begin_env_pos + 1ul);
                    
                    std::string environment_variable_name =
                        environment_variable_name_with_braces.substr(2, environment_variable_name_with_braces.size() - 3ul);
                    
                    auto environment_variable_value = GetEnvironmentVariable(environment_variable_name, __FILE__, __LINE__);
                    
                    Utilities::String::Replace(environment_variable_name_with_braces,
                                               environment_variable_value,
                                               string);
                    
                    // Look out for other environment variables to substitute.
                    begin_env_pos = string.find("${");
                    end_env_pos = string.find("}");
                }
                
                return string;
                
            }
            
            
        } // namespace EnvironmentNS

        
    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
