///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Mar 2015 17:20:59 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HPP_
# define MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HPP_

#include <cstdlib>
#include <string>


namespace MoReFEM
{


    namespace Utilities
    {


        namespace EnvironmentNS
        {


            //! Returns whether \a variable do exist or not.
            bool DoExist(const char* variable);

            //! Overload for std::string.
            template<class T>
            bool DoExist(T&& variable);


            /*!
             * \brief Get the value of an environment variable.
             *
             * \param[in] variable Environment variable which value is sought.
             * If the environment variable doesn't exist, an exception is thrown.
             * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
             * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
             *
             * \return Value of the \a variable environment variable.
             */
            std::string GetEnvironmentVariable(const char* variable, const char* invoking_file, int invoking_line);

            //! Overload for std::string.
            template<class T>
            std::string GetEnvironmentVariable(T&& variable, const char* invoking_file, int invoking_line);

            /*!
             * \brief Replace in the string the environment variables by their values.
             *
             * Expected format for environment variables is ${NAME} where NAME stands for the name of the environment
             * variable.
             *
             * \param[in] string Original string. Pass by value is important here; do not replace by reference!
             *
             * \return String with the environment variables replaced by their values.
             */
            std::string SubstituteValues(std::string string);


        } // namespace EnvironmentNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Environment/EnvironmentVariable.hxx"


#endif // MOREFEM_x_UTILITIES_x_ENVIRONMENT_x_ENVIRONMENT_VARIABLE_HPP_
