///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 16:17:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_
# define MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_


# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Crtp
    {


        /*!
         * \brief This CRTP class provides to its derived class a Mpi object and an accessor to it.
         */
        template<class DerivedT>
        class CrtpMpi
        {
        public:


            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydetails doxygen_hide_mpi_param
             */
            explicit CrtpMpi(const Wrappers::Mpi& mpi);

            //! Destructor.
            ~CrtpMpi() = default;

            //! Copy constructor.
            CrtpMpi(const CrtpMpi&) = delete;

            //! Move constructor.
            CrtpMpi(CrtpMpi&&) = default;

            //! Copy assignation..
            CrtpMpi& operator=(const CrtpMpi&) = delete;

            //! Move assignation.
            CrtpMpi& operator=(CrtpMpi&&) = default;


            ///@}

            //! Const accessor to underlying Mpi object.
            const Wrappers::Mpi& GetMpi() const noexcept;

        private:

            //! Mpi object.
            const Wrappers::Mpi& mpi_;

        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Mpi/Mpi.hxx"


#endif // MOREFEM_x_UTILITIES_x_MPI_x_MPI_HPP_
