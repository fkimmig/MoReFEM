target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Array.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Array.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/BoolArray.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/EnumClass.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PointerComparison.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PointerComparison.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Print.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Sort.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Tuple.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnorderedMap.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnorderedMap.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Vector.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
