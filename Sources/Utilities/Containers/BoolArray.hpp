///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 10 Mar 2014 16:05:29 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HPP_


# include <iostream>
# include <vector>
# include <cassert>


namespace MoReFEM
{


    namespace Utilities
    {



        /*!
         * \brief A class to handle a dynamic array of bool with some safety.
         *
         * std::vector<bool> is an anomaly in the STL: contrary to what we could expect, this vector does not
         * contains bool but rather bits. It spares thus some memory, but is troublesome to use in
         * lots of situation... Moreover, its gain is rather dubious: what is saved in memory can very easily
         * be lost in execution time!
         *
         * The common trick is to use either a std::bitset (much more efficient, but size is fixed and known
         * at compile-time) or a std::deque<bool> (no trick in it: really bool inside).
         *
         * However, with MPI use I do not know the size at compile-time and I absolutely need contiguity
         * in memory, thus ruling out std::deque.
         *
         * Hence this class that is essentially a wrapper over a dynamic array, with RAII.
         *
         * \internal <b><tt>[internal]</tt></b> Due to its specific role, this class is one of the very few that does not follow the coding style:
         * names of the methods match their STL counterpart instead of respecting MoReFEM coding style.
         *
         */
        class BoolArray final
        {
        public:

            //! \name Special members.
            ///@{

            //! Bare constructor.
            explicit BoolArray();

            //! This constructor allocates the memory for \a Nelt.
            explicit BoolArray(unsigned int Nelt);

            /*!
             * Constructor from an initializer list \a list.
             *
             * \param[in] list List used for creation.
             *
             * For instance:
             * \code
             * BoolArray array { true, false, true };
             * \endcode
             */
            explicit BoolArray(std::initializer_list<bool> list);

            /*!
             * \brief Constructor from a std::vector<bool>.
             */
            explicit BoolArray(const std::vector<bool>& vector);


            //! Destructor, in charge of deallocating underlying dynamic array.
            ~BoolArray();

            /*!
             * \brief Recopy constructor.
             *
             * A new dynamic array is allocated for the new object.
             */
            BoolArray(const BoolArray&);

            //! Move constructor.
            BoolArray(BoolArray&&) = default;

            //! Copy affectation operator.
            BoolArray& operator=(const BoolArray&);

            //! Move affectation operator.
            BoolArray& operator=(BoolArray&&) = default;



            ///@}

            /*!
             * \brief Allocate the underlying dynamic array with \a Nelt.
             *
             * \param[in] Nelt Allocate the space for \a Nelt in the BoolArray container.
             *
             * This method should only be called within an object in which no memory was previously allocated.
             */
            void AllocateSize(unsigned int Nelt);



            /*!
             * \brief Allow explicit conversion to a bool* object.
             *
             * \code
             * BoolArray array { true, false, true };
             * ...
             * static_cast<bool*>(array); // yields the underlying pointer to the first element of the dynamic array.
             * \endcode
             */
            explicit operator bool*() const;


            //! Subscript operator.
            bool operator[](unsigned int i) const;

            //! Subscript operator.
            bool& operator[](unsigned int i);

            //! Returns the number of elements in the array.
            unsigned int Size() const;


            //! Print to \a out the content of the array.
            void Print(std::ostream& out) const;


        public:


            //! \name STL counterparts.
            //! The purpose is to provide the same interface as the STL for some operations; it is actually an inline
            //! call to other methods defined previously.
            ///@{
            using value_type = bool;

            //! Access to underlying pointer.
            bool* data();


            //! Const access to underlying pointer.
            const bool* data() const;


            /*!
             * \brief Resize the container.
             *
             * \param[in] Nelt Size requested.
             *
             * \internal <b><tt>[internal]</tt></b> Same as AllocateSize().
             */
            void resize(unsigned int Nelt);


            /*!
             * \brief Returns the number of elements in the array.
             *
             * \return Number of elements in the array.
             *
             * \internal <b><tt>[internal]</tt></b> Same as Size().
             */
            unsigned int size() const;

            //! Whether the array is empty or not.
            bool empty() const;


            ///@}



        private:

            //! Underlying dynamic bool array.
            bool* array_;

            //! Number of elements in the array.
            unsigned int Nelt_;

        };


        /*!
         * \brief Create a std::vector<bool> with the same content as \a array object.
         */
        std::vector<bool> VectorFromBoolArray(const BoolArray& array);


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Containers/BoolArray.hxx"


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HPP_
