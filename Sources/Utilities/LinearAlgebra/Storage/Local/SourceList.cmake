target_sources(${MOREFEM_UTILITIES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/LocalMatrixStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalMatrixStorage.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVectorStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LocalVectorStorage.hxx" / 
)

