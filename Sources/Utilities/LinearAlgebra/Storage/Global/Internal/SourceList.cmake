target_sources(${MOREFEM_UTILITIES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorTemporaryAccess.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorTemporaryAccess.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorTemporaryStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorTemporaryStorage.hxx" / 
)

