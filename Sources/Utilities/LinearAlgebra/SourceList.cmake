target_sources(${MOREFEM_UTILITIES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/Storage/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/SparseMatrix/SourceList.cmake)
