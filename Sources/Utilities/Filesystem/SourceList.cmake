target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/File.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Folder.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/File.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Folder.hpp" / 
)

