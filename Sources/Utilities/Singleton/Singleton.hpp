///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 17:41:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HPP_
# define MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HPP_

# include <mutex>
# include <memory>
# include <cstdlib>
# include <cassert>

# include "Utilities/Singleton/Exceptions/Singleton.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Provides part of the singleton interface to \a T through a CRTP.
         *
         * \tparam T Class to which the singleton behaviour is given.
         *
         * It should be noticed that all the behaviour of the singleton can't be assigned through this class;
         * for instance the following code unfortunately works:
         *
         * \code
         * // In .hpp file
         * class Single : public Singleton<Single>
         * {
         *      public:
         *          Single()
         * };
         *
         * // In main.cpp
         * Single single; // no compile error.
         *
         * \endcode
         *
         * So this class only helps to enforce part of the singleton behaviour; you have to add two steps if you want
         * it to be completely ensured:
         *
         * . Make the constructor of the derived class private.
         * . Add a friendship line to Utilities::Singleton<T> in T class declaration.
         *
         * As a rule, I would advise to define only ONE constructor in the derived class to avoid any ambiguity.
         *
         * This Singleton is what Andrei Alexandrescu calls a 'Meyers singleton'in "Modern C++ Design"
         *  (see chapter 6, section 6.4).
         */
        template<class T>
        class Singleton
        {

        public:

            /*!
             * \brief Call instance of the singleton.
             *
             * If not already existing it will be created on the fly, otherwise the existing one will be used.
             * This method should be called in two cases:
             * - If we do not know whether the instance already exists or not. The most typical example is
             * when a value is registered within the singleton in an anonymous namespace.
             * - If we knwo for sure it is the first call to the singleton. For instance the initialization
             * of TimeKeep sinfgleton class that sets the initial time.
             *
             * In all other cases, call instead GetInstance().
             *
             * \tparam Args Variadic template arguments.
             * \param[in] args Arguments passed to the constructor if the object is to be built.
             *
             * \return Instance of the singleton.
             */
            template <class... Args>
            static T& CreateOrGetInstance(Args&&... args);


            /*!
             * \brief Call an instance of the singleton that must already have been created.
             *
             * This must be called instead of CreateOrGetInstance() if T doesn't get a default constructor.
             *
             * \return Instance of the singleton.
             */
            static T& GetInstance();

            /*!
             * \brief Destroy the singleton.
             */
            static void Destroy();


        protected:

            //! Ensure singleton behaviour.
            //@{
            //! Disable constructor call.
            Singleton() = default;

            /*!
             * \brief Destructor.
             *
             * Disable public destruction: destruction of a Singleton must occur either
             * through atexit() stack or through a call to Destroy().
             *
             */
            ~Singleton() = default;


        private:

            //! Disable copy constructor.
            Singleton(const Singleton&) = delete;

            //! Disable move constructor.
            Singleton(Singleton&&) = delete;

            //! Disable copy affectation.
            Singleton& operator=(const Singleton&) = delete;

            //! Disable move affectation.
            Singleton& operator=(Singleton&&) = delete;

            //@}


            /*!
             * \brief Create the singleton in first invocation.
             *
             * \tparam Args Variadic template arguments.
             * \param[in] args Arguments passed to the constructor.
             */
            template <class... Args>
            static void Create(Args&&... args);


            //! Gets called if dead reference detected.
            [[noreturn]] static void OnDeadReference();

            //! Get reference to the singleton mutex.
            static std::mutex& GetNonCstSingletonMutex();


        private:

            //! Internal pointer to the actual instance.
            static T* instance_;


            /*!
             * \brief Used for protection against dead reference problem (invocation while the instance has been destroyed).
             *
             * Should not occur in our case, but it is much safer to put the protection there anyway.
             */
            static bool destroyed_;

            /*!
             * \brief Mutex object.
             *
             * This follows item 16 of Scott Meyers's "Effective Modern C++", which advises to make const member
             * functions thread safe.
             *
             * However, it is here enclosed in a unique_ptr as mutex doesn't seem to be movable (current implementations
             * of both clang and gcc don't define the move constructor and the move assignation; the copy is deleted).
             */
            static std::mutex singleton_mutex_;

        };



    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Singleton/Singleton.hxx"


#endif // MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HPP_
