///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Jan 2015 10:49:18 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_

# include <memory>
# include <vector>

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"
# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/AnalyticalPrestress.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                //! Implements an active stress Sigma_c*Tau*Tau, with dSigma_c/dt = sigma0 + |u|+ - |u|dSigma_c.
                template <unsigned int FiberIndexT>
                class AnalyticalPrestress
                {
                public:

                    //! \copydoc doxygen_hide_gvo_local_policy_alias
                    using local_policy =
                        ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>;

                    //! Alias to the type of the input.
                    using input_active_stress_policy_type = typename local_policy::input_active_stress_policy_type;

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = AnalyticalPrestress<FiberIndexT>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit AnalyticalPrestress() = default;

                    //! Destructor.
                    ~AnalyticalPrestress() = default;

                    //! Copy constructor.
                    AnalyticalPrestress(const AnalyticalPrestress&) = delete;

                    //! Move constructor.
                    AnalyticalPrestress(AnalyticalPrestress&&) = delete;

                    //! Copy affectation.
                    AnalyticalPrestress& operator=(const AnalyticalPrestress&) = delete;

                    //! Move affectation.
                    AnalyticalPrestress& operator=(AnalyticalPrestress&&) = delete;

                    ///@}

                public:

                    //! Initialize Sigma_c.
                    template<class LocalOperatorStorageT>
                    void InitializeActiveStressPolicy(const Domain& domain,
                                                      const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                                      const TimeManager& time_manager,
                                                      const LocalOperatorStorageT& local_operator_storage,
                                                      input_active_stress_policy_type* input_active_stress_policy);

                    //! Tmp #9 Complete!
                    const MatchDofInNumberingSubset* GetMatchDofInNumberingSubsetOperatorPtr() const noexcept;

                private:

                    //! Tmp #9 Complete!
                    const input_active_stress_policy_type& GetInputActiveStressPolicy() const noexcept;

                    //! Tmp #9 Complete!
                    input_active_stress_policy_type& GetNonCstInputActiveStressPolicy() noexcept;

                private:

                    //! Tmp  #9 Complete!
                    input_active_stress_policy_type* input_active_stress_policy_  = nullptr;

                private:

                    //! Sigma_c.
                    ParameterAtQuadraturePoint<ParameterNS::Type::scalar>::unique_ptr sigma_c_ = nullptr;

                };


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/AnalyticalPrestress.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_ANALYTICAL_PRESTRESS_HPP_
