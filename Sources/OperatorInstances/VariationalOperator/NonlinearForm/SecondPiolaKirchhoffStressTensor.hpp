///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:40:32 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_

# include <set>

# include "Parameters/ParameterType.hpp"

# include "Parameters/Parameter.hpp"
# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"


# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"
# include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/Internal/Helper.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hpp"
# include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Instantiation of the SecondPiolaKirchhoffStressTensor related to hyperelastic matrix.
         *
         * \tparam HyperelasticityPolicyT Policy that defines if an hyperelastic contribution is present in the tensor.
         * \tparam ViscoelasticityPolicyT Policy that defines if an viscoelastic contribution is present in the tensor.
         * \tparam ActiveStressPolicyT Policy that defines if an active stress contribution is present in the tensor.
         */
        template
        <
            class HyperelasticityPolicyT,
            class ViscoelasticityPolicyT,
            class ActiveStressPolicyT
        >
        class SecondPiolaKirchhoffStressTensor final
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            SecondPiolaKirchhoffStressTensor
            <
                HyperelasticityPolicyT,
                ViscoelasticityPolicyT,
                ActiveStressPolicyT
            >,
            Advanced::OperatorNS::Nature::nonlinear,
            typename Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
            <
                typename HyperelasticityPolicyT::local_policy,
                typename ViscoelasticityPolicyT::local_policy,
                typename ActiveStressPolicyT::local_policy
            >
        >,
        public HyperelasticityPolicyT,
        public ViscoelasticityPolicyT,
        public ActiveStressPolicyT
        {

        public:

            //! Convenient alias.
            using self = SecondPiolaKirchhoffStressTensor
            <
                HyperelasticityPolicyT,
                ViscoelasticityPolicyT,
                ActiveStressPolicyT
            >;


            //! Alias to local operator.
            using local_operator_type =
            typename Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
            <
                typename HyperelasticityPolicyT::local_policy,
                typename ViscoelasticityPolicyT::local_policy,
                typename ActiveStressPolicyT::local_policy
            >;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::nonlinear,
                local_operator_type
            >;

            //! \copydoc doxygen_hide_namespace_cluttering
            using input_active_stress_policy_type = typename ActiveStressPolicyT::input_active_stress_policy_type;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias to CauchyGreenTensor.
            using cauchy_green_tensor_type =
                ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \param[in] unknown_ptr Vectorial unknown considered (should be a displacement).
             * \param[in] test_unknown_ptr Vectorial unknown considered for test function.
             * \param[in] solid Object which provides the required material parameters for the solid.
             * \param[in] time_manager Time manager need for Viscoelasticity and Active Stress.
             * \param[in] input_active_stress_policy Object required only for Active Stress to compute U0 and U1 locally.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \param[in] hyperelastic_law Hyperelastic law considered. Do not delete this pointer!
             */
            explicit SecondPiolaKirchhoffStressTensor(const FEltSpace& felt_space,
                                                      const Unknown::const_shared_ptr unknown_ptr,
                                                      const Unknown::const_shared_ptr test_unknown_ptr,
                                                      const Solid& solid,
                                                      const TimeManager& time_manager,
                                                      const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                                      const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr,
                                                      input_active_stress_policy_type* input_active_stress_policy = nullptr);

            //! Destructor.
            ~SecondPiolaKirchhoffStressTensor() = default;

            //! Move constructor.
            SecondPiolaKirchhoffStressTensor(SecondPiolaKirchhoffStressTensor&&) = delete;

            //! Copy constructor.
            SecondPiolaKirchhoffStressTensor(const SecondPiolaKirchhoffStressTensor&) = delete;

            //! Copy affectation.
            SecondPiolaKirchhoffStressTensor& operator=(const SecondPiolaKirchhoffStressTensor&) = delete;

            //! Move affectation.
            SecondPiolaKirchhoffStressTensor& operator=(SecondPiolaKirchhoffStressTensor&&) = delete;


            ///@}

            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there are neither viscoelasticity nor active policy
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const Domain& domain = Domain()) const;

            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there is viscoelasticity and no active stress
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration. Used for Viscoelasticity Policy (its nature
             * varies depending on the time scheme used).
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const Domain& domain = Domain()) const;


            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there are both active stress and visoelasticity
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration. Used for Viscoelasticity Policy.(its nature
             * varies depending on the time scheme used).
             * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the previous time. Used for ActiveStress Policy.
             * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current time. Used for ActiveStress Policy.
             * \param[in] do_update_sigma_c Indicates if it is the first Newton loop or not to advance Sigma c in time.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const bool do_update_sigma_c,
                          const Domain& domain = Domain()) const;



            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there are both active stress and visoelasticity
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration. Used for Viscoelasticity Policy.(its nature
             * varies depending on the time scheme used).
             * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the previous time. Used for ActiveStress Policy.
             * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current time. Used for ActiveStress Policy.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const GlobalVector& velocity_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const Domain& domain = Domain()) const;


            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there is active stress and no visoelasticity
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the previous time. Used for ActiveStress Policy.
             * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current time. Used for ActiveStress Policy.
             * \param[in] do_update_sigma_c Indicates if it is the first Newton loop or not to advance Sigma c in time.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const bool do_update_sigma_c,
                          const Domain& domain = Domain()) const;


            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * This overload is expected to be used when there is active stress and no visoelasticity
             * (hyperelasticity doesn't matter as it requires no additional arguments here).
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
             * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] displacement_previous_iteration Vector that includes data from the previous iteration. (its nature
             * varies depending on the time scheme used).
             * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the previous time. Used for ActiveStress Policy.
             * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current time. Used for ActiveStress Policy.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& displacement_previous_iteration,
                          const GlobalVector& electrical_activation_previous_time,
                          const GlobalVector& electrical_activation_at_time,
                          const Domain& domain = Domain()) const;

        public:

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here just one vector for the displacement.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&>& additional_arguments) const;

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here two vectors for the displacement and the velocity.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&, const GlobalVector&>& additional_arguments) const;


            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here four vectors for the displacement, the velocity, the two electrical activations and
             * a boolean to indicate if sigma c must be updated or not.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const GlobalVector&, const bool>& additional_arguments) const;

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here four vectors for the displacement, the velocity and the two electrical activations.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const GlobalVector&>& additional_arguments) const;

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here three vectors for the displacement and the two electrical activations and
             * a boolean to indicate if sigma c must be updated or not.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const bool>& additional_arguments) const;


            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
             * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
             * and understand, and induces unwanted copies).
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally. Here three vectors for the displacement and the two electrical activations.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&>& additional_arguments) const;

        private:

            //! Accessor to Cauchy Green tensor operator.
            const GlobalParameterOperatorNS::UpdateCauchyGreenTensor& GetCauchyGreenTensorOperator() const noexcept;

        private:

            //! CauchyGreen tensor.
            typename cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_ = nullptr;

            //! Cauchy Green operator.
            GlobalParameterOperatorNS::UpdateCauchyGreenTensor::const_unique_ptr cauchy_green_tensor_operator_ = nullptr;
        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HPP_
