///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:40:32 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SecondPiolaKirchhoffStressTensor(const FEltSpace& felt_space,
                                           const Unknown::const_shared_ptr unknown_ptr,
                                           const Unknown::const_shared_ptr test_unknown_ptr,
                                           const Solid& solid,
                                           const TimeManager& time_manager,
                                           const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                           const QuadratureRulePerTopology* const a_quadrature_rule_per_topology,
                                           input_active_stress_policy_type* input_active_stress_policy)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 std::move(a_quadrature_rule_per_topology),
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::yes,
                 solid,
                 time_manager,
                 hyperelastic_law,
                 input_active_stress_policy)
        {
            assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);

            const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
            const auto& god_of_dof = *god_of_dof_ptr;

            const auto& mesh = god_of_dof.GetGeometricMeshRegion();
            decltype(auto) local_operator_storage = parent::GetLocalOperatorPerRefGeomElt();

            const auto& active_stress_ptr = static_cast<ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressHelper<ActiveStressPolicyT>
            ::InitActiveStress(felt_space.GetDomain(),
                               parent::GetQuadratureRulePerTopology(),
                               time_manager,
                               local_operator_storage,
                               input_active_stress_policy,
                               active_stress);

            LocalVector initial_value;

            switch(mesh.GetDimension())
            {
                case 1u:
                    initial_value.Resize(1u);
                    break;
                case 2u:
                    initial_value.Resize(3u);
                    break;
                case 3u:
                    initial_value.Resize(6u);
                    break;
                default:
                    assert(false);
                    exit(EXIT_FAILURE);
            }

            decltype(auto) quadrature_rule_per_topology = parent::GetQuadratureRulePerTopology();

            cauchy_green_tensor_ =
                std::make_unique<cauchy_green_tensor_type>("Cauchy-Green tensor",
                                                           felt_space.GetDomain(),
                                                           quadrature_rule_per_topology,
                                                           initial_value,
                                                           time_manager);

            cauchy_green_tensor_operator_ =
                std::make_unique<GlobalParameterOperatorNS::UpdateCauchyGreenTensor>(felt_space,
                                                                                     *unknown_ptr,
                                                                                     *cauchy_green_tensor_,
                                                                                     &quadrature_rule_per_topology);

            // Feed CauchyGreen param to each local operator.

            decltype(auto) cauchy_green_tensor_raw = cauchy_green_tensor_.get();


            Internal::GlobalVariationalOperatorNS::ApplySetCauchyGreenTensor
            <
                typename parent::local_operator_storage_type,
                0ul,
                std::tuple_size<typename parent::local_operator_storage_type>::value
            >
            ::Perform(local_operator_storage,
                      cauchy_green_tensor_raw);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        const std::string& SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::ClassName()
        {
            static std::string name("SecondPiolaKirchhoffStressTensor");
            return name;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, displacement_previous_iteration);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const GlobalVector& velocity_previous_iteration,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                        domain,
                                        displacement_previous_iteration,
                                        velocity_previous_iteration);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const GlobalVector& velocity_previous_iteration,
                   const GlobalVector& electrical_activation_previous_time,
                   const GlobalVector& electrical_activation_at_time,
                   const bool do_update,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                        domain,
                                        displacement_previous_iteration,
                                        velocity_previous_iteration,
                                        electrical_activation_previous_time,
                                        electrical_activation_at_time,
                                        do_update);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const GlobalVector& velocity_previous_iteration,
                   const GlobalVector& electrical_activation_previous_time,
                   const GlobalVector& electrical_activation_at_time,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                        domain,
                                        displacement_previous_iteration,
                                        velocity_previous_iteration,
                                        electrical_activation_previous_time,
                                        electrical_activation_at_time);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const GlobalVector& electrical_activation_previous_time,
                   const GlobalVector& electrical_activation_at_time,
                   const bool do_update,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                        domain,
                                        displacement_previous_iteration,
                                        electrical_activation_previous_time,
                                        electrical_activation_at_time,
                                        do_update);
        }



        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LinearAlgebraTupleT>
        inline void
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& displacement_previous_iteration,
                   const GlobalVector& electrical_activation_previous_time,
                   const GlobalVector& electrical_activation_at_time,
                   const Domain& domain) const
        {
            GetCauchyGreenTensorOperator().Update(displacement_previous_iteration);

            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple),
                                        domain,
                                        displacement_previous_iteration,
                                        electrical_activation_previous_time,
                                        electrical_activation_at_time);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&, const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());

            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<1>(additional_arguments),
                                  local_operator.GetNonCstLocalVelocity());
        }

        namespace // anonymous
        {


            template<class LocalVariationalOperatorT>
            void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                         const LocalFEltSpace& local_felt_space,
                                                         const ExtendedUnknown& nth_unknown,
                                                         const MatchDofInNumberingSubset& match_dof_in_numbering_subset_operator,
                                                         const GlobalVector& a_electrical_activation_previous_time,
                                                         const GlobalVector& a_electrical_activation_at_time,
                                                         const bool do_update);


            template<class LocalVariationalOperatorT>
            void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                         const LocalFEltSpace& local_felt_space,
                                                         const ExtendedUnknown& nth_unknown,
                                                         const MatchDofInNumberingSubset& match_dof_in_numbering_subset_operator,
                                                         const GlobalVector& a_electrical_activation_previous_time,
                                                         const GlobalVector& a_electrical_activation_at_time);


        } // anonymous


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const GlobalVector&, const bool>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());

            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<1>(additional_arguments),
                                  local_operator.GetNonCstLocalVelocity());

            const auto& active_stress_ptr = static_cast<const ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            const MatchDofInNumberingSubset* raw_match_dof_numbering_subset =
                Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressHelper<ActiveStressPolicyT>::GetMatchDof(active_stress);

            assert(!(!raw_match_dof_numbering_subset));

            SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                    local_felt_space,
                                                    this->GetNthUnknown(),
                                                    *raw_match_dof_numbering_subset,
                                                    std::get<2>(additional_arguments),
                                                    std::get<3>(additional_arguments),
                                                    std::get<4>(additional_arguments));
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());

            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<1>(additional_arguments),
                                  local_operator.GetNonCstLocalVelocity());

            const auto& active_stress_ptr = static_cast<const ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            const MatchDofInNumberingSubset* raw_match_dof_numbering_subset =
                Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressHelper<ActiveStressPolicyT>::GetMatchDof(active_stress);

            assert(!(!raw_match_dof_numbering_subset));

            SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                    local_felt_space,
                                                    this->GetNthUnknown(),
                                                    *raw_match_dof_numbering_subset,
                                                    std::get<2>(additional_arguments),
                                                    std::get<3>(additional_arguments));
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&, const bool>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());

            const auto& active_stress_ptr = static_cast<const ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            const MatchDofInNumberingSubset* raw_match_dof_numbering_subset =
                Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressHelper<ActiveStressPolicyT>::GetMatchDof(active_stress);

            assert(!(!raw_match_dof_numbering_subset));

            SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                    local_felt_space,
                                                    this->GetNthUnknown(),
                                                    *raw_match_dof_numbering_subset,
                                                    std::get<1>(additional_arguments),
                                                    std::get<2>(additional_arguments),
                                                    std::get<3>(additional_arguments));
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<class LocalOperatorTypeT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&, const GlobalVector&, const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());

            const auto& active_stress_ptr = static_cast<const ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            const MatchDofInNumberingSubset* raw_match_dof_numbering_subset =
                Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressHelper<ActiveStressPolicyT>::GetMatchDof(active_stress);

            assert(!(!raw_match_dof_numbering_subset));

            SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                    local_felt_space,
                                                    this->GetNthUnknown(),
                                                    *raw_match_dof_numbering_subset,
                                                    std::get<1>(additional_arguments),
                                                    std::get<2>(additional_arguments));
        }


        namespace // anonymous
        {


            template<class LocalVariationalOperatorT>
            void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                         const LocalFEltSpace& local_felt_space,
                                                         const ExtendedUnknown& nth_unknown,
                                                         const MatchDofInNumberingSubset& match_dof_in_numbering_subset_operator,
                                                         const GlobalVector& a_electrical_activation_previous_time,
                                                         const GlobalVector& a_electrical_activation_at_time,
                                                         const bool do_update_sigma_c)
            {
                SetComputeEltArrayArgumentsActivePolicy(local_operator,
                                                        local_felt_space,
                                                        nth_unknown,
                                                        match_dof_in_numbering_subset_operator,
                                                        a_electrical_activation_previous_time,
                                                        a_electrical_activation_at_time);

                local_operator.SetDoUpdateSigmaC(do_update_sigma_c);
            }


            template<class LocalVariationalOperatorT>
            void SetComputeEltArrayArgumentsActivePolicy(LocalVariationalOperatorT& local_operator,
                                                         const LocalFEltSpace& local_felt_space,
                                                         const ExtendedUnknown& nth_unknown,
                                                         const MatchDofInNumberingSubset& match_dof_in_numbering_subset_operator,
                                                         const GlobalVector& a_electrical_activation_previous_time,
                                                         const GlobalVector& a_electrical_activation_at_time)
            {
                const auto& extended_unknown = local_operator.GetElementaryData().GetRefFElt(nth_unknown).GetExtendedUnknown();

                const auto& local_2_global =
                local_felt_space.GetLocal2Global<MpiScale::processor_wise>(extended_unknown);

                auto& u0 = local_operator.GetNonCstLocalElectricalActivationPreviousTime();
                auto& u1 = local_operator.GetNonCstLocalElectricalActivationAtTime();

                {
                    Wrappers::Petsc::AccessGhostContent ghost_vector_u0(a_electrical_activation_previous_time, __FILE__, __LINE__);
                    Wrappers::Petsc::AccessGhostContent ghost_vector_u1(a_electrical_activation_at_time, __FILE__, __LINE__);

                    const auto& vector_with_ghost_u0 = ghost_vector_u0.GetVectorWithGhost();
                    const auto& vector_with_ghost_u1 = ghost_vector_u1.GetVectorWithGhost();

                    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
                    ghost_vector_content_u0(vector_with_ghost_u0, __FILE__, __LINE__);
                    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
                    ghost_vector_content_u1(vector_with_ghost_u1, __FILE__, __LINE__);

                    decltype(auto) match = match_dof_in_numbering_subset_operator.GetData();

                    std::set<unsigned int> buf_global_index_scalar;

                    unsigned int local_scalar_counter = static_cast<unsigned int>(-1);

                    const unsigned int local_2_global_size = static_cast<unsigned int>(local_2_global.size());

                    for (auto i = 0ul ; i < local_2_global_size ; ++i)
                    {
                        const auto global_index_vectorial = static_cast<unsigned int>(local_2_global[i]);

                        const auto& global_index_scalar_container = match[global_index_vectorial];

                        const auto global_index_scalar = global_index_scalar_container.back();

                        assert(global_index_scalar_container.size() == 1 && "In current example target is scalar...");

                        const auto it = buf_global_index_scalar.find(global_index_scalar);

                        if (it == buf_global_index_scalar.cend())
                        {
                            buf_global_index_scalar.insert(global_index_scalar);
                            ++local_scalar_counter;
                            assert(local_scalar_counter < u0.size());
                            assert(local_scalar_counter < u1.size());
                            u0[local_scalar_counter] = ghost_vector_content_u0.GetValue(static_cast<unsigned int>(global_index_scalar));
                            u1[local_scalar_counter] = ghost_vector_content_u1.GetValue(static_cast<unsigned int>(global_index_scalar));
                        }
                    }

                    assert(buf_global_index_scalar.size() == u0.size());
                    assert(buf_global_index_scalar.size() == u1.size());
                }
            }


        } // anonymous


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        inline const GlobalParameterOperatorNS::UpdateCauchyGreenTensor&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetCauchyGreenTensorOperator() const noexcept
        {
            assert(!(!cauchy_green_tensor_operator_));
            return *cauchy_green_tensor_operator_;
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
