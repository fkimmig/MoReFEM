///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HPP_

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

# include "Geometry/Coords/Internal/Factory.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

# include "Parameters/Parameter.hpp"

# include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        /*!
         * \brief Local FollowingPressure operator.
         *
         * \tparam TypeT Type of the pressure parameter. Only scalar is possible.
         * \tparam TimeDependencyT Policy of the time dependency of the pressure.
         */
        template
        <
            template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
        >
        class FollowingPressure final : public NonlinearLocalVariationalOperator
        <
            LocalMatrix,
            LocalVector
        >,
        public Crtp::LocalMatrixStorage<FollowingPressure<TimeDependencyT>, 2ul>,
        public Crtp::LocalVectorStorage<FollowingPressure<TimeDependencyT>, 3ul>
        {

//            static_assert(TypeT ==  ParameterNS::Type::scalar,
//                          "Only meaningful for this type.");

        public:

            //! Alias to self.
            using self = FollowingPressure<TimeDependencyT>;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Type of the elementary matrix.
            using matrix_type = LocalMatrix;

            //! Type of the elementary vector.
            using vector_type = LocalVector;

            //! Alias to the parent that provides LocalMatrixStorage.
            using matrix_parent = Crtp::LocalMatrixStorage<self, 2ul>;

            //! Alias to the parent that provides LocalVectorStorage.
            using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

            //! Alias to the parameter type.
            using parameter_type = Parameter<ParameterNS::Type::scalar, LocalCoords, TimeDependencyT>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * two unknowns (the first one vectorial and the second one scalar).
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in] pressure \a Parameter which holds static pressure values.
             *
             * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
             * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
             */
            explicit FollowingPressure(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                       const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                       elementary_data_type&& elementary_data,
                                       const parameter_type& pressure);

            //! Destructor.
            ~FollowingPressure();

            //! Copy constructor.
            FollowingPressure(const FollowingPressure&) = delete;

            //! Move constructor.
            FollowingPressure(FollowingPressure&&) = delete;

            //! Copy affectation.
            FollowingPressure& operator=(const FollowingPressure&) = delete;

            //! Move affectation.
            FollowingPressure& operator=(FollowingPressure&&) = delete;

            ///@}


            //! Compute the elementary vector.
            void ComputeEltArray();

            //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
            void InitLocalComputation() {}

            //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
            void FinalizeLocalComputation() {}


            //! Constant accessor to the former local displacement required by ComputeEltArray().
            const std::vector<double>& GetFormerLocalDisplacement() const noexcept;

            //! Non constant accessor to the former local displacement required by ComputeEltArray().
            std::vector<double>& GetNonCstFormerLocalDisplacement() noexcept;



        private:

            //! Get the applied pressure.
            const parameter_type& GetPressure() const noexcept;

            /*!
             * \brief Non constant accessor to the helper Coords object, useful when Ops function acts upon global
             * elements.
             */
            SpatialPoint& GetNonCstWorkCoords() noexcept;

        private:

            //! \name Parameters.
            ///@{

            //! Scalar parameter pressure.
            const parameter_type& pressure_;

            ///@}


            /*!
             * \brief Displacement obtained at previous time iteration expressed at the local level.
             *
             * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within ComputeEltArray.
             */
            std::vector<double> former_local_displacement_;

            //! Helper Coords object.
            SpatialPoint work_coords_;

        private:

            /// \name Useful indexes to fetch the work matrices and vectors.
            ///@{

            //! Indexes related to local matrices.
            enum class LocalMatrixIndex : std::size_t
            {
                phi_dphi_testdr = 0,
                phi_dphi_testds
            };


            //! Indexes related to local vectors.
            enum class LocalVectorIndex : std::size_t
            {
                dxdr = 0,
                dxds,
                dxdr_cross_dxds
            };
            ///@}

        };


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/FollowingPressure.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HPP_
