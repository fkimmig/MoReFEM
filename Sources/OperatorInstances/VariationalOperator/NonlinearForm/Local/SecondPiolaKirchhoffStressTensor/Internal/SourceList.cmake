target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Helper.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Helper.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PartialSpecialization.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PartialSpecialization.hxx" / 
)

