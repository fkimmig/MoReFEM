///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Jan 2016 15:10:18 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                template <unsigned int DimensionT, class HyperelasticityPolicyT>
                void ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyT>
                ::Perform(const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          const LocalVector& cauchy_green_tensor_value,
                          HyperelasticityPolicyT& hyperelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    hyperelasticity.ComputeWDerivates(quad_pt,
                                                      geom_elt,
                                                      cauchy_green_tensor_value,
                                                      dW,
                                                      d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesHyperelasticity<DimensionT, SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None>
                ::Perform(const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          const LocalVector& cauchy_green_tensor_value,
                          SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None& hyperelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(quad_pt);
                    static_cast<void>(geom_elt);
                    static_cast<void>(cauchy_green_tensor_value);
                    static_cast<void>(hyperelasticity);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                }


                template <unsigned int DimensionT, class ActiveStressPolicyT>
                void ComputeWDerivatesActiveStress<DimensionT, ActiveStressPolicyT>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalVector& cauchy_green_tensor_value,
                          const LocalMatrix& transposed_De,
                          ActiveStressPolicyT& active_stress,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    active_stress.template ComputeWDerivates<DimensionT>(infos_at_quad_pt,
                                                                         geom_elt,
                                                                         ref_felt,
                                                                         cauchy_green_tensor_value,
                                                                         transposed_De,
                                                                         dW,
                                                                         d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesActiveStress<DimensionT, SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalVector& cauchy_green_tensor_value,
                          const LocalMatrix& transposed_De,
                          SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None& active_stress,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(infos_at_quad_pt);
                    static_cast<void>(geom_elt);
                    static_cast<void>(ref_felt);
                    static_cast<void>(cauchy_green_tensor_value);
                    static_cast<void>(transposed_De);
                    static_cast<void>(active_stress);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                }


                template <class ActiveStressPolicyT>
                void CorrectRHSWithActiveSchurComplement<ActiveStressPolicyT>
                ::Perform(ActiveStressPolicyT& active_stress,
                          LocalVector& rhs)
                {
                    active_stress.CorrectRHSWithActiveSchurComplement(rhs);
                }


                template <unsigned int FiberIndexT>
                inline void CorrectRHSWithActiveSchurComplement<SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>>
                ::Perform(SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::AnalyticalPrestress<FiberIndexT>& active_stress,
                          LocalVector& rhs)
                {
                    static_cast<void>(rhs);
                    static_cast<void>(active_stress);
                }


                inline void CorrectRHSWithActiveSchurComplement<SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None>
                ::Perform(SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None& active_stress,
                          LocalVector& rhs)
                {
                    static_cast<void>(rhs);
                    static_cast<void>(active_stress);
                }


                template <unsigned int DimensionT, class ViscoelasticityPolicyT>
                void ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyT>
                ::Perform(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                          const GeometricElt& geom_elt,
                          const LocalMatrix& De,
                          const LocalMatrix& transposed_De,
                          ViscoelasticityPolicyT& viscoelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    viscoelasticity.template ComputeWDerivates<DimensionT>(infos_at_quad_pt,
                                                                           geom_elt,
                                                                           De,
                                                                           transposed_De,
                                                                           dW,
                                                                           d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesViscoelasticity<DimensionT, SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None>
                ::Perform(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                          const GeometricElt& geom_elt,
                          const LocalMatrix& De,
                          const LocalMatrix& transposed_De,
                          SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None& viscoelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(infos_at_quad_pt);
                    static_cast<void>(geom_elt);
                    static_cast<void>(viscoelasticity);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                    static_cast<void>(De);
                    static_cast<void>(transposed_De);
                }


                template <class ViscoelasticityPolicyT>
                void AddTangentMatrixViscoelasticity<ViscoelasticityPolicyT>
                ::Perform(LocalMatrix& tangent_matrix,
                          ViscoelasticityPolicyT& viscoelasticity)
                {
                    const auto& tangent_matrix_visco = viscoelasticity.GetMatrixTangentVisco();

                    Seldon::Add(1., tangent_matrix_visco, tangent_matrix);
                }


                inline void AddTangentMatrixViscoelasticity<SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None>
                ::Perform(LocalMatrix& tangent_matrix,
                          ViscoelasticityPolicyNS::None& viscoelasticity)
                {
                    static_cast<void>(tangent_matrix);
                    static_cast<void>(viscoelasticity);
                }


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_
