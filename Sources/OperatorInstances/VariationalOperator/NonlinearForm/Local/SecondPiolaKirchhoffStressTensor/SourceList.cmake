target_sources(${MOREFEM_OP_INSTANCES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/ActiveStressPolicy/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/HyperelasticityPolicy/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ViscoelasticityPolicy/SourceList.cmake)
