///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ViscoelasticityPolicyNS
            {


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                Viscoelasticity<DerivatesWithRespectToT>::Viscoelasticity(const unsigned int mesh_dimension,
                                                                          const unsigned int Ndof_unknown,
                                                                          const unsigned int Ncomponent,
                                                                          const Solid& solid,
                                                                          const TimeManager& time_manager)
                : matrix_parent(),
                vector_parent(),
                time_manager_(time_manager),
                viscosity_(solid.GetViscosity())
                {
                    std::pair<unsigned int, unsigned int> identity_size;
                    const unsigned int square_Ncomponent = NumericNS::Square(Ncomponent);

                    switch (mesh_dimension)
                    {
                        case 2u:
                            identity_size = { 3, 3 };
                            break;
                        case 3u:
                            identity_size = { 6, 6 };
                            break;
                        default:
                            assert(false);
                            break;
                    }

                    this->matrix_parent::InitLocalMatrixStorage
                    ({{
                        identity_size,
                        { square_Ncomponent, square_Ncomponent },
                        { mesh_dimension, mesh_dimension }
                    }});


                    this->vector_parent::InitLocalVectorStorage
                    ({{
                        mesh_dimension * mesh_dimension
                    }});

                    auto& d2W_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();
                    d2W_visco.SetIdentity();

                    this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>().Zero();

                    local_velocity_.resize(Ndof_unknown);

                    deriv_eta_ =
                    std::make_unique<Advanced::LocalVariationalOperatorNS::DerivativeGreenLagrange<GreenLagrangeOrEta::eta>>(mesh_dimension);
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                void Viscoelasticity<DerivatesWithRespectToT>::MatrixToVector(const LocalMatrix& gradient_matrix, LocalVector& vector)
                {
                    int size = gradient_matrix.GetM();
                    assert(size == gradient_matrix.GetN() && "Implemented for square matrix");

                    for (int i = 0; i < size ; ++i)
                    {
                        for (int j = 0; j < size ; ++j)
                        {
                            vector(i * size + j) = gradient_matrix(i, j);
                        }
                    }
                }


                template <>
                template<unsigned int DimensionT>
                void Viscoelasticity<DerivatesWithRespectTo::displacement_and_velocity>::ComputeWDerivates(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                                                                                           const GeometricElt& geom_elt,
                                                                                                           const LocalMatrix& De,
                                                                                                           const LocalMatrix& transposed_De,
                                                                                                           LocalVector& dW,
                                                                                                           LocalMatrix& d2W)
                {
                    const auto run_case = GetTimeManager().GetStaticOrDynamic();

                    if (run_case == StaticOrDynamic::dynamic_)
                    {
                        double eta = GetViscosity().GetValue(infos_at_quad_pt.GetQuadraturePoint(), geom_elt);
                        double dt = GetTimeManager().GetTimeStep();

                        const auto& local_velocity = GetLocalVelocity();

                        auto& tangent_matrix_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>();
                        tangent_matrix_visco.Zero();
                        auto& d2W_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();
                        auto& matrix_gradient_velocity = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_velocity)>();

                        auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::velocity_gradient)>();

                        matrix_gradient_velocity.Zero();
                        Advanced::OperatorNS::ComputeGradientDisplacementMatrix(infos_at_quad_pt,
                                                                                local_velocity,
                                                                                matrix_gradient_velocity);

                        MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                        auto& derivative_eta = GetNonCstDerivativeEta();
                        const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                        Seldon::MltAdd(eta, De, vector_gradient_velocity, 1., dW);

                        Seldon::Add(2. * eta / dt, d2W_visco, d2W);

                        Seldon::Mlt(eta, transposed_De, Deta, tangent_matrix_visco);
                    }
                }


                template <>
                template<unsigned int DimensionT>
                void Viscoelasticity<DerivatesWithRespectTo::displacement>::ComputeWDerivates(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                                                                              const GeometricElt& geom_elt,
                                                                                              const LocalMatrix& De,
                                                                                              const LocalMatrix& transposed_De,
                                                                                              LocalVector& dW,
                                                                                              LocalMatrix& d2W)
                {
                    static_cast<void>(d2W);
                    const auto run_case = GetTimeManager().GetStaticOrDynamic();

                    if (run_case == StaticOrDynamic::dynamic_)
                    {
                        double eta = GetViscosity().GetValue(infos_at_quad_pt.GetQuadraturePoint(), geom_elt);
                        const auto& local_velocity = GetLocalVelocity();

                        auto& tangent_matrix_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>();
                        tangent_matrix_visco.Zero();
                        auto& matrix_gradient_velocity = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_velocity)>();

                        auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::velocity_gradient)>();

                        matrix_gradient_velocity.Zero();
                        Advanced::OperatorNS::ComputeGradientDisplacementMatrix(infos_at_quad_pt, local_velocity, matrix_gradient_velocity);

                        MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                        auto& derivative_eta = GetNonCstDerivativeEta();
                        const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                        Seldon::MltAdd(eta, De, vector_gradient_velocity, 1., dW);

                        Seldon::Mlt(eta, transposed_De, Deta, tangent_matrix_visco);
                    }
                }


                template <>
                template<unsigned int DimensionT>
                void Viscoelasticity<DerivatesWithRespectTo::velocity>::ComputeWDerivates(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                                                                          const GeometricElt& geom_elt,
                                                                                          const LocalMatrix& De,
                                                                                          const LocalMatrix& transposed_De,
                                                                                          LocalVector& dW,
                                                                                          LocalMatrix& d2W)
                {
                    static_cast<void>(dW);
                    static_cast<void>(transposed_De);
                    static_cast<void>(De);

                    const auto run_case = GetTimeManager().GetStaticOrDynamic();

                    if (run_case == StaticOrDynamic::dynamic_)
                    {
                        double eta = GetViscosity().GetValue(infos_at_quad_pt.GetQuadraturePoint(), geom_elt);

                        auto& tangent_matrix_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>();
                        tangent_matrix_visco.Zero();
                        auto& d2W_visco = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();

                        Seldon::Add(eta, d2W_visco, d2W);
                    }
                }



                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline const TimeManager& Viscoelasticity<DerivatesWithRespectToT>::GetTimeManager() const noexcept
                {
                    return time_manager_;
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline const std::vector<double>&
                Viscoelasticity<DerivatesWithRespectToT>::GetLocalVelocity() const noexcept
                {
                    return local_velocity_;
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline std::vector<double>&
                Viscoelasticity<DerivatesWithRespectToT>::GetNonCstLocalVelocity() noexcept
                {
                    return const_cast<std::vector<double>&>(GetLocalVelocity());
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline Advanced::LocalVariationalOperatorNS::DerivativeGreenLagrange<Viscoelasticity<DerivatesWithRespectToT>::GreenLagrangeOrEta::eta>&
                Viscoelasticity<DerivatesWithRespectToT>::GetNonCstDerivativeEta() noexcept
                {
                    assert(!(!deriv_eta_));
                    return *deriv_eta_;
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline const LocalMatrix& Viscoelasticity<DerivatesWithRespectToT>::GetMatrixTangentVisco() const noexcept
                {
                    return  this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>();
                }


                template <DerivatesWithRespectTo DerivatesWithRespectToT>
                inline const typename Viscoelasticity<DerivatesWithRespectToT>::scalar_parameter&
                Viscoelasticity<DerivatesWithRespectToT>::GetViscosity() const noexcept
                {
                    return viscosity_;
                }


            } // namespace ViscoelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HXX_
