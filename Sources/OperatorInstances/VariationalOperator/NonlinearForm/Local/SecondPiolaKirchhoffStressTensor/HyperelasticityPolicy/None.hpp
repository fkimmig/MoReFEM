///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_


# include "ParameterInstances/Compound/Solid/Solid.hpp"

namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace HyperelasticityPolicyNS
            {


                /*!
                 * \brief Policy to use when there are no hyperelasticity involved in the
                 * \a SecondPiolaKirchhoffStressTensor operator.
                 */
                class None
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = None;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                    //! Expected alias.
                    using law_type = std::nullptr_t;

                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit None(unsigned int mesh_dimension,
                                  const law_type* hyperelastic_law);

                    //! Destructor.
                    ~None() = default;

                    //! Copy constructor.
                    None(const None&) = delete;

                    //! Move constructor.
                    None(None&&) = delete;

                    //! Copy affectation.
                    None& operator=(const None&) = delete;

                    //! Move affectation.
                    None& operator=(None&&) = delete;

                    ///@}

                };


            } // namespace HyperelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_NONE_HPP_
