target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InputAnalyticalPrestress.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/None.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InputAnalyticalPrestress.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InputAnalyticalPrestress.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InputNone.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/None.hpp" / 
)

