target_sources(${MOREFEM_OP_INSTANCES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FollowingPressure.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/SecondPiolaKirchhoffStressTensor/SourceList.cmake)
