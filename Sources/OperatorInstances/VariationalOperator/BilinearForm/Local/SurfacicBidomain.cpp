///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"
#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        SurfacicBidomain::SurfacicBidomain(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                           const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                           elementary_data_type&& a_elementary_data,
                                           const scalar_parameter& intracellular_trans_diffusion_tensor,
                                           const scalar_parameter& extracellular_trans_diffusion_tensor,
                                           const scalar_parameter& intracellular_fiber_diffusion_tensor,
                                           const scalar_parameter& extracellular_fiber_diffusion_tensor,
                                           const scalar_parameter& heterogeneous_conductivity_coefficient,
                                           const FiberList<ParameterNS::Type::vector>& fibers,
                                           const FiberList<ParameterNS::Type::scalar>& angles)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent(),
        intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
        extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
        intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
        extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor),
        heterogeneous_conductivity_coefficient_(heterogeneous_conductivity_coefficient),
        fibers_(fibers),
        angles_(angles)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unkown1 = static_cast<int>(unknown1_ref_felt.Nnode());
            
            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            const int Nnode_for_unkown2 = static_cast<int>(unknown2_ref_felt.Nnode());
            
            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unkown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());
            
            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
            const int Nnode_for_test_unkown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());
            
            const unsigned int ref_felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension();
            
            const unsigned int geometric_mesh_region_dimension = unknown1_ref_felt.GetMeshDimension();
            
            assert(ref_felt_space_dimension < geometric_mesh_region_dimension && "This operator is a surfacic operator.");
            assert((ref_felt_space_dimension + 1) == geometric_mesh_region_dimension && "This operator is a surfacic operator.");
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_unkown1, Nnode_for_test_unkown1}, // block matrix 1 (Vm,Vm)
                    { Nnode_for_unkown2, Nnode_for_test_unkown2}, // block matrix 2 (Ue,Ue)
                    { Nnode_for_unkown1, Nnode_for_test_unkown2}, // block matrix 3 (Vm, Ue)
                    { Nnode_for_unkown2, Nnode_for_test_unkown1}, // block matrix 4 (Ue, Vm)
                    { ref_felt_space_dimension, Nnode_for_test_unkown1}, // transposed dPhi
                    { ref_felt_space_dimension, Nnode_for_test_unkown2}, // transposed dPsi
                    { Nnode_for_unkown1, ref_felt_space_dimension}, // dPhi*sigma
                    { Nnode_for_unkown2, ref_felt_space_dimension}, // dPsi*sigma
                    { Nnode_for_unkown1, ref_felt_space_dimension}, // dPhi*contravariant_metric_tensor
                    { Nnode_for_unkown2, ref_felt_space_dimension}, // dPsi*contravariant_metric_tensor
                    { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_X_tau_0
                    { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_ortho_X_tau_0_ortho
                    { geometric_mesh_region_dimension, geometric_mesh_region_dimension}, // covariant basis
                    { geometric_mesh_region_dimension, geometric_mesh_region_dimension}, // transpose covariant basis
                    { geometric_mesh_region_dimension, geometric_mesh_region_dimension}, // contravariant basis
                    { geometric_mesh_region_dimension, geometric_mesh_region_dimension}, // covariant metric tensor
                    { geometric_mesh_region_dimension, geometric_mesh_region_dimension}, // full contravariant metric tensor
                    { ref_felt_space_dimension, ref_felt_space_dimension} // reduced contravariant metric tensor
                }});
            
            this->InitLocalVectorStorage
            ({{
                geometric_mesh_region_dimension, // tau_interpolate_ortho
                ref_felt_space_dimension, // tau_covariant_basis
                ref_felt_space_dimension // tau_ortho_covariant_basis
            }});
        }
        
                                                                       
        SurfacicBidomain::~SurfacicBidomain() = default;
        
                                                                       
        const std::string& SurfacicBidomain::ClassName()
        {
            static std::string name("SurfacicBidomain");
            return name;
        }
        
        
        void SurfacicBidomain::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            
            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
            
            const int geometric_mesh_region_dimension = static_cast<int>(unknown1_ref_felt.GetMeshDimension());
            
            auto& block_matrix1 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
            auto& block_matrix2 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
            auto& block_matrix3 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
            auto& block_matrix4 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
            auto& transposed_dphi_test = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_test)>();
            auto& transposed_dpsi_test = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dpsi_test)>();
            auto& dphi_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_sigma)>();
            auto& dpsi_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_sigma)>();
            auto& dphi_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_contravariant_metric_tensor)>();
            auto& dpsi_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_contravariant_metric_tensor)>();
            auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
            auto& tau_ortho_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_ortho_sigma)>();
            auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
            auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();
            
            auto& tau_interpolate_ortho = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_interpolate_ortho)>();
            auto& tau_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_covariant_basis)>();
            auto& tau_ortho_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_ortho_covariant_basis)>();;
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const int Nnode_for_unknown1 = static_cast<int>(unknown1_ref_felt.Nnode());
            const int Nnode_for_unknown2 = static_cast<int>(unknown2_ref_felt.Nnode());
            
            assert(unknown1_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            assert(unknown2_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            
            const int Nnode_for_test_unknown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());
            const int Nnode_for_test_unknown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());
            
            assert(test_unknown1_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            assert(test_unknown2_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            
            const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
            const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
            const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
            const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();
            const auto& heterogeneous_conductivity_coefficient = GetHeterogeneousConductivityCoefficient();
            
            auto& fibers = GetFibers();
            auto& angles = GetAngles();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());
            
            unsigned int quad_pt_index = 0;
            
            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];
                
                ComputeContravariantBasis(infos_at_quad_pt_for_unknown);

                const double factor = infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetWeight()
                * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                const auto& quad_pt = infos_at_quad_pt_for_test_unknown.GetQuadraturePoint();
                
                const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const double heterogeneous_conductivity_coefficient_at_quad = heterogeneous_conductivity_coefficient.GetValue(quad_pt, geom_elt);
                
                const auto& grad_felt_phi_for_unknown = infos_at_quad_pt_for_unknown.GetGradientRefFEltPhi();
                
                const auto& dphi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown1_ref_felt);
                
                const auto& dpsi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown2_ref_felt);
                
                const auto& grad_felt_phi_for_test_unknown = infos_at_quad_pt_for_test_unknown.GetGradientRefFEltPhi();
                
                const auto& dphi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown1_ref_felt);
                
                const auto& dpsi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown2_ref_felt);
                
                assert(dphi.GetM() == Nnode_for_unknown1);
                assert(dpsi.GetM() == Nnode_for_unknown2);
                assert(dphi_test.GetM() == Nnode_for_test_unknown1);
                assert(dpsi_test.GetM() == Nnode_for_test_unknown2);
                
                Wrappers::Seldon::Transpose(dphi_test, transposed_dphi_test);
                Wrappers::Seldon::Transpose(dpsi_test, transposed_dpsi_test);
                
                block_matrix1.Zero();
                block_matrix2.Zero();
                block_matrix3.Zero();
                block_matrix4.Zero();
                
                Seldon::Mlt(factor1,
                            dphi,
                            reduced_contravariant_metric_tensor,
                            dphi_contravariant_metric_tensor);
                
                Seldon::Mlt(1.,
                            dphi_contravariant_metric_tensor,
                            transposed_dphi_test,
                            block_matrix1);
                
                Seldon::Mlt(factor1 + factor2,
                            dpsi,
                            reduced_contravariant_metric_tensor,
                            dpsi_contravariant_metric_tensor);
                
                Seldon::Mlt(1.,
                            dpsi_contravariant_metric_tensor,
                            transposed_dpsi_test,
                            block_matrix2);
                
                Seldon::Mlt(factor1,
                            dphi,
                            reduced_contravariant_metric_tensor,
                            dphi_contravariant_metric_tensor);
                
                Seldon::Mlt(1.,
                            dphi_contravariant_metric_tensor,
                            transposed_dpsi_test,
                            block_matrix3);
                
                Seldon::Mlt(factor1,
                            dpsi,
                            reduced_contravariant_metric_tensor,
                            dpsi_contravariant_metric_tensor);
                
                Seldon::Mlt(1.,
                            dpsi_contravariant_metric_tensor,
                            transposed_dphi_test,
                            block_matrix4);
                
                // Fiber part of the operator.
                const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);
                
                double norm = 0.;
                for (int component = 0 ; component < geometric_mesh_region_dimension ; ++component)
                    norm += tau_interpolate(component) * tau_interpolate(component);
                
                tau_sigma.Zero();
                tau_ortho_sigma.Zero();
                
                if (!(NumericNS::IsZero(norm)))
                {
                    // tau_interpolate_ortho
                    tau_interpolate_ortho.Zero();
                    tau_interpolate_ortho(0) = tau_interpolate(1) * contravariant_basis(2,2) - tau_interpolate(2) * contravariant_basis(1,2);
                    tau_interpolate_ortho(1) = tau_interpolate(2) * contravariant_basis(0,2) - tau_interpolate(0) * contravariant_basis(2,2);
                    tau_interpolate_ortho(2) = tau_interpolate(0) * contravariant_basis(1,2) - tau_interpolate(1) * contravariant_basis(0,2);
                    
                    tau_covariant_basis.Zero();
                    tau_ortho_covariant_basis.Zero();
                    
                    // Projection in the covariant basis.
                    for (int cov_component = 0 ; cov_component < felt_space_dimension ; ++cov_component)
                    {
                        for (int component = 0 ; component < geometric_mesh_region_dimension ; ++component)
                        {
                            tau_covariant_basis(cov_component) += tau_interpolate(component) * contravariant_basis(component,cov_component);
                            tau_ortho_covariant_basis(cov_component) += tau_interpolate_ortho(component) * contravariant_basis(component,cov_component);
                        }
                    }
                    
                    Wrappers::Seldon::OuterProd(tau_covariant_basis, tau_covariant_basis, tau_sigma);
                    
                    Wrappers::Seldon::OuterProd(tau_ortho_covariant_basis, tau_ortho_covariant_basis, tau_ortho_sigma);
                    
                    const auto& theta = angles.GetValue(quad_pt, geom_elt);
                    
                    double i_theta = 0.;
                    
                    if (std::fabs(theta) < NumericNS::DefaultEpsilon<double>())
                        i_theta = 1.;
                    else
                        i_theta = 0.5 + std::sin(2. * theta) / (4. * theta);

                    Seldon::Mlt(1. - i_theta, tau_ortho_sigma);
                    
                    Mlt(i_theta / norm, tau_sigma);
                    Seldon::Add(1. / norm, tau_ortho_sigma, tau_sigma);
                   
                    Seldon::Mlt((factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad,
                                dphi,
                                tau_sigma,
                                dphi_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dphi_sigma,
                                   transposed_dphi_test,
                                   1.,
                                   block_matrix1);
                    
                    Seldon::Mlt((factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad,
                                dphi,
                                tau_sigma,
                                dphi_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dphi_sigma,
                                   transposed_dpsi_test,
                                   1.,
                                   block_matrix3);
                    
                    Seldon::Mlt((factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad,
                                dpsi,
                                tau_sigma,
                                dpsi_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dpsi_sigma,
                                   transposed_dphi_test,
                                   1.,
                                   block_matrix4);
                    
                    Seldon::Mlt((factor3 - factor1 + factor4 - factor2) * heterogeneous_conductivity_coefficient_at_quad,
                                dpsi,
                                tau_sigma,
                                dpsi_sigma);
                    
                    Seldon::MltAdd(1.,
                                   dpsi_sigma,
                                   transposed_dpsi_test,
                                   1.,
                                   block_matrix2);
                }

                // Then report it into the elementary matrix.
                for (int node_unknown1_index_j = 0; node_unknown1_index_j < Nnode_for_test_unknown1; ++node_unknown1_index_j)
                {
                    for (int node_unknown1_index_i = 0; node_unknown1_index_i < Nnode_for_unknown1; ++node_unknown1_index_i)
                    {
                        const double value1 = block_matrix1(node_unknown1_index_i, node_unknown1_index_j);
                        
                        matrix_result(node_unknown1_index_i, node_unknown1_index_j) += value1;
                    }
                }
                
                for (int node_unknown2_index_j = 0; node_unknown2_index_j < Nnode_for_test_unknown2; ++node_unknown2_index_j)
                {
                    for (int node_unknown2_index_i = 0; node_unknown2_index_i < Nnode_for_unknown2; ++node_unknown2_index_i)
                    {
                        const double value2 = block_matrix2(node_unknown2_index_i, node_unknown2_index_j);
                        
                        matrix_result(node_unknown2_index_i + Nnode_for_unknown1, node_unknown2_index_j + Nnode_for_test_unknown1) +=  value2;
                    }
                }
                
                for (int node_unknown2_index_j = 0; node_unknown2_index_j < Nnode_for_test_unknown2; ++node_unknown2_index_j)
                {
                    for (int node_unknown1_index_i = 0; node_unknown1_index_i < Nnode_for_unknown1; ++node_unknown1_index_i)
                    {
                        const double value3 = block_matrix3(node_unknown1_index_i, node_unknown2_index_j);
                        
                        matrix_result(node_unknown1_index_i, node_unknown2_index_j + Nnode_for_test_unknown1) +=  value3;
                    }
                }
                
                for (int node_unknown1_index_j = 0; node_unknown1_index_j < Nnode_for_test_unknown1; ++node_unknown1_index_j)
                {
                    for (int node_unknown2_index_i = 0; node_unknown2_index_i < Nnode_for_unknown2; ++node_unknown2_index_i)
                    {
                        const double value4 = block_matrix4(node_unknown2_index_i, node_unknown1_index_j);
                        
                        matrix_result(node_unknown2_index_i + Nnode_for_unknown1 , node_unknown1_index_j) +=  value4;
                    }
                }
                
                ++quad_pt_index;
            }
        }
        
        
        void SurfacicBidomain::ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt)
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
            auto& transposed_covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
            auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
            auto& covariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
            auto& contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();
            auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();
            
            covariant_basis.Zero();
            transposed_covariant_basis.Zero();
            contravariant_basis.Zero();
            covariant_metric_tensor.Zero();
            contravariant_metric_tensor.Zero();
            reduced_contravariant_metric_tensor.Zero();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const auto& dphi_geo = infos_at_quad_pt.GetGradientRefGeometricPhi();

            const unsigned int Ncomponent = geom_elt.GetDimension();
            
            const unsigned int geometric_mesh_region_dimension = infos_at_quad_pt.GetMeshDimension();
            
            const int int_geometric_mesh_region_dimension = static_cast<int>(geometric_mesh_region_dimension);
            
            covariant_basis.Zero();
            
            const unsigned int Nshape_function = static_cast<unsigned int>(dphi_geo.GetM());
            
            for (unsigned int component_shape_function = 0u; component_shape_function < Ncomponent; ++component_shape_function)
            {
                const int int_component_shape_function = static_cast<int>(component_shape_function);
                
                for (unsigned int shape_fct_index = 0u; shape_fct_index < Nshape_function; ++shape_fct_index)
                {
                    const int int_shape_fct_index = static_cast<int>(shape_fct_index);
                    
                    const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

                    for (unsigned int coord_index = 0u; coord_index < geometric_mesh_region_dimension; ++coord_index)
                    {
                        const int int_coord_index = static_cast<int>(coord_index);
                        
                        covariant_basis(int_coord_index, int_component_shape_function) += coords_in_geom_elt[coord_index] * dphi_geo(int_shape_fct_index, int_component_shape_function);
                    }
                }
            }
            
            double norm = 0.;
            
            // Normal computation.
            switch (geometric_mesh_region_dimension)
            {
                case 2:
                    covariant_basis(0, int_geometric_mesh_region_dimension - 1) = covariant_basis(1,0);
                    covariant_basis(1, int_geometric_mesh_region_dimension - 1) = - covariant_basis(0,0);
                    norm = std::sqrt(covariant_basis(0, int_geometric_mesh_region_dimension - 1) * covariant_basis(0, int_geometric_mesh_region_dimension - 1) + covariant_basis(1, int_geometric_mesh_region_dimension - 1) * covariant_basis(1, int_geometric_mesh_region_dimension - 1));
                    covariant_basis(0, int_geometric_mesh_region_dimension - 1) /= norm;
                    covariant_basis(1, int_geometric_mesh_region_dimension - 1) /= norm;
                    break;
                case 3:
                    covariant_basis(0, int_geometric_mesh_region_dimension - 1) = covariant_basis(1,0) * covariant_basis(2,1) - covariant_basis(2,0) * covariant_basis(1,1);
                    covariant_basis(1, int_geometric_mesh_region_dimension - 1) = covariant_basis(2,0) * covariant_basis(0,1) - covariant_basis(0,0) * covariant_basis(2,1);
                    covariant_basis(2, int_geometric_mesh_region_dimension - 1) = covariant_basis(0,0) * covariant_basis(1,1) - covariant_basis(1,0) * covariant_basis(0,1);
                    norm = std::sqrt(covariant_basis(0, int_geometric_mesh_region_dimension - 1) * covariant_basis(0, int_geometric_mesh_region_dimension - 1) +
                                     covariant_basis(1, int_geometric_mesh_region_dimension - 1) * covariant_basis(1, int_geometric_mesh_region_dimension - 1) +
                                     covariant_basis(2, int_geometric_mesh_region_dimension - 1) * covariant_basis(2, int_geometric_mesh_region_dimension - 1));
                    covariant_basis(0, int_geometric_mesh_region_dimension - 1) /= norm;
                    covariant_basis(1, int_geometric_mesh_region_dimension - 1) /= norm;
                    covariant_basis(2, int_geometric_mesh_region_dimension - 1) /= norm;
                    break;
            }
            
            covariant_metric_tensor.Zero();
            
            Wrappers::Seldon::Transpose(covariant_basis, transposed_covariant_basis);
            
            Seldon::Mlt(1., transposed_covariant_basis, covariant_basis, covariant_metric_tensor);
            
            double determinant = 0;
            
            Wrappers::Seldon::ComputeInverseSquareMatrix(covariant_metric_tensor, contravariant_metric_tensor, determinant);
            
            int int_Ncomponent = static_cast<int>(Ncomponent);
            
            for (int j = 0; j < int_Ncomponent ; ++j)
            {
                for (int i = 0 ; i < int_Ncomponent ; ++i)
                {
                    reduced_contravariant_metric_tensor(i,j) = contravariant_metric_tensor(i,j);
                }
            }
           
            contravariant_basis.Zero();
            
            Seldon::Mlt(1., covariant_basis, contravariant_metric_tensor, contravariant_basis);
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
