///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 09:39:20 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        GradOnGradientBasedElasticityTensor
        ::GradOnGradientBasedElasticityTensor(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                              const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                              elementary_data_type&& a_elementary_data,
                                              const scalar_parameter& young_modulus,
                                              const scalar_parameter& poisson_ratio,
                                              const ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
        : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent()
        {
            assert(unknown_list.size() == 1);
            assert(test_unknown_list.size() == 1);
            
            const auto& elementary_data = GetElementaryData();
            
            gradient_based_elasticity_tensor_parameter_ =
                Internal::LocalVariationalOperatorNS::InitGradientBasedElasticityTensor(young_modulus,
                                                                                        poisson_ratio,
                                                                                        configuration);
            
            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unknown = static_cast<int>(unknown_ref_felt.Nnode());
            
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown = static_cast<int>(test_unknown_ref_felt.Nnode());
            
            const int felt_space_dimension = static_cast<int>(unknown_ref_felt.GetFEltSpaceDimension());
            
            InitLocalMatrixStorage(
            {
                {
                    { felt_space_dimension, felt_space_dimension }, // gradient_based_block
                    { Nnode_for_unknown, Nnode_for_test_unknown }, // block_contribution
                    { felt_space_dimension, Nnode_for_test_unknown }, // transposed dphi test
                    { Nnode_for_unknown, felt_space_dimension } // dphi x gradient_based_block
                }
            });
        }
        
        
        GradOnGradientBasedElasticityTensor::~GradOnGradientBasedElasticityTensor() = default;
        
        
        const std::string& GradOnGradientBasedElasticityTensor::ClassName()
        {
            static std::string name("GradOnGradientBasedElasticityTensor");
            return name;
        }
        
        
        void GradOnGradientBasedElasticityTensor::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            
            auto& gradient_based_block = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
            auto& block_contribution = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
            auto& transposed_dphi_test = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_test)>();
            auto& dPhi_mult_gradient_based_block = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dPhi_mult_gradient_based_block)>();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unknown = static_cast<int>(unknown_ref_felt.Nnode());
            
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown = static_cast<int>(test_unknown_ref_felt.Nnode());
            
            const auto Ncomponent = unknown_ref_felt.Ncomponent();
            
            assert(Ncomponent == test_unknown_ref_felt.Ncomponent());
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            unsigned int quad_pt_index = 0;
            
            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];
                
                const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
                
                const auto& gradient_based_elasticity_tensor =
                    GetNonCstGradientBasedElasticityTensor().GetValue(quad_pt, geom_elt);
                                
                const double factor = infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant() * quad_pt.GetWeight();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                
                const LocalMatrix& dphi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi();
                
                const auto& dphi_test = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi();
                
                Wrappers::Seldon::Transpose(dphi_test, transposed_dphi_test);
                
                for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                {
                    const auto row_first_index = static_cast<int>(unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                    
                    for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
                    {
                        const auto col_first_index = static_cast<int>(test_unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component));
                        
                        Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(gradient_based_elasticity_tensor,
                                                                                        row_component,
                                                                                        col_component,
                                                                                        gradient_based_block);
                        
                        Seldon::Mlt(1., dphi,
                                    gradient_based_block,
                                    dPhi_mult_gradient_based_block);
                        
                        Seldon::Mlt(factor,
                                    dPhi_mult_gradient_based_block,
                                    transposed_dphi_test,
                                    block_contribution);
                        
                        for (int row_node = 0; row_node < Nnode_for_unknown; ++row_node)
                        {
                            for (int col_node = 0u; col_node < Nnode_for_test_unknown; ++col_node)
                                matrix_result(row_first_index + row_node, col_first_index + col_node)
                                += block_contribution(row_node, col_node);
                        }
                    }
                }
                
                ++quad_pt_index;
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
         
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
