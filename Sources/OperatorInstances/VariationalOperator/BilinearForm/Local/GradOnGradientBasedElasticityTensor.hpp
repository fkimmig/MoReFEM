///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 09:39:20 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_

# include <memory>
# include <vector>

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"

# include "Parameters/Parameter.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"
# include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace ParameterNS
    {


        enum class GradientBasedElasticityTensorConfiguration;


    } // namespace ParameterNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        /*!
         * \brief Implementation of elastic operator.
         *
         * \todo Improve the comment by writing its mathematical definition!
         */
        class GradOnGradientBasedElasticityTensor final
        : public BilinearLocalVariationalOperator<LocalMatrix>,
        public Crtp::LocalMatrixStorage<GradOnGradientBasedElasticityTensor, 4ul>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = GradOnGradientBasedElasticityTensor;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to parent.
            using parent = BilinearLocalVariationalOperator<LocalMatrix>;

            //! Unique ptr to type of scalar parameters (Young modulus, Poisson ratio, etc...).
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! Type of matricial parameters (gradient-based elasticity tensor).
            using matrix_parameter = Parameter<ParameterNS::Type::matrix, LocalCoords, ParameterNS::TimeDependencyNS::None>;

            //! Alias to the parent that provides LocalMatrixStorage.
            using matrix_parent = Crtp::LocalMatrixStorage<self, 4ul>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (STL vector)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * one vectorial unknown.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in] young_modulus Young modulus of the solid.
             * \param[in] poisson_ratio Poisson coefficient of the solid.
             * \param[in] configuration Whether we consider 2D/plane_strain, 2D/plane_stress operator or 3D operator.
             *
             * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
             * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
             */
            explicit GradOnGradientBasedElasticityTensor(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                                         const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                                         elementary_data_type&& elementary_data,
                                                         const scalar_parameter& young_modulus,
                                                         const scalar_parameter& poisson_ratio,
                                                         const ParameterNS::GradientBasedElasticityTensorConfiguration configuration);

            //! Destructor.
            ~GradOnGradientBasedElasticityTensor();

            //! Copy constructor.
            GradOnGradientBasedElasticityTensor(const GradOnGradientBasedElasticityTensor&) = delete;

            //! Move constructor.
            GradOnGradientBasedElasticityTensor(GradOnGradientBasedElasticityTensor&&) = delete;

            //! Copy affectation.
            GradOnGradientBasedElasticityTensor& operator=(const GradOnGradientBasedElasticityTensor&) = delete;

            //! Move affectation.
            GradOnGradientBasedElasticityTensor& operator=(GradOnGradientBasedElasticityTensor&&) = delete;

            ///@}


            /*!
             * \brief Compute the elementary \a OperatorNatureT.
             *
             * For current operator, only matrix makes sense; so only this specialization is actually defined.
             *
             */
            void ComputeEltArray();

            //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
            void InitLocalComputation() {}

            //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
            void FinalizeLocalComputation() {}



        private:

            //! Init the gradient based elasticity tensor depending on the scheme chosen in the input file.
            void InitGradientBasedElasticityTensor(const scalar_parameter& young_modulus,
                                                   const scalar_parameter& poisson_ratio,
                                                   const ParameterNS::GradientBasedElasticityTensorConfiguration configuration);

            //! Gradient-based elasticity tensor.
            matrix_parameter& GetNonCstGradientBasedElasticityTensor();

        private:

            //! Gradient-based elasticity tensor.
            typename matrix_parameter::unique_ptr gradient_based_elasticity_tensor_parameter_ = nullptr;

        private:

            /// \name Useful indexes to fetch the work matrices and vectors.
            ///@{

            enum class LocalMatrixIndex : std::size_t
            {
                gradient_based_block = 0,
                block_contribution,
                transposed_dphi_test,
                dPhi_mult_gradient_based_block
            };

            ///@}

        };


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_ON_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
