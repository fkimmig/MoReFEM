target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Ale.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Bidomain.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauOrthoTauGradPhi.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauTauGradPhi.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mass.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Stokes.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SurfacicBidomain.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Ale.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Ale.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Bidomain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Bidomain.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauOrthoTauGradPhi.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauOrthoTauGradPhi.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauTauGradPhi.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradPhiTauTauGradPhi.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mass.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mass.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Stokes.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Stokes.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/SurfacicBidomain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SurfacicBidomain.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/VariableMass.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VariableMass.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
