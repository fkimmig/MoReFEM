///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Jan 2016 10:30:12 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        Ale::Ale(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                 const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                 elementary_data_type&& a_elementary_data,
                 const scalar_parameter& density)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        density_(density)
        {
            assert(a_unknown_storage.size() == 1 && "Operator currently written to be used with one unknown only!");
            assert(!(!a_unknown_storage.back()));
            assert(a_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);
            
            assert(a_test_unknown_storage.size() == 1 && "Operator currently written to be used with one unknown only!");
            assert(!(!a_test_unknown_storage.back()));
            assert(a_test_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);
        
            const auto& elementary_data = GetElementaryData();
            
            former_local_velocity_.resize(elementary_data.NdofRow());
        }
                                                                       
                                                                       
        Ale::~Ale() = default;
        
                                                                       
        const std::string& Ale::ClassName()
        {
            static std::string name("Ale");
            return name;
        }
        

        void Ale::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            const auto& local_velocity = GetFormerLocalVelocity();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto Ncomponent = static_cast<std::size_t>(ref_felt.Ncomponent());
            const auto int_Ncomponent = static_cast<int>(Ncomponent);
            
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            assert(Ncomponent == test_ref_felt.Ncomponent());
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.Zero();
            
            const auto Nnode_for_unknown = static_cast<std::size_t>(ref_felt.Nnode());
            const auto int_Nnode_for_unknown = static_cast<int>(Nnode_for_unknown);
            
            const auto Nnode_for_test_unknown = static_cast<std::size_t>(test_ref_felt.Nnode());
            const auto int_Nnode_for_test_unknown = static_cast<int>(Nnode_for_test_unknown);
            
            decltype(auto) geom_elt = elementary_data.GetCurrentGeomElt();
            
            unsigned int quad_pt_index = 0;
            
            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];
                
                const auto& phi = infos_at_quad_pt_for_unknown.GetFEltPhi();
                const auto& grad_phi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi();
                
                const auto& phi_test = infos_at_quad_pt_for_test_unknown.GetFEltPhi();
                
                decltype(auto) quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
                
                const double geometric_factor = quad_pt.GetWeight()
                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                const double factor = geometric_factor * GetDensity().GetValue(quad_pt, geom_elt);
                
                {
                    // ===========================================
                    // First add the modified_v * gradV * Vtest contribution.
                    // Only the block for the first component is filled here.
                    // ===========================================
                    for (auto component = 0ul; component < Ncomponent; ++component)
                    {
                        double modified_velocity_at_quad_pt = 0.;
                        
                        // Compute the \a component for velocity at quadrature point.
                        for (auto node_index = 0ul; node_index < Nnode_for_unknown; ++node_index)
                        {
                            assert(node_index + component * Nnode_for_unknown < local_velocity.size());
                            modified_velocity_at_quad_pt += local_velocity[node_index + component * Nnode_for_unknown]
                            * phi(static_cast<int>(node_index));
                        }

                        for (int m = 0; m < int_Nnode_for_unknown; ++m)
                        {
                            for (int n = 0; n < int_Nnode_for_test_unknown; ++n)
                            {
                                matrix_result(m, n) +=
                                    factor * modified_velocity_at_quad_pt * grad_phi(m, static_cast<int>(component)) * phi_test(n) ;
                            }
                        }
                    }
                }
                
                {
                    // ===========================================
                    // Then add the div (modified_v) * (Vx Vtextx + Vy Vtesty) contribution.
                    // Likewise, only the block for the first component is filled here.
                    // ===========================================
                    double div_modified_velocity_at_quad_pt = 0.;
                    
                    for (auto component = 0ul; component < Ncomponent; ++component)
                    {
                        // Compute the \a component for velocity at quadrature point.
                        for (auto node_index = 0ul; node_index < Nnode_for_unknown; ++node_index)
                            div_modified_velocity_at_quad_pt += local_velocity[node_index + component * Nnode_for_unknown]
                                                            * grad_phi(static_cast<int>(node_index), static_cast<int>(component));
                    }

                    for (int m = 0; m < int_Nnode_for_unknown; ++m)
                    {
                        const double m_value = factor * div_modified_velocity_at_quad_pt * phi(m);
                        
                        for (int n = 0; n < int_Nnode_for_test_unknown; ++n)
                            matrix_result(m, n) += m_value * phi_test(n);
                    }
                }
                
                
                {
                    // ===========================================
                    // Duplicate the block for each component.
                    // ===========================================
                    for (int m = 0; m < int_Nnode_for_unknown; ++m)
                    {
                        for (int n = 0; n < int_Nnode_for_test_unknown; ++n)
                        {
                            for (int component_block = 1; component_block < int_Ncomponent; ++component_block)
                            {
                                const auto shift_line = int_Nnode_for_unknown * component_block;
                                const auto shift_col = int_Nnode_for_test_unknown * component_block;
                                matrix_result(m + shift_line, n + shift_col) = matrix_result(m, n);
                            }
                        }
                    }
                }
                
                ++quad_pt_index;
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
