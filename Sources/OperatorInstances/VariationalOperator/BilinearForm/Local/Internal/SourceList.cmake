target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hpp" / 
)

