///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Jan 2016 10:30:12 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "OperatorInstances/VariationalOperator/BilinearForm/Ale.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalVariationalOperatorNS
    {
        
        
        Ale::Ale(const FEltSpace& felt_space,
                 const Unknown::const_shared_ptr unknown_ptr,
                 const Unknown::const_shared_ptr test_unknown_ptr,
                 const scalar_parameter& density,
                 const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::yes,
                 density)        
        { }
        
        
        const std::string& Ale::ClassName()
        {
            static std::string name("Ale");
            return name;
        }
        
     
        
    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
