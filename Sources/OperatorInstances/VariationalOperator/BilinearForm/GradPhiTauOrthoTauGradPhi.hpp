///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 15:57:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauOrthoTauGradPhi.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {



        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        class GradPhiTauOrthoTauGradPhi final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            GradPhiTauOrthoTauGradPhi,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::GradPhiTauOrthoTauGradPhi
        >
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = GradPhiTauOrthoTauGradPhi;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::GradPhiTauOrthoTauGradPhi;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! Unique ptr.
            using const_unique_ptr = std::unique_ptr<const GradPhiTauOrthoTauGradPhi>;

            //! Alias to relevant scalar parameter type used in this operator.
            using scalar_parameter = local_operator_type::scalar_parameter;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_ptr The (scalar) potential.
             * \copydoc doxygen_hide_test_unknown_ptr_param
             * \param[in] transverse_diffusion_tensor Tmp \todo #9 Complete!
             * \param[in] fiber_diffusion_tensor Tmp \todo #9 Complete!
             * \param[in] fibers Tmp \todo #9 Complete!
             * \param[in] angles Tmp \todo #9 Complete!
             * \copydoc doxygen_hide_gvo_felt_space_arg
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit GradPhiTauOrthoTauGradPhi(const FEltSpace& felt_space,
                                               const Unknown::const_shared_ptr unknown_ptr,
                                               const Unknown::const_shared_ptr test_unknown_ptr,
                                               const scalar_parameter& transverse_diffusion_tensor,
                                               const scalar_parameter& fiber_diffusion_tensor,
                                               const FiberList<ParameterNS::Type::vector>& fibers,
                                               const FiberList<ParameterNS::Type::scalar>& angles,
                                               const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

            //! Destructor.
            ~GradPhiTauOrthoTauGradPhi() = default;

            //! Copy constructor.
            GradPhiTauOrthoTauGradPhi(const GradPhiTauOrthoTauGradPhi&) = delete;

            //! Move constructor.
            GradPhiTauOrthoTauGradPhi(GradPhiTauOrthoTauGradPhi&&) = delete;

            //! Copy affectation.
            GradPhiTauOrthoTauGradPhi& operator=(const GradPhiTauOrthoTauGradPhi&) = delete;

            //! Move affectation.
            GradPhiTauOrthoTauGradPhi& operator=(GradPhiTauOrthoTauGradPhi&&) = delete;

            ///@}


        public:

            // TODO Replace matrix by vector if relevant.
            /*!
             * \brief Assemble into one or several matrices.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
             *
             * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
             * assembled. These matrices are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;


        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauOrthoTauGradPhi.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_
