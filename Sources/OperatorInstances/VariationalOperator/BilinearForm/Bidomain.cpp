///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 1 Sep 2015 14:09:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalVariationalOperatorNS
    {
        
        
        Bidomain::Bidomain(const FEltSpace& felt_space,
                           const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                           const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                           const scalar_parameter& intracellular_trans_diffusion_tensor,
                           const scalar_parameter& extracellular_trans_diffusion_tensor,
                           const scalar_parameter& intracellular_fiber_diffusion_tensor,
                           const scalar_parameter& extracellular_fiber_diffusion_tensor,
                           const FiberList<ParameterNS::Type::vector>& fibers,
                           const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_list,
                 test_unknown_list,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::no,
                 intracellular_trans_diffusion_tensor,
                 extracellular_trans_diffusion_tensor,
                 intracellular_fiber_diffusion_tensor,
                 extracellular_fiber_diffusion_tensor,
                 fibers)
        {
        }
        
        
        const std::string& Bidomain::ClassName()
        {
            static std::string name("Bidomain");
            return name;
        }
        
     
        
    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
