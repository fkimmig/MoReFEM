target_sources(${MOREFEM_OP_INSTANCES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/NonLinearSource.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NonLinearSource.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
