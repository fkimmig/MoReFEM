///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 5 Oct 2015 10:59:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HPP_

# include <vector>
# include <limits>

# include "Utilities/MatrixOrVector.hpp"
# include "Utilities/Containers/EnumClass.hpp"

# include "Core/TimeManager/TimeManager.hpp"

# include "FiniteElement/Unknown/Unknown.hpp"

# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

# include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/ReactionLaw.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace ReactionLawNS
        {


            /*!
             * \brief Implementation of the CourtemancheRamirezNattel reaction law. Defines f and g for a ReactionDiffusion model.
             */
            template<>
            class ReactionLaw<ReactionLawName::CourtemancheRamirezNattel>
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = ReactionLaw;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the reaction law.
                static const std::string& ClassName();

                //! Alias to a scalar parameter at quadrature point.
                using ScalarParameterAtQuadPt =
                ParameterAtQuadraturePoint
                <
                    ParameterNS::Type::scalar,
                    ParameterNS::TimeDependencyNS::None // There is a time dependency but it is much more sophisticated
                    // that what this template parameter is able to cope with,
                    // namely only something like f(x) * g(t).
                >;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_input_parameter_data_arg
                 * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
                 * \param[in] domain \a Domain upon which the \a Parameters are defined.
                 * \copydoc doxygen_hide_time_manager_arg
                 */
                template <class InputParameterDataT>
                explicit ReactionLaw(const InputParameterDataT& input_parameter_data,
                                     const Domain& domain,
                                     const TimeManager& time_manager,
                                     const QuadratureRulePerTopology& quadrature_rule_per_topology);

                //! Destructor.
                ~ReactionLaw() = default;

                //! Copy constructor.
                ReactionLaw(const ReactionLaw&) = delete;

                //! Move constructor.
                ReactionLaw(ReactionLaw&&) = delete;

                //! Copy affectation.
                ReactionLaw& operator=(const ReactionLaw&) = delete;

                //! Move affectation.
                ReactionLaw& operator=(ReactionLaw&&) = delete;

                ///@}

                /*!
                 *
                 * \brief Defines f(u,w) = See NonLinearSource
                 *
                 * \todo #9 (Gautier) Improve this comment!
                 *
                 * \param[in] local_potential Local potential.
                 * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
                 * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
                 *
                 * \return Value of the reaction law at the requested location.
                 */
                double ReactionLawFunction(const double local_potential,
                                           const QuadraturePoint& quad_pt,
                                           const GeometricElt& geom_elt);

                //! Accessor to the local potential.
                double GetLocalPotential() const noexcept;

                //! Non constant accessor to the local potential.
                double& GetNonCstLocalPotential() noexcept;

                /*!
                 * \brief Write gate into a file.
                 *
                 * \param[in] filename Path of the file into which gate must be written.
                 */
                void WriteGate(const std::string& filename) const;
                
                //! Non constant accessor the the gate \a Parameter. We decided to put the m parameter in it.
                ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetNonCstGate() noexcept;
                
                //! Accessor the the gate \a Parameter.
                const ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetGate() const noexcept;

            private:

                /*!
                 * \brief Compute the concentrations.
                 *
                 * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
                 * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
                 */
                void ComputeConcentrations(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

            private:

                //! Accessor to the \a TimeManager of the model.
                const TimeManager& GetTimeManager() const noexcept;

            private:

                /*!
                 * \brief Convenient enum to pinpoint quantities in the \a parameter_list_ array.
                 *
                 * \todo #9 Explain meaning of each of those! (Gautier)
                 */
                enum class parameter_index : std::size_t
                {
                    m = 0,
                    h = 1,
                    j = 2,
                    ao = 3,
                    io = 4,
                    ua = 5,
                    ui = 6,
                    xr = 7,
                    xs = 8,
                    d = 9,
                    f = 10,
                    fca = 11,
                    urel = 12,
                    vrel = 13,
                    wrel = 14,
                    nai = 15,
                    nao = 16,
                    cao = 17,
                    ki = 18,
                    ko = 19,
                    cai = 20,
                    naiont = 21,
                    kiont = 22,
                    caiont = 23,
                    ileak = 24,
                    iup = 25,
                    itr = 26,
                    irel = 27,
                    nsr = 28,    /* NSR Ca Concentration (mM) */
                    jsr = 29,
                };

            private:

                //! List of all scalar parameters to consider.
                std::array<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>::unique_ptr, 30> parameter_list_;

                /*!
                 * \brief Non constant accessor to the parameter which index is \a index.
                 *
                 * \param[in] index Index of the sought \a Parameter. To make the code more readable, use the enum class:
                 * \code
                 * decltype(auto) nsr_param = GetNonCstParameter(EnumUnderlyingType(parameter_index::nsr));
                 \endcode
                 *
                 * \return Reference to the required \a Parameter.
                 */
                ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetNonCstParameter(parameter_index index) noexcept;


                /*!
                 * \brief Constant accessor to the parameter which index is \a index.
                 *
                 * \param[in] index Index of the sought \a Parameter. To make the code more readable, use the enum class:
                 * \code
                 * decltype(auto) nsr_param = GetParameter(EnumUnderlyingType(parameter_index::nsr));
                 \endcode
                 *
                 * \return Constant reference to the required \a Parameter.
                 */
                const ParameterAtQuadraturePoint<ParameterNS::Type::scalar>& GetParameter(parameter_index index) const noexcept;

            private:

                //! Time manager.
                const TimeManager& time_manager_;

                //! Value of the local potential.
                double local_potential_ = std::numeric_limits<double>::lowest();

            };


        } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/CourtemancheRamirezNattel.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_COURTEMANCHE_RAMIREZ_NATTEL_HPP_
