///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_

# include <memory>
# include <vector>

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Parameters/Parameter.hpp"
# include "Parameters/ParameterType.hpp"

# include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        /*!
         * \brief Implementation of local TransientSource operator.
         *
         * \todo Improve the comment by writing its mathematical definition!
         */
        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        class TransientSource final : public LinearLocalVariationalOperator<LocalVector>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = TransientSource<TypeT, TimeDependencyT>;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to parameter type.
            using parameter_type = Parameter<TypeT, LocalCoords, TimeDependencyT>;

            //! Alias to parent.
            using parent = LinearLocalVariationalOperator<LocalVector>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * one vectorial unknown.
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in] source Lua function describing the source applied.
             *
             * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
             * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
             */
            explicit TransientSource(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                     const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                     typename parent::elementary_data_type&& elementary_data,
                                     const parameter_type& source);


            //! Destructor.
            ~TransientSource() = default;

            //! Copy constructor.
            TransientSource(const TransientSource&) = delete;

            //! Move constructor.
            TransientSource(TransientSource&&) = delete;

            //! Copy affectation.
            TransientSource& operator=(const TransientSource&) = delete;

            //! Move affectation.
            TransientSource& operator=(TransientSource&&) = delete;

            ///@}


            /*!
             * \brief Compute the elementary \a OperatorNatureT.
             *
             * For current operator, only vector makes sense; so only this specialization is actually defined.
             *
             * \internal <b><tt>[internal]</tt></b> This parameter is computed by
             * GlobalVariationalOperatorNS::TransientSource::SetComputeEltArrayArguments() method.
             *
             */
            void ComputeEltArray();

            //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
            void InitLocalComputation() {}

            //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
            void FinalizeLocalComputation() {}


            //! Set the time.
            void SetTime(double time) noexcept;


        private:

            /*!
             * \brief Access to the transient source parameter.
             */
            const parameter_type& GetSource() const;


        private:

            //! Source  considered.
            const parameter_type& source_;

            //! Current time in seconds.
            double time_;

        };


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/LinearForm/Local/TransientSource.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HPP_
