target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/P1_to_P1b.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/P1_to_P2.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/P1b_to_P1.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/P2_to_P1.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
