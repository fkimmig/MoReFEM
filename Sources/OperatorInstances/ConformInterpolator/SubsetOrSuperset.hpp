///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Sep 2015 15:38:52 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HPP_

# include <memory>
# include <vector>

# include "Operators/ConformInterpolator/Crtp/LagrangianInterpolator.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        /*!
         * \brief Interpolate from a (\a FEltSpace, \a NumberingSubset) to either a larger or smaller target.
         *
         * Target must be either a subset or a superset of source.
         */
        class SubsetOrSuperset final : public Crtp::LagrangianInterpolator<SubsetOrSuperset>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = SubsetOrSuperset;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;


        private:

            //! Alias to parent.
            using parent = Crtp::LagrangianInterpolator<SubsetOrSuperset>;

            //! Frienship to parent.
            friend parent;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] source_felt_space \a FEltSpace of the source.
             * \param[in] source_numbering_subset \a NumberingSubset of the source.
             * \param[in] target_felt_space \a FEltSpace of the target.
             * \param[in] target_numbering_subset \a NumberingSubset of the target.
             */
            explicit SubsetOrSuperset(const FEltSpace& source_felt_space,
                                      const NumberingSubset& source_numbering_subset,
                                      const FEltSpace& target_felt_space,
                                      const NumberingSubset& target_numbering_subset);

            //! Destructor.
            ~SubsetOrSuperset() = default;

            //! Copy constructor.
            SubsetOrSuperset(const SubsetOrSuperset&) = delete;

            //! Move constructor.
            SubsetOrSuperset(SubsetOrSuperset&&) = delete;

            //! Copy affectation.
            SubsetOrSuperset& operator=(const SubsetOrSuperset&) = delete;

            //! Move affectation.
            SubsetOrSuperset& operator=(SubsetOrSuperset&&) = delete;

            ///@}

        private:

            /*!
             * \brief Init interpolation matrix.
             *
             * \internal <b><tt>[internal]</tt></b> This method is expected by the parent/Crtp class.
             */
            void ComputeInterpolationMatrix();


        };


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HPP_
