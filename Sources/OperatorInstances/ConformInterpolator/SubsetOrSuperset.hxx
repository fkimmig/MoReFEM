///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Oct 2015 11:54:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HXX_


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {



    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_SUBSET_OR_SUPERSET_HXX_
