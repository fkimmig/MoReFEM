target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Check.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Phigher_to_P1.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Check.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Phigher_to_P1.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Phigher_to_P1.hxx" / 
)

