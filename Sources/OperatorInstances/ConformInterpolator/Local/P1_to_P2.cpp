///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 17:20:58 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"

#include "OperatorInstances/ConformInterpolator/Local/P1_to_P2.hpp"
#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"


namespace MoReFEM
{
    
    
    namespace ConformInterpolatorNS
    {
        
        
        namespace Local
        {
            
            
            const std::string& P1_to_P2::GetTargetShapeFunctionLabel()
            {
                static std::string ret("P2");
                return ret;
            }

            
            P1_to_P2::~P1_to_P2() = default;
            
            
            P1_to_P2::P1_to_P2(const FEltSpace& p1_felt_space,
                               const Internal::RefFEltNS::RefLocalFEltSpace& p2_ref_local_felt_space,
                               const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
            : parent(p2_ref_local_felt_space.GetRefGeomElt(),
                     interpolation_data)
            {
                const auto& ref_geom_elt = GetRefGeomElt();
                
                const auto& p1_ref_local_felt_space = parent::GetRefLocalFEltSpace(p1_felt_space, ref_geom_elt);
                
                auto& local_projection_matrix = GetNonCstProjectionMatrix();
                
                const auto& source_data = interpolation_data.GetSourceData();
                const auto& target_data = interpolation_data.GetTargetData();

                
                const auto& p1_ref_felt =
                    source_data.GetCommonBasicRefFElt(p1_ref_local_felt_space);
                
                const auto& p2_ref_felt =
                    target_data.GetCommonBasicRefFElt(p2_ref_local_felt_space);
                
                const auto& p1_local_node_list = p1_ref_felt.GetLocalNodeList();
                const auto& p2_local_node_list = p2_ref_felt.GetLocalNodeList();
                
                #ifndef NDEBUG
                Internal::ConformInterpolatorNS::Local::Impl::AssertLocalNodeConsistency(p1_local_node_list, p2_local_node_list);
                #endif // NDEBUG
                
                local_projection_matrix.Resize(static_cast<int>(p2_local_node_list.size()) * target_data.NunknownComponent(),
                                               static_cast<int>(p1_local_node_list.size()) * source_data.NunknownComponent());
                
                local_projection_matrix.Zero();
          
                const auto Nnode_in_col = static_cast<int>(p1_local_node_list.size());
                const auto Nnode_in_row = static_cast<int>(p2_local_node_list.size());
                
                assert(Nnode_in_row > Nnode_in_col);
                
                // Prepare the content of the block that will be repeated for each unknown component.
                LocalMatrix block(Nnode_in_row, Nnode_in_col);
                block.Zero();
                
                for (auto i = 0; i < Nnode_in_col; ++i)
                    block(i, i) = 1.;
                
                for (auto i = Nnode_in_col; i < Nnode_in_row; ++i)
                {
                    const auto& local_node_ptr = p2_local_node_list[static_cast<std::size_t>(i)];
                    assert(!(!local_node_ptr));
                    const auto& p2_vertex_list = local_node_ptr->GetLocalInterface().GetVertexIndexList();
                    assert(p2_vertex_list.size() == 2ul);
                    
                    for (const auto& vertex_index : p2_vertex_list)
                        block(i, static_cast<int>(vertex_index)) = 0.5;
                }
                

                // Now repeat the block for all the relevant unknowns or components.
                FillMatrixFromNodeBlock(std::move(block));
            }
            
            
        } // namespace Local
        
        
    } // namespace ConformInterpolatorNS
  

} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
