target_sources(${MOREFEM_OP_INSTANCES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/ParameterOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/VariationalOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/HyperelasticLaws/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ConformInterpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/NonConformInterpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/StateOperator/SourceList.cmake)
