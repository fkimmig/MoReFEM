target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CiarletGeymonat.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExponentialJ1J4.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MooneyRivlin.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/StVenantKirchhoff.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/CiarletGeymonat.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/CiarletGeymonat.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExponentialJ1J4.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ExponentialJ1J4.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MooneyRivlin.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MooneyRivlin.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/StVenantKirchhoff.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/StVenantKirchhoff.hxx" / 
)

