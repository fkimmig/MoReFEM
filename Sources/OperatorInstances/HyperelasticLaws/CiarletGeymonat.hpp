///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HPP_

# include <vector>
# include <array>
# include <bitset>
# include <cmath>
# include <exception>

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Parameters/Parameter.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class QuadraturePoint;
    class Solid;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace HyperelasticLawNS
    {


        /*!
         * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
         */
        class CiarletGeymonat
        {
        public:

            //! Return the name of the hyperelastic law.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_operator_alias_scalar_parameter
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! \copydoc doxygen_hide_alias_self
            using self = CiarletGeymonat;

            //! \copydoc doxygen_hide_alias_const_unique_ptr
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias on the type of the invariant holder for the current law. 3 is the number of invariants.
            using invariant_holder_type = InvariantHolder<3>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] solid Object which provides the required material parameters for the solid.
             */
            explicit CiarletGeymonat(const Solid& solid);

            //! Destructor.
            ~CiarletGeymonat() = default;

            //! Copy constructor.
            CiarletGeymonat(const CiarletGeymonat&) = delete;

            //! Move constructor.
            CiarletGeymonat(CiarletGeymonat&&) = delete;

            //! Copy affectation.
            CiarletGeymonat& operator=(const CiarletGeymonat&) = delete;

            //! Move affectation.
            CiarletGeymonat& operator=(CiarletGeymonat&&) = delete;

            ///@}


        public:

            //! Number of invariants of the law.
            static constexpr unsigned int Ninvariants() { return 3; }

            //! If the law needs I4 or not.
            static constexpr bool DoI4Activate() { return false; }

        public:

            //! Function W.
            double W(const invariant_holder_type& invariant_holder,
                     const QuadraturePoint& quadrature_point,
                     const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of first invariant (dWdI1)
            double FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of second invariant (dWdI2)
            double FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI3)
            double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of first invariant (d2WdI1dI1)
            static constexpr double SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                    const QuadraturePoint& quadrature_point,
                                                                    const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second invariant (d2WdI2dI2)
            static constexpr double SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                              const QuadraturePoint& quadrature_point,
                                                              const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of third invariant (d2WdI3dI3)
            double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
            static constexpr double SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                             const QuadraturePoint& quadrature_point,
                                                                             const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
            double SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                           const QuadraturePoint& quadrature_point,
                                                           const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
            double SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                            const QuadraturePoint& quadrature_point,
                                                            const GeometricElt& geom_elt) const;
        private:

            //! Kappa1.
            const scalar_parameter& GetKappa1() const noexcept;

            //! Kappa2.
            const scalar_parameter& GetKappa2() const noexcept;

            //! Hyperelastic bulk.
            const scalar_parameter& GetBulk() const noexcept;

        private:


            //! Kappa1.
            const scalar_parameter& kappa1_;

            //! Kappa2.
            const scalar_parameter& kappa2_;

            //! Bulk.
            const scalar_parameter& bulk_;

        };


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HPP_
