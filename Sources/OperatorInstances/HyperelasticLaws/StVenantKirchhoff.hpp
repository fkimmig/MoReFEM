///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_


# include <vector>
# include <array>
# include <bitset>
# include <cmath>
# include <cassert>

# include "Utilities/Numeric/Numeric.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Parameters/Parameter.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class QuadraturePoint;
    class Solid;

    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace HyperelasticLawNS
    {

        /*!
         * \brief StVenant-Kirchhoff laws, to use a a policy of class HyperElasticityLaw.
         *
         * Note: this class defines the St venant Kirchhoff laws using invariants, thus complying with the generic
         * interface of ElasticityModel class. However, these laws could be defined without those invariants, and
         * this definition would be probably better in term of calculation time. So if Saint Venant Kirchhoff laws are
         * to be used extensively, the possibility to implement them without invariants should remain open.
         */
        class StVenantKirchhoff
        {

        public:

            //! Return the name of the hyperelastic law.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_alias_self
            using self = StVenantKirchhoff;

            //! \copydoc doxygen_hide_operator_alias_scalar_parameter
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! \copydoc doxygen_hide_alias_const_unique_ptr
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias on the type of the invariant holder for the current law. 3 is the number of invariants.
            using invariant_holder_type = InvariantHolder<3>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] solid Object which provides the required material parameters for the solid.
             */
            explicit StVenantKirchhoff(const Solid& solid);

            //! Destructor.
            ~StVenantKirchhoff() = default;

            //! Copy constructor.
            StVenantKirchhoff(const StVenantKirchhoff&) = delete;

            //! Move constructor.
            StVenantKirchhoff(StVenantKirchhoff&&) = delete;

            //! Copy affectation.
            StVenantKirchhoff& operator=(const StVenantKirchhoff&) = delete;

            //! Move affectation.
            StVenantKirchhoff& operator=(StVenantKirchhoff&&) = delete;

            ///@}

        public:

            //! Number of invariants of the law.
            static constexpr unsigned int Ninvariants() { return 3; }

            //! If the law needs I4 or not.
            static constexpr bool DoI4Activate() { return false; }

        public:

            //! Function W.
            double W(const invariant_holder_type& invariant_holder,
                     const QuadraturePoint& quadrature_point,
                     const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of first invariant (dWdI1).
            double FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of second invariant (dWdI2).
            double FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Derivative of W with respect of third invariant (dWdI3).
            double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quadrature_point,
                                                  const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of first invariant (d2WdI1dI1).
            double SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const;

            //! Second derivative of W with respect of second invariant (d2WdI2dI2).
            static constexpr double SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                              const QuadraturePoint& quadrature_point,
                                                              const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of third invariant (d2WdI3dI3).
            double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quadrature_point,
                                                   const GeometricElt& geom_elt) const noexcept;

            //! Second derivative of W with respect of first and second invariant (d2WdI1dI2).
            static constexpr double SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                      const QuadraturePoint& quadrature_point,
                                                                      const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of first and third invariant (d2WdI1dI3).
            static constexpr double SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quadrature_point,
                                                                     const GeometricElt& geom_elt) noexcept;

            //! Second derivative of W with respect of second and third invariant (d2WdI2dI3).
            static constexpr double SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                      const QuadraturePoint& quadrature_point,
                                                                      const GeometricElt& geom_elt) noexcept;

        protected:

            //! Lame coefficient lambda.
            const scalar_parameter& GetLameLambda() const noexcept;

            //! Lame coefficient mu.
            const scalar_parameter& GetLameMu() const noexcept;

            //! Hyperelastic bulk.
            const scalar_parameter& GetBulk() const noexcept;


        private:


            //! Lame coefficient lambda.
            const scalar_parameter& lame_lambda_;

            //! Lame coefficient mu.
            const scalar_parameter& lame_mu_;

            //! Bulk.
            const scalar_parameter& bulk_;

        };


    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_
