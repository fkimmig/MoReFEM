target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/UpdateCauchyGreenTensor.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UpdateFiberDeformation.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/UpdateCauchyGreenTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UpdateCauchyGreenTensor.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/UpdateFiberDeformation.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UpdateFiberDeformation.hxx" / 
)

