/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 13:43:40 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HXX_
# define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HXX_


namespace MoReFEM
{


    namespace ElasticityNS
    {



        inline void VariationalFormulationElasticity
        ::UpdateDisplacementAndVelocity()
        {
            // Ordering of both calls is capital here!
            UpdateVelocity();
            UpdateDisplacement();
        }



        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulationElasticity
        ::ImplementSnesFunction() const noexcept
        {
            return nullptr;
        }



        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulationElasticity
        ::ImplementSnesJacobian() const noexcept
        {
            return nullptr;
        }



        inline Wrappers::Petsc::Snes::SNESViewer
        VariationalFormulationElasticity
        ::ImplementSnesViewer() const noexcept
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        VariationalFormulationElasticity::ImplementSnesConvergenceTestFunction() const noexcept
        {
            return nullptr;
        }


        inline const GlobalVector& VariationalFormulationElasticity
        ::GetVectorCurrentDisplacement() const noexcept
        {
            assert(!(!vector_current_displacement_));
            return *vector_current_displacement_;
        }



        inline GlobalVector& VariationalFormulationElasticity
        ::GetNonCstVectorCurrentDisplacement() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
        }



        inline const GlobalVector& VariationalFormulationElasticity
        ::GetVectorCurrentVelocity() const noexcept
        {
            assert(!(!vector_current_velocity_));
            return *vector_current_velocity_;
        }



        inline GlobalVector& VariationalFormulationElasticity
        ::GetNonCstVectorCurrentVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
        }



        inline const GlobalMatrix& VariationalFormulationElasticity
        ::GetMatrixCurrentDisplacement() const noexcept
        {
            assert(!(!matrix_current_displacement_));
            return *matrix_current_displacement_;
        }



        inline GlobalMatrix& VariationalFormulationElasticity
        ::GetNonCstMatrixCurrentDisplacement() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixCurrentDisplacement());
        }



        inline const GlobalMatrix& VariationalFormulationElasticity
        ::GetMatrixCurrentVelocity() const noexcept
        {
            assert(!(!matrix_current_velocity_));
            return *matrix_current_velocity_;
        }



        inline GlobalMatrix& VariationalFormulationElasticity
        ::GetNonCstMatrixCurrentVelocity() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixCurrentVelocity());
        }



        inline const GlobalMatrix& VariationalFormulationElasticity
        ::GetMass() const noexcept
        {
            assert(!(!mass_));
            return *mass_;
        }



        inline GlobalMatrix& VariationalFormulationElasticity
        ::GetNonCstMass() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMass());
        }



        inline const GlobalMatrix& VariationalFormulationElasticity
        ::GetStiffness() const noexcept
        {
            assert(!(!stiffness_));
            return *stiffness_;
        }



        inline GlobalMatrix& VariationalFormulationElasticity
        ::GetNonCstStiffness() noexcept
        {
            return const_cast<GlobalMatrix&>(GetStiffness());
        }



        inline const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor&
        VariationalFormulationElasticity
        ::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::Mass& VariationalFormulationElasticity
        ::GetMassOperator() const noexcept
        {
            assert(!(!mass_operator_));
            return *mass_operator_;
        }


        inline const NumberingSubset& VariationalFormulationElasticity
        ::GetNumberingSubset() const noexcept
        {
            return numbering_subset_;
        }


        inline const VariationalFormulationElasticity::solid_scalar_parameter& VariationalFormulationElasticity
        ::GetVolumicMass() const noexcept
        {
            assert(!(!volumic_mass_));
            return *volumic_mass_;
        }


        inline const VariationalFormulationElasticity::solid_scalar_parameter& VariationalFormulationElasticity
        ::GetYoungModulus() const noexcept
        {
            assert(!(!young_modulus_));
            return *young_modulus_;
        }


        inline const VariationalFormulationElasticity::solid_scalar_parameter& VariationalFormulationElasticity
        ::GetPoissonRatio() const noexcept
        {
            assert(!(!poisson_ratio_));
            return *poisson_ratio_;
        }


        inline const Unknown& VariationalFormulationElasticity
        ::GetSolidDisplacement() const noexcept
        {
            return solid_displacement_;
        }


        inline const FEltSpace& VariationalFormulationElasticity
        ::GetMainFEltSpace() const noexcept
        {
            return main_felt_space_;
        }


        inline const FEltSpace& VariationalFormulationElasticity
        ::GetNeumannFEltSpace() const noexcept
        {
            return neumann_felt_space_;
        }


    } // namespace ElasticityNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HXX_
