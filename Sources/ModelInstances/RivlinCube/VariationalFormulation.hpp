/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/OpsFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "Core/InputParameter/Parameter/Solid/Solid.hpp"


# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
# include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/None.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/FollowingPressure.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
# include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"
# include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
# include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"
# include "FormulationSolver/Crtp/HyperelasticLaw.hpp"

# include "ModelInstances/RivlinCube/InputParameterList.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Solid;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================




    namespace RivlinCubeNS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulation
        : public MoReFEM::VariationalFormulation
        <
            VariationalFormulation,
            EnumUnderlyingType(SolverIndex::solver)
        >,
        public FormulationSolverNS::HyperelasticLaw
        <
            VariationalFormulation,
            HyperelasticLawNS::MooneyRivlin
        >
        {
        private:

            //! \copydoc doxygen_hide_alias_self
            using self = VariationalFormulation;

            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation
            <
                VariationalFormulation,
                EnumUnderlyingType(SolverIndex::solver)
            >;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

            //! Alias to hyperelastic parent.
            using hyperelastic_law_parent = FormulationSolverNS::HyperelasticLaw
            <
                VariationalFormulation,
                HyperelasticLawNS::MooneyRivlin
            >;

            //! Alias to hyperelastic law used.
            using hyperelastic_law_type = HyperelasticLawNS::MooneyRivlin;

            //! Alias to hyperelastic policy.
            using hyperelasticity_policy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
                ::Hyperelasticity<hyperelastic_law_type>;

            //! Alias to viscoelastic policy.
            using ViscoelasticityPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None;

            //! Alias to acgtive stress policy.
            using ActiveStressPolicy =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ActiveStressPolicyNS::None;

            //! Alias to the type of the stiffness operator to use.
            using StiffnessOperatorType =
                GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
                <
                    hyperelasticity_policy,
                    ViscoelasticityPolicy,
                    ActiveStressPolicy
                >;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

        public:

            /// \name Special members.
            ///@{

            //! copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            const NumberingSubset& numbering_subset,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() = default;

            //! Copy constructor.
            VariationalFormulation(const VariationalFormulation&) = delete;

            //! Move constructor.
            VariationalFormulation(VariationalFormulation&&) = delete;

            //! Copy affectation.
            VariationalFormulation& operator=(const VariationalFormulation&) = delete;

            //! Move affectation.
            VariationalFormulation& operator=(VariationalFormulation&&) = delete;

            ///@}

            /*!
             * \brief Get the only numbering subset relevant for this VariationalFormulation.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return Only \a NumberingSubset relevant for the variational formulation.
             */
            const NumberingSubset& GetNumberingSubset() const;

            //! Indicates at the end of the simulation if the tangent was quadratic.
            void WasTangentQuadratic();

        private:

            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const InputParameterList& input_parameter_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to calculate the function required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;

            //! Define the pointer function required to calculate the jacobian required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;

            //! Define the pointer function required to view the results required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}

        private:

            /*!
             * \brief This function is given to SNES to compute the function.
             *
             * However as a matter of fact the jacobian will also be computed here, as it is the way Felisce works
             * (it hence avoids duplication or storage of intermediate matrixes)
             *
             * \param[in,out] snes SNES object. Not explicitly used but required by Petsc prototype.
             * \param[in,out] evaluationState State at which to evaluate residual.
             * \param[in,out] residual Vector to put residual. What is used as residual in VariationalFormulation is
             * \a GetSystemRHS().
             * \param[in,out] context_as_void Optional user-defined function context. In our case a pointer to the
             * VariationalFormulationT object.
             *
             * \return Petsc error code.
             */
            static PetscErrorCode Function(SNES snes, Vec evaluationState, Vec residual, void* context_as_void);


            /*!
             * \brief This function is used to monitor evolution of the convergence.
             *
             * Is is intended to be passed to SNESMonitorSet()
             *
             * \param[in] snes SNES object. Not explicitly used but required by Petsc prototype.
             * \param[in] its Index of iteration.
             * \param[in] norm Current L^2 norm.
             * \param[in] context_as_void Optional user-defined function context. In our case is is a pointer to a
             * VariationalFormulationT object.
             *
             * \return Petsc error code.
             */
            static PetscErrorCode Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void);


            /*!
             * \brief This function is supposed to be given to SNES to compute the jacobian.
             *
             * \copydoc doxygen_hide_snes_interface_common_arg
             * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
             * \param[in,out] jacobian Jacobian matrix. Actually unused in our wrapper.
             * \param[in,out] preconditioner Preconditioner matrix. Actually unused in our wrapper.
             */
            static PetscErrorCode Jacobian(SNES snes, Vec evaluation_state, Mat jacobian , Mat preconditioner,
                                           void* context_as_void);


        private:

            /*!
             * \brief Assemble method for all the static operators.
             */
            void AssembleStaticOperators();

            /*!
             * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
             * and the residual.
             */
            void SnesUpdate(const Vec& a_evaluation_state);


            /*!
             * \brief Compute the matrix of the system.
             *
             * The index of the time iteration is used to know whether the static or dynamic system muste be computed.
             */
            void ComputeTangent();

            /*!
             * \brief Compute the RHS of the system.
             *
             * The index of the time iteration is used to know whether the static or dynamic system muste be computed.
             */
            void ComputeResidual();

            //! Check if the tangent is quadratic at each newton iteration.
            void TangentCheck(GlobalVector& rhs);

        private:

            /*!
             * \brief Define the properties of all the global variational operators involved.
             */
            void DefineOperators();

            //! Get the hyperelastic stiffness operator.
            const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

            //! Get the following pressure operator.
            const GlobalVariationalOperatorNS::FollowingPressure<ParameterNS::TimeDependencyNS::None>&
            GetFollowingPressureOperator() const noexcept;

        private:


            /// \name Global variational operators.
            ///@{

            //! Stiffness operator.
            StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

            // Following pressure operator.
            GlobalVariationalOperatorNS::FollowingPressure<ParameterNS::TimeDependencyNS::None>::const_unique_ptr following_pressure_operator_ = nullptr;

            ///@}

        private:

            /// \name Accessors to the global vectors and matrices managed by the class.
            ///@{

            const GlobalVector& GetVectorStiffnessResidual() const noexcept;

            GlobalVector& GetNonCstVectorStiffnessResidual();

            const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

            GlobalMatrix& GetNonCstMatrixTangentStiffness();

            const GlobalVector& GetVectorFollowingPressureResidual() const noexcept;

            GlobalVector& GetNonCstVectorFollowingPressureResidual();

            const GlobalMatrix& GetMatrixTangentFollowingPressure() const noexcept;

            GlobalMatrix& GetNonCstMatrixTangentFollowingPressure();

            const GlobalVector& GetEvaluationState() const noexcept;

            ///@}

            //! Applied pressure.
            const ScalarParameter<>& GetStaticPressure() const noexcept;

            //! Access to the solid.
            const Solid& GetSolid() const noexcept;

        private:

            //! Constant accessor to the file in which the tangent quadratic verification is printed.
            const std::string& GetTangentQuadraticVerificationFile() const noexcept;

        private:

            /*!
             * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
             *
             * \param[in] evaluation_state Evaluation state given by the Petsc's snes function.
             */
            void UpdateEvaluationState(Vec evaluation_state);

            /*!
             * \brief Non constant access to the evaluation state, i.e. the values at the time Petsc evaluates
             * the residual.
             *
             * \internal <b><tt>[internal]</tt></b> This accessor should not be used except in the Snes::Function() method: the point is to
             * store the value given by Petsc internal Newton algorithm.
             * Its value should be modified only through the call if \a UpdateEvaluationState().
             *
             * \return Reference to the evaluation state vector.
             */
            GlobalVector& GetNonCstEvaluationState() noexcept;

        private:

            //! Accessor on the displacement displacement pair.
            const UnknownPair& GetDisplacementDisplacementPair() const noexcept;

        private:

            /// \name Global vectors and matrices specific to the problem.
            ///@{

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

            //! Matrix tangent following pressure.
            GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

            //! Following pressure residual vector.
            GlobalVector::unique_ptr vector_following_pressure_residual_ = nullptr;

            //! Matrix tangent following pressure.
            GlobalMatrix::unique_ptr matrix_tangent_following_pressure_ = nullptr;

            //! Evaluation state of the residual of the problem (only useful in SNES method)
            GlobalVector::unique_ptr evaluation_state_ = nullptr;

            ///@}

        private:

            /// \name Numbering subsets used in the formulation.
            ///@{

            //! Only relevant \a NumberingSubset
            const NumberingSubset& numbering_subset_;

            ///@}
            

        private:

            //! Material parameters of the solid.
            Solid::const_unique_ptr solid_ = nullptr;

            //! Applied pressure.
            ScalarParameter<>::unique_ptr static_pressure_ = nullptr;

        private:

            //! Needed to verify if tangent is quadratic.
            double rhs_norm_previous_iter_;

            //! Path to the directory that stores the energy file.
            std::string tangent_quadratic_verification_file_;

            //! Maximum string size of the time.
            int stew_time_size_;

            //! Maximum string size of the iterations.
            int stew_iteration_size_;

            //! Indicates if the tangent was quadratic over the static resolution.
            bool tangent_not_quadratic_in_static_ = false;

            //! Indicates if the tangent was not quadratic at another iteration than the first in the static resolution.
            bool tangent_not_quadratic_at_other_than_first_iteration_in_static_ = false;

            //! Enables to print in the console the warnings at each iterations.
            bool print_tangent_warning_ = false;


        };


    } // namespace RivlinCubeNS


} // namespace MoReFEM


# include "ModelInstances/RivlinCube/VariationalFormulation.hxx"


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HPP_
