/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Filesystem/Folder.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"

#include "ModelInstances/RivlinCube/VariationalFormulation.hpp"


namespace MoReFEM
{
    
    
    namespace RivlinCubeNS
    {


        VariationalFormulation::VariationalFormulation(const morefem_data_type& morefem_data,
                                                       const NumberingSubset& numbering_subset,
                                                       TimeManager& time_manager,
                                                       const GodOfDof& god_of_dof,
                                                       DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list)),
        numbering_subset_(numbering_subset)
        { }
      

        void VariationalFormulation::SupplInit(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            decltype(auto) domain =
                DomainManager::GetInstance().GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            
            const auto& felt_space_highest_dimension = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            
            solid_ = std::make_unique<Solid>(input_parameter_data,
                                             domain,
                                             felt_space_highest_dimension.GetQuadratureRulePerTopology()
                                             );
                        
            static_pressure_ =
                InitScalarParameterFromInputData<InputParameter::Source::StaticPressure>("StaticPressure",
                                                                                         domain,
                                                                                         input_parameter_data);
            
            {
                const auto& numbering_subset = GetNumberingSubset();
                
                const auto& displacement = UnknownManager::GetInstance().GetUnknown(EnumUnderlyingType(UnknownIndex::displacement));
                
                constexpr auto index = EnumUnderlyingType(InitialConditionIndex::displacement_initial_condition);
                
                InitializeVectorSystemSolution<index>(input_parameter_data,
                                                      numbering_subset,
                                                      displacement,
                                                      felt_space_highest_dimension);
            }
            
            
            namespace IPL = Utilities::InputParameterListNS;
            
            tangent_quadratic_verification_file_ =
                parent::GetResultDirectory() + "/tangent_quadratic_verification.dat";
            
            const auto& mpi = GetMpi();
            
            if (mpi.IsRootProcessor())
            {                
                if (FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                {
                    FilesystemNS::File::Remove(GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);
                }
                
                if (!FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                {
                    std::ofstream stream;
                    FilesystemNS::File::Create(stream, GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);
                    
                    stream << "Time | Newton iteration | Residual norm at current iteration | Previous residual norm square\n\n";
                    stream << "===========================================\n";
                    stream << "Static\n";
                    stream << "===========================================\n";
                    
                    namespace ipl = Utilities::InputParameterListNS;
                    using ip_petsc = InputParameter::Petsc<1>; //#892 hardcoded.
                    const unsigned int max_iteration = IPL::Extract<typename ip_petsc::MaxIteration>::Value(input_parameter_data);
                    
                    std::stringstream temp;
                    temp <<  max_iteration;
                    stew_iteration_size_ = static_cast<int>(temp.str().size());
                    
                    using TimeManager = InputParameter::TimeManager;
                    double time_max = IPL::Extract<TimeManager::TimeMax>::Value(input_parameter_data);
                    
                    temp.clear();
                    temp.str(std::string());
                    temp << (GetTimeManager().GetTimeStep() + time_max);
                    stew_time_size_ = static_cast<int>(temp.str().size());
                }
            }
            
            mpi.Barrier();

            DefineOperators();
        }
        
        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& numbering_subset = GetNumberingSubset();
            
            parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
            parent::AllocateSystemVector(numbering_subset);
            
            const auto& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
            const auto& system_rhs = GetSystemRhs(numbering_subset);
            
            vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
            vector_following_pressure_residual_ = std::make_unique<GlobalVector>(system_rhs);
            evaluation_state_ = std::make_unique<GlobalVector>(system_rhs);
            
            matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
            matrix_tangent_following_pressure_ = std::make_unique<GlobalMatrix>(system_matrix);
        }
        
        
        void VariationalFormulation::DefineOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_highest_dimension = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            const auto& felt_space_surface_pressure = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface_pressure));
            
            const auto& displacement_ptr = UnknownManager::GetInstance().GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            namespace IPL = Utilities::InputParameterListNS;
            
            hyperelastic_law_parent::Create(GetSolid());
            
            stiffness_operator_ =
                std::make_unique<StiffnessOperatorType>(felt_space_highest_dimension,
                                                        displacement_ptr,
                                                        displacement_ptr,
                                                        GetSolid(),
                                                        GetTimeManager(),
                                                        GetHyperelasticLawPtr());

            following_pressure_operator_ =
                std::make_unique<GVO::FollowingPressure<ParameterNS::TimeDependencyNS::None>>
                (felt_space_surface_pressure,
                 displacement_ptr,
                 displacement_ptr,
                 GetStaticPressure());
        }
        

        void VariationalFormulation::SnesUpdate(const Vec& a_evaluation_state)
        {
            UpdateEvaluationState(a_evaluation_state);
            
            // Assemble into vectors and matrices the relevant operators
            AssembleStaticOperators();
        }
        

        void VariationalFormulation::UpdateEvaluationState(Vec a_evaluation_state)
        {
            auto& evaluation_state = GetNonCstEvaluationState();
            evaluation_state.SetFromPetscVec(a_evaluation_state, __FILE__, __LINE__);
            evaluation_state.UpdateGhosts(__FILE__, __LINE__);
        }
        
        void VariationalFormulation::AssembleStaticOperators()
        {           
            const auto& evaluation_state = GetEvaluationState();
            
            {
                auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
                auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();
                
                matrix_tangent_stiffness.ZeroEntries(__FILE__, __LINE__);
                vector_stiffness_residual.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
                GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                evaluation_state);
            }
            
            {
                auto& matrix_tangent_following_pressure = GetNonCstMatrixTangentFollowingPressure();
                auto& vector_following_pressure_residual = GetNonCstVectorFollowingPressureResidual();
                
                matrix_tangent_following_pressure.ZeroEntries(__FILE__, __LINE__);
                vector_following_pressure_residual.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient mat(matrix_tangent_following_pressure, 1.);
                GlobalVectorWithCoefficient vec(vector_following_pressure_residual, 1.);
                
                GetFollowingPressureOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                        evaluation_state);
            }
        }
        
        void VariationalFormulation::ComputeResidual()
        {
            const auto& numbering_subset = GetNumberingSubset();
            auto& rhs = GetNonCstSystemRhs(numbering_subset);
            rhs.ZeroEntries(__FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(1.,
                                  GetVectorStiffnessResidual(),
                                  rhs,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(1.,
                                  GetVectorFollowingPressureResidual(),
                                  rhs,
                                  __FILE__, __LINE__);

            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                        numbering_subset);
                        
            TangentCheck(rhs);
        }
        
        
        void VariationalFormulation::ComputeTangent()
        {
            const auto& numbering_subset = GetNumberingSubset();
            auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);
            system_matrix.ZeroEntries(__FILE__, __LINE__);
            
            #ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixTangentStiffness(), system_matrix);
            AssertSameNumberingSubset(GetMatrixTangentFollowingPressure(), system_matrix);
            #endif // NDEBUG
            
            Wrappers::Petsc::AXPY<NonZeroPattern::same>( 1.,
                                  GetMatrixTangentStiffness(),
                                  system_matrix, __FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>( 1.,
                                  GetMatrixTangentFollowingPressure(),
                                  system_matrix, __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(numbering_subset,
                                                                          numbering_subset);
        }
        
        
        PetscErrorCode VariationalFormulation::Function(SNES snes,
                                                        Vec a_evaluation_state,
                                                        Vec a_residual,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(a_residual); // a_residual is not used as its content mirror the one of system_rhs_.
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            #ifndef NDEBUG
            
            const auto& numbering_subset = variational_formulation.GetNumberingSubset();
            
            {
                std::string desc;
                
                namespace Petsc = Wrappers::Petsc;
                
                const bool is_same = Petsc::AreEqual(Petsc::Vector(a_residual, false),
                                                     variational_formulation.GetSystemRhs(numbering_subset),
                                                     1.e-12,
                                                     desc,
                                                     __FILE__, __LINE__);
                
                if (!is_same)
                {
                    std::cerr << "Bug in SNES implementation: third argument of snes function implemention is expected "
                    "to be the residual, which is born in VariationalFormulationT by the attribute RHS(). It is not the case "
                    "here.\n" << desc << std::endl;
                    assert(false);
                }
            }
            #endif // NDEBUG

            variational_formulation.SnesUpdate(a_evaluation_state);
            variational_formulation.ComputeResidual();
    
            return 0;
        }
        
        
        
        
        PetscErrorCode VariationalFormulation::Viewer(SNES snes,
                                                      PetscInt its,
                                                      PetscReal norm,
                                                      void* context_as_void)
        {
            static_cast<void>(snes);
            assert(context_as_void);
            
            const VariationalFormulation* variational_formulation_ptr =
            static_cast<const VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            const auto& variational_formulation = *variational_formulation_ptr;
            
            const auto& vector = variational_formulation.GetEvaluationState();
            
            const PetscReal evaluation_state_min = vector.Min(__FILE__, __LINE__).second;
            const PetscReal evaluation_state_max = vector.Max(__FILE__, __LINE__).second;
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("%3D SNES Function Norm is %14.12e and extrema are %8.6e and %8.6e\n",
                                                          variational_formulation.GetMpi(),
                                                          __FILE__, __LINE__,
                                                          its,
                                                          norm,
                                                          evaluation_state_min,
                                                          evaluation_state_max
                                                          );
            
            return 0;
        }
        
        
        PetscErrorCode VariationalFormulation::Jacobian(SNES snes,
                                                        Vec evaluation_state,
                                                        Mat jacobian,
                                                        Mat preconditioner,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(jacobian);
            static_cast<void>(preconditioner);
            static_cast<void>(evaluation_state);
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            variational_formulation.ComputeTangent();

            return 0;
        }
        
        
        void VariationalFormulation::TangentCheck(GlobalVector& rhs)
        {
            // Verifying if the tangent is quadratic.
            const double rhs_norm_iter = rhs.Norm(NORM_2, __FILE__, __LINE__);
            
            unsigned int iteration = GetSnes().GetSnesIteration(__FILE__, __LINE__);
                        
            if (iteration == 0)
            {
                rhs_norm_previous_iter_ = 0;
            }
            
            if (!(iteration == 0))
            {
                if (rhs_norm_iter > rhs_norm_previous_iter_*rhs_norm_previous_iter_)
                {
                    tangent_not_quadratic_in_static_ = true;
                    
                    if (iteration > 1)
                        tangent_not_quadratic_at_other_than_first_iteration_in_static_ = true;
                    
                    if (GetMpi().IsRootProcessor())
                    {
                        if (FilesystemNS::File::DoExist(GetTangentQuadraticVerificationFile()))
                        {
                            std::ofstream inout;
                            FilesystemNS::File::Append(inout, GetTangentQuadraticVerificationFile(), __FILE__, __LINE__);
                            
                            inout << GetTimeManager().GetTime();
                            inout << " | ";
                            inout << std::setw(stew_iteration_size_) << iteration;
                            inout << std::scientific << std::setprecision(12) << " | " << rhs_norm_iter;
                            inout <<  " | " << rhs_norm_previous_iter_*rhs_norm_previous_iter_;
                            inout << std::endl;
                        }
                    }
                    
                    GetMpi().Barrier();
                    
                    if (print_tangent_warning_)
                    {
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n",
                                                                      GetMpi(),
                                                                      __FILE__, __LINE__);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] Tangent is not quadratic at iteration %d.\n",
                                                                      GetMpi(),
                                                                      __FILE__, __LINE__,
                                                                      iteration);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("Residual norm at current iteration    %1.12e\n",
                                                                      GetMpi(),
                                                                      __FILE__, __LINE__,
                                                                      rhs_norm_iter);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("Previous residual norm square         %1.12e\n",
                                                                      GetMpi(),
                                                                      __FILE__, __LINE__,
                                                                      rhs_norm_previous_iter_*rhs_norm_previous_iter_);
                        Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n\n",
                                                                      GetMpi(),
                                                                      __FILE__, __LINE__);
                    }
                }
            }
            
            rhs_norm_previous_iter_ = rhs_norm_iter;
        }
        
        void VariationalFormulation::WasTangentQuadratic()
        {
            if (tangent_not_quadratic_in_static_)
            {
                Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__);
                Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] In the static resolution the tangent was not quadratic.\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__);
                
                if (tangent_not_quadratic_at_other_than_first_iteration_in_static_)
                {
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] The tangent was not quadratic at another step than the first one.\n",
                                                                  GetMpi(),
                                                                  __FILE__, __LINE__);
                }
                
                Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__);
            }
            
            if (tangent_not_quadratic_in_static_)
            {
                Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__);
                Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] Informations about the tangent have been printed in %s\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__,
                                                              GetTangentQuadraticVerificationFile().c_str());
                Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n\n",
                                                              GetMpi(),
                                                              __FILE__, __LINE__);
            }
        }


    } // namespace RivlinCubeNS


} // namespace MoReFEM
