/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HXX_
# define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HXX_


namespace MoReFEM
{


    namespace RivlinCubeNS
    {


        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation::ImplementSnesFunction() const
        {
            return &VariationalFormulation::Function;
        }


        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation::ImplementSnesJacobian() const
        {
            return &VariationalFormulation::Jacobian;
        }


        inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation::ImplementSnesViewer() const
        {
            return &VariationalFormulation::Viewer;
        }


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const VariationalFormulation::StiffnessOperatorType&
        VariationalFormulation::GetStiffnessOperator() const noexcept
        {
            assert(!(!stiffness_operator_));
            return *stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::FollowingPressure<ParameterNS::TimeDependencyNS::None>&
        VariationalFormulation::GetFollowingPressureOperator() const noexcept
        {
            assert(!(!following_pressure_operator_));
            return *following_pressure_operator_;
        }


        inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
        {
            assert(!(!vector_stiffness_residual_));
            return *vector_stiffness_residual_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual()
        {
            return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
        }


        inline const GlobalVector& VariationalFormulation::GetEvaluationState() const noexcept
        {
            assert(!(!evaluation_state_));
            return *evaluation_state_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstEvaluationState() noexcept
        {
            return const_cast<GlobalVector&>(GetEvaluationState());
        }

        inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
        {
            assert(!(!matrix_tangent_stiffness_));
            return *matrix_tangent_stiffness_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness()
        {
            return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorFollowingPressureResidual() const noexcept
        {
            assert(!(!vector_following_pressure_residual_));
            return *vector_following_pressure_residual_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorFollowingPressureResidual()
        {
            return const_cast<GlobalVector&>(GetVectorFollowingPressureResidual());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentFollowingPressure() const noexcept
        {
            assert(!(!matrix_tangent_following_pressure_));
            return *matrix_tangent_following_pressure_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentFollowingPressure()
        {
            return const_cast<GlobalMatrix&>(GetMatrixTangentFollowingPressure());
        }


        inline const ScalarParameter<>& VariationalFormulation::GetStaticPressure() const noexcept
        {
            assert(!(!static_pressure_));
            return *static_pressure_;
        }


        inline const NumberingSubset& VariationalFormulation::GetNumberingSubset() const
        {
            return numbering_subset_;
        }


        inline const Solid& VariationalFormulation::GetSolid() const noexcept
        {
            assert(!(!solid_));
            return *solid_;
        }


        inline const std::string& VariationalFormulation::GetTangentQuadraticVerificationFile() const noexcept
        {
            return tangent_quadratic_verification_file_;
        }


    } // namespace RivlinCubeNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_VARIATIONAL_FORMULATION_HXX_
