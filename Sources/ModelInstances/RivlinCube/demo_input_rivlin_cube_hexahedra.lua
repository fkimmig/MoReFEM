-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'constant_time_step', 'variable_time_step'}})
	time_evolution_policy = "constant_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.

} -- transient

NumberingSubset1 = {

	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
    name = "monolithic",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset1

Unknown1 = {

	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = "solid_displacement",

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'scalar', 'vectorial'})
	nature = "vectorial",

} -- Unknown1

Mesh1 = {

	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = "${HOME}/Codes/MoReFEM/CoreLibrary/Data/Mesh/rivlin_cube_one_hexa.mesh",

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.

} -- Mesh1

Domain1 = {

	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at Ops level, as some geometric element types could be added after 
	-- generation of current input parameter file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }

} -- Domain1

Domain2 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 1 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain2

Domain3 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 2 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain3

Domain4 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 3 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain4

Domain5 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 4 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain5

Domain6 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 5 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain6

Domain7 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 6 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain7

Domain8 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 4, 5, 6 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain8


Domain9 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain9



EssentialBoundaryCondition1 = {

	-- Name of the boundary condition (must be unique).
	-- Expected format: "VALUE"
	name = "face1",

	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = 'Comp1',

    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'solid_displacement',

    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },

    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 2,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false

} -- EssentialBoundaryCondition1

EssentialBoundaryCondition2 = {

    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "face2",

    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp2',

    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'solid_displacement',

    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },

    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 3,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false

} -- EssentialBoundaryCondition2

EssentialBoundaryCondition3 = {
    
    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "face3",
    
    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp3',
    
    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'solid_displacement',
    
    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },
    
    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 4,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false
    
} -- EssentialBoundaryCondition3

FiniteElementSpace1 = {

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 1,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = {"solid_displacement"},

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = {"Q1"},

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
	numbering_subset_list = { 1 },

} -- FiniteElementSpace1

FiniteElementSpace2 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 8,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"solid_displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace2


-- Petsc
Petsc1 = {
    -- Absolute tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    absoluteTolerance = 1e-10,
    
    -- gmresStart
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v >= 0
    gmresRestart = 200,
    
    -- Maximum iteration
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0
    maxIteration = 1000,
    
    -- List of preconditioner: { none jacobi sor lu bjacobi ilu asm cholesky }.
    -- To use mumps: preconditioner = lu
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, 'lu')
    preconditioner = 'lu',
    
    -- Relative tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    relativeTolerance = 1e-10,
    
    -- Step size tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    stepSizeTolerance = 1e-8,
    
    -- List of solver: { chebychev cg gmres preonly bicg python };
    -- To use Mumps choose preonly.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, {'Mumps', 'Umfpack'})
    solver = 'Mumps'
}


Solid = {

	HyperelasticBulk = {

        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {"ignore", "constant", "lua_function", "at_quadrature_point", "piecewise_constant_by_domain"})
        nature = 'constant',

        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        --scalar_value = 3.e6, -- StVenant
        scalar_value = 0.3, -- Mooney

        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,

        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},

        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {}

	}, -- HyperelasticBulk

	Kappa1 = {

        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {"ignore", "constant", "lua_function", "at_quadrature_point", "piecewise_constant_by_domain"})
        nature = "constant",

        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 0.3,

        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,

        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},

        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},

	}, -- Kappa1

	Kappa2 = {

        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {"ignore", "constant", "lua_function", "at_quadrature_point", "piecewise_constant_by_domain"})
        nature = "constant",

        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 0.2,

        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,

        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},

        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},

	}, -- Kappa2


    
    VolumicMass = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {"ignore", "constant", "lua_function", "at_quadrature_point", "piecewise_constant_by_domain"})
        nature = 'ignore',
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 1.e99,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = { },
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = { }
        
    }, -- VolumicMass
    
} -- Solid

InitialCondition1 = {
    
    -- How is given the initial condition value (as a constant or as a Lua function.)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = { "ignore", "ignore", "ignore" },
    
    -- Value of the initial condition in the case nature is 'constant.
    -- Expected format: {VALUE1, VALUE2, ...}
    scalar_value = { 0. , 0., 0.},
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_x = function(x, y, z)
        return 0.;
    end,
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_y = function(x, y, z)
        return 0.;
    end,
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function. 
    lua_function_z = function(x, y, z)
        return 0.;
    end
    
} -- InitialCondition1

StaticPressure = {
    
    -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {"ignore", "constant", "lua_function", "at_quadrature_point", "piecewise_constant_by_domain"})
    nature = 'piecewise_constant_by_domain',
    
    -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 0.,
    
    -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function = none,
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { 5, 6, 7 },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    --piecewise_constant_domain_value = { -4.024412712798533e+05, -4.024412712798533e+05, -4.024412712798533e+05 } -- StVenant : Li = 0.01 0.01 0.01
    --piecewise_constant_domain_value = { -8.251245297324177e+05, -8.459174860476693e+05, -8.677204020631138e+05} -- StVenant : Li = 0.01 0.02 0.03
    piecewise_constant_domain_value = { -0.1986, -0.1986, -0.1986} -- Mooney : Li = 0.1 0.1 0.1
    --piecewise_constant_domain_value = { -0.331023944673954, -0.431233871219854, -0.526542184106192} -- Mooney : Li = 0.1 0.2 0.3
    
} -- StaticPressure

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step.
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_RESULT_DIR}/RivlinCubeHexahedra",
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1

} -- Result
