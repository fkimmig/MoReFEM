/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/OpsFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"

# include "ModelInstances/Laplacian/InputParameterList.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
# include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"


namespace MoReFEM
{


    namespace LaplacianNS
    {


        //! Variational formulaton to solve for a laplacian problem with Dirichlet BC.
        class VariationalFormulation final
        : public MoReFEM::VariationalFormulation
        <
            VariationalFormulation,
            EnumUnderlyingType(SolverIndex::solver)
        >
        {
        private:

            //! Alias to the parent class.
            using parent = MoReFEM::VariationalFormulation
            <
                VariationalFormulation,
                EnumUnderlyingType(SolverIndex::solver)
            >;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;



        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<VariationalFormulation>;

            //! Alias to source operator type.
            using source_operator_type = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar>;

            //! Alias to source parameter type.
            using source_parameter_type = ScalarParameter<>;

        public:

            /// \name Special members.
            ///@{

            //! copydoc doxygen_hide_varf_constructor
            explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                            TimeManager& time_manager,
                                            const GodOfDof& god_of_dof,
                                            DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulation() = default;

            //! Copy constructor.
            VariationalFormulation(const VariationalFormulation&) = delete;

            //! Move constructor.
            VariationalFormulation(VariationalFormulation&&) = delete;

            //! Copy affectation.
            VariationalFormulation& operator=(const VariationalFormulation&) = delete;

            //! Move affectation.
            VariationalFormulation& operator=(VariationalFormulation&&) = delete;

            ///@}

        private:

            /*!
             * \brief Assemble method for all the static operators except the capacity.
             */
            void AssembleStaticOperators();

        private:

            /// \name CRTP-required methods.
            ///@{

            //! \copydoc doxygen_hide_varf_suppl_init
            void SupplInit(const InputParameterList& input_parameter_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();


            //! Define the pointer function required to calculate the function required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;

            //! Define the pointer function required to calculate the jacobian required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;

            //! Define the pointer function required to view the results required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

            ///@}


        private:


            /*!
             * \brief Define the properties of all the global variational operators involved.
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             */
            void DefineOperators(const InputParameterList& input_parameter_data);

            //! Get the conductivity operator.
            const GlobalVariationalOperatorNS::GradPhiGradPhi& GetGradGradOperator() const noexcept;

            //! Get the volumic source operator.
            const source_operator_type& GetVolumicSourceOperator() const noexcept;

        private:


            /// \name Global variational operators.
            ///@{


            //! Conductivity operator.
            GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr grad_grad_operator_ = nullptr;

            //! Volumic source operator.
            source_operator_type::const_unique_ptr volumic_source_operator_ = nullptr;

            ///@}


            /// \name Parameters used to define TransientSource operators.
            ///@{

            //! Volumic source parameter.
            source_parameter_type::unique_ptr volumic_source_parameter_ = nullptr;

            ///@}



        };


    } // namespace LaplacianNS


} // namespace MoReFEM


# include "ModelInstances/Laplacian/VariationalFormulation.hxx"


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_VARIATIONAL_FORMULATION_HPP_
