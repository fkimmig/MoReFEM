/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 22 Nov 2017 13:30:46 +0100
/// Copyright (c) Inria. All rights reserved.
///

# include "Geometry/Domain/DomainManager.hpp"

# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

# include "ModelInstances/Laplacian/VariationalFormulation.hpp"


namespace MoReFEM
{
    
    
    namespace LaplacianNS
    {
        
        
        VariationalFormulation
        ::VariationalFormulation(const morefem_data_type& morefem_data,
                                 TimeManager& time_manager,
                                 const GodOfDof& god_of_dof,
                                 DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list))
        { }
        
        
        void VariationalFormulation::SupplInit(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            const auto& pressure = UnknownManager::GetInstance().GetUnknown(EnumUnderlyingType(UnknownIndex::pressure));
            
            constexpr auto initial_condition_index = EnumUnderlyingType(InitialConditionIndex::initial_condition);
            
            InitializeVectorSystemSolution<initial_condition_index>(input_parameter_data,
                                                                    monolithic_numbering_subset,
                                                                    pressure,
                                                                    felt_space_volume);
            
            DefineOperators(input_parameter_data);
            AssembleStaticOperators();
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(monolithic_numbering_subset,
                                                                                                 monolithic_numbering_subset);

            # ifdef MOREFEM_EXTENDED_TIME_KEEP
            auto& time_keeper = TimeKeep::GetInstance();
            time_keeper.PrintTimeElapsed("Before solving with factorization.");
            #endif // MOREFEM_EXTENDED_TIME_KEEP
            
            if (GetMpi().IsRootProcessor())
                std::cout << "Before solving with factorization." << std::endl;
            
            SolveLinear<IsFactorized::no>(monolithic_numbering_subset,
                                          monolithic_numbering_subset,
                                          __FILE__, __LINE__);
            
            # ifdef MOREFEM_EXTENDED_TIME_KEEP
            time_keeper.PrintTimeElapsed("After solving with factorization.");
            #endif // MOREFEM_EXTENDED_TIME_KEEP
            
            if (GetMpi().IsRootProcessor())
                std::cout << "After solving with factorization." << std::endl;
            
            unsigned int Nsolve = 20;
            
            for (unsigned int i = 0 ; i < Nsolve ; ++i)
            {
                SolveLinear<IsFactorized::yes>(monolithic_numbering_subset,
                                               monolithic_numbering_subset,
                                               __FILE__, __LINE__);
                
                # ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("After solving without factorization.");
                #endif // MOREFEM_EXTENDED_TIME_KEEP
            }
        }
        
        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            parent::AllocateSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            parent::AllocateSystemVector(monolithic_numbering_subset);
        }
        
        
        void VariationalFormulation::DefineOperators(const InputParameterList& input_parameter_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            
            decltype(auto) volume_domain = DomainManager::GetInstance().GetDomain(EnumUnderlyingType(DomainIndex::volume),
                                                                                  __FILE__, __LINE__);
            
            const auto& pressure = UnknownManager::GetInstance().GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            grad_grad_operator_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume,
                                                                        pressure,
                                                                        pressure);
            
            namespace IPL = Utilities::InputParameterListNS;
            
            
            
            {
                using parameter_type =
                    InputParameter::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>;
                
                volumic_source_parameter_ =
                    InitScalarParameterFromInputData<parameter_type>("Volumic source",
                                                                     volume_domain,
                                                                     input_parameter_data);
                
                if (volumic_source_parameter_ != nullptr)
                {
                    volumic_source_operator_ = std::make_unique<source_operator_type>(felt_space_volume,
                                                                                      pressure,
                                                                                      *volumic_source_parameter_);
                }
            }
        }
        
        

        
        
        void VariationalFormulation::AssembleStaticOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            auto& system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            
            # ifdef MOREFEM_EXTENDED_TIME_KEEP
            auto& time_keeper = TimeKeep::GetInstance();
            #endif // MOREFEM_EXTENDED_TIME_KEEP
            
            {
                GlobalMatrixWithCoefficient matrix(system_matrix, 1.);
                
                # ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("Before assembling laplacian operator.");
                #endif // MOREFEM_EXTENDED_TIME_KEEP
                
                if (GetMpi().IsRootProcessor())
                    std::cout << "Before assembling laplacian operator." << std::endl;
                
                GetGradGradOperator().Assemble(std::make_tuple(std::ref(matrix)));
                # ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("After assembling laplacian operator.");
                #endif // MOREFEM_EXTENDED_TIME_KEEP
                
                if (GetMpi().IsRootProcessor())
                    std::cout << "After assembling laplacian operator." << std::endl;
            }
            
            if (volumic_source_parameter_ != nullptr)
            {
                auto& rhs = GetNonCstSystemRhs(monolithic_numbering_subset);
                
                {
                    GlobalVectorWithCoefficient vector(rhs, 1.);
                    
                    # ifdef MOREFEM_EXTENDED_TIME_KEEP
                    time_keeper.PrintTimeElapsed("Before assembling rhs.");
                    #endif // MOREFEM_EXTENDED_TIME_KEEP
                    
                    if (GetMpi().IsRootProcessor())
                        std::cout << "Before assembling rhs." << std::endl;
                    
                    GetVolumicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), 0.);
                    
                    # ifdef MOREFEM_EXTENDED_TIME_KEEP
                    time_keeper.PrintTimeElapsed("After assembling rhs.");
                    #endif // MOREFEM_EXTENDED_TIME_KEEP
                    
                    if (GetMpi().IsRootProcessor())
                        std::cout << "After assembling rhs." << std::endl;
                }
            }
        }
        
        
    } // namespace LaplacianNS
    
    
} // namespace MoReFEM
