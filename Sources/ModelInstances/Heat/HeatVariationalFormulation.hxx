/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_VARIATIONAL_FORMULATION_HXX_
# define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_VARIATIONAL_FORMULATION_HXX_


namespace MoReFEM
{


    namespace HeatNS
    {


        inline Wrappers::Petsc::Snes::SNESFunction HeatVariationalFormulation::ImplementSnesFunction() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESJacobian HeatVariationalFormulation::ImplementSnesJacobian() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESViewer HeatVariationalFormulation::ImplementSnesViewer() const
        {
            return nullptr;
        }


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
        HeatVariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const HeatVariationalFormulation::source_operator_type&
        HeatVariationalFormulation::GetVolumicSourceOperator() const noexcept
        {
            assert(!(!volumic_source_operator_));
            return *volumic_source_operator_;
        }


        inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
        HeatVariationalFormulation::GetConductivityOperator() const noexcept
        {
            assert(!(!conductivity_operator_));
            return *conductivity_operator_;
        }


        inline const GlobalVariationalOperatorNS::Mass& HeatVariationalFormulation
        ::GetCapacityOperator() const noexcept
        {
            assert(!(!capacity_operator_) && "Only exists in dynamic");
            return *capacity_operator_;
        }


        inline const HeatVariationalFormulation::source_operator_type&
        HeatVariationalFormulation::GetNeumannOperator() const noexcept
        {
            assert(!(!neumann_operator_));
            return *neumann_operator_;
        }


        inline const GlobalVariationalOperatorNS::Mass& HeatVariationalFormulation
        ::GetRobinBilinearPartOperator() const noexcept
        {
            assert(!(!robin_bilinear_part_operator_));
            return *robin_bilinear_part_operator_;
        }


        inline const HeatVariationalFormulation::source_operator_type& HeatVariationalFormulation
        ::GetRobinLinearPartOperator()  const noexcept
        {
            assert(!(!robin_linear_part_operator_));
            return *robin_linear_part_operator_;
        }


        inline const GlobalVector& HeatVariationalFormulation::GetVectorCurrentVolumicSource() const
        {
            assert(!(!vector_current_volumic_source_));
            return *vector_current_volumic_source_;
        }


        inline GlobalVector& HeatVariationalFormulation::GetNonCstVectorCurrentVolumicSource()
        {
            return const_cast<GlobalVector&>(GetVectorCurrentVolumicSource());
        }

        inline const GlobalVector& HeatVariationalFormulation::GetVectorCurrentNeumann() const
        {
            assert(!(!vector_current_neumann_));
            return *vector_current_neumann_;
        }


        inline GlobalVector& HeatVariationalFormulation::GetNonCstVectorCurrentNeumann()
        {
            return const_cast<GlobalVector&>(GetVectorCurrentNeumann());
        }


        inline const GlobalVector& HeatVariationalFormulation::GetVectorCurrentRobin() const
        {
            assert(!(!vector_current_robin_));
            return *vector_current_robin_;
        }


        inline GlobalVector& HeatVariationalFormulation::GetNonCstVectorCurrentRobin()
        {
            return const_cast<GlobalVector&>(GetVectorCurrentRobin());
        }


        inline const GlobalMatrix& HeatVariationalFormulation::GetMatrixConductivity() const
        {
            assert(!(!matrix_conductivity_));
            return *matrix_conductivity_;
        }


        inline GlobalMatrix& HeatVariationalFormulation::GetNonCstMatrixConductivity()
        {
            return const_cast<GlobalMatrix&>(GetMatrixConductivity());
        }


        inline const GlobalMatrix& HeatVariationalFormulation::GetMatrixCapacityPerTimeStep() const
        {
            assert(!(!matrix_capacity_));
            return *matrix_capacity_;
        }


        inline GlobalMatrix& HeatVariationalFormulation::GetNonCstMatrixCapacityPerTimeStep()
        {
            return const_cast<GlobalMatrix&>(GetMatrixCapacityPerTimeStep());
        }


        inline const GlobalMatrix& HeatVariationalFormulation::GetMatrixRobin() const
        {
            assert(!(!matrix_robin_));
            return *matrix_robin_;
        }


        inline GlobalMatrix& HeatVariationalFormulation::GetNonCstMatrixRobin()
        {
            return const_cast<GlobalMatrix&>(GetMatrixRobin());
        }


        inline HeatVariationalFormulation::StaticOrDynamic HeatVariationalFormulation::GetStaticOrDynamic() const
        {
            return run_case_;
        }


        inline const NumberingSubset& HeatVariationalFormulation::GetNumberingSubset() const
        {
            return numbering_subset_;
        }


        inline const ScalarParameter<>& HeatVariationalFormulation::GetDiffusionDensity() const
        {
            assert(!(!density_));
            return *density_;
        }


        inline const ScalarParameter<>& HeatVariationalFormulation::GetTransfertCoefficient() const
        {
            assert(!(!transfert_coefficient_));
            return *transfert_coefficient_;
        }


        inline const ScalarParameter<>& HeatVariationalFormulation::GetDiffusionTensor() const
        {
            assert(!(!diffusion_tensor_));
            return *diffusion_tensor_;
        }


    } // namespace HeatNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_VARIATIONAL_FORMULATION_HXX_
