-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

-- transient
transient = {
    
    -- Tells which policy is used to describe time evolution.
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'constant_time_step', 'variable_time_step'}})
    time_evolution_policy = "constant_time_step",
    
    
    -- Time at the beginning of the code (in seconds).
    -- Expected format: VALUE
    -- Constraint: v >= 0.
    init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 2
}


-- NumberingSubset1
NumberingSubset1 = {
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = "monolithic",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

}


-- Unknown1: heat.
Unknown1 = {
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "temperature",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'scalar', 'vectorial'})
    nature = "scalar"
}


-- Mesh
Mesh1 = {
    -- Input Mesh file name
    -- Expected format: "VALUE"
    mesh = "${HOME}/Codes/MoReFEM/CoreLibrary/Data/Mesh/heat_bar_1D.geo",
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'Ensight', 'Medit'})
    format = "Ensight",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the mesh
    -- file; in which case Coords will be reduced provided all the dropped values are 0. If not, an exception is
    -- thrown.
    -- Constraint: v <= 3 and v > 0
    dimension = 1,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.
}


-- Domain1 - The 1D elements.
Domain1 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- Domain2 - Neumann boundary conditions.
Domain2 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    -- Expected format: VALUE
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 0 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 1 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- Domain3 - Robin boundary conditions.
Domain3 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    -- Expected format: VALUE
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 0 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 2 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- Domain 4 - upon which first essential condition is defined.
Domain4 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    -- Expected format: VALUE
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 1 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- Domain 5 - upon which first essential condition is defined.
Domain5 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    -- Expected format: VALUE
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 2 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


Domain6 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1}
    -- Expected format: VALUE
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}



EssentialBoundaryCondition1 = {
    
    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "first",
    
    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp1',
    
    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'temperature',
    
    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = {0.},
    
    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 4,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false
    
} -- EssentialBoundaryCondition1

EssentialBoundaryCondition2 = {
    
    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "second",
    
    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp1',
    
    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'temperature',
    
    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = {0.},
    
    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 5,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false
    
    
} -- EssentialBoundaryCondition2


-- FiniteElementSpace1
FiniteElementSpace1 = {
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"temperature"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"P1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 }
}

-- FiniteElementSpace2
FiniteElementSpace2 = {
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 2,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"temperature"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"P1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 }
}


-- FiniteElementSpace3
FiniteElementSpace3 = {
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 3,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"temperature"},

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"P1"},

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 }
}


-- Petsc
Petsc1 = {
	-- Absolute tolerance
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v > 0
	maxIteration = 1000,

	-- List of preconditioner: { none jacobi sor lu bjacobi ilu asm cholesky }.
	-- To use mumps: preconditioner = lu
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: ops_in(v, 'lu')
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: v > 0.
	relativeTolerance = 1e-9,
    
    -- Step size tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    stepSizeTolerance = 1e-8,

	-- List of solver: { chebychev cg gmres preonly bicg python };
	-- To use Mumps choose preonly.
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: ops_in(v, {'Mumps', 'Umfpack'})
	solver = 'Mumps'
}

-- Result
Result = {
	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step.
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_RESULT_DIR}/Heat/1D",
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1
}

-- DiffusionDensity
Diffusion = {
    
    Density = {
        -- How is given the diffusion density (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the diffusion density in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise). This density is
        -- the coefficient in front of the time derivative of the unknown (e.g. mass density x specific heat
        -- capacity for the heat problem).
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 1.,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the Young modulus in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
        -- Value of the diffusion density in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise). This density
        -- is the coefficient in front of the time derivative of the unknown (e.g. mass density x specific heat
        -- capacity for the heat problem).
        -- Expected format: Function in Lua language, for instance:
        -- 	-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 - arg3
        -- end
        -- sin, cos and tan require a
        -- 'math.' preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the content
        -- is not interpreted by Ops until an actual use of the underlying function.
        
        lua_function = none
    },

    -- DiffusionTensor
    Tensor1 = {
        -- How is given the diffusion tensor (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the diffusion tensor in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise). E.g. the thermal
        -- conductivity tensor in heat conduction.
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 1.,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the Young modulus in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
        -- Value of the diffusion tensor in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise). E.g. the
        -- thermal conductivity tensor in heat conduction.
        -- Expected format: Function in Lua language, for instance:
        -- 	-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 - arg3
        -- end
        -- sin, cos and tan require a
        -- 'math.' preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the content
        -- is not interpreted by Ops until an actual use of the underlying function.
        
        lua_function = none
    },

    -- DiffusionTransfertCoefficient
    TransfertCoefficient = {
        -- How is given the transfert coefficient (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",
        
        -- Value of the transfert coefficient in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise), e.g. the heat
        -- transfer coefficient.
        -- Expected format: VALUE
        -- Constraint: v > 0.
        scalar_value = 0.,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the Young modulus in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {},
        
        -- Value of the transfert coefficient in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise), e.g. the
        -- heat transfer coefficient.
        -- Expected format: Function in Lua language, for instance:
        -- 	-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 - arg3
        -- end
        -- sin, cos and tan require a
        -- 'math.' preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the content
        -- is not interpreted by Ops until an actual use of the underlying function.
        
        lua_function = none
    }
}

-- TransientSource - Volumic
TransientSource1 = {
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = "constant",
    
    -- Value of the transient source in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 10.,
    
    -- Value of the transient source for the component x in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- function(arg1, arg2, arg3)
    -- return arg1 + arg2 - arg3
    -- end
    -- sin, cos and tan require a 'math.' preffix.
    lua_function = function (x, y, z)
    return 0.;
    end,
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_value = { }
    
    
}

-- TransientSource - Neumann
TransientSource2 = {
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = "constant",
    
    -- Value of the transient source in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 100.,
    
    -- Value of the transient source  in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- function(arg1, arg2, arg3)
    -- return arg1 + arg2 - arg3
    -- end
    -- sin, cos and tan require a 'math.' preffix.
    lua_function = function (x, y, z)
    return 0;
    end,
    
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_value = { }
    
}

-- TransientSource - Robin
TransientSource3 = {
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = "constant",
    
    -- Value of the transient source in the case nature is 'constant'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 100.,
    
    -- Value of the transient source for the component x in the case nature is 'lua_function'(and also initial value if nature is 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- function(arg1, arg2, arg3)
    -- return arg1 + arg2 - arg3
    -- end
    -- sin, cos and tan require a 'math.' preffix.
    lua_function = function (x, y, z)
    return 0.;
    end,
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_value = { }
    
}

InitialCondition1 = {
    
    -- How is given the initial condition value (as a constant or as a Lua function.)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = { "lua_function", "constant", "constant" },
    
    -- Value of the initial condition in the case nature is 'constant.
    -- Expected format: {VALUE1, VALUE2, ...}
    scalar_value = { 0. , 0., 0.},
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_x = function(x, y, z)
        return 10*math.exp(-(x-0.5-10/2)^2);
    end,
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_y = function(x, y, z)
        return 0.;
    end,
    
    -- Value of the initial condition in the case nature is 'lua_function'.
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_z = function(x, y, z)
        return 0.;
    end
    
} -- InitialCondition1
