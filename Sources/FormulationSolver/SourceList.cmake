target_sources(${MOREFEM_FORMULATION_SOLVER}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Snes/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/DofSourcePolicy/SourceList.cmake)
