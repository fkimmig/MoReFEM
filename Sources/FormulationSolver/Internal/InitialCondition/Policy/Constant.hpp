///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_

# include <memory>
# include <vector>

# include "Parameters/ParameterType.hpp"

# include "Geometry/Mesh/GeometricMeshRegion.hpp"
# include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Policy
            {


                /*!
                 * \brief Parameter policy when the parameter gets the same value everywhere.
                 *
                 * \tparam TypeT  Type of the parameter (real, vector, matrix).
                 */
                template<ParameterNS::Type TypeT>
                class Constant
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = Constant<TypeT>;


                private:

                    //! Alias to traits class related to TypeT.
                    using traits = ::MoReFEM::ParameterNS::Traits<TypeT>;

                public:

                    //! Alias to the return type.
                    using return_type = typename traits::return_type;

                    //! Alias to the type of the value actually stored.
                    using storage_type = std::decay_t<return_type>;


                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Constant(const GeometricMeshRegion& geometric_mesh_region,
                                      storage_type value);

                    //! Destructor.
                    ~Constant() = default;

                    //! Copy constructor.
                    Constant(const Constant&) = delete;

                    //! Move constructor.
                    Constant(Constant&&) = delete;

                    //! Copy affectation.
                    Constant& operator=(const Constant&) = delete;

                    //! Move affectation.
                    Constant& operator=(Constant&&) = delete;

                    ///@}

                protected:

                    //! Get the value.
                    return_type GetConstantValueFromPolicy() const;

                    //! Provided here to make the code compile, but should never be called.
                    [[noreturn]] return_type GetValueFromPolicy(const SpatialPoint& coords) const;

                protected:

                    //! Whether the parameter varies spatially or not (true here!)
                    bool IsConstant() const noexcept;


                private:

                    //! Get the value of the parameter.
                    storage_type value_;
                };


            } // namespace Policy


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_CONSTANT_HPP_
