target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ThreeDimensionalInitialCondition.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ThreeDimensionalInitialCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ThreeDimensionalInitialCondition.hxx" / 
)

