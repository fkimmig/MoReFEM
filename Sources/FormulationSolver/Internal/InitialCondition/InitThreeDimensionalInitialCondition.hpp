///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_

# include <memory>
# include <vector>

# include "Core/InputParameter/InitialCondition/InitialCondition.hpp"

# include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hpp"
# include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"
# include "FormulationSolver/Internal/InitialCondition/Policy/OpsFunction.hpp"
# include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief Init a \a InitialCondition from the content of the input parameter file.
             *
             * \tparam LuaFieldT Lua field considered in the input file.
             *
             * \param[in] geometric_mesh_region Mesh upon which the InitialCondition is applied. Initial condition
             * might be requested at each of each \a Coords.
             * \copydoc doxygen_hide_input_parameter_data_arg
             *
             * Condition is actually splitted in three \a ScalarInitialCondition.
             *
             * \return The \a InitialCondition object properly initialized.
             */

            template
            <
                class LuaFieldT,
                class InputParameterDataT
            >
            InitialCondition<ParameterNS::Type::vector>::unique_ptr
            InitThreeDimensionalInitialCondition(const GeometricMeshRegion& geometric_mesh_region,
                                                 const InputParameterDataT& input_parameter_data);


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HPP_
