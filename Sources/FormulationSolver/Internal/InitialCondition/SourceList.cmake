target_sources(${MOREFEM_FORMULATION_SOLVER}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InitScalarInitialCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitScalarInitialCondition.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitThreeDimensionalInitialCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitThreeDimensionalInitialCondition.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
