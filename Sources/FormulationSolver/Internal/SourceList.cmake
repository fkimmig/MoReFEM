target_sources(${MOREFEM_FORMULATION_SOLVER}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/InitialCondition/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Storage/SourceList.cmake)
