target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrixStorage.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorStorage.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrixStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalMatrixStorage.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorStorage.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GlobalVectorStorage.hxx" / 
)

