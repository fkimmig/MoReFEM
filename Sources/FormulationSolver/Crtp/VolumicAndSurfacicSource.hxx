///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Mar 2015 14:24:25 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_VOLUMIC_AND_SURFACIC_SOURCE_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_VOLUMIC_AND_SURFACIC_SOURCE_HXX_


namespace MoReFEM
{


    namespace Crtp
    {


        template
        <
            class DerivedT,
            ParameterNS::Type TypeT,
            unsigned int VolumicIndexT,
            unsigned int SurfacicIndexT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<SourceType SourceTypeT, class T, class InputParameterDataT>
        void VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>
        ::SetIfTaggedAsActivated(T&& name,
                                 const InputParameterDataT& input_parameter_data,
                                 const FEltSpace& felt_space,
                                 const Unknown::const_shared_ptr unknown_ptr,
                                 const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        {
            namespace GVO = GlobalVariationalOperatorNS;
            namespace IPL = Utilities::InputParameterListNS;

            typename GVO::TransientSource<TypeT, TimeDependencyT>::const_unique_ptr global_operator = nullptr;
            typename parameter_type::unique_ptr parameter_ptr = nullptr;

            // If tagged as activated, compute here the global variational operator.
            {
                constexpr auto index = SourceTypeT == SourceType::volumic ? VolumicIndexT : SurfacicIndexT;

                using local_parameter_type = InputParameter::VectorialTransientSource<index>;

                parameter_ptr =
                    InitThreeDimensionalParameterFromInputData
                    <
                        local_parameter_type,
                        TimeDependencyT
                    >(std::forward<T>(name),
                      felt_space.GetDomain(),
                      input_parameter_data);

                if (parameter_ptr == nullptr)
                    return;

                global_operator =
                    std::make_unique<GVO::TransientSource<TypeT, TimeDependencyT>>(felt_space,
                                                                                   unknown_ptr,
                                                                                   *parameter_ptr,
                                                                                   quadrature_rule_per_topology);
            }

            // Assign properly the created global variational operator.
            switch (SourceTypeT)
            {
                case SourceType::volumic:
                    volumic_force_operator_ = std::move(global_operator);
                    volumic_force_parameter_ = std::move(parameter_ptr);
                    break;
                case SourceType::surfacic:
                    surfacic_force_operator_ = std::move(global_operator);
                    surfacic_force_parameter_ = std::move(parameter_ptr);
                    break;
            }
        }



        template
        <
            class DerivedT,
            ParameterNS::Type TypeT,
            unsigned int VolumicIndexT,
            unsigned int SurfacicIndexT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<SourceType SourceTypeT>
        bool VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>
        ::IsOperatorActivated() const noexcept
        {
            switch (SourceTypeT)
            {
                case SourceType::volumic:
                    return volumic_force_operator_ != nullptr;
                case SourceType::surfacic:
                    return surfacic_force_operator_ != nullptr;
            }
            assert(false && "All cases should be handled in the switch!");
            return false; // to avoid warning in release mode.
        }


        template
        <
            class DerivedT,
            ParameterNS::Type TypeT,
            unsigned int VolumicIndexT,
            unsigned int SurfacicIndexT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<SourceType SourceTypeT>
        inline const GlobalVariationalOperatorNS::TransientSource<TypeT, TimeDependencyT>&
        VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>
        ::GetForceOperator() const noexcept
        {
            assert(IsOperatorActivated<SourceTypeT>());

            switch (SourceTypeT)
            {
                case SourceType::volumic:
                {
                    assert(!(!volumic_force_operator_));
                    return *volumic_force_operator_;
                }
                case SourceType::surfacic:
                {
                    assert(!(!surfacic_force_operator_));
                    return *surfacic_force_operator_;
                }
            }

            assert(false && "All cases should be handled in the switch!");
            return *volumic_force_operator_; // to avoid warning in release mode.
        }


        template
        <
            class DerivedT,
            ParameterNS::Type TypeT,
            unsigned int VolumicIndexT,
            unsigned int SurfacicIndexT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        const typename  VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>::parameter_type&
        VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>
        ::GetVolumicForceParameter() const
        {
            assert(!(!volumic_force_parameter_));
            return *volumic_force_parameter_;
        }


        template
        <
            class DerivedT,
            ParameterNS::Type TypeT,
            unsigned int VolumicIndexT,
            unsigned int SurfacicIndexT,
            template<ParameterNS::Type> class TimeDependencyT
        >
        const typename VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>::parameter_type&
        VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeDependencyT>
        ::GetSurfacicForceParameter() const
        {
            assert(!(!surfacic_force_parameter_));
            return *surfacic_force_parameter_;
        }


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_VOLUMIC_AND_SURFACIC_SOURCE_HXX_
