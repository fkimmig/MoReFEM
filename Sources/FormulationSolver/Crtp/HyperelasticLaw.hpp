///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 22 Jun 2016 16:37:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace FormulationSolverNS
    {


        /*!
         * \brief Crtp to add in a VariationalFormulation an hyperelastic_law_ data attribute and its accessors.
         *
         * \tparam DerivedT Class upon which the CRTP is applied.
         * \tparam HyperelasticLawT Type of the hyperelastic law (e.g. HyperelasticLawNS::CiarletGeymonat).
         *
         * \attention Create() must be explicitly called before the law is used.
         */
        template
        <
            class DerivedT,
            class HyperelasticLawT
        >
        class HyperelasticLaw
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = HyperelasticLaw<DerivedT, HyperelasticLawT>;

            //! Alias to hyperelastic law type.
            using hyperelastic_law_type = HyperelasticLawT;


        protected:

            /// \name Special members.
            ///@{

            //! Constructor.

            explicit HyperelasticLaw() = default;

            //! Destructor.
            ~HyperelasticLaw() = default;

            //! Copy constructor.
            HyperelasticLaw(const HyperelasticLaw&) = delete;

            //! Move constructor.
            HyperelasticLaw(HyperelasticLaw&&) = delete;

            //! Copy affectation.
            HyperelasticLaw& operator=(const HyperelasticLaw&) = delete;

            //! Move affectation.
            HyperelasticLaw& operator=(HyperelasticLaw&&) = delete;

            ///@}

            /*!
             * \brief Instantiate the hyperelastic law.
             *
             * \param[in] args hyperelastic_law_type's constructor arguments.
             */
            template<typename... Args>
            void Create(Args&&... args);

            //! Get the underlying hyperelastic law.
            const hyperelastic_law_type& GetHyperelasticLaw() const noexcept;

            //! Get the underlying hyperelastic law as a raw pointer.
            const hyperelastic_law_type* GetHyperelasticLawPtr() const noexcept;


        private:

            //! Hyperelastic law used in stiffness operator.
            typename hyperelastic_law_type::const_unique_ptr hyperelastic_law_ = nullptr;


        };


    } // namespace FormulationSolverNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Crtp/HyperelasticLaw.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_CRTP_x_HYPERELASTIC_LAW_HPP_
