///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 13:35:14 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FormulationSolver/DofSourcePolicy/DofSource.hpp"


namespace MoReFEM
{
    
    
    namespace VariationalFormulationNS
    {
        
        
        namespace DofSourcePolicyNS
        {
            
            
            DofSource::DofSource(double factor, const GlobalVector& dof_source)
            : dof_source_(dof_source),
            factor_(factor)
            { }
            
            
            void DofSource::AddToRhs(GlobalVector& rhs)
            {
                Wrappers::Petsc::AXPY(factor_, dof_source_, rhs, __FILE__, __LINE__);
            }
            
            
        } // namespace DofSourcePolicyNS
        
        
    } // namespace VariationalFormulationNS
  

} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup
