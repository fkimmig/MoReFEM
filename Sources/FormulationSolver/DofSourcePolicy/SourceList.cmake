target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DofSource.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/None.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DofSource.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofSource.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/None.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/None.hxx" / 
)

