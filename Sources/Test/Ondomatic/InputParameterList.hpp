/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputParameter/InitialCondition/InitialCondition.hpp"


namespace MoReFEM
{


    namespace OndomaticNS
    {

        
        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };
        
        
        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            highest_dimension = 1,
            face1 = 2,
            face2 = 3,
            face3 = 4,
            face4 = 5,
            face5 = 6,
            face6 = 7,
            face123 = 8,
            full_mesh = 9
        };
        
        
        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            face123 = 1
        };
        
        
        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            surface_pressure_4 = 2,
            surface_pressure_5 = 3,
            surface_pressure_6 = 4
        };
        
        
        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            pressure = 1
        };
        
        
        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            monolithic = 1
        };
        
        
        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        
        {
            solver = 1
        };
        
        
        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            pressure_initial_condition = 1
        };
        
        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : unsigned int
        {
            surface_pressure_4 = 1,
            surface_pressure_5 = 2,
            surface_pressure_6 = 3
        };
            
            
        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,
        
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,
        
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,
        
            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,
        
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face1)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face2)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face3)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face4)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face5)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face6)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::face123)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
        
            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face123)>,
        
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_4)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_5)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure_6)>,
        
            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,
        
            InputParameter::InitialCondition<EnumUnderlyingType(InitialConditionIndex::pressure_initial_condition)>,
        
            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_4)>,
            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_5)>,
            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::surface_pressure_6)>,
        
            InputParameter::Result
        >;
        
        
        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;
        
        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;
        
        
    } // namespace OndomaticNS
    
    
} // namespace MoReFEM
    
    
#endif // MOREFEM_x_TEST_x_ONDOMATIC_x_INPUT_PARAMETER_LIST_HPP_
