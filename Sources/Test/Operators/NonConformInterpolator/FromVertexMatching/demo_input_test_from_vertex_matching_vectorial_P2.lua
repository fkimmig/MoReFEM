-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'constant_time_step', 'variable_time_step'}})
	time_evolution_policy = "constant_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.

} -- transient

NumberingSubset10 = {

	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
	name = 'Unknown on fluid',
    
	-- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a 
	-- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation. 
	-- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a 
	-- displacement. 
	-- Expected format: 'true' or 'false' (without the quote)
	do_move_mesh = false

} -- NumberingSubset10


NumberingSubset20 = {

	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
	name = 'Unknown on solid',

	-- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a 
	-- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation. 
	-- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a 
	-- displacement. 
	-- Expected format: 'true' or 'false' (without the quote)
	do_move_mesh = false

} -- NumberingSubset20



NumberingSubset22 = {
    
    -- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground
    -- the possible values to choose elsewhere).
    -- Expected format: "VALUE"
    name = 'Chaos',
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false
    
} -- NumberingSubset22


Unknown1 = {

	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = 'unknown',

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'scalar', 'vectorial'})
	nature = 'vectorial'

} -- Unknown1



Unknown3 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = 'chaos',
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'scalar', 'vectorial'})
    nature = 'vectorial'
    
} -- Unknown3




Mesh1 = {

	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = '${HOME}/Codes/MoReFEM/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh',

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 2,

	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh1

Mesh2 = {

	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = '${HOME}/Codes/MoReFEM/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh',

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 2,

	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh2



-- Domain10
Domain10 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}


-- Domain20
Domain20 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 2 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
}



FiniteElementSpace10 = {

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 10,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { 'unknown' },

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { 'P2' },

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 10 }

} -- FiniteElementSpace10


FiniteElementSpace20 = {

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 2,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 20,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = { 'unknown', 'chaos' },

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { 'P2', 'P2' }, -- chaos shape function should be a yet unused one to ensure different dof repartition for both meshes.

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
	numbering_subset_list = { 20 , 22}

} -- FiniteElementSpace20

Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-9,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-8,

	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'Mumps'

} -- Petsc1

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. 
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_RESULT_DIR}/Test/NonConformInterpolator/FromVertexMatching',

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1

} -- Result


InitVertexMatchingInterpolator10 = {
    
    -- Finite element space for which the dofs index will be associated to each vertex.
    -- Expected format: VALUE
    finite_element_space = 10,
    
    -- Numbering subsetfor which the dofs index will be associated to each vertex.
    -- Expected format: "VALUE"
    numbering_subset = 10
    
} -- InitVertexMatchingInterpolator10



InitVertexMatchingInterpolator20 = {
    
    -- Finite element space for which the dofs index will be associated to each vertex.
    -- Expected format: VALUE
    finite_element_space = 20,
    
    -- Numbering subsetfor which the dofs index will be associated to each vertex.
    -- Expected format: "VALUE"
    numbering_subset = 20
    
} -- InitVertexMatchingInterpolator20



-- File that gives for each vertex on the first mesh on the interface the index of the equivalent vertex in
-- the second mesh.
-- Expected format: "VALUE"
interpolation_file = "${HOME}/Codes/MoReFEM/Data/Interpolation/TestVertexMatching.hhdata"

