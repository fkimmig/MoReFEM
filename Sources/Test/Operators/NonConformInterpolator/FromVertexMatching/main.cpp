/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/NonConformInterpolator/FromVertexMatching/Model.hpp"
#include "Test/Operators/NonConformInterpolator/FromVertexMatching/InputParameterList.hpp"
#include "Test/Operators/NonConformInterpolator/FromVertexMatching/CheckInterpolator.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = TestVertexMatchingNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            TestVertexMatchingNS::Model model(morefem_data);
            model.Run(morefem_data);
            
            mpi.Barrier();
            
            // Post processing is a sequential process...
            if (mpi.IsRootProcessor())
                CheckInterpolator(model);
            
            model.GetMatrixUnknownSolidToFluid().View(mpi,
                                                      model.GetOutputDirectory() + "/interpolation_matrix.hhdata",
                                                      __FILE__, __LINE__,
                                                      PETSC_VIEWER_DEFAULT
                                                      );

            
            input_parameter_data.PrintUnused(std::cout);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());            
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
        
        if (mpi.IsRootProcessor())
        {
            std::cout << std::endl;
            std::cout << "**********************************************************" << std::endl;
            std::cout << "If your read this, FromVertexMatching behaves as expected." << std::endl;
            std::cout << "**********************************************************" << std::endl;
            std::cout << std::endl;
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

