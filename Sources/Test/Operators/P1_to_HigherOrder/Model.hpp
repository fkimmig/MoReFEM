/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 16:00:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HPP_

# include <memory>
# include <vector>

#include "Core/InputParameter/Result.hpp"

# include "OperatorInstances/ConformInterpolator/P1_to_P2.hpp"
# include "OperatorInstances/ConformInterpolator/P2_to_P1.hpp"

# include "OperatorInstances/ConformInterpolator/P1_to_P1b.hpp"
# include "OperatorInstances/ConformInterpolator/P1b_to_P1.hpp"

# include "Model/Model.hpp"

# include "Test/Operators/P1_to_HigherOrder/InputParameterList.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace P1_to_P_HigherOrder_NS
        {


            //! List of all higher orders that might be considered in the test model.
            enum class HigherOrder { P1b, P2 };


            //! Traits class.
            template<HigherOrder>
            struct TraitsHigherOrder;


            //! \cond IGNORE_BLOCK_IN_DOXYGEN


            template<>
            struct TraitsHigherOrder<HigherOrder::P1b>
            {
                using to_higher_order_type = ConformInterpolatorNS::P1_to_P1b;

                using from_higher_order_type = ConformInterpolatorNS::P1b_to_P1;
            };


            template<>
            struct TraitsHigherOrder<HigherOrder::P2>
            {
                using to_higher_order_type = ConformInterpolatorNS::P1_to_P2;

                using from_higher_order_type = ConformInterpolatorNS::P2_to_P1;
            };


            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Toy model used to perform tests about P1 -> Phigher interpolation.
             *
             * \tparam HigherOrderT Which is the target order of the interpolation. For instance, P2 to consider P1 -> P2.
             */
            template<HigherOrder HigherOrderT>
            //! \copydoc doxygen_hide_model_4_test
        class Model : public MoReFEM::Model<Model<HigherOrderT>, DoConsiderProcessorWiseLocal2Global::yes>
            {

            private:

                //! \copydoc doxygen_hide_alias_self
                using self = Model<HigherOrderT>;

                //! Convenient alias.
                using parent = MoReFEM::Model<self, DoConsiderProcessorWiseLocal2Global::yes>;

                //! Traits class.
                using traits = TraitsHigherOrder<HigherOrderT>;


            public:

                //! Return the name of the model.
                static const std::string& ClassName();

                //! Friendship granted to the base class so this one can manipulates private methods.
                friend parent;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_morefem_data_param
                 */
                Model(const morefem_data_type& morefem_data);

                //! Destructor.
                ~Model() = default;

                //! Copy constructor.
                Model(const Model&) = delete;

                //! Move constructor.
                Model(Model&&) = delete;

                //! Copy affectation.
                Model& operator=(const Model&) = delete;

                //! Move affectation.
                Model& operator=(Model&&) = delete;

                ///@}


                /// \name Crtp-required methods.
                ///@{


               /*!
                * \brief Initialise the problem.
                *
                * This initialisation includes the resolution of the static problem.
                * \copydoc doxygen_hide_morefem_data_param
                */
               void SupplInitialize(const morefem_data_type& morefem_data);


               //! Manage time iteration.
               void Forward();

               /*!
                * \brief Additional operations to finalize a dynamic step.
                *
                * Base class already update the time for next time iterations.
                */
               void SupplFinalizeStep();


               /*!
                * \brief Initialise a dynamic step.
                *
                */
               void SupplFinalize();

                //! Output directory.
                const std::string& GetOutputDirectory() const noexcept;



           private:


               //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
               bool SupplHasFinishedConditions() const;


               /*!
                * \brief Part of InitializedStep() specific to Elastic model.
                *
                * As there are none, the body of this method is empty.
                */
               void SupplInitializeStep();


               ///@}

            private:

                //! Output directory.
                const std::string output_directory_;

                //! Interpolator that extend P1 to higher order.
                typename traits::to_higher_order_type::unique_ptr interpolator_p1_p_higher_order_ = nullptr;

                //! Reverse operator that reduce higher order to P1.
                typename traits::from_higher_order_type::unique_ptr interpolator_p_higher_order_p1_ = nullptr;
            };


        } // namespace P1_to_P_HigherOrder_NS


    } // namespace TestNS


} // namespace MoReFEM


# include "Test/Operators/P1_to_HigherOrder/Model.hxx"


#endif // MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_MODEL_HPP_
