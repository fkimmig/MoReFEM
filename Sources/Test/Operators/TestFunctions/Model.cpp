/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/TestFunctions/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestFunctionsNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }


        void Model::SupplInitialize(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            
            {                
                variational_formulation_ =
                    std::make_unique<VariationalFormulation>(morefem_data,
                                                             GetNonCstTimeManager(),
                                                             god_of_dof,
                                                             DirichletBoundaryCondition::vector_shared_ptr());
            }
            
            auto& variational_formulation = GetNonCstVariationalFormulation();
            
            variational_formulation.Init(morefem_data.GetInputParameterList());
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }
        

    } // namespace TestFunctionsNS


} // namespace MoReFEM
