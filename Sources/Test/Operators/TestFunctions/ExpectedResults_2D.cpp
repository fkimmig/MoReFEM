/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 23:32:19 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/TestFunctions/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestFunctionsNS
    {

        
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults2D(bool is_parallel)
        {
            static_cast<void>(is_parallel); // For 2D case matrices are identical in both sequential and
                                            // parallel case.
            
            expected_results_type<IsMatrixOrVector::matrix> expected_results;
            
            constexpr double one_3rd = 1. / 3.;
            constexpr double one_6th = 1. / 6.;
            constexpr double one_12th = 1. / 12.;
            constexpr double one_24th = 1. / 24.;
            
            using content_type = content_type<IsMatrixOrVector::matrix>;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1",
                                 content_type
                                 {
                                     { one_12th, one_24th, one_24th },
                                     { one_24th, one_12th, one_24th },
                                     { one_24th, one_24th, one_12th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2",
                                 content_type
                                 {
                                     { one_12th, one_24th, one_24th },
                                     { one_24th, one_12th, one_24th },
                                     { one_24th, one_24th, one_12th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_1",
                                 content_type
                                 {
                                     { one_12th, 0., one_24th, 0., one_24th, 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { one_24th, 0., one_12th, 0., one_24th, 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { one_24th, 0., one_24th, 0., one_12th, 0. },
                                     { 0., 0., 0., 0., 0., 0. }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
        
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_2",
                                 content_type
                                 {
                                     { one_12th, one_12th, one_24th, one_24th, one_24th, one_24th },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { one_24th, one_24th, one_12th, one_12th, one_24th, one_24th },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { one_24th, one_24th, one_24th, one_24th, one_12th, one_12th },
                                     { 0., 0., 0., 0., 0., 0. }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2_potential_1",
                                 content_type
                                 {
                                     { one_12th, one_12th, one_24th, one_24th, one_24th, one_24th },
                                     { one_12th, 0., one_24th, 0., one_24th, 0. },
                                     { one_24th, one_24th, one_12th, one_12th, one_24th, one_24th },
                                     { one_24th, 0., one_12th, 0., one_24th, 0. },
                                     { one_24th, one_24th, one_24th, one_24th, one_12th, one_12th },
                                     { one_24th, 0., one_24th, 0., one_12th, 0. }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2_potential_2",
                                 content_type
                                 {
                                     { one_12th, one_12th, one_24th, one_24th, one_24th, one_24th },
                                     { one_12th, one_12th, one_24th, one_24th, one_24th, one_24th },
                                     { one_24th, one_24th, one_12th, one_12th, one_24th, one_24th },
                                     { one_24th, one_24th, one_12th, one_12th, one_24th, one_24th },
                                     { one_24th, one_24th, one_24th, one_24th, one_12th, one_12th },
                                     { one_24th, one_24th, one_24th, one_24th, one_12th, one_12th }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double one_60th = 1. / 60.;
            constexpr double one_120th = 1. / 120.;
            constexpr double one_90th = 1. / 90.;
            constexpr double two_45th = 2. / 45.;
            constexpr double four_45th = 4. / 45.;
            constexpr double one_360th = 1. / 360.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_3",
                                 content_type
                                 {
                                     { one_60th, -one_360th, -one_360th, 0., -one_90th, 0. },
                                     { -one_360th, one_60th, -one_360th, 0., 0., -one_90th },
                                     { -one_360th, -one_360th, one_60th, -one_90th, 0., 0. },
                                     { 0., 0., -one_90th, four_45th, two_45th, two_45th },
                                     { -one_90th, 0., 0., two_45th, four_45th, two_45th },
                                     { 0., -one_90th, 0., two_45th, two_45th, four_45th }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double one_15th = 1. / 15.;
            constexpr double one_30th = 1. / 30.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_3",
                                 content_type
                                 {
                                     { one_60th, -one_120th, -one_120th, one_15th, one_30th, one_15th },
                                     { -one_120th, one_60th, -one_120th, one_15th, one_15th, one_30th },
                                     { -one_120th, -one_120th, one_60th, one_30th, one_15th, one_15th }
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1",
                                 content_type
                                 {
                                     { 1., -.5, -.5 },
                                     { -.5, .5, 0. },
                                     { -.5, 0., .5 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_1",
                                 content_type
                                 {
                                     { 1., 0., -.5, 0., -.5, 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { -.5, 0., .5, 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { -.5, 0., 0., 0., .5, 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_2",
                                 content_type
                                 {
                                     { 1., 1., -.5, -.5, -.5, -.5 },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { -.5, -.5, .5, .5, 0., 0. },
                                     { 0., 0., 0., 0., 0., 0. },
                                     { -.5, -.5, 0., 0., .5, .5 },
                                     { 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_2_potential_1",
                                 content_type
                                 {
                                     { 1., 1., -.5, -.5, -.5, -.5 },
                                     { 1., 0., -.5, 0., -.5, 0. },
                                     { -.5, -.5, .5, .5, 0., 0. },
                                     { -.5, 0., .5, 0., 0., 0. },
                                     { -.5, -.5, 0., 0., .5, .5 },
                                     { -.5, 0., 0., 0., .5, 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_2_potential_2",
                                 content_type
                                 {
                                     { 1., 1., -.5, -.5, -.5, -.5 },
                                     { 1., 1., -.5, -.5, -.5, -.5 },
                                     { -.5, -.5, .5, .5, 0., 0. },
                                     { -.5, -.5, .5, .5, 0., 0. },
                                     { -.5, -.5, 0., 0., .5, .5 },
                                     { -.5, -.5, 0., 0., .5, .5 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double two_3rd = 2. / 3.;
            constexpr double four_3rd = 4. / 3.;
            constexpr double eight_3rd = 8. / 3.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_3",
                                 content_type
                                 {
                                     { 1., one_6th , one_6th , -two_3rd, 0., -two_3rd },
                                     { one_6th , .5, 0., -two_3rd, 0., 0. },
                                     { one_6th , 0., .5, 0., 0., -two_3rd },
                                     { -two_3rd, -two_3rd, 0., eight_3rd, -four_3rd, 0. },
                                     { 0., 0., 0., -four_3rd, eight_3rd, -four_3rd },
                                     { -two_3rd, 0., -two_3rd, 0., -four_3rd, eight_3rd },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_3",
                                 content_type
                                 {
                                     { one_3rd, -one_6th, -one_6th, two_3rd, -four_3rd, two_3rd },
                                     { -one_6th, one_6th, 0., 0., two_3rd, -two_3rd },
                                     { -one_6th, 0., one_6th, -two_3rd, two_3rd, 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("variable_mass_operator_potential_1",
                                 content_type
                                 {
                                     { one_12th, one_24th, one_24th },
                                     { one_24th, one_12th, one_24th },
                                     { one_24th, one_24th, one_12th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_operator",
                                 content_type
                                 {
                                     { 2., 2., -1., -1., -1., -1. },
                                     { 2., 4., -1., -2., -1., -2. },
                                     { -1., -1., .75, .75, .25, .25 },
                                     { -1., -2., .75, 1.5, .25, .5 },
                                     { -1., -1., .25, .25, .75, .75 },
                                     { -1., -2., .25, .5, .75, 1.5 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_potential_124_operator",
                                 content_type
                                 {
                                     { 2., 2., 0., -1., -1., 0., -1., -1., 0. },
                                     { 2., 4., 0., -1., -2., 0., -1., -2., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -1., -1., 0., .75, .75, 0., .25, .25, 0. },
                                     { -1., -2., 0., .75, 1.5, 0., .25, .5, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -1., -1., 0., .25, .25, 0., .75, .75, 0. },
                                     { -1., -2., 0., .25, .5, 0., .75, 1.5, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("grad_phi_tau_tau_grad_phi_operator",
                                 content_type
                                 {
                                     { 2., -1., -1. },
                                     { -1., .75, .25 },
                                     { -1., .25, .75 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double five_12th = 5. / 12.;
            constexpr double seven_12th = 7. / 12.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("ale_operator_2",
                                 content_type
                                 {
                                     { -seven_12th, -0., -five_12th, -0., -one_3rd, -0. },
                                     { -0., -seven_12th, -0., -five_12th, -0., -one_3rd },
                                     { one_24th, -0., -one_6th, -0., -one_24th, -0. },
                                     { -0., one_24th, -0., -one_6th, -0., -one_24th },
                                     { one_24th, -0., one_12th, -0., -.125, -0. },
                                     { -0., one_24th, -0., one_12th, -0., -.125 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("pk2_operator",
                                 content_type
                                 {
                                     { 31. * one_6th, 7. * one_6th, -2., 5. * one_6th, -19. * one_6th, -2. },
                                     { 7. * one_6th, 31. * one_6th, -2., -19. * one_6th, 5. * one_6th, -2. },
                                     { -2., -2., 2., -0., -0., 2. },
                                     { 5. * one_6th, -19. * one_6th, -0., 19. * one_6th, -5. * one_6th, -0. },
                                     { -19. * one_6th, 5. * one_6th, -0., -5. * one_6th, 19. * one_6th, -0. },
                                     { -2., -2., 2., -0., -0., 2. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("scalar_div_vectorial_operator",
                                 content_type
                                 {
                                     { 0., 0., 0., 0., 0., 0., -1.5 },
                                     { 0., 0., 0., 0., 0., 0., -1.5 },
                                     { 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 1.5 },
                                     { 0., 0., 0., 0., 0., 0., 1.5 },
                                     { 0., 0., 0., 0., 0., 0., 0. },
                                     { -1., -1., 0., 1., 1., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stokes_operator",
                                 content_type
                                 {
                                     { 1., 0., -.5, 0., -.5, 0., .5 },
                                     { 0., 1., 0., -.5, 0., -.5, .5 },
                                     { -.5, 0., .5, 0., 0., 0., 0. },
                                     { 0., -.5, 0., .5, 0., 0., -.5 },
                                     { -.5, 0., 0., 0., .5, 0., -.5 },
                                     { 0., -.5, 0., 0., 0., .5, 0. },
                                     { .5, .5, 0., -.5, -.5, 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("elasticity_operator",
                                 content_type
                                 {
                                     { 1.5, .5, -.5, 0., -1., -.5 },
                                     { .5, 1.5, -.5, -1., 0., -.5 },
                                     { -.5, -.5, .5, 0., 0., .5 },
                                     { 0., -1., 0., 1., 0., 0. },
                                     { -1., 0., 0., 0., 1., 0. },
                                     { -.5, -.5, .5, 0., 0., .5 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            
            InsertNewEntry<IsMatrixOrVector::matrix>("ale_operator_1",
                                 content_type
                                 {
                                     { -one_3rd, 0., -one_3rd, 0., -one_3rd, 0. },
                                     { 0., -one_3rd, 0., -one_3rd, 0., -one_3rd },
                                     { one_6th, 0., one_6th, 0., one_6th, 0. },
                                     { 0., one_6th, 0., one_6th, 0., one_6th },
                                     { one_6th, 0., one_6th, 0., one_6th, 0. },
                                     { 0., one_6th, 0., one_6th, 0., one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
       
            
            return expected_results;
        }
        
        
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults2D(bool is_parallel)
        {
            static_cast<void>(is_parallel); // For 2D case vectors are identical in both sequential and
                                            // parallel case.
            
            expected_results_type<IsMatrixOrVector::vector> expected_results;
            
            constexpr double one_6th = 1. / 6.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1",
                                                     { one_6th, one_6th, one_6th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
         
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1_potential_1",
                                                     { one_6th, 0., one_6th, 0., one_6th, 0. },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1_potential_2",
                                                     { one_6th, one_6th, one_6th, one_6th, one_6th, one_6th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::vector>("pk2_operator",
                                                     { 1., 2., 2., 0., 0., 0. },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_3",
                                                     { 0., 0., 0., one_6th, one_6th, one_6th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            constexpr double one_36th = 1. / 36.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("non_linear_source_operator",
                                                     { -one_36th, -one_36th, -one_36th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            return expected_results;
        }
        
     
    } // namespace TestFunctionsNS


} // namespace MoReFEM


