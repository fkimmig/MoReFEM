/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Apr 2014 12:11:28 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"
#include "Geometry/Domain/DomainManager.hpp"

# include "Utilities/InputParameterList/Private/TupleIteration.hpp"

#include "Test/OndomaticNumbering/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char * argv[])
{
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = OndomaticNumberingNS::InputParameterList ;
    
    std::cerr << "#523 This program needs a complete overhaul!" << std::endl;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
        
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            using WhateverTuple = std::tuple<std::integral_constant<unsigned int, 1>>;
            
            {
                // Create the mesh.
                auto& manager = GeometricMeshRegionManager::CreateOrGetInstance();
                Private::TupleIteration<WhateverTuple>::Create(input_parameter_data, manager);
                
            }
            
            
            {
                // Create the unknown manager.
                auto& manager = UnknownManager::CreateOrGetInstance();
                Private::TupleIteration<WhateverTuple>::Create(input_parameter_data, manager);
            }
            
            
            {
                // Create the domain manager.
                auto& manager = DomainManager::CreateOrGetInstance();
                Private::TupleIteration<WhateverTuple>::Create(input_parameter_data, manager);
            }
            
            
            namespace IPL = Utilities::InputParameterListNS;
            namespace Result = InputParameter::Result;
            
            decltype(auto) output_directory = morefem_data.GetResultDirectory();
            
            {
                // Create the god of dof.
                auto& manager = GodOfDofManager::CreateOrGetInstance();
                
                Private::TupleIteration<WhateverTuple>::Create(input_parameter_data,
                                                               manager,
                                                               mpi);
                
                
                // Look for all finite element spaces in the input parameter file and create them.
                // Each must be attached to a GodOfDof.
                std::map<unsigned int, FEltSpace::vector_unique_ptr> felt_space_list_per_god_of_dof_index;
                
                Private::TupleIteration<WhateverTuple>::CreateFEltSpace(input_parameter_data,
                                                                        felt_space_list_per_god_of_dof_index);
                
                // Init properly each GodOfDof with its finite element spaces.
                
                Private::TupleIteration<WhateverTuple>::InitGodOfDof(felt_space_list_per_god_of_dof_index,
                                                                     DoConsiderProcessorWiseLocal2Global::yes);
            }
            
            auto& manager = GodOfDofManager::CreateOrGetInstance();
            
            const auto& god_of_dof = manager.GetGodOfDof(1);
            
            const auto& numbering_subset_list = god_of_dof.GetNumberingSubsetList();
            assert(numbering_subset_list.size() == 1ul);
            
            god_of_dof.OndomaticLikePrint(*(numbering_subset_list.front()), output_directory);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;

}

