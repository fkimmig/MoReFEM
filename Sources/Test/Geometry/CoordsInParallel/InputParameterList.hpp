/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"


namespace MoReFEM
{


  namespace TestNS
    {


        namespace CoordsInParallelNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };


            //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
            {
                unknown = 1
            };


            //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

            {
                solver = 1
            };


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
            {
                mesh1 = 1,
                mesh2 = 2
            };



            //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,

                InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>,
                InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>,

                InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,

                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>,
                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>,

                InputParameter::Domain<EnumUnderlyingType(DomainIndex::mesh1)>,
                InputParameter::Domain<EnumUnderlyingType(DomainIndex::mesh2)>,

                InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>,
                InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>,

                InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

                InputParameter::Result

            >;


            //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


        } // namespace CoordsInParallelNS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_PARAMETER_LIST_HPP_
