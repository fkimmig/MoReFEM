/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_

# include <memory>
# include <vector>

# include "Core/InputParameter/Result.hpp"

# include "Model/Model.hpp"

# include "Test/Geometry/DomainListInCoords/InputParameterList.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {



            /*!
             * \brief Toy model used to perform tests about variable domain.
             *
             */
            //! \copydoc doxygen_hide_model_4_test
        class Model : public MoReFEM::Model<Model, DoConsiderProcessorWiseLocal2Global::no>
            {

            private:

                //! \copydoc doxygen_hide_alias_self
                using self = Model;

                //! Convenient alias.
                using parent = MoReFEM::Model<self, DoConsiderProcessorWiseLocal2Global::no>;


            public:

                //! Return the name of the model.
                static const std::string& ClassName();

                //! Friendship granted to the base class so this one can manipulates private methods.
                friend parent;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_morefem_data_param
                 */
                Model(const morefem_data_type& morefem_data);

                //! Destructor.
                ~Model() = default;

                //! Copy constructor.
                Model(const Model&) = delete;

                //! Move constructor.
                Model(Model&&) = delete;

                //! Copy affectation.
                Model& operator=(const Model&) = delete;

                //! Move affectation.
                Model& operator=(Model&&) = delete;

                ///@}


                /// \name Crtp-required methods.
                ///@{


               /*!
                * \brief Initialise the problem.
                *
                * This initialisation includes the resolution of the static problem.
                * \copydoc doxygen_hide_morefem_data_param
                */
               void SupplInitialize(const morefem_data_type& morefem_data);


               //! Manage time iteration.
               void Forward();

               /*!
                * \brief Additional operations to finalize a dynamic step.
                *
                * Base class already update the time for next time iterations.
                */
               void SupplFinalizeStep();


               /*!
                * \brief Initialise a dynamic step.
                *
                */
               void SupplFinalize();

                //! Output directory.
                const std::string& GetOutputDirectory() const noexcept;



           private:


               //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
               bool SupplHasFinishedConditions() const;


               /*!
                * \brief Part of InitializedStep() specific to Elastic model.
                *
                * As there are none, the body of this method is empty.
                */
               void SupplInitializeStep();


               ///@}

            private:

                //! Output directory.
                const std::string output_directory_;

            };


        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM


# include "Test/Geometry/DomainListInCoords/Model.hxx"


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_
