# Build and intermediate build directories.
# Subdirectories for each type of compilation will be created there.
BUILD_DIR = ''
INTERMEDIATE_BUILD_DIR =  ''

# Choose C and C++ compilers. You might also specifies here clang static analyzer (paths to ccc-analyzer and c++-analyzer respectively) to perform static analysis of the code.
COMPILER= 'clang'
COMPILER_DIRECTORY = 'clang'
CC = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpicc'
CXX = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpic++'

# Choose either 'debug' or 'release'.
MODE='debug'

# Choose either 'static' or 'shared'.
LIBRARY_TYPE='static'

# Whether specific macros should be activated or not.

    # If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.
MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE = False

    # If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored.
MOREFEM_EXTENDED_TIME_KEEP = False

    # If true, there are additional checks that no nan and inf appears in the code. Even if False, solver always check for the validity of its solution (if a nan or an inf is present the SolveLinear() or SolveNonLinear() operation throws with a dedicated Petsc error). Advised in debug mode and up to you in release mode.
MOREFEM_CHECK_NAN_AND_INF = False

# OpenMPI libary.
OPEN_MPI_INCL_DIR='/Users/Shared/LibraryVersions/clang/Openmpi/include'
OPEN_MPI_LIB_DIR='/Users/Shared/LibraryVersions/clang/Openmpi/lib'

# BLAS Library.
BLAS_CUSTOM_LINKER=True
BLAS_LIB_DIR=None
BLAS_LIB='-framework Accelerate'

# Petsc library.
PETSC_GENERAL_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/include'
PETSC_DEBUG_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/debug/include'
PETSC_RELEASE_INCL_DIR='/Users/Shared/LibraryVersions/clang/Petsc/release/include'

PETSC_DEBUG_LIB_DIR='/Users/Shared/LibraryVersions/clang/Petsc/debug/lib'
PETSC_RELEASE_LIB_DIR='/Users/Shared/LibraryVersions/clang/Petsc/release/lib'

# Parmetis library.
PARMETIS_INCL_DIR='/Users/Shared/LibraryVersions/clang/Parmetis/include'
PARMETIS_LIB_DIR='/Users/Shared/LibraryVersions/clang/Parmetis/lib'

# Lua library.
LUA_INCL_DIR='/Users/Shared/LibraryVersions/clang/Lua/include'
LUA_LIB_DIR='/Users/Shared/LibraryVersions/clang/Lua/lib'

# Boost library
BOOST_INCL_DIR='/Users/Shared/LibraryVersions/clang/Boost/include'
BOOST_LIB_DIR='/Users/Shared/LibraryVersions/clang/Boost/lib'

# If you want to couple MoReFEM with Phillips library. None in most of the cases!
PHILLIPS_LINKER=False
PHILLIPS_DIR=None
