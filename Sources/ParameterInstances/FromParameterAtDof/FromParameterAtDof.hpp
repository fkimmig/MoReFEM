///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 16:28:39 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HPP_

# include <memory>

# include "Parameters/ParameterAtDof.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hpp"
# include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        /*!
         * \brief Define a convertor from a \a ParameterAtDof to a \a ParameterAtQuadraturePoint.
         *
         * \a ParameterAtDof is a bit overkill for a value defined at dofs that never vary: each time the value is queried
         * at a \a QuadraturePoint, the value is recomputed on the fly from the \a GlobalVector, which is much more
         * costly than what can achieve a \a ParameterAtQuadraturePoint, which stores the values directly.
         *
         * Current function (static method if you want to nitpick, just to ease friendship declaration) aims to provide
         * such a convertor.
         *
         * \copydetails doxygen_hide_at_dof_policy_tparam
         * \tparam TimeDependencyT Policy in charge of time dependency.
         *
         * \internal This is a struct which static function to ease friendship declaration.
         * * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
         */

        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
            unsigned int NfeltSpaceT = 1
        >
        struct FromParameterAtDof
        {

            static_assert(TypeT != ParameterNS::Type::matrix, "Matricial parameter doesn't make sense for AtDof policy.");


            /*!
             * \brief
             *
             * \tparam StringT Any string type that might be interpreted directly into a std::string. Universal reference
             * tick is used here so it might be a pointer, a reference or a plain chain of characters.
             *
             * \param[in] name Name given to the parameter, just for output logs.
             * \param[in] domain Domain upon which both \a Parameter are defined.
             * \param[in] quadrature_rule_per_topology \a QuadratureRule to use for each topology. Make sure you cover
             * all the cases met in the \a FEltSpace underlying \a param_at_dof.
             * \copydetails doxygen_hide_time_manager_arg
             * \param[in] param_at_dof \a ParameterAtDof to be converted into a \a ParameterAtQuadraturePoint.
             *
             * \return \a ParameterAtQuadraturePoint which contain same data as the source \a ParameterAtDof.
             */
            template<class StringT>
            static typename ParameterAtQuadraturePoint<TypeT, TimeDependencyT>::unique_ptr
            Perform(StringT&& name,
                    const Domain& domain,
                    const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                    const TimeManager& time_manager,
                    const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpaceT>& param_at_dof);


        };




    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HPP_
