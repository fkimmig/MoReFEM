target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CopyValues.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/CopyValues.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FromAtDofToAtQuadPt.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FromAtDofToAtQuadPt.hxx" / 
)

