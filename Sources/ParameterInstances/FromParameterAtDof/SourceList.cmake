target_sources(${MOREFEM_PARAM_INSTANCES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FromParameterAtDof.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FromParameterAtDof.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
