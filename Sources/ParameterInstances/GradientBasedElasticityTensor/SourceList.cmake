target_sources(${MOREFEM_PARAM_INSTANCES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Configuration.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Configuration.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradientBasedElasticityTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/GradientBasedElasticityTensor.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
