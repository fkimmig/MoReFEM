///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 15:35:41 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace ParameterNS
        {
            
            
            namespace // anonymous
            {
                
                LocalMatrix Gradient2Strain_1D()
                {
                    LocalMatrix ret(1, 1);
                    ret.Zero();
                    
                    ret(0, 0) = 1.;

                    return ret;
                }
                
                
                LocalMatrix Gradient2Strain_2D()
                {
                    LocalMatrix ret(3, 4);
                    ret.Zero();
                    
                    ret(0, 0) = 1.;
                    ret(1, 3) = 1.;
                    ret(2, 1) = 1.;
                    ret(2, 2) = 1.;
                    
                    return ret;
                }
                
                
                LocalMatrix Gradient2Strain_3D()
                {
                    LocalMatrix ret(6, 9);
                    ret.Zero();
                    
                    ret(0, 0) = 1.;
                    ret(1, 4) = 1.;
                    ret(2, 8) = 1.;
                    ret(3, 1) = 1.;
                    ret(3, 3) = 1.;
                    ret(4, 5) = 1.;
                    ret(4, 7) = 1.;
                    ret(5, 2) = 1.;
                    ret(5, 6) = 1.;
                    
                    return ret;
                }
                
                
            } // namespace anonymous
            
            
            template<>
            const LocalMatrix& Gradient2Strain<1>()
            {
                static auto ret = Gradient2Strain_1D();
                return ret;
            };
            
            template<>
            const LocalMatrix& Gradient2Strain<2>()
            {
                static auto ret = Gradient2Strain_2D();
                return ret;
            };
            
            
            template<>
            const LocalMatrix& Gradient2Strain<3>()
            {
                static auto ret = Gradient2Strain_3D();
                return ret;
            };
            
            
            
        } // namespace ParameterNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
