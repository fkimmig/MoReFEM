target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGradientBasedElasticityTensor.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Gradient2Strain.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGradientBasedElasticityTensor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ComputeGradientBasedElasticityTensor.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Configuration.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Gradient2Strain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Gradient2Strain.hxx" / 
)

