target_sources(${MOREFEM_PARAM_INSTANCES}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FiberList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FiberList.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
