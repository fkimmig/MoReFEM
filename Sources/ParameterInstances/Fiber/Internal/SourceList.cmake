target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Traits.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FiberListManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FiberListManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FillGlobalVector.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FillGlobalVector.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReadFiberFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReadFiberFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Traits.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Traits.hxx" / 
)

