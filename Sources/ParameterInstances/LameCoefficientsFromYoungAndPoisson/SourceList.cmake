target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/LameLambda.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameMu.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/LameLambda.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameLambda.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameMu.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameMu.hxx" / 
)

