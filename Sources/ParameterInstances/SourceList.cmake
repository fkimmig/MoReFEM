target_sources(${MOREFEM_PARAM_INSTANCES}

	PUBLIC
)

include(${CMAKE_CURRENT_LIST_DIR}/ScalarParameterFromFile/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LameCoefficientsFromYoungAndPoisson/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ThreeDimensionalParameter/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FromParameterAtDof/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/GradientBasedElasticityTensor/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fiber/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Compound/SourceList.cmake)
