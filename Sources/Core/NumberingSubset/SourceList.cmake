target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
