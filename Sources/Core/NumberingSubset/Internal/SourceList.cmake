target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FetchFunction.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FetchFunction.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/FindFunctor.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FindFunctor.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.hxx" / 
)

