///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 10:26:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HXX_
# define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            template<class T>
            inline std::enable_if_t
            <
                Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                const NumberingSubset&
            >
            FetchNumberingSubset(const T& object) noexcept
            {
                assert(!(!object));
                return FetchNumberingSubset(*object);
            }


            template<class T>
            inline std::enable_if_t
            <
                !(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                const NumberingSubset&
            >
            FetchNumberingSubset(const T& object) noexcept
            {
                return object.GetNumberingSubset();
            }


            template<class T>
            std::enable_if_t
            <
                Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                std::pair<const NumberingSubset&, const NumberingSubset&>
            >
            FetchNumberingSubsetPair(const T& object) noexcept
            {
                assert(!(!object));
                return FetchNumberingSubsetPair(*object);
            }



            template<class T>
            std::enable_if_t
            <
                !(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                std::pair<const NumberingSubset&, const NumberingSubset&>
            >
            FetchNumberingSubsetPair(const T& object) noexcept
            {
                return std::make_pair(std::cref(object.GetRowNumberingSubset()),
                                      std::cref(object.GetColNumberingSubset()));
            }



        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_FETCH_FUNCTION_HXX_
