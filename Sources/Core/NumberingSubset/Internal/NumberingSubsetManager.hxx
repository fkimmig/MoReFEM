///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_
# define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace NumberingSubsetNS
        {


            template<class SectionT>
            void NumberingSubsetManager::Create(const SectionT& section)
            {
                namespace ipl = Internal::InputParameterListNS;

                Create(section.GetUniqueId(),
                       ipl::ExtractParameter<typename SectionT::DoMoveMesh>(section));
            }


            inline const NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetList() const
            {
                return list_;
            }


            inline NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetNonCstList()
            {
                return const_cast<NumberingSubset::vector_const_shared_ptr&>(GetList());
            }


            inline const NumberingSubset& NumberingSubsetManager::GetNumberingSubset(unsigned int unique_id) const
            {
                return *(GetNumberingSubsetPtr(unique_id));
            }


            inline NumberingSubset::const_shared_ptr NumberingSubsetManager
            ::GetNumberingSubsetPtr(unsigned int unique_id) const
            {
                auto it = GetIterator(unique_id);
                assert(it != GetList().cend());
                assert(!(!(*it)));
                return *it;
            }


        } // namespace NumberingSubsetNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_
