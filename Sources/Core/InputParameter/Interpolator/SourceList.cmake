target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InitVertexMatching.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitVertexMatching.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
