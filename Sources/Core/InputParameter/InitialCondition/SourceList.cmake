target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
