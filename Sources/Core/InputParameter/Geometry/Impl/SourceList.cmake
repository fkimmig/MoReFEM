target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Domain.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.hpp" / 
)

