///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_DOMAIN_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_DOMAIN_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template<unsigned int IndexT>
        constexpr unsigned int Domain<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template<unsigned int IndexT>
        const std::string& Domain<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("Domain", IndexT);
            return ret;
        };


        template<unsigned int IndexT>
        typename Domain<IndexT>::section_content_type& Domain<IndexT>::GetNonCstSectionContent() noexcept
        {
            return section_content_;
        }


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_DOMAIN_HXX_
