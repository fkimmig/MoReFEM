target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationFile.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterpolationFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
