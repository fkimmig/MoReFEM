///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_

# include <string>

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Geometry/Impl/PseudoNormals.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {

            /*!
             * \brief Common base class from which all InputParameter::PseudoNormals should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct PseudoNormals
            { };



        } // namespace BaseNS




        /*!
         * \brief Holds information related to the input parameter PseudoNormals.
         *
         * \tparam IndexT Several PseudoNormals can be present in the input parameter file, provided they each
         * use a different value for this template parameter. For instance:
         *
         * \code
         * PseudoNormals1 = {
         * mesh_index = 1
         * ...
         * }
         *
         * PseudoNormals2 = {
         * mesh_index = 2
         * ...
         * }
         * \endcode
         */
        template<unsigned int IndexT>
        struct PseudoNormals
        : public Crtp::Section<PseudoNormals<IndexT>, NoEnclosingSection>,
        public BaseNS::PseudoNormals
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'PseudoNormals1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = PseudoNormals<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Indicates the mesh upon which the pseudo-normals are defined (if any).
             */
            struct MeshIndex : public Crtp::InputParameter<MeshIndex, self, unsigned int>,
                               public Impl::PseudoNormalsNS::MeshIndexImpl
            { };


            /*!
             * \brief Indicates the domain upon which the pseudo-normals are defined (if any).
             */
            struct DomainIndexList : public Crtp::InputParameter<DomainIndexList, self, std::vector<unsigned int>>,
            public Impl::PseudoNormalsNS::DomainIndexListImpl
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                MeshIndex,
                DomainIndexList
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct PseudoNormals


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Geometry/PseudoNormals.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_
