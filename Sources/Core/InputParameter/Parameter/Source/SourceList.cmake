target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Pressure.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Pressure.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Pressure.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/RectangularSourceTimeParameter.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RectangularSourceTimeParameter.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ScalarTransientSource.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ScalarTransientSource.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/VectorialTransientSource.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/VectorialTransientSource.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
