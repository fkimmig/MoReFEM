///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 20 Oct 2015 11:54:10 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

# include "Utilities/Containers/Print.hpp"

# include "Utilities/String/EmptyString.hpp"

# include "Core/InputParameter/Parameter/Source/Impl/RectangularSourceTimeParameter.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        namespace Impl
        {
            
            
            namespace RectangularSourceTimeParameterNS
            {
                
                
                const std::string& InitialTimeOfActivationList::NameInFile()
                {
                    static std::string ret("initial_time_of_activation");
                    return ret;
                }
                
                
                const std::string& InitialTimeOfActivationList::Description()
                {
                    static std::string ret("Value of the initial time to activate the source.");
                    return ret;
                }
                
                
                const std::string& InitialTimeOfActivationList::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                
                const std::string& InitialTimeOfActivationList::DefaultValue()
                {
                    static std::string ret("0.");
                    return ret;
                }
                
                
                
                const std::string& FinalTimeOfActivationList::NameInFile()
                {
                    static std::string ret("final_time_of_activation");
                    return ret;
                }
                
                
                const std::string& FinalTimeOfActivationList::Description()
                {
                    static std::string ret("Value of the final time to activate the source.");
                    return ret;
                }
                
                
                const std::string& FinalTimeOfActivationList::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                
                const std::string& FinalTimeOfActivationList::DefaultValue()
                {
                    static std::string ret("0.");
                    return ret;
                }
                
                
            } // namespace RectangularSourceTimeParameterNS
            
            
        } // namespace Impl
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
