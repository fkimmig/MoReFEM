target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/RectangularSourceTimeParameter.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/RectangularSourceTimeParameter.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TransientSource.hpp" / 
)

