///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 3 Dec 2015 14:11:41 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

# include "Core/InputParameter/Parameter/Source/Pressure.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        namespace Source
        {
        
        
            const std::string& StaticPressure::GetName()
            {
                static std::string ret("StaticPressure");
                return ret;
            }
            
            
            const std::string& StaticPressure::Scalar::Constraint()
            {
                return Utilities::EmptyString();
            }
            
        
        } // namespace Source
    
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
