///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/Source/Impl/TransientSource.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct ScalarTransientSource
        : public Crtp::Section<ScalarTransientSource<IndexT>, NoEnclosingSection>
        {


            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'TransientSource1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = ScalarTransientSource<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the Poisson coefficient (through a scalar, a function, etc...)
             */
            struct Nature
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Nature, self, TransientSourceNS::ScalarNature::storage_type>,
            public TransientSourceNS::ScalarNature
            { };


            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<Scalar, self, double>,
            public TransientSourceNS::Scalar
            { };


            /*!
             * \brief Function that determines value for the transient source along component x. Irrelevant if nature is not lua_function.
             */
            struct LuaFunction
            : public Crtp::InputParameter<LuaFunction, self, TransientSourceNS::LuaFunction::storage_type>,
            public TransientSourceNS::LuaFunction
            { };


            /*!
             * \brief Piecewise Constant domain index.
             */
            struct PiecewiseConstantByDomainId
            : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
            public Impl::PiecewiseConstantByDomainId
            { };


            /*!
             * \brief Piecewise Constant value by domain.
             */
            struct PiecewiseConstantByDomainValue
            : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
            public Impl::PiecewiseConstantByDomainValue
            { };



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Nature,
                Scalar,
                LuaFunction,
                PiecewiseConstantByDomainId,
                PiecewiseConstantByDomainValue
            >;



        private:

            //! Content of the section.
            section_content_type section_content_;


        }; // struct TransientSource


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HPP_
