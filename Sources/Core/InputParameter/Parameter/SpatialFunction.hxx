///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 2 Jun 2015 15:29:57 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template<CoordsType CoordsTypeT>
        inline CoordsType SpatialFunction<CoordsTypeT>::GetCoordsType()
        {
            return CoordsTypeT;
        }




    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SPATIAL_FUNCTION_HXX_
