target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ViscoelasticBoundaryCondition.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ViscoelasticBoundaryCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ViscoelasticBoundaryCondition.hxx" / 
)

