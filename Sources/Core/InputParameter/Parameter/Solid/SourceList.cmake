target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/C_i_Mu_i.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/HyperelasticBulk.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Kappa1.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Kappa2.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameLambda.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/LameMu.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PoissonRatio.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Solid.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Viscosity.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/YoungModulus.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Solid.hpp" / 
)

