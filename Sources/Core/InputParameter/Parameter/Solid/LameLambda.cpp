///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputParameter/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{
    
    
    namespace InputParameter
    {
        
        
        const std::string& Solid::LameLambda::GetName()
        {
            static std::string ret("LameLambda");
            return ret;
        }
        
        
    } // namespace InputParameter
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
