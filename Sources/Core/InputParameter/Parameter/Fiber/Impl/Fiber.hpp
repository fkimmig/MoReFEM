///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_IMPL_x_FIBER_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_IMPL_x_FIBER_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace FiberNS
        {


            /*!
             * \brief Ensight file from which data are read.
             *
             */
            struct EnsightFile
            {

                //! Convenient alias.
                using storage_type = std::string;


                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                 * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                 */
                static const std::string& DefaultValue();
            };




            /*!
             * \brief Index of the geometric mesh region.
             *
             */
            struct DomainIndex
            {


                //! Convenient alias.
                using storage_type = unsigned int;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                 * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Index of the finite element space.
             *
             */
            struct FEltSpaceIndex
            {


                //! Convenient alias.
                using storage_type = unsigned int;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                 * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                 */
                static const std::string& DefaultValue();
            };


            /*!
             * \brief Name of the unknown considered.
             *
             */
            struct UnknownName
            {


                //! Convenient alias.
                using storage_type = std::string;

                //! Name of the input parameter in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input parameter.
                static const std::string& Description();

                /*!
                 * \return Constraint to fulfill.
                 *
                 * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                 *
                 * An age should be greater than 0 and less than, say, 150. It is possible
                 * to check it with a logical expression (written in Lua). The expression
                 * should be written with 'v' being the variable to be checked.
                 * \a constraint = "v >= 0 and v < 150"
                 *
                 * It is possible to check whether a variable is in a set of acceptable
                 * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                 * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                 *
                 * If a vector is retrieved, the constraint must be satisfied on every
                 * element of the vector.
                 */
                static const std::string& Constraint();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no value has been given
                 * in the input parameter file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                 */
                static const std::string& DefaultValue();
            };



        } // namespace FiberNS


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_IMPL_x_FIBER_HPP_
