target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Fiber.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Fiber.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
