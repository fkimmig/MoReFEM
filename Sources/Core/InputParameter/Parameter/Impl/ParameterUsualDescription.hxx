///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace Impl
        {




        } // namespace Impl


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_IMPL_x_PARAMETER_USUAL_DESCRIPTION_HXX_
