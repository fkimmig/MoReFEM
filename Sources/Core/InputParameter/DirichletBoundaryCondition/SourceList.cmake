target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
