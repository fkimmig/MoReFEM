target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.hpp" / 
)

