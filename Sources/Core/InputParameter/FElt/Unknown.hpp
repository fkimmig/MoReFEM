///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 17:15:58 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/FElt/Impl/Unknown.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputParameter::Unknown should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct Unknown
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct Unknown
        : public BaseNS::Unknown,
        public Crtp::Section<Unknown<IndexT>, NoEnclosingSection>
        {


            //! Convenient alias.
            using self = Unknown<IndexT>;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! \copydoc doxygen_hide_alias_self intended to be used in classes enclosed in current class.
            using enclosing_type = self;


            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'Unknown1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();


            /*!
             * \brief Name of the unknown.
             */
            struct Name : public Crtp::InputParameter<Name, self, std::string>,
                          public Impl::UnknownNS::Name
            { };


            /*!
             * \brief Nature of the unknown.
             */
            struct Nature : public Crtp::InputParameter<Nature, self, std::string>,
                            public Impl::UnknownNS::Nature
            { };



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Name,
                Nature
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;



        }; // struct Unknown


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/FElt/Unknown.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_UNKNOWN_HPP_
