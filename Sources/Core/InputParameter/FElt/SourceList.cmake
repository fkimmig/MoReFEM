target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FEltSpace.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubset.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Unknown.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
