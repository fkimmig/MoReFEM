target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.hxx" / 
)

