target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InitialConditionGate.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Result.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InitialConditionGate.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OutputDeformedMesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/OutputDeformedMesh.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Result.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/InitialCondition/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Reaction/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FElt/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Interpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solver/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Parameter/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Geometry/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeManager/SourceList.cmake)
