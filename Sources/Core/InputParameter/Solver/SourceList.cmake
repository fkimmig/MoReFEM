target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ConvergenceTest.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ConvergenceTest.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/ConvergenceTest.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/Petsc.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Petsc.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
