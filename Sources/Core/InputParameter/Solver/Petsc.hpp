///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 15:17:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_SOLVER_x_PETSC_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_SOLVER_x_PETSC_HPP_

# include <vector>
# include <string>

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Solver/Impl/Petsc.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputParameter::DirichletBoundaryCondition should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct Petsc
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct Petsc
        : public BaseNS::Petsc,
        public Crtp::Section<Petsc<IndexT>, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'DirichletBoundaryCondition' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = Petsc<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            //! Holds information related to the input parameter Petsc::Solver.
            struct Solver : public Crtp::InputParameter<Solver, self, std::string>,
            public Impl::PetscNS::Solver
            { };


            //! Holds information related to the input parameter Petsc::gmresRestart.
            struct GmresRestart : public Crtp::InputParameter<GmresRestart, self, unsigned int>,
            public Impl::PetscNS::GmresRestart
            { };


            //! Holds information related to the input parameter Petsc::preconditioner.
            struct Preconditioner : public Crtp::InputParameter<Preconditioner, self, std::string>,
            public Impl::PetscNS::Preconditioner
            { };


            //! Holds information related to the input parameter Petsc::relativeTolerance.
            struct RelativeTolerance : public Crtp::InputParameter<RelativeTolerance, self, double>,
            public Impl::PetscNS::RelativeTolerance
            { };


            //! Holds information related to the input parameter Petsc::absoluteTolerance.
            struct AbsoluteTolerance : public Crtp::InputParameter<AbsoluteTolerance, self, double>,
            public Impl::PetscNS::AbsoluteTolerance
            { };


            //! Holds information related to the input parameter Petsc::stepSizeTolerance.
            struct StepSizeTolerance : public Crtp::InputParameter<StepSizeTolerance, self, double>,
            public Impl::PetscNS::StepSizeTolerance
            { };


            //! Holds information related to the input parameter Petsc::maxIteration.
            struct MaxIteration : public Crtp::InputParameter<MaxIteration, self, unsigned int>,
            public Impl::PetscNS::MaxIteration
            { };



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                AbsoluteTolerance,
                GmresRestart,
                MaxIteration,
                Preconditioner,
                RelativeTolerance,
                StepSizeTolerance,
                Solver
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;




        }; // struct Petsc


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Solver/Petsc.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_SOLVER_x_PETSC_HPP_
