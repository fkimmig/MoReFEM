target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/TimeManager.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeManagerInstance.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/TimeManager.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeManager.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeManagerInstance.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeManagerInstance.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
