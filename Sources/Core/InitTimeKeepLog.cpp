///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Jan 2015 14:19:22 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "InitTimeKeepLog.hpp"


namespace MoReFEM
{


    void InitTimeKeepLog(const Wrappers::Mpi& mpi,
                         const std::string& result_directory)
    {
        std::string time_log_filename;

        {
            std::ostringstream oconv;
            oconv << result_directory << "/time_log." << mpi.template GetRank<int>() << ".hhdata";
            time_log_filename = oconv.str();
        }

        std::ofstream out;

        FilesystemNS::File::Create(out, time_log_filename, __FILE__, __LINE__);

        TimeKeep::CreateOrGetInstance(std::move(out));
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
