target_sources(${MOREFEM_CORE}

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hxx" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
