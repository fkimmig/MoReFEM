target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InitTimeKeepLog.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InitTimeKeepLog.hpp" / 
)

include(${CMAKE_CURRENT_LIST_DIR}/MoReFEMData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/NumberingSubset/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/EnsightCaseReader/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solver/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LinearAlgebra/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputParameterData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputParameter/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeManager/SourceList.cmake)
