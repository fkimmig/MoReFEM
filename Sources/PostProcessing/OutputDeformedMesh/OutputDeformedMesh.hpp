///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 11 Apr 2017 11:30:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_
# define MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_

# include <memory>
# include <vector>

# include "Geometry/Mesh/GeometricMeshRegion.hpp"
# include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"
# include "Geometry/Domain/Domain.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace PostProcessingNS
    {

        /*!
         * \brief Generates a deformed mesh form a domain of the mesh.
         *
         */
        class OutputDeformedMesh
        {

        public:

            //! Alias to self.
            using self = OutputDeformedMesh;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to vector of unique pointers.
            using vector_unique_ptr = std::vector<unique_ptr> ;

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit OutputDeformedMesh(const std::string& data_directory,
                                        const std::string& unknown_id,
                                        const unsigned int numbering_subset_id,
                                        Internal::MeshNS::GeometricMeshRegionManager& mesh_manager,
                                        const GeometricMeshRegion& mesh,
                                        MeshNS::Format format,
                                        const Domain& domain,
                                        const unsigned int output_mesh_index,
                                        const std::string& output_name,
                                        const std::string& output_format,
                                        const double output_space_unit,
                                        const unsigned int output_offset,
                                        const unsigned int output_frequence);

            //! Destructor.
            ~OutputDeformedMesh() = default;

            //! Copy constructor.
            OutputDeformedMesh(const OutputDeformedMesh&) = delete;

            //! Move constructor.
            OutputDeformedMesh(OutputDeformedMesh&&) = delete;

            //! Copy affectation.
            OutputDeformedMesh& operator=(const OutputDeformedMesh&) = delete;

            //! Move affectation.
            OutputDeformedMesh& operator=(OutputDeformedMesh&&) = delete;

            ///@}

        private:



        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/OutputDeformedMesh/OutputDeformedMesh.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_
