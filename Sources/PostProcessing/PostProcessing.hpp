///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HPP_
# define MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HPP_

# include <string>

# include "Geometry/Mesh/GeometricMeshRegion.hpp"

# include "PostProcessing/File/InterfaceFile.hpp"
# include "PostProcessing/File/UnknownInformationFile.hpp"
# include "PostProcessing/File/TimeIterationFile.hpp"
# include "PostProcessing/File/DofInformationFile.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace PostProcessingNS
    {


        /*!
         * \brief This class is in charge of loading the output files of an MoReFEM simulation and interpret
         * their content, so that output files in any format can be written.
         */
        class PostProcessing final
        {
        public:

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const PostProcessing>;


        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] data_directory This directory is the one used as output directory by MoReFEM. It must
             * contain the following files:
             * - input_parameter_file.lua A copy of the Lua file used in the simulation.
             * - dof_information_proc_0.hhdata Informations about all the dofs, required to interpret the raw solution vectors.
             * - solution_****_proc#.hhdata Raw solution vector on processor '#' at time iteration '****'.
             * - time_iteration.hhdata Description of each time iteration and format of the related solution files.
             * - unknowns.hhdata List of all unknowns considered.
             * - interfaces.hhdata For all geometric interfaces, the list of Coords that delimit them.
             * - mpi.hhdata The number of processor involved in the simulation (to determine the files to retrieve).
             *
             * \param[in] numbering_subset_id_list List of \a NumberingSubset unique identifiers to consider.
             * \param[in] geometric_mesh_region \a GeometricMeshRegion for which the post-processing is performed.
             *
            */
            //! Same as above except the GeometricMeshRegion has already been read earlier.
            explicit PostProcessing(const std::string& data_directory,
                                    const std::vector<unsigned int>& numbering_subset_id_list,
                                    const GeometricMeshRegion& geometric_mesh_region);


            //! Destructor.
            ~PostProcessing() = default;

            //! Copy constructor.
            PostProcessing(const PostProcessing&) = delete;

            //! Move constructor.
            PostProcessing(PostProcessing&&) = delete;

            //! Affectation.
            PostProcessing& operator=(const PostProcessing&) = delete;

            //! Move affectation.
            PostProcessing& operator=(PostProcessing&&) = delete;

            ///@}


            /*!
             * \brief Load in memory the solution vector for a given time iteration and a given processor.
             *
             * \param[in] file File to load.
             *
             * \return Solution vector.
             */
            std::vector<double> LoadVector(const std::string& file) const;

            //! Get access to the informations about unknowns.
            const Data::UnknownInformation::vector_const_shared_ptr& GetExtendedUnknownList() const;

            //! Get access to the informations about time iterations.
            const Data::TimeIteration::vector_const_unique_ptr& GetTimeIterationList() const;

            //! Get access to the geometric mesh region.
            const GeometricMeshRegion& GetGeometricMeshRegion() const noexcept;

            //! Get the number of processors.
            std::size_t Nprocessor() const noexcept;

            //! Get the dof informations related to \a processor.
            const Data::DofInformation::vector_const_shared_ptr&
            GetDofInformationList(unsigned int numbering_subset_id,
                                  std::size_t processor) const;

            //! Constant accessor to the data included in the interface file.
            const InterfaceFile& GetInterfaceData() const noexcept;

            //! Number of dofs.
            unsigned int Ndof() const noexcept;

            //! Get the dof information related to a given processor and a given numbering subset.
            const DofInformationFile& GetDofFile(unsigned int numbering_subset_id,
                                                 std::size_t processor) const;


        private:

            //! Get access to the directory used as output directory by MoReFEM.
            const std::string& GetDataDirectory() const noexcept;

            //! Get access to a subdirectory matching the \a numbering_subset_id.
            std::string GetNumberingSubsetDirectory(unsigned int numbering_subset_id) const;

            //! Read the interface file and store its content into the class.
            void ReadInterfaceFile();

            //! Read the unknown file and store its content into the class.
            void ReadUnknownFile();

            //! Read the time iteration file and store its content into the class.
            void ReadTimeIterationFile();

            //! Read how many processors were involved in the simulation.
            void ReadNprocessor();

            //! Read the dof files (one per processor) and store its content into the class.
            void ReadDofFiles();

            //! Get the unknown file data.
            const UnknownInformationFile& GetUnknownFile() const noexcept;

            //! Get the time iteration file data.
            const TimeIterationFile& GetTimeIterationFile() const noexcept;

            //! List of \a NumberingSubset for which we want to convert outputs into Ensight format.
            const std::vector<unsigned int>& GetNumberingSubsetIdList() const noexcept;


        private:

            //! Directory used as output directory by MoReFEM.
            const std::string& data_directory_;

            //! Geometric mesh region considered in the problem. \todo #289 Extend to several meshes!
            const GeometricMeshRegion& geometric_mesh_region_;

            //! Number of processors used in the simulation.
            std::size_t Nprocessor_;

            //! Interface file data.
            InterfaceFile::const_unique_ptr interface_file_;

            //! Unknown file data.
            UnknownInformationFile::const_unique_ptr unknown_file_;

            //! Time iteration file data.
            TimeIterationFile::const_unique_ptr time_iteration_file_;

            //! List of \a NumberingSubset for which we want to convert outputs into Ensight format.
            const std::vector<unsigned int> numbering_subset_id_list_;

            /*!
             * \brief Store here all the informations that characterizes a Dof in a given numbering subset.
             *
             * . Key: numbering subset unique id.
             * . Value: Dof file data (one file per processor).
             */
            std::map<unsigned int, DofInformationFile::vector_unique_ptr> dof_file_per_processor_list_per_numbering_subset_;

            //! Total number of dofs.
            unsigned int Ndof_ = 0u;

        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/PostProcessing.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HPP_
