///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:53:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_DOF_INFORMATION_HXX_
# define MOREFEM_x_POST_PROCESSING_x_DATA_x_DOF_INFORMATION_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            inline unsigned int DofInformation::GetProgramWiseIndex() const
            {
                return program_wise_index_;
            }


            inline unsigned int DofInformation::GetProcessorWiseIndex() const
            {
                return processor_wise_index_;
            }


            inline InterfaceNS::Nature DofInformation::GetInterfaceNature() const
            {
                return GetInterface().GetNature();
            }


            inline const Data::Interface& DofInformation::GetInterface() const noexcept
            {
                assert(interface_ != nullptr);
                return *interface_;
            }


            inline unsigned int DofInformation::GetInterfaceIndex() const
            {
                return GetInterface().GetIndex();
            }


            inline const std::string& DofInformation::GetUnknown() const
            {
                return unknown_name_;
            }


            inline unsigned int DofInformation::GetUnknownComponent() const
            {
                return unknown_component_;
            }


            inline const std::string& DofInformation::GetShapeFunctionLabel() const
            {
                return shape_function_label_;
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_DOF_INFORMATION_HXX_
