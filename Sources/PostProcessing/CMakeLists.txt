set(MOREFEM_POST_PROCESSING MoReFEMPostProcessing_lib)

add_library(MoReFEMPostProcessing_lib ${LIBRARY_TYPE} "")
target_link_libraries(MoReFEMPostProcessing_lib 
                      ${MOREFEM_MODEL})
                      
target_sources(MoReFEMPostProcessing_lib

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/PostProcessing.hxx" / 
)



include(${CMAKE_CURRENT_LIST_DIR}/ConvertLinearAlgebra/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/File/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/OutputFormat/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Data/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/RefineMeshQuadranglesSpectral/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/OutputDeformedMesh/CMakeLists.txt)


add_executable(MoReFEMRefineMeshOutput ${CMAKE_CURRENT_LIST_DIR}/main_refined_mesh_output.cpp)
target_link_libraries(MoReFEMRefineMeshOutput 
                      MoReFEMPostProcessing_lib)


add_executable(MoReFEMConvertMatrixToFreefem ${CMAKE_CURRENT_LIST_DIR}/main_convert_matrix_to_freefem.cpp)
target_link_libraries(MoReFEMConvertMatrixToFreefem 
                      MoReFEMPostProcessing_lib)


morefem_install(MoReFEMRefineMeshOutput MoReFEMConvertMatrixToFreefem MoReFEMPostProcessing_lib)
