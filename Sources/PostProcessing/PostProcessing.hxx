///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_
# define MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {



        inline const Data::UnknownInformation::vector_const_shared_ptr& PostProcessing
        ::GetExtendedUnknownList() const
        {
            return GetUnknownFile().GetExtendedUnknownList();
        }


        inline const Data::TimeIteration::vector_const_unique_ptr& PostProcessing::GetTimeIterationList() const
        {
            return GetTimeIterationFile().GetTimeIterationList();
        }


        inline const Data::DofInformation::vector_const_shared_ptr& PostProcessing
        ::GetDofInformationList(const unsigned int numbering_subset_id,
                                const std::size_t processor) const
        {
            return GetDofFile(numbering_subset_id, processor).GetDofList();
        }


        inline const std::string& PostProcessing::GetDataDirectory() const noexcept
        {
            return data_directory_;
        }


        inline const GeometricMeshRegion& PostProcessing::GetGeometricMeshRegion() const noexcept
        {
            return geometric_mesh_region_;
        }


        inline std::size_t PostProcessing::Nprocessor() const noexcept
        {
            assert(Nprocessor_ != NumericNS::UninitializedIndex<std::size_t>());
            return Nprocessor_;
        }


        inline const InterfaceFile& PostProcessing::GetInterfaceData() const noexcept
        {
            assert(!(!interface_file_));
            return *interface_file_;
        }


        inline const UnknownInformationFile& PostProcessing::GetUnknownFile() const noexcept
        {
            assert(!(!unknown_file_));
            return *unknown_file_;
        }


        inline const TimeIterationFile& PostProcessing::GetTimeIterationFile() const noexcept
        {
            assert(!(!time_iteration_file_));
            return *time_iteration_file_;
        }


        inline unsigned int PostProcessing::Ndof() const noexcept
        {
            return Ndof_;
        }


        inline const std::vector<unsigned int>& PostProcessing::GetNumberingSubsetIdList() const noexcept
        {
            return numbering_subset_id_list_;
        }



    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_POST_PROCESSING_HXX_
