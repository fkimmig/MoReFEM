target_sources(${MOREFEM_POST_PROCESSING}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Model.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefineMesh.cpp" / 

	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefineMesh.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/RefineMesh.hxx" / 
)

