///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HXX_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        inline const Data::UnknownInformation::vector_const_shared_ptr& UnknownInformationFile
        ::GetExtendedUnknownList() const
        {
            return unknown_list_;
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_UNKNOWN_INFORMATION_FILE_HXX_
