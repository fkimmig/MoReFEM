///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 May 2016 11:23:22 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_
# define MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace FreefemNS
        {


            inline const Wrappers::Petsc::Matrix& MatrixConversion::GetOriginalMatrix() const noexcept
            {
                return original_matrix_;
            }


            inline const PostProcessing& MatrixConversion::GetPostProcessingData() const noexcept
            {
                assert(!(!post_processing_data_));
                return *post_processing_data_;
            }


            inline const std::vector<std::vector<unsigned int>>& MatrixConversion
            ::GetFreefemDofNumberingPerGeomElt() const noexcept
            {
                return freefem_dof_numbering_per_geom_elt_;
            }


            inline const std::vector<unsigned int>& MatrixConversion
            ::GetFreefemDofList(const unsigned int highest_dim_geom_elt_index) const noexcept
            {
                decltype(auto) freefem_dof_numbering_per_geom_elt = GetFreefemDofNumberingPerGeomElt();

                assert(highest_dim_geom_elt_index < freefem_dof_numbering_per_geom_elt.size());

                return freefem_dof_numbering_per_geom_elt[highest_dim_geom_elt_index];
            }


            inline const std::vector<unsigned int>& MatrixConversion::GetDofMapping() const noexcept
            {
                return dof_mapping_;
            }


            inline void MatrixConversion::SetDofMapping(std::vector<unsigned int>&& value)
            {
                assert(dof_mapping_.empty());
                dof_mapping_ = std::move(value);
            }


            inline const std::string& MatrixConversion::GetOutputDirectory() const noexcept
            {
                assert(FilesystemNS::Folder::DoExist(output_directory_));
                return output_directory_;
            }


        } // namespace FreefemNS


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_CONVERT_LINEAR_ALGEBRA_x_FREEFEM_x_MATRIX_CONVERSION_HXX_
