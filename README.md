A HPC finite element library developed for the implementation of the models of M3DISIM Inria team.



# Installation

## Requirements

### Third-party libraries

MoReFEM relies on several third party libraries:

- An optimized Blas library. On macos you may use the Accelerate framework; Openblas has also been successfully tested.
- Lua 5.1, which is required by Ops library (more on that one below).
- Boost C++ library; Boost is present only while waiting for full support of filesystem module in libstdc++ and libc++ STL.
- Openmpi 3.0.
- Parmetis 4.0.3 used to partition the meshes.
- SuperLU\_dist 5.2.2, ScaLAPACK v2.0.2 and Mumps 5.1.2: Solvers interfaced in Petsc.
- Petsc 3.8.3: The linear algebra library in charge of the large matrices and vectors. Most of the mpi heavy-lifting is in fact handled by this library.


The [ThirdPartyCompilationFactory](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory) project on gitlab provides smooth ways to install these dependencies: either by running directly a Python script or by using one of the given Docker images.

### FYI: Embedded libraries

Four libraries are embedded in MoReFEM directly (and their compilation is already handled through MoReFEM different builds systems):

- [TClap](http://tclap.sourceforge.net), a header-only library to handle command line arguments.
- [Seldon](seldon.sourceforge.net), a linear algebra C++ library used to hande small matrices and vectors in MoReFEM. We foresee to benchmark this library against [Xtensor](https://github.com/QuantStack/xtensor) and [Eigen](eigen.tuxfamily.org).
- [Ops](https://gitlab.com/libops), a C++ library for reading configuration files.
- [Libmesh](http://pages.saclay.inria.fr/loic.marechal/libMeshb.html), a mesh utility.


### Compilers

MoReFEM relies on modern C++ and required features shipped with new standard C++ 17. Its has been successfully tested with gcc 7.1, AppleClang 8.0 and LLVM Clang 7.0. 


## MoReFEM compilation


There are currently three different ways to compile MoReFEM library: XCode (for macOS users), SCons (soon-to-be deprecated) and CMake (introduced in early 2018).

### XCode

XCode is widely used among developers in M3DISIM team, so a XCode project is provided with MoReFEM library.

For it to work directly without having to tinker with it, the local hierarchy of projects should be:

- `/Users/${USER}/Codes/MoReFEM/CoreLibrary` which should include the repository of the main library,
- `/Users/${USER}/Codes/MoReFEM/Models` which should include the repositories of the models not embedded within the main library, typically found on [this gitlab folder](https://gitlab.inria.fr/MoReFEM/Models).

where `USER` is your login on your computer.

The commands to create this structure are:

````
cd 
mkdir -p Codes/MoReFEM/Models
cd Codes/MoReFEM
git clone https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM.git CoreLibrary
````

and if you want to install one Model (say AcousticWave for the example):

````
cd ~/Codes/MoReFEM/Models
git clone https://gitlab.inria.fr/MoReFEM/Models/AcousticWave 

````

One you're there, you may run one of the embedded model left there for play here; to change the input file used you just have to edit a scheme in Product > Scheme.


### SCons

SCons is the command line build tool used up to now to compile on command line the library. However, its maintainance proved to be a bit tricky and the compilation is rather slow, so a switch to CMake is in progress.

To build SCons file:

- Copy one of the build_configuration.sample.***.py file in the Sources directory.
- Edit your local copy to specify the correct settings you want; for a first approach you just have to specify where the programs ans libraries will be installed (BUILD\_DIR) and another used for intermediate steps (INTERMEDIATE\_BUILD\_DIR).
- The run:
````
scons -j *nproc* --config_file=*your configuration file* 
````
This command will display all possible targets. If you want to build everything, choose the target 'all':

````
scons -j *nproc* --config_file=*your configuration file* all
````

### CMake

CMake build is intended to replace SCons this year as the command line build tools; the goal is to be a bit more easy to maintain than SCons (and compilation is markedly faster, especially using ninja back-end).

To compile the code:

- Go in cmake/Settings directory and copy one of the file there to tailor your need (it is very similar to what is done in the SCons build). There are more choices here though: main MoReFEM libraries might for instance be compiled as one huge library (easier to link with) or as several thematic libraries (useful for dev purposes, to check the inner dependencies between them are correctly handled).
- Then go to the place into which you want to build your code (say a build directory at the root of MoReFEM library) and run the command:

````
cmake -G Ninja -DSETTINGS_FILE=*the settings file you wrote; specify its path from MoReFEM root library* ..
````

This will configure the project; to compile and install it just run:

````
ninja install
````

Make build works fine as well; however the IDE build do not seem to work satisfactorily at the moment (at least XCode version which is very slow to open).


# Using MoReFEM


## Levels of users

We have identified 4 levels of users:

- Model user: someone that takes an already implemented Model (e.g. the ones defined in Sources/ModelInstances, or some in [dedicated Model group](https://gitlab.inria.fr/MoReFEM/Models) and provides their own Lua file to compute on their own data.
- Model developer: someone able to implement its own model, using for that purpose existent parameters and operators. All the Models defined in Sources/ModelInstances are at this level of expertise.
- Advanced model developer: Same as Model developer, but who also uses up more advanced features (that are in _Advanced_ namespace). Typically, an advanced model user might develop new operators tailored for its own purpose.
- Library developer: someone that writes code involved in low level operations of the library (typically in namespace _Internal_). 


## Structure of the source code

The hierarchy of the source code is the following:

- Utilities: a lot of handy tools that might as well be used in completely different projects (e.g. displaying content of a container, creating safely a file on a filesystem, etc...)
- ThirdParty: stuff related to third party libraries: actual sources for some of them, wrappers to provide a nice C++ interface within the code, facility to avoid getting compiler warnings from header files, etc...

Both those directories are compiled together when the compilation is set to generate MoReFEM in several libraries, as they are strongly intertwined together.

- Core: This module encompass some generic stuff that may be used at every level of MoReFEM; contrary to Utilities ones however they are already specific to the library.
- Geometry: This module encompass purely geometric objects of the code, such as the mesh, geometric elements, domains and so forth.
- FiniteElement: Large module to encompass stuff closely related to finite elements: nodes and dofs, boundary conditions, finite element spaces, unknowns and so forth...
- Parameters: This module encompass the API of so-called Parameters, which are objects for which values at quadrature points might be queried.
- Operators: This module encompass operators the API for several type of operators: variational operators, interpolators, etc...
- ParameterInstances: This module provides implementation of several parameters; it is separated from Parameters module as knowledge of stuff defined in Operators is required.
- OperatorInstances: This module provides implementation of several operators. As for ParameterInstances, it is separated from Operators module as it requires stuff from Parameters and ParameterInstances.
- FormulationSolver: module that provides the base class used to define a variational formulation.
- Model: module around the Model class, which drives the whole MoReFEM program.

The modules above form the core of MoReFEM library, which may be used to create a model of your own.

Besides them, three additional modules are provided:

- PostProcessing: a module which uses up the output data to provide a usable output. For instance default output might be used to provide input for Ensight visualisation software.
- ModelInstances: few models let here to show how the library might be used to define a Model. 
- Test: several integration tests that are run before each tag. They should soon be adapted on Inria continuous integration platform.




# How to contribute

As explained [earlier](#levels-of-users), most of the time you shouldn't have to contribute directly to the library and should be able to define locally in your own model what you need. However, if you have somes fixes to provide or maybe something to share (for instance an operator not implemented that might be of interest for others), feel free to submit a [merge request](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/merge_requests/new) on our gitlab.

MoReFEM follows the [integration manager](https://git-scm.com/book/it/v2/Distributed-Git-Distributed-Workflows) workflow; internal developers are actually using a Redmine installed on a server of M3DISIM team.



# Documentation

## Doxygen

Doxygen documentation is comprehensive and up-to-date; there are actually three flavors of them. For more informations, see the README.md in Documentation/Doxygen directory.

Doxygen should run with no warnings with Doxygen version 1.8.11 (change in policy in more recent versions caused lots of them due to a lesser tolerance for one-line comments).

## Introduction talks

Two explaning talks were given in 2017 to explain the structure of the library; they are in Documentation/IntroductionTalks directory.

Both were originally make in macOS Keynote; conversions to ppt and html are also given (with the later probably more useful). 'HappyHeart' was the codename of MoReFEM until late 2017.

## Coding standards

Coding standards are provided in Documentation/CodingStandards. They are mostly inspired by those of [Verdandi](verdandi.sourceforge.net). Some items were still open in discussion; a definite version should be issued soon.


# Getting help

The library was developed primarily by Sébastien Gilles (sebastien.gilles [AT] inria.fr), with also many contributions from Gautier Bureau, a very advanced model developer which is the author of most of the [advanced models](https://gitlab.inria.fr/MoReFEM/Models).










