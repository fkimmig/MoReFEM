import os

import Utilities


def FilterCppFiles(file_list):
    """Keep in the \a file_list given in input only the files which extension is cpp, hpp or hxx.
    
    \param[in] file_list Initial file list.
    
    \return A pair with cpp file list in the first slot and header file list (hpp and hxx) in the second one.
    """
    ret = []
    
    cpp_extension = "cpp"
    header_extension_list = ("hpp", "hxx")
    
    source_list = []
    header_list = []
    
    for myfile in file_list:
        extension = os.path.splitext(myfile)[1]

        if extension.startswith('.'):
            extension = extension[1:]
            
        if extension == cpp_extension:
            source_list.append(myfile)
        elif extension in header_extension_list:
            header_list.append(myfile)
            
    source_list.sort()
    header_list.sort()
    
    return (source_list, header_list)
    


def GenerateInModule(morefem_directory, subdirectory, module_alias, ignore_list = None):
    """
    \param[in] morefem_directory Path to the Morefem directory. Current file should be in {morefem_directory}/Scripts directory.
    \param[in] subdirectory Name of the subdirectory being investigated, within "Sources" directory. e.g 'Utilities', 'ThirdParty', etc...
    \param[in] module_alias Alias to the library being built, e.g. 'MOREFEM_UTILITIES'. If BUILD_MOREFEM_UNIQUE_LIBRARY is set, this alias will actually point to the MoReFEM library.
    \param[in] ignore_list If None, all subdirectories of \a subdirectory are included. If a list is given, each subfolder named after an element of the list is ignored. For instance for 'ThirdParty' there is 'Tclap'in the list; Tclap is a header only library embedded in MoReFEM which source files aren't required in the build.
    """    
    morefem_directory = Utilities.SubstituteEnvVariable(morefem_directory)
    source_directory = directory = os.path.join(morefem_directory, "Sources")
    directory = os.path.join(source_directory, subdirectory)
    
    tab_prefix = "\t\t"
    
    for root, dirs, files in os.walk(directory):

        if ignore_list:
            dirs[:] = [d for d in dirs if d not in ignore_list]
    
        (source_list, header_list) = FilterCppFiles(files)      
        
        
#        reduced_source_root = os.path.relpath(root, source_directory)
        
        with open(os.path.join(root, "SourceList.cmake"), "w") as SourceList:
            
            SourceList.write("target_sources({}{}{}\n".format("${", module_alias, "}"))
            
            if source_list:
                SourceList.write("\n\tPRIVATE\n")

                for myfile in source_list:
                    SourceList.write("{}\"{}/{}\" / \n".format(tab_prefix, r"${CMAKE_CURRENT_LIST_DIR}", myfile))
            
            SourceList.write("\n\tPUBLIC\n")
            
            for myfile in header_list:
                SourceList.write("{}\"{}/{}\" / \n".format(tab_prefix, r"${CMAKE_CURRENT_LIST_DIR}", myfile))
            
            SourceList.write(")\n\n")

            for directory in dirs:
                SourceList.write("include({}/{}/SourceList.cmake)\n".format(r"${CMAKE_CURRENT_LIST_DIR}", directory))
            
            
        
        
            


if __name__ == "__main__":
    morefem_path = "${HOME}/Codes/Morefem/CoreLibrary"
    
    GenerateInModule(morefem_path, "Utilities", "MOREFEM_UTILITIES", None)
    GenerateInModule(morefem_path, "ThirdParty", "MOREFEM_UTILITIES", ['Tclap', 'Source'])
    GenerateInModule(morefem_path, "Core", "MOREFEM_CORE")
    GenerateInModule(morefem_path, "Geometry", "MOREFEM_GEOMETRY")
    GenerateInModule(morefem_path, "FiniteElement", "MOREFEM_FELT")
    GenerateInModule(morefem_path, "Parameters", "MOREFEM_PARAM")
    GenerateInModule(morefem_path, "Operators", "MOREFEM_OP")
    GenerateInModule(morefem_path, "OperatorInstances", "MOREFEM_OP_INSTANCES")
    GenerateInModule(morefem_path, "ParameterInstances", "MOREFEM_PARAM_INSTANCES")
    GenerateInModule(morefem_path, "FormulationSolver", "MOREFEM_FORMULATION_SOLVER")
    GenerateInModule(morefem_path, "Model", "MOREFEM_MODEL")
    
    
    
