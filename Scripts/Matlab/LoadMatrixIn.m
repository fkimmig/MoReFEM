function Mat_in = LoadMatrixIn(matrix_in_name_file)

scriptname = matrix_in_name_file;
clear scriptname;

run(matrix_in_name_file);

matrix_name = who('Mat_*');

if (exist(matrix_name{1}))
    Mat_in = full(eval(matrix_name{1}));
else
    error('The name of the matrix is not right, Matlab renamed it. Just restart Matlab.');
end

end