import os
import sys
import shutil
import subprocess
import stat
import platform
import six
from update_xcode_template import UpdateXCodeTemplate


class InitMoReFEM:
    """Init properly MoReFEM.
    
    This is intended to be called just after the MoReFEM.git folder has been cloned.
    
    \param[in] m3disim_git_folder_on_server Path on the server that leads to the repository in which M3DISIM-MoReFEM
    is stored. Choose None if you do not want to consider (or you do not have clearance) this repository.
    """
    
    def __init__(self, server, git_folder_on_server, m3disim_git_folder_on_server):
        self.SetAttributes(server, git_folder_on_server, m3disim_git_folder_on_server)
        
        if self.__do_consider_m3disim:
            self.CloneIfNotYetThereM3DISIM()
        
        self.SetCommitHook()

        if platform.system() == "Darwin": # for OS X
            UpdateXCodeTemplate()

        self.CreateUserDistantRepository(0)
        self.UpdateGitRemote(0)

        if self.__do_consider_m3disim:
            self.CreateUserDistantRepository(1)
            self.UpdateGitRemote(1)
         
        
        #self.GitConfig()
        
    
    def SetAttributes(self, server, git_folder_on_server, m3disim_git_folder_on_server):
        """Set most of the data attributes values.
        
        This method must be called first in __init__().
        """
        self.main_folder = [os.getcwd()]
        
        current_folder = os.path.split(self.main_folder[0])[1]
        
        if current_folder != "MoReFEM":
            raise Exception("This script must be run from MoReFEM root folder!")
        
        print("Please enter your M3DISIM login (you might left it empty if it is the same as current shell user):")
        input = six.input()
        
        if input:
            self.login = input
        else:
            self.login = os.environ['USER']
            

        self.server_access = "{0}@{1}".format(self.login, server)
        
        # Tuples with 1 or 2 elements: second if present refer to M3DISIM-MoReFEM repository.
        self.distant_repository_name = ["{0}-MoReFEM.git".format(self.login)]        
        self.git_folder_on_server = [git_folder_on_server]
        self.server_folder = ["{0}:{1}".format(self.server_access, self.git_folder_on_server[0])]
        self.repository_name = ["MoReFEM"]
        self.local_repository_name = ["MoReFEM"]

        if m3disim_git_folder_on_server:
            self.distant_repository_name.append("{0}-M3DISIM_MoReFEM.git".format(self.login))
            self.git_folder_on_server.append(m3disim_git_folder_on_server)
            self.server_folder.append("{0}:{1}".format(self.server_access, self.git_folder_on_server[1]))
            self.__do_consider_m3disim = True
            self.main_folder.append(os.path.join(self.main_folder[0], "M3DISIM"))
            self.repository_name.append("M3DISIM_MoReFEM")
            self.local_repository_name.append("M3DISIM")
        else:
            self.__do_consider_m3disim = False
     
        
    def CloneIfNotYetThereM3DISIM(self):
        """If self.__do_consider_m3disim is True AND M3DISIM folder doesn't exist yet, clone it from distant repository."""
        assert(self.__do_consider_m3disim is True), "Should not be called otherwise."
        
        if not os.path.exists(self.main_folder[1]):
            print ("M3DISIM-MoReFEM repository is required but has not yet been found (if present there should be a {folder} directory). Please enter your access password to {access}".format(access = self.server_access,
                                                                           folder = self.main_folder[1]))
        
        
            cmd = ('git', 'clone', os.path.join(self.server_folder[1], "{0}.git".format(self.repository_name[1])), self.local_repository_name[1])
            subprocess.Popen(cmd).communicate()
        
        

    def CreateUserDistantRepository(self, index):
        """Create on ocean a repository specific to current user (name is *username*-MoReFEM.git) if not already existing.
    
        User is expected to work with this distant repository, and signal it to the integration manager when
        something is ready to be included in the main MoReFEM repository (named MoReFEM.git).
    
        This function is expected to be called just after MoReFEM was cloned from MoReFEM.git
        
        \param[in] index 0 deals with MoReFEM.git, 1 with M3DISIM_MoReFEM.git if self.__do_consider_m3disim is True.
        """
        print("=== Create if required a distant repository for current user on ocean for {repo}. ===".format(repo = self.repository_name[index]))
        print("Please enter your access password to {0}".format(self.server_access))
        
        # First check whether current user already gets a repository on ocean.
        target_on_server = os.path.join(self.git_folder_on_server[index], self.distant_repository_name[index])

        cmd = "if ssh {0} test -d \"{1}\"; then echo 1; else echo 0; fi".format(self.server_access, target_on_server)

        ret = subprocess.Popen(cmd, shell = True, stdout=subprocess.PIPE).communicate()[0]
        ret = ret.strip()
        
        if ret != '0' and ret != '1':
            raise Exception("Command returned an unexpected value (namely {0} when '0'or '1' were expected.)".format(ret))

        if ret == '1':
            print("[WARNING] The distant repository {0} already exists; you should check it is on par with current state of the code.".format(target_on_server))
            return
        
        # Create the distant repository: create first a bare git repository, and copy it onto the server.
        up_folder = os.path.split(self.main_folder[index])[0]
        os.chdir(up_folder)
        
        # Prepare the new git repository locally.
        cmd = "git clone --bare {local} {distant}".format(local = self.local_repository_name[index], distant =self.distant_repository_name[index])
        subprocess.Popen(cmd, shell = True).communicate()
        
        # Send it to ocean.
        print("Please enter your access password to {0}".format(self.server_access))
        cmd = "rsync -r -v {distant} {server_folder}".format(distant =self.distant_repository_name[index], server_folder = self.server_folder[index])
                                           
        subprocess.Popen(cmd, shell = True).communicate()
        
        # Clean-up: remove local git repository and put the prompt to the initial path.
        shutil.rmtree(self.distant_repository_name[index])
        os.chdir(self.main_folder[0])
    
    
    def UpdateGitRemote(self, index):
        """Add the newly created distant repository in the remote under the name 'mine' (that can easily be changed 
        by user)"""

        print("=== Add distant repository to remotes if not already done for {repo}. If existing, check it points to the expected repository.===".format(repo = self.repository_name[index]))
        
        # Very important step: depending on the position it might be main or M3DISIM repository that is actually
        # considered.
        os.chdir(self.main_folder[index])
        
        # Extract the list of existing remotes.
        remote_list = subprocess.Popen("git remote", shell = True, stdout =  subprocess.PIPE).communicate()[0].split('\n')
        remote_list = [ x for x in remote_list if x ]
        
        # If 'mine' not in remotes, add it.
        mine_repo = os.path.join(self.server_folder[index], self.distant_repository_name[index])
        
        if "mine" not in remote_list:
            cmd = "git remote add mine {0}".format(mine_repo)
            subprocess.Popen(cmd, shell = True).communicate()
        # If it is, check the related repository is the right one.
        else:
            verbose_remote_list = subprocess.Popen("git remote -v", shell = True, stdout =  subprocess.PIPE).communicate()[0].split('\n')
            verbose_remote_list = [ x for x in verbose_remote_list if x ]
            
            for remote in verbose_remote_list:
                split = remote.split('\t')
                assert(len(split) == 2)
                if split[0] != "mine":
                    continue
                    
                if not split[1].startswith(mine_repo):
                    print("[WARNING] 'mine' remote doesn't match expected distant repository (namely {0})".format(mine_repo))
 
        
    
    def SetCommitHook(self):
        """Set the commit hook so that all commits messages respect the expected format."""
        
        print("=== Placing git commit hook. ===")
        
        morefem_folder = self.main_folder[0]
        
        target = os.path.join(morefem_folder, ".git", "hooks", "commit-msg")
        
        # Open thew commit hook file.The 'w' is on purpose: the newly generated one must replace any pre-existing file
        # (there are no reasons to keep an older or a modified version of it).
        with open(target, 'w') as f:
            # Write the python she-bang.
            f.write("#!/bin/bash\n")
            
            # Call the python script.
            f.write("{0} {1} $1".format(sys.executable, \
                                         os.path.join(morefem_folder, "Scripts", "commit_hook.py")))
                                         
        os.chmod(target, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        
        if self.__do_consider_m3disim:
            m3disim_target = os.path.join(self.main_folder[1], ".git", "hooks", "commit-msg")
            shutil.copyfile(target, m3disim_target)
            os.chmod(m3disim_target, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
                                         
           
                                         
    def GitConfig(self):
        """Check whether the user already has a gitconfig, and if not propose a default one."""
        print ("=== Check gitconfig. ===")
        
        gitconfig_path = os.path.join(os.path.expanduser("~"), ".gitconfig")
        
        if os.path.exists(gitconfig_path):
            print("\tAlready existing, do nothing.")
            return
            
        print("No gitconfig was found; would you like to install a default one? (y/n)")
        
        input = six.input()
        
        while input not in ("y", "n"):
            input = six.input()
            
        if input == "y":
            print("Please enter your first and last name (this will appear in your git commits).")
            name = six.input()
            print("Please enter your e-mail address (this will appear in your git commits).")
            email = six.input()
            
            with open(gitconfig_path, 'w') as gitconfig_file:
                gitconfig_file.write("[user]\n")
                gitconfig_file.write("\tname = {0}\n".format(name))
                gitconfig_file.write("\temail = {0}\n\n".format(email))
                
                with open(os.path.join(self.main_folder[0], "Scripts", "gitconfig_without_user")) as input_content:
                    for line in input_content:
                        gitconfig_file.write(line)
                    
            print("Your git config has been set; you can edit it at {0}.".format(gitconfig_path))
            

if __name__ == "__main__":
    
    # If M3DISIM-MoReFEM repository is not considered.
    #InitMoReFEM("ocean.saclay.inria.fr", "/Volumes/Data/Web/git", None)
    
    # If M3DISIM-MoReFEM is considered.
    InitMoReFEM("ocean.saclay.inria.fr", "/Volumes/Data/Web/git", "/Volumes/Data/Web/git/M3DISIM_MoReFEM")

