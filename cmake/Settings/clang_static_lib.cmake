# Installation directory for executables and libraries.
# This will be completed with a subdirectory giving away the compilation mode (Debug, Release, etc...)
set(MOREFEM_INSTALL_DIR /Volumes/Data/sebastien/MoReFEM/CMake/)

# Choose C and C++ compilers. You might also specifies here clang static analyzer (paths to ccc-analyzer and c++-analyzer respectively) to perform static analysis of the code.
# Attention: those will be used only if user does not supersede them by prioviding -DCMAKE_C_COMPILER or -DCMAKE_CXX_COMPILER on command line!
set(MPI_CC /Users/Shared/LibraryVersions/clang/Openmpi/bin/mpicc )
set(MPI_CXX /Users/Shared/LibraryVersions/clang/Openmpi/bin/mpic++ )

# Choose either STATIC or SHARED.
set(LIBRARY_TYPE STATIC)

# Whether a unique library is built for MoReFEM core libraries or on the contrary if it is splitted in modules.
set(BUILD_MOREFEM_UNIQUE_LIBRARY False)

# Whether specific macros should be activated or not.

    # If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.
set(MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE False)

    # If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored.
set(MOREFEM_EXTENDED_TIME_KEEP False)

    # If true, there are additional checks that no nan and inf appears in the code. Even if False, solver always check for the validity of its solution (if a nan or an inf is present the SolveLinear() or SolveNonLinear() operation throws with a dedicated Petsc error). Advised in debug mode and up to you in release mode.
set(MOREFEM_CHECK_NAN_AND_INF False)

# OpenMPI libary.
set(OPEN_MPI_INCL_DIR /Users/Shared/LibraryVersions/clang/Openmpi/include )
set(OPEN_MPI_LIB_DIR /Users/Shared/LibraryVersions/clang/Openmpi/lib )

# BLAS Library.
# If BLAS_CUSTOM_LINKER is true, BLAS_LIB field must give the command use to link with Blas. For instance on macOS it is usually "-framework Accelerate" Beware: The quotes are important (otherwise CMake will mute this into -framework -Accelerate).
# If False, FindLibrary is used to find the Blas library to be used, as for the other libraries in this file. The difference is that the name of the .a, .so or .dylib is not known, so it must be given in BLAS_LIB_NAME field. For instance openblas to find libopenblas.a in BLAS_LIB_DIR.
set(BLAS_CUSTOM_LINKER True)
set(BLAS_LIB_DIR None)
set(BLAS_LIB "-framework Accelerate" )

# Petsc library.
set(PETSC_GENERAL_INCL_DIR /Users/Shared/LibraryVersions/clang/Petsc/include )
set(PETSC_DEBUG_INCL_DIR /Users/Shared/LibraryVersions/clang/Petsc/debug/include )
set(PETSC_RELEASE_INCL_DIR /Users/Shared/LibraryVersions/clang/Petsc/release/include )

set(PETSC_DEBUG_LIB_DIR /Users/Shared/LibraryVersions/clang/Petsc/debug/lib )
set(PETSC_RELEASE_LIB_DIR /Users/Shared/LibraryVersions/clang/Petsc/release/lib )

# Parmetis library.
set(PARMETIS_INCL_DIR /Users/Shared/LibraryVersions/clang/Parmetis/include )
set(PARMETIS_LIB_DIR /Users/Shared/LibraryVersions/clang/Parmetis/lib )

# Lua library.
set(LUA_INCL_DIR /Users/Shared/LibraryVersions/clang/Lua/include )
set(LUA_LIB_DIR /Users/Shared/LibraryVersions/clang/Lua/lib )

# Boost library
set(BOOST_INCL_DIR /Users/Shared/LibraryVersions/clang/Boost/include )
set(BOOST_LIB_DIR /Users/Shared/LibraryVersions/clang/Boost/lib )

# If you want to couple Morefem with Phillips library. False in most of the cases!
# Beware: it is not put in MOREFEM_COMMON_DEP; if you need it you must add it in your add_executable command.
set(PHILLIPS_DIR False)


