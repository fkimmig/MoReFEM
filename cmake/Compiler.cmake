
message(STATUS "C compiler identifier found to be ${CMAKE_C_COMPILER_ID}; settings (warnings, STL, etc...) will be set accordingly.")

if(${CMAKE_C_COMPILER_ID} STREQUAL "AppleClang" OR ${CMAKE_C_COMPILER_ID} STREQUAL "Clang")
    add_cxx_compiler_flag("-Weverything")
    add_cxx_compiler_flag("-Wno-c++98-compat")
    add_cxx_compiler_flag("-Wno-c++98-compat-pedantic")
    add_cxx_compiler_flag("-Wno-padded")
    add_cxx_compiler_flag("-Wno-exit-time-destructors")
    add_cxx_compiler_flag("-Wno-global-constructors")
    add_cxx_compiler_flag("-Wno-documentation")
    add_cxx_compiler_flag("-Wno-documentation-unknown-command")
    add_cxx_compiler_flag("-Wno-undefined-func-template")
    add_cxx_compiler_flag("-Wno-c++1z-extensions")

    add_cxx_compiler_flag("-stdlib=libc++") 
    
    if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
        add_definitions(-D_LIBCPP_DEBUG2=0) # Additional checks such as index out of bounds in vectors in libc++; might 
                                            # be removed at any point from libc++.
    endif()
    
    # Convenient macro to tag LLVM clang; useful as some warnings needs to be suppressed only for this more recent version of clang.
    if (${CMAKE_C_COMPILER_ID} STREQUAL "Clang")
        add_definitions(-DMOREFEM_LLVM_CLANG)
    endif()
    
elseif(${CMAKE_C_COMPILER_ID} STREQUAL "GNU")    
    add_cxx_compiler_flag("-Wall")
    add_cxx_compiler_flag("-Wextra")
    #add_cxx_compiler_flag("-Wpedantic") Can't be activated because of third-party error; pragma doesn't work to disable it only for third party.
    add_cxx_compiler_flag("-Wcast-align")
    add_cxx_compiler_flag("-Wcast-qual")
    add_cxx_compiler_flag("-Wconversion")
    add_cxx_compiler_flag("-Wdisabled-optimization")
    add_cxx_compiler_flag("-Wfloat-equal")
    add_cxx_compiler_flag("-Wformat=2")
    add_cxx_compiler_flag("-Wformat-nonliteral")
    add_cxx_compiler_flag("-Wformat-security")
    add_cxx_compiler_flag("-Wformat-y2k")
    add_cxx_compiler_flag("-Wimport")
    add_cxx_compiler_flag("-Winit-self")
    # add_cxx_compiler_flag("-Winline") Tag destructor defined with = default as inline...
    add_cxx_compiler_flag("-Winvalid-pch")
    add_cxx_compiler_flag("-Wmissing-field-initializers")
    add_cxx_compiler_flag("-Wmissing-format-attribute")
    add_cxx_compiler_flag("-Wmissing-include-dirs")
    add_cxx_compiler_flag("-Wmissing-noreturn")
    add_cxx_compiler_flag("-Wpacked")
    add_cxx_compiler_flag("-Wpointer-arith")
    add_cxx_compiler_flag("-Wredundant-decls")
    # add_cxx_compiler_flag("-Wshadow") g++ shadow is too cumbersome: can't name function argument same as a method for instance...
    add_cxx_compiler_flag("-Wstack-protector")
    add_cxx_compiler_flag("-Wstrict-aliasing=2")
    add_cxx_compiler_flag("-Wswitch-enum")
    add_cxx_compiler_flag("-Wunreachable-code")
    add_cxx_compiler_flag("-Wunused")
    add_cxx_compiler_flag("-Wunused-parameter")
    add_cxx_compiler_flag("-Wvariadic-macros")
    add_cxx_compiler_flag("-Wwrite-strings")
else()
    message(FATAL_ERROR "Sorry, your compiler family wasn't recognized. If CMake has updated its flag, please modify the cmake/Compiler.cmake file in MoReFEM accoerdingly. See CMAKE_LANG_COMPILER_ID in CMake tutorial to find out the valid options.")
endif()

